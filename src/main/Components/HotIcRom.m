classdef HotIcRom < IcRom & handle
    % This class represents an implicit condensation (and expansion)
    % reduced order model with thermal influences based upon a "hot" modes
    % rom. 
    %   The hot rom is based upon : 
    %   
    %   The hot modes rom is based upon the current thermal equilibrium in
    %   which the basis set consists of thos about the thermal equilibrium
    %   and the nonlinear stiffness coefficients are identified about 
    %   the thermal equilibrium.  
    properties
        thermal % thermal step object
        temperature; % temperature of the rom, or a vector of temperatures
        fThermal; % thermal force vector in modal coordinates
    end
    methods
        % Constructor
        function obj = HotIcRom(name,osFern,thermal)
            obj = obj@IcRom(name,osFern);
            obj.thermal = thermal;
        end
        
        % ---- Abstract Methods ---- %
        
        % Build Implicit Condensation Rom
        function Build(obj,modeIndices,loadScalings)
            % This function builds an IC rom at specified temperatures
            % the 
            % Inputs :
            %           obj = ColdIcRom object
            %           modeIndices = which linear modes to include within
            %           the basis set
            %           loadScalings = load levels to excite structure with
            %           
            %           temperatures = vector of temperatures about which
            %           to build the rom around
            
            
            % Create a basis using linear modes only.
            obj.modeIndices = sort(modeIndices);
            obj.m = length(modeIndices);
            p = modeIndices(end);
            
            % Solve nonlinear static thermal equilibrium
            obj.thermal.Solve(obj.staticControls.nSteps);
            obj.initialDisplacement = obj.thermal.finalDisplacement;
            
            % Eigenvalue about thermal equilibrium
            obj.thermal.Frequency(p); 
            fn = obj.thermal.fn; phi = obj.thermal.phi;
            
            % Set basis, mass, and stiffness matrices
            obj.basis = phi(:, modeIndices);
            obj.Kbb = diag((2*pi*fn(modeIndices)).^2);
            obj.Mbb = eye(obj.m);  obj.Cbb = zeros(obj.m);
            
            % Build mass matrix
            M = sparse(obj.osFern.nDof,obj.osFern.nDof);
            M(obj.osFern.freeDof,obj.osFern.freeDof) = obj.osFern.GetMassMatrix();
            
            % Create load scalings
            [~,n] = size(loadScalings);
            
            % Initialize data
            uModal = cell(n,1); fModal = cell(n,1); Knl_Ind = cell(n,1);
            U =cell(n,1); Ueq=cell(n,1); F = cell(n,1);
            tStart = tic;

            % Loop over all load cases
            for ii = 1:n
                % Create loading cases for specific instance
                [~,F{ii}]=icloading(fn(modeIndices),phi(:, modeIndices),...
                    loadScalings(:,ii),M,[],obj.controls);
                
                % Solve the system
                [Utemp,Ueq{ii}] = obj.thermal.Solve_Heat_n_Load(obj.staticControls.nSteps,...
                                                        F{ii},true);
                                                    
                % Remove thermal portion
                U{ii} = Utemp-Ueq{ii};
                
                % Generate modal static data
                mFitControls.quad = 1; mFitControls.triple = 1;
                [uModal{ii},fModal{ii},obj.tlist]=GenStaticModalData(fn(modeIndices),...
                    phi(:, modeIndices),F{ii},U{ii},mFitControls,...
                    [],phi(:, modeIndices)'*M);
                
                % If doing basic least squares
                Knl_Ind{ii} = (uModal{ii}\fModal{ii})';
            end
            tStatic = toc(tStart);

            
            % Assemble training data from each individual run
            fModalMat = cell2mat(fModal); uModalMat = cell2mat(uModal);
            obj.Knl = (uModalMat\fModalMat)';
             
            % Convert to nash form
            [N1,N2,Nlist]=Knl2Nash(obj.Knl,obj.tlist);
            obj.NashForm.N1 = N1; obj.NashForm.N2 = N2;
            obj.NashForm.Nlist = Nlist;
            
            % Perfrom membrane expansion
            obj.Compute_ExpansionModes(U{1},M);
            
            % Store data
            obj.Kbb = diag((obj.thermal.fn(modeIndices)*2*pi).^2);
            obj.Mbb = eye(obj.m);
            
            % Store training data
            obj.training.uModal = uModal;
            obj.training.fModal = fModal;
            obj.training.KnlInd = Knl_Ind;
            obj.training.F = F; obj.training.U = U;
            obj.training.staticSolveTime = tStatic; 
        end
        
        % Assemble nonlinear stiffness matric
        function [Knl,varargout] = Get_NlStiffness(obj, dNew)
            % Initialzie matrices
            N1_u=zeros(obj.m,obj.m); N2_u=zeros(obj.m,obj.m);

            % Generate quadractic combinations
            d_d = dNew(obj.NashForm.Nlist(:,1)).*dNew(obj.NashForm.Nlist(:,2));
            for jj=1:obj.m
                N1_u(:,jj)= obj.NashForm.N1(:,:,jj) * dNew;
                N2_u(:,jj)= obj.NashForm.N2(:,:,jj) * d_d;
            end
            
            % Nonlinear stiffness
            Knl = N1_u+N2_u;
            
            % Nonlinear internal force
            if nargout > 1
                varargout{1}=(0.5*N1_u+(1/3)*N2_u)*dNew;
            end
        end
        
        % Assemble nonlinear internal force
        function [fnl,varargout] = Get_NlForce(obj,dNew)
            
            % Initialzie matrices
            N1_u=zeros(obj.m,obj.m); N2_u=zeros(obj.m,obj.m);

            % Generate quadractic combinations
            d_d = dNew(obj.NashForm.Nlist(:,1)).*dNew(obj.NashForm.Nlist(:,2));
            for jj=1:obj.m
                N1_u(:,jj)= obj.NashForm.N1(:,:,jj) * dNew;
                N2_u(:,jj)= obj.NashForm.N2(:,:,jj) * d_d;
            end
            
            % Nonlinear internal force
            fnl=(0.5*N1_u + (1/3)*N2_u)*dNew;

            % Nonlinear stiffness
            if nargout > 1
                varargout{1}=N1_u+N2_u;
            end
        end
        
        % Assemble energy
        function [Enl] = Get_Energy(obj, dNew)
            % Initialzie matrices
            N1_u=zeros(obj.m,obj.m); N2_u=zeros(obj.m,obj.m);

            % Generate quadractic combinations
            d_d = dNew(obj.NashForm.Nlist(:,1)).*dNew(obj.NashForm.Nlist(:,2));
            for jj=1:obj.m
                N1_u(:,jj)= obj.NashForm.N1(:,:,jj) * dNew;
                N2_u(:,jj)= obj.NashForm.N2(:,:,jj) * d_d;
            end
            
            % Nonlinear stiffness
            Enl = (1/6)*dNew'*N1_u*dNew+(1/12)*dNew'*N2_u*dNew;
        end
        
    end
    
    
end

