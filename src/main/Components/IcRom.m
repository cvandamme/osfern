classdef IcRom < Rom & handle
    % This class represents an implicit condensation (and expansion)
    % reduced order model
    %   Detailed explanation goes here
    
    properties
        Knl, tlist
        NashForm = struct('N1',[],'N2',[],'Nlist',[]);
        training = struct('uModal',[],'fModal',[],'KnlInd',[],...
                          'U',[],'F',[],'fitInfo',[]);
        phi_m, num_m
        controls = struct('sf',true,'df',true,'tf',true,...
                          'rf',true,'mf',false,'cf',true);
        staticControls = struct('parallel',false,'numWorkers',4,...
                                'nSteps',10);
    end
    methods
        % Constructor
        function obj = IcRom(name,osFern)
            obj = obj@Rom(name,osFern);
        end
        
        % Expand memebrane displacements
        function [x_m,q_m] = Expand(obj,q)
            % See "Reduced-order models for nonlinear
            % response prediction:  Implicit condensation and expansion"...
            % JSV 2008 by J Hollkamp and R Gordon.
            % Based on script written by Robert Kuether when at UW-Madison 

            % Initialzie variable
            q_m = zeros(size(obj.num_m,1),1);
            
            % Loop over the modes
            for jj = 1:size(obj.num_m,1)
                entry = q.^(obj.num_m(jj,:).');
                q_m(jj) = prod(entry); 
            end
            
            % expand to physical dof
            x_m = obj.phi_m*q_m;
        end
        
        % Compute membrane expansion modes
        function Compute_ExpansionModes(obj,U,M)
            % Inputs : 
            %       obj = IcRom Object
            %       U = Displacements used in training
            %       M = Mass matrix
            % Outputs :
            %       phi_m - membrane modes (stored in object)
            %       num_m - permutation matrix (stored in object)
            %
            % See "Reduced-order models for nonlinear
            % response prediction:  Implicit condensation and expansion"...
            % JSV 2008 by J Hollkamp and R Gordon.
            %
            % Based on script written by Robert Kuether when at UW-Madison 
             
            % Number of load cases
            [~,Nloads] = size(U);
            
            % bending modal amplitude
            if ~isempty(M)
                q_b = (obj.basis'*M)*U;
            else
                q_b = pinv(obj.basis)*U;
            end
            
            % build coefficients
            order_matrix = npermutek(0:2,obj.m);
            order_sum = sum(order_matrix,2);   
            nums = order_matrix(order_sum==2,:);

            % a - # of membrane modes to be computed
            [a] = size(nums,1);    
            
            % allocate space for membrane modal amplitudes
            q_m = zeros(a,Nloads);   

            % build membrane amplitude matrix from bending amplitudes
            for i = 1:Nloads   
                for j = 1:a
                    entry = q_b(:,i).^(nums(j,:).');   
                    q_m(j,i) = prod(entry); 
                end
            end

            % Store data
            obj.phi_m = (U-obj.basis*q_b)*pinv(q_m); obj.num_m = nums;
        end
        
        % ---- Abstract Methods ---- %
        
        % Build Implicit Condensation Rom
        function Build(obj,modeIndices,loadScalings)
            % This function builds an IC rom
            
            % Create linear normal mode basis set
            obj.BasisCreate_LNM(modeIndices); phi = obj.basis;
            fn = obj.modal.fn(modeIndices);
            
            % Build mass matrix
            M = zeros(obj.osFern.nDof);
            M(obj.osFern.freeDof,obj.osFern.freeDof) = obj.osFern.GetMassMatrix();
            
            % Create static object if it doesnt exist
            if isempty(obj.static)
                obj.MakeStatic()
            end
            
            % Create load scalings
            [~,n] = size(loadScalings);
            
            % Initialize data
            uModal = cell(n,1); fModal = cell(n,1); Knl_Ind = cell(n,1);
            U =cell(n,1); F = cell(n,1);
            
            % Loop over all load cases
            for ii = 1:n
                % Create loading cases for specific instance
                [~,F{ii}]=icloading(fn,phi,...
                    loadScalings(:,ii),M,[],obj.controls);
                
                % Solve the system
                tStart = tic;
                [U{ii}] = obj.static.SolveMultiple(obj.staticControls.nSteps,...
                                                   F{ii});
                tStatic = toc(tStart);
            
                % Generate modal static data
                mFitControls.quad = 1; mFitControls.triple = 1;
                [uModal{ii},fModal{ii},obj.tlist]=GenStaticModalData(fn,...
                    phi,F{ii},U{ii},mFitControls,...
                    [],phi'*M);
                
                % If doing basic least squares
                Knl_Ind{ii} = (uModal{ii}\fModal{ii})';
            end
            
            % Assemble training data from each individual run
            fModalMat = cell2mat(fModal); uModalMat = cell2mat(uModal);
            obj.Knl = (uModalMat\fModalMat)';
             
            % Convert to nash form
            [N1,N2,Nlist]=Knl2Nash(obj.Knl,obj.tlist);
            obj.NashForm.N1 = N1; obj.NashForm.N2 = N2;
            obj.NashForm.Nlist = Nlist;
            
            % Perfrom membrane expansion
            obj.Compute_ExpansionModes(U{1},M);
            
            % Store data
            obj.Kbb = diag((obj.modal.fn(modeIndices)*2*pi).^2);
            obj.Mbb = eye(obj.m);
            
            % Store training data
            obj.training.uModal = uModal;
            obj.training.fModal = fModal;
            obj.training.KnlInd = Knl_Ind;
            obj.training.F = F; obj.training.U = U;
            obj.training.staticSolveTime = tStatic; 
        end
        
        % Assemble nonlinear stiffness matric
        function [Knl,varargout] = Get_NlStiffness(obj, dNew)
            % Initialzie matrices
            N1_u=zeros(obj.m,obj.m); N2_u=zeros(obj.m,obj.m);

            % Generate quadractic combinations
            d_d = dNew(obj.NashForm.Nlist(:,1)).*dNew(obj.NashForm.Nlist(:,2));
            for jj=1:obj.m
                N1_u(:,jj)= obj.NashForm.N1(:,:,jj) * dNew;
                N2_u(:,jj)= obj.NashForm.N2(:,:,jj) * d_d;
            end
            
            % Nonlinear stiffness
            Knl = N1_u+N2_u;
            
            % Nonlinear internal force
            if nargout > 1
                varargout{1}=(0.5*N1_u+(1/3)*N2_u)*dNew;
            end
        end
        
        % Assemble nonlinear internal force
        function [fnl,varargout] = Get_NlForce(obj,dNew)
            
            % Initialzie matrices
            N1_u=zeros(obj.m,obj.m); N2_u=zeros(obj.m,obj.m);

            % Generate quadractic combinations
            d_d = dNew(obj.NashForm.Nlist(:,1)).*dNew(obj.NashForm.Nlist(:,2));
            for jj=1:obj.m
                N1_u(:,jj)= obj.NashForm.N1(:,:,jj) * dNew;
                N2_u(:,jj)= obj.NashForm.N2(:,:,jj) * d_d;
            end
            
            % Nonlinear internal force
            fnl=(0.5*N1_u + (1/3)*N2_u)*dNew;

            % Nonlinear stiffness
            if nargout > 1
                varargout{1}=N1_u+N2_u;
            end
        end
        
        % Assemble energy
        function [Enl] = Get_Energy(obj, dNew)
            % Initialzie matrices
            N1_u=zeros(obj.m,obj.m); N2_u=zeros(obj.m,obj.m);

            % Generate quadractic combinations
            d_d = dNew(obj.NashForm.Nlist(:,1)).*dNew(obj.NashForm.Nlist(:,2));
            for jj=1:obj.m
                N1_u(:,jj)= obj.NashForm.N1(:,:,jj) * dNew;
                N2_u(:,jj)= obj.NashForm.N2(:,:,jj) * d_d;
            end
            
            % Nonlinear stiffness
            Enl = (1/6)*dNew'*N1_u*dNew+(1/12)*dNew'*N2_u*dNew;
        end
        
        % ---- Utility Methods ---- %
        
        % Create string of coefficients for labeling
        function knlStrings = GenKnlStrings(obj)
            % Generate legend entries
            knlStrings = cell(size(obj.Knl,1),1); index = 1;
            for jj = 1:size(obj.tlist,1)
                if obj.tlist(jj,3) == 0
                    knlStrings{jj} = ['\alpha_{'...
                        ,num2str(obj.tlist(jj,1)),num2str(obj.tlist(jj,2)),'}'];
                else
                    knlStrings{jj} = ['\beta_{'...
                        ,num2str(obj.tlist(jj,1)),num2str(obj.tlist(jj,2)),...
                        num2str(obj.tlist(jj,3)),'}'];
                end
            end
        end
        
        % Plot the nonlinear stiffness matrix as a colormap
        function PlotKnl(obj,varargin)
            % Function which generates a nonlinear stiffness plot of the 
            % input matrix Knl 
            %
            % Based upon MACPlot by 
            % Matt Allen Janary 2004
            % msalle@sandia.gov
            % 
            % Edited for KnlPlot by 
            % Christopher Van Damme 2019
            % cvandamme@wisc.edu
           
            % See if user provides knl 
            if nargin > 1
                Knl2Plot = varargin{1};
            else
                Knl2Plot = obj.Knl;
            end
            
            % Check size of Knl
            nModes = size(Knl2Plot,1); nTerms = size(Knl2Plot,2);
            xs = 1:nModes;ys = 1:nTerms;  w2 = 0.40;

            % Generate xtick labels
            knlStrings = obj.GenKnlStrings();
            
            % Generate Colors
            if nargin > 2
                cmap_name = varargin{2};
            else
                cmap_name = 'bone';
            end
            
            % Assumes scale from 0 to 1;
            cmap = eval([cmap_name,'(1001)']);
            if strcmp(cmap_name,'gray') || strcmp(cmap_name,'bone')
                cmap = flipud(cmap);
            end
            
            
            for ii = 1:1:nModes
                for jj = 1:1:nTerms
                    xcoord = xs(ii) + [-w2 w2 w2 -w2];
                    ycoord = ys(jj) + [-w2 -w2 w2 w2];
                    han = patch(xcoord,ycoord,log10(abs(Knl2Plot(ii,jj))));
%                     han = patch(xcoord,ycoord,(abs(Knl2Plot(ii,jj))));
%                     C = han.CData; han.CDataMapping = 'scaled';
                end
            end
            colorbar;
            title('\bf Plot of NLROM K_{nl} Matrix');
            if length(knlStrings) == size(Knl2Plot,1)
                set(gca, 'XLim', [0.5 size(Knl2Plot,1)+0.5],...
                 'YLim', [0.5 size(Knl2Plot,2)+0.5],...
                  'YTick', [1:size(Knl2Plot,2)],...
                  'XTick', [1:size(Knl2Plot,1)]);
                xticklabels(knlStrings)
                xlabel('\bf K_{nl} Term'); ylabel('\bf Mode Index'); 
            else
                set(gca, 'XLim', [0.5 size(Knl2Plot,1)+0.5],...
                 'YLim', [0.5 size(Knl2Plot,2)+0.5],...
                  'YTick', [1:size(Knl2Plot,2)],...
                  'XTick', [1:size(Knl2Plot,1)]);
                yticklabels(knlStrings);
                xlabel('\bf K_{nl} Term'); ylabel('\bf Mode Index');  
            end
            
            eval(['colormap(',cmap_name,')']);
            box on; grid on;
            if strcmp(cmap_name,'gray') || strcmp(cmap_name,'bone')
                eval(['colormap(flipud(',cmap_name,'))']);
            else
                eval(['colormap(',cmap_name,')']);
            end
        end
        
        % Way to set Knl based upong other roms, usefull for model updating
        function SetKnl(obj,keepKnl,varargin)
            
            
            % Check the inputs 
            if nargin == 3
                if isa(varargin{1},'IcRom')
                    KnlIn = varargin{1}.Knl;
                    tlistIn = varargin{1}.tlist;
                else
                    error('Should be a rom object')
                end
            elseif nargin == 4
                KnlIn = varargin{1}; tlistIn = varargin{2};
            end
            
            % Create initial nonlinear stiffness
            maxIndex = max(max(tlistIn));
            if keepKnl
                KnlOut = obj.Knl;
            else
                KnlOut = zeros(max(max(obj.tlist)),size(obj.tlist,1));
            end
            
            % Update the nonlinear stiffness matrix
            for jj = 1:maxIndex
                for ii = 1:size(tlistIn,1)
                    for kk = 1:size(obj.tlist,1)
                        if tlistIn(ii,:) == obj.tlist(kk,:)
                            KnlOut(jj,kk) = KnlIn(jj,ii);
                        end
                    end
                end
            end
           
            obj.Knl = KnlOut;
             % Convert to nash form
            [N1,N2,Nlist] = Knl2Nash(KnlOut,obj.tlist);
            obj.NashForm.N1 = N1;obj.NashForm.N2 = N2;
            obj.NashForm.Nlist = Nlist;
        end
    end 
end

