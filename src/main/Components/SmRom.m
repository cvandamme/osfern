classdef SmRom < Rom & handle
    % This class represents a she and mei reduced order model
    % More detailed explanation to come soon
    
    properties
        K1m;
        K2m;  % Tangent stiffness matrix; d(fnl)/dx = Kt
        K1h;  % Expanded forms for tensor integration
        K2h;
    end
    methods
        % constructor
        function obj = SmRom(name,osFern)
            obj = obj@Rom(name,osFern);
        end
        
        % ---- Abstract Methods ---- %
        
        % Build the Shi and Mei Rom
        function Build(obj,modeIndices)
            % This function builds a shi and mei rom
            
            % Create linear normal mode basis set
            obj.BasisCreate_LNM(modeIndices);
            
            % For Shie and Mei the modes must be normalized wrt to max unity
%             for ii = 1:obj.m
%                 obj.basis(:,ii) = obj.basis(:,ii)/max(abs(obj.basis(:,ii)));
%             end
%             phi = obj.basis;
            
            
            
            % Allocate memory
            nEL = length(obj.osFern.element);
            m = size(obj.basis, 2);
            
            % allocate memory for set of N.L. modal stiffness matrices
            obj.K2m = zeros(m, m, m, m);
            obj.K1m = zeros(m, m, m);
            
            % double loop to form mxm set of nonlinear modal stiffness matrices, Km
            for i = 1:m
                for j = 1:m
                    
                    % loop to assemble matrices, K1m and K2m for modes (i,j)
                    modestr = [num2str(i),':',num2str(j)];
                    disp(['Assembling Km for modes ',modestr]);
                    
                    % Loop through element-by-element
                    for ii = 1:nEL
                        
                        % Obtain element-local deformation of each mode
                        globalDof = obj.osFern.element{ii}.dofVec;
                        xi = obj.basis(globalDof, i);
                        xj = obj.basis(globalDof, j);
                                                
                        % Build individual element matrix
                        [K1, K2] = obj.osFern.element{ii}.BuildNLmodal(xi, xj);
                        
                        % modalize the element nonlinear stiffness matrices and add them
                        % to the global matrices (for beam and plate elements only)
                        if i == j
                            obj.K1m(:,:,i) = obj.K1m(:,:,i)+ ...
                                obj.basis(globalDof,:)'*K1*obj.basis(globalDof,:);
                        end
                        obj.K2m(:,:,i,j) = obj.K2m(:,:,i,j)+ ...
                            obj.basis(globalDof,:)'*K2*obj.basis(globalDof,:);
                        
                        
                    end
                    
                end
            end
            
            
            % convert K1m and K2m to 2D block format for computation speed
            obj.K1h = zeros(m^2, m);
            obj.K2h = zeros(m^2);
            nm1 = m - 1;
            for r = 1:m
                irow = m*r - nm1:m*r;
                obj.K1h(irow, :) = obj.K1m(:, :, r);
                for s = 1:m
                    icol = m*s - nm1:m*s;
                    obj.K2h(irow, icol) = obj.K2m(:, :, r, s);
                end
            end
            
        end
        
        % Assemble nonlinear stiffness matric
        function [Knl,varargout] = Get_NlStiffness(obj, dNew)
            % Apply displacements to nonlinear coefficients
            m = size(obj.basis, 2);
            dmat = repmat(eye(m),1,m);
            dind = dmat == 1;
            v1 = ones(m,1);
            dd = v1*dNew';
            dmat(dind) = dd;
            
            Knl = dmat*obj.K1h + dmat*obj.K2h*dmat';
            
            if nargout > 1
                varargout{1} = (1/2)*dmat*obj.K1h*dNew + (1/3)*(dmat*obj.K2h*dmat')*dNew; %Knl*dNew;
            end
        end
        
        % Assemble nonlinear internal force
        function [fnl,varargout] = Get_NlForce(obj,dNew)
            % Apply displacements to nonlinear coefficients
            m = size(obj.basis, 2);
            dmat = repmat(eye(m),1,m);
            dind = dmat == 1;
            v1 = ones(m,1);
            dd = v1*dNew';
            dmat(dind) = dd;
            
            % internal force computation
            fnl = (1/2)*dmat*obj.K1h*dNew + (1/3)*(dmat*obj.K2h*dmat')*dNew; 

            % nonlinear stiffness if desired
            if nargout > 1
                varargout{1} = dmat*obj.K1h + dmat*obj.K2h*dmat';
            end
        end
        
    end
    
  
end

