classdef LassoIcRom < IcRom & handle
    % This class represents an implicit condensation (and expansion)
    % reduced order model that is created via least absolute shrinkage and 
    % selection (LASSO) during training.
    
    
    properties
        trainingControls = struct('method','lasso','nObs',20,...
                                  'numLambda',100,'kFold',5,...
                                  'mcReps',1,'maxIters',1e5);
        Knl_La % matrix of coefficients estimated via lasso
        Knl_Ls % matric of coefficients estimated via least squares
        Knl_Rf
        lambda % vector of current lambas for current Knl [1 x m]
        alpha = 1.0  % elastic net parameter, controls the relative amount
                      % of l2 norm to l1 norm control [0,1]
                      % 0 = lasso, 1 = ridge regression, 
    end
    methods
        
        % Constructor
        function obj = LassoIcRom(name,osFern)
            obj = obj@IcRom(name,osFern);
        end
         
        % Set the current sparsity values to a desired % 
        function MakeSparse(obj,spFactor)
            
            % Based upon input decide which route to go
            if ischar(spFactor) % use optimal MSE fit
                if strcmp(spFactor,'std')
                    Knltemp = zeros(size(obj.Knl));
                    lambdas = zeros(obj.m,1);
                    for ii = 1:obj.m
                       lambdas(ii) = obj.training.fitInfo{ii}.Lambda(...
                                     obj.training.fitInfo{ii}.Index1SE);
                       Knltemp(ii,:) = obj.Knl_La{ii}(:,...
                                    obj.training.fitInfo{ii}.Index1SE)';
                    end
                else 
                    Knltemp = zeros(size(obj.Knl));
                    lambdas = zeros(obj.m,1);
                    for ii = 1:obj.m
                       lambdas(ii) = obj.training.fitInfo{ii}.Lambda(...
                                     obj.training.fitInfo{ii}.IndexMinMSE);
                       Knltemp(ii,:) = obj.Knl_La{ii}(:,...
                                    obj.training.fitInfo{ii}.IndexMinMSE)';
                    end
                end
                % Save the data
                obj.Knl = Knltemp;  obj.lambda = lambdas;
            elseif length(spFactor) > 1% sparsity specified for each mode
                if length(spFactor) ~= obj.m
                    error('Incorrect size')
                else
%                     if size(spFactor,1)
                end
                spNum = round(size(obj.Knl,2).*spFactor);
                Knltemp = zeros(size(obj.Knl));
                lambdas = zeros(obj.m,1);
                for ii = 1:obj.m
                   [~,spEqn] = min(abs(spNum(ii)-obj.training.fitInfo{ii}.DF));
                   lambdas(ii) = obj.training.fitInfo{ii}.Lambda(spEqn);
                   Knltemp(ii,:) = obj.Knl_La{ii}(:,spEqn)';
                end

                % Save the data
                obj.Knl = Knltemp;  obj.lambda = lambdas;
            else % all modes have same sparsity
                if spFactor >= 1 % use least squares fit 
                    % Store least squares as current value
                    obj.Knl = obj.Knl_Ls;
                elseif spFactor < 0 
                    error('Cant have negative sparsity')
                else % use specified sparsity 
                    spNum = round(size(obj.Knl,2)*spFactor);
                    Knltemp = zeros(size(obj.Knl));
                    lambdas = zeros(obj.m,1);
                    for ii = 1:obj.m
                       [~,spEqn] = min(abs(spNum-obj.training.fitInfo{ii}.DF));
                       lambdas(ii) = obj.training.fitInfo{ii}.Lambda(spEqn);
                       Knltemp(ii,:) = obj.Knl_La{ii}(:,spEqn)';
                    end

                    % Save the data
                    obj.Knl = Knltemp;  obj.lambda = lambdas;
                end
            end
            
            % Convert to nash form
            [N1,N2,Nlist]=Knl2Nash(obj.Knl,obj.tlist);
            obj.NashForm.N1 = N1; obj.NashForm.N2 = N2;
            obj.NashForm.Nlist = Nlist;
        end
        
        % Plot the trianing MSE on validation
        function PlotTrainingFit(obj)
            
            % Plot MSE Estiamates
            figure; 
            for ii = 1:obj.m
                ax{ii} = subplot(ceil(obj.m/2),2,ii);
                l1 = 1;%obj.training.fitInfo{ii}.MSE(1);
                l2 = 1;%obj.training.fitInfo{ii}.SE(1);
                loglog(obj.training.fitInfo{ii}.Lambda,...
                       obj.training.fitInfo{ii}.MSE/l1); hold on;
                loglog(obj.training.fitInfo{ii}.Lambda,...
                       obj.training.fitInfo{ii}.SE/l2);   
                numTermsMin = length(find(obj.Knl_La{ii}(:,...
                              obj.training.fitInfo{ii}.IndexMinMSE)));
                numTermsStd = length(find(obj.Knl_La{ii}(:,...
                              obj.training.fitInfo{ii}.Index1SE)));
                loglog(obj.training.fitInfo{ii}.Lambda(obj.training.fitInfo{ii}.IndexMinMSE),...
                         obj.training.fitInfo{ii}.MSE(obj.training.fitInfo{ii}.IndexMinMSE)/l1,...
                         'ko','MarkerFaceColor','r','MarkerSize',12)
                loglog(obj.training.fitInfo{ii}.Lambda(obj.training.fitInfo{ii}.Index1SE),...
                       obj.training.fitInfo{ii}.MSE(obj.training.fitInfo{ii}.Index1SE)/l1,'ko',...
                          'MarkerFaceColor','g','MarkerSize',12)
                legend('MSE on Cross Validation','SE of MSE on Validation',...
                        ['Optimal \lambda, Num. Terms = ',num2str(numTermsMin)],...
                        ['1 STD \lambda, Num. Terms = ',num2str(numTermsStd)])
                xlabel('Regularization Term (\lambda)');
                ylabel('MSE during Cross Validation')
                box on; grid on;
                % try adding a second second of axes plot
%                 ax2{ii} = axes('Position',ax{ii}.Position,...
%                             'XAxisLocation','top',...
%                             'YAxisLocation','right',...
%                             'Color','none');
%                 ax2{ii}.XColor = 'r'; %ax2{ii}.xColor = 'r';
%                 ax2{ii}.YTick([]);   %yTick = [];
%                 incrs = 
%                 ax2{ii}.xTickLabel = obj.training.fitInto{ii}.DF(1:
            end

        end
        
        % Plot force-displacement data used in training
        function PlotTrainingData(obj,varargin)
            
            if nargin > 1
                
            else
               cNode = obj.osFern.nset('CenterNode');
               dof2plot =  cNode{1}.globalDof(3);       
            end
            
            % Reorganzie data
            fMat = cell2mat(obj.training.F'); 
            uMat = cell2mat(obj.training.U');


            qMat = cell2mat(obj.training.uModal); 
            bMat = cell2mat(obj.training.fModal);
            qMat111 = qMat(:,2); bMat1 = bMat(:,1);

            % Plot force-displacement data
            figure;  
            plot(qMat,bMat1,'s')
            xlabel('Displacement ()');
            ylabel('Force ()')
        end
        
        % Plot the lasso estimatin versus regularization parameter
        function PlotKnl_v_Lambda(obj,varargin)
            
            figure
            for ii = 1:obj.m
                % Create lambda strings
                for jj = 1:length(obj.training.fitInfo{ii}.Lambda)
                    xss1{jj} = sprintf('%08.2d',...
                        obj.training.fitInfo{ii}.Lambda(jj));
                end
                ax{ii} = subplot(2,ceil(obj.m/2),ii);
                obj.KnlPlot((obj.Knl_La{ii}./obj.Knl_La{ii}(:,1))');
                xticks(1:5:length(xss1))
                xticklabels(xss1(1:5:end))
            end

            
        end
        
        % Plot the sparsity of the rom
        function PlotSparsity(obj,varargin)
            % Function which generates a nonlinear stiffness plot of the 
            % input matrix Knl 
            %
            % Based upon MACPlot by 
            % Matt Allen Janary 2004
            % msalle@sandia.gov
            % 
            % Edited for KnlPlot by 
            % Christopher Van Damme 2019
            % cvandamme@wisc.edu
           
            % See if user provides knl 
            if nargin > 1
                Knl2Plot = varargin{1};
            else
                Knl2Plot = obj.Knl;
            end
           
            % Generate xtick labels
            knlStrings = obj.GenKnlStrings();
            
            % Create a spy plot
            spy(Knl2Plot);
            
            % Create axis labels
            title('\bf Plot of NLROM K_{nl} Matrix');
            if length(knlStrings) == size(Knl2Plot,1)
                set(gca, 'XLim', [0.5 size(Knl2Plot,1)+0.5],...
                 'YLim', [0.5 size(Knl2Plot,2)+0.5],...
                  'YTick', [1:size(Knl2Plot,2)],...
                  'XTick', [1:size(Knl2Plot,1)]);
                xticklabels(knlStrings)
                xlabel('\bf K_{nl} Term'); ylabel('\bf Mode Index'); 
            else
                set(gca, 'YLim', [0.5 size(Knl2Plot,1)+0.5],...
                 'XLim', [0.5 size(Knl2Plot,2)+0.5],...
                  'YTick', [1:size(Knl2Plot,1)],...
                  'XTick', [1:size(Knl2Plot,2)]);
                xticklabels(knlStrings);
                xlabel('\bf K_{nl} Term'); ylabel('\bf Mode Index');  
            end
            
            box on; grid on;
        end
        
        % Load Scaling Generation Procedures
        function loadScalings = GenMvnScalings(obj,mu,sigma)
            
            % Statistical information
%             loadMean = mu*ones(obj.m,1); loadStd = sigma*ones(obj.m,1);
            
            % Create load cases
            try
                loadScalings = mvnrnd(mu,sigma,obj.trainingControls.nObs);
            catch
                loadScalings = mvnrnd(mu',sigma',obj.trainingControls.nObs);
            end
            % Generate load samplings using gaussian distribution
%             loadScalings = zeros(n,obj.trainingControls.nObs);
%             for ii = 1:n
%                 loadScalings(ii,:) = normrnd(loadMean(ii),loadStd(ii),...
%                     [1,nObservations]);
%             end
        end
        
        % Fit Knl to force displacment data
        function [Knl_la,fitInfo]=FitLasso(obj,trainX,trainY)

            % First find the value before a null model
            [~,fitInfo] = lasso(trainX,trainY,...
                                'DFmax',1,...
                                'MaxIter',obj.trainingControls.maxIters);
            lambdaUb = fitInfo.Lambda(end);

            % Generate lambda values (not sure best way)
            lambdas = logspace(-14,log10(lambdaUb),...
                      obj.trainingControls.numLambda);

            % Compute lasso fit
            [Knl_la,fitInfo] = lasso(trainX,trainY,...
                            'Lambda',lambdas,...
                            'CV',obj.trainingControls.kFold,...
                            'MaxIter',obj.trainingControls.maxIters,...
                            'MCReps',obj.trainingControls.mcReps,...
                            'alpha',obj.alpha);
        end
        
        % Retrain the model
        function ReTrain(obj,nObs,varargin)
            % Retrain the rom using a prescribed subst of training
            % information. 
            % 
            % Inputs :
            %       obj = LassoIcRom object
            %       nObs = number of observations used to used
            % Ouputs :
            %       obj = LassoIcROm with retrained nonlinear stiffness
            
            % if a smaller subset of observations is not desired then use
            % full set of data
            if isempty(nObs)
                nObs = length(obj.training.uModal);
            end
            
%             if nargin > 2
%                 weights = varargin{1}
%             else
%                 weights = ones(size
%             end

            % Assemble training data from each individual run
            fModalMat = cell2mat(obj.training.fModal); 
            uModalMat = cell2mat(obj.training.uModal);
            obj.Knl_Ls = (uModalMat\fModalMat)';
            
            % By default the rom will use least squares lasso
            obj.Knl = obj.Knl_Ls;
            
            % Perform lasso on each modal equation
            KnlEqn_La = cell(obj.m,1); fitInfo_LA = cell(obj.m,1);
            for ii = 1:obj.m
                [KnlEqn_La{ii},fitInfo_LA{ii}]=obj.FitLasso(uModalMat,...
                                               fModalMat(:,ii));
                disp(['Finished Mode # ', num2str(ii), ' Lasso Fit']);
                disp(' ');
            end
            
            % Expand back to entire system
            obj.Knl_La = KnlEqn_La;
            obj.training.fitInfo = fitInfo_LA;
            
            % Convert to nash form
            [N1,N2,Nlist]=Knl2Nash(obj.Knl,obj.tlist);
            obj.NashForm.N1 = N1; obj.NashForm.N2 = N2;
            obj.NashForm.Nlist = Nlist;
            
            % Plot the training
            obj.PlotTrainingFit();
        end
        
        % This function makes the current nonlinear stiffness by removing 
        % the smallest terms
        function obj = MakeMagSparse(obj,spFactor)
            
            KnlIn = obj.Knl_Ls;
            minVal = inf; KnlOut = KnlIn;
            nTermsPerEqn = round(size(KnlIn,2)*(1-spFactor));
            for ii = 1:size(KnlOut,1)
                for n = 1:nTermsPerEqn
                    for jj = 1:size(KnlOut,2)
                        if abs(KnlOut(ii,jj)) < minVal
                            iIndex = ii; jIndex = jj;
                            minVal = abs(KnlOut(ii,jj));
                        end
                    end
                    KnlOut(iIndex,jIndex) = inf; minVal = inf;
                end
            end
            KnlOut(KnlOut==Inf) =  0;
            obj.Knl = KnlOut;
        end
        
        function PlotKnlStats(obj)
           % This function computes and plots the statistics of the
           % nonlinear stiffness coefficients from each individual load
           % case. 
           
           % Concatendate cell array into 3D matrix
           KnlMat = cat(3, obj.training.KnlInd{:});
           
           % Compute statistics along 3d dimension
           mu_Knl = mean(KnlMat,3);
           sigma_Knl = std(KnlMat,[],3);
           
        end
        % ---- Abstract Methods ---- %
        
        % Build Implicit Condensation Rom
        function Build(obj,modeIndices,mu,sigma)
            % This function builds an IC rom
            
            % Create linear normal mode basis set
            obj.BasisCreate_LNM(modeIndices); phi = obj.basis;
            fn = diag(sqrt(obj.Kbb))./2./pi;
            
            % Build mass matrix
            M = sparse(obj.osFern.nDof,obj.osFern.nDof); K = M;
            M(obj.osFern.freeDof,obj.osFern.freeDof) = obj.osFern.GetMassMatrix();
            K(obj.osFern.freeDof,obj.osFern.freeDof) = obj.osFern.GetStiffnessMatrix();
            
            % Create static object if it doesnt exist
            if isempty(obj.static)
                obj.MakeStatic()
                if obj.staticControls.parallel
                   obj.static.SetParallelOpts(true,...
                       obj.staticControls.numWorkers);
                end
            end
            
            % Create load scalings
            loadScalings = obj.GenMvnScalings(mu,sigma);
            loadScalings = loadScalings';
            [~,n] = size(loadScalings);
            obj.training.loadScalings = loadScalings;
            obj.training.mu = mu; obj.training.sigma = sigma;
            
            % Initialize data
            uModal = cell(n,1); fModal = cell(n,1); Knl_Ind = cell(n,1);
            U =cell(n,1); F = cell(n,1);
            
            % Loop over all load cases
            tStart = tic;
            for ii = 1:n
                % Create loading cases for specific instance
                [P,F{ii}]=icloading(fn,phi,...
                    loadScalings(:,ii),M,[],obj.controls);
                
                % Solve the system
                [U{ii}] = obj.static.SolveMultiple(obj.staticControls.nSteps,...
                                                   F{ii});   
                % Remove linear contribution of the force - physical domain
                Fnl{ii} = F{ii} - K*U{ii}; fModal2{ii} = phi.'*Fnl{ii};
                
                % Generate modal static data
                mFitControls.quad = 1; mFitControls.triple = 1;
                [uModal{ii},fModal{ii},obj.tlist]=GenStaticModalData(fn,...
                    phi,F{ii},U{ii},mFitControls,...
                    [],phi.'*M);
                
                % Remove any nans from the data
                [nan_i,~] = find(isnan(uModal{ii}));
                uModal{ii}(nan_i,:) = []; fModal{ii}(nan_i,:) = [];
                
                % If doing basic least squares
                Knl_Ind{ii} = (uModal{ii}\fModal{ii})';
            end
            tStatic = toc(tStart);
            disp(['Finished Gather Static Load Case Data .. took ',...
                 num2str(tStatic), ' seconds'])
             
            % Assemble training data from each individual run
            fModalMat = cell2mat(fModal); uModalMat = cell2mat(uModal);
            obj.Knl_Ls = (uModalMat\fModalMat)';
%             [R,P] = corrcoef(uModalMat);
%             obj.Knl_Ls = fit(uModalMat,fModalMat,f,'StartPoint',[1 1],'Robust','on');
            % By default the rom will use least squares lasso
            obj.Knl = obj.Knl_Ls;
%             obj.Knl_Rf(1,:) = robustfit(uModalMat,fModalMat(:,1),'bisquare');
%             obj.Knl_Rf(2,:) = robustfit(uModalMat,fModalMat(:,2),'bisquare');
%             obj.Knl_Rf = obj.Knl_Rf';
            % Perform lasso on each modal equation
            KnlEqn_La = cell(obj.m,1); fitInfo_LA = cell(obj.m,1);
            for ii = 1:obj.m
                [KnlEqn_La{ii},fitInfo_LA{ii}]=obj.FitLasso(uModalMat,...
                            fModalMat(:,ii));
                disp(['Finished Mode # ', num2str(ii), ' Lasso Fit']);
                disp(' ');
            end
            
            % Expand back to entire system
            obj.Knl_La = KnlEqn_La;
            obj.training.fitInfo = fitInfo_LA;
            
            % Convert to nash form
            [N1,N2,Nlist]=Knl2Nash(obj.Knl,obj.tlist);
            obj.NashForm.N1 = N1; obj.NashForm.N2 = N2;
            obj.NashForm.Nlist = Nlist;
            
            % Perfrom membrane expansion
            obj.Compute_ExpansionModes(U{1},M);
            
            % Store training data
            obj.training.uModal = uModal;
            obj.training.fModal = fModal;
            obj.training.KnlInd = Knl_Ind;
            obj.training.F = F; obj.training.U = U; obj.training.P = P;
            obj.training.staticSolveTime = tStatic; 
            
            % Plot the training
            obj.PlotTrainingFit();
        end

        % ---- Overloaded Method ---- %
        function obj = plus(obj,obj2)
            % Combine the data from two LassoIcRom objects. The purpose of
            % this function is to form a way to easily combine training
            % data and retrain a rom. 
            % 
            % Inputs :
            %       obj = LassoIcRom object with target
            %       obj2 = LassoIcRom object with source
            %           
            % Ouputs :
            %       obj = LassoIcRom object with target
            
            
            % Gather all the physical data
            fData = [cell2mat(obj.training.F),cell2mat(obj2.training.F)];
            uData = [cell2mat(obj.training.U),cell2mat(obj2.training.U)];
            
            % Grab the basis set of the target object
            if obj.m < obj1.m 
                warning(['Target object has smaller basis set than ', ....
                        '... information may lbe lost'])
            end
            
            % Generate modal static data
            mFitControls.quad = 1; mFitControls.triple = 1;
            [uModal,fModal,obj.tlist]=GenStaticModalData(fn,...
                phi,fData,uData,mFitControls,...
                [],phi'*M);

            % Remove any nans from the data
            [nan_i,~] = find(isnan(uModal));
            uModal(nan_i,:) = []; fModal(nan_i,:) = [];

            % If doing basic least squares
            Knl_Ind = (uModal\fModal)';
        end
    end
    
end

