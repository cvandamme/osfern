classdef EdRom < Rom & handle
    % This class reperesents an Enforced Displacements Reduced Order Model
    % The ROM has the capabilities of including dual modes within the
    % basis set
    
    properties
        Knl, tlist
        NashForm = struct('N1',[],'N2',[],'Nlist',[]);
        training = struct('uModal',[],'fModal',[],'KnlIndx',[],...
                          'U',[],'F',[]);
        dual_m 
        controls = struct('sf',true,'df',true,'tf',true,...
                          'rf',true,'mf',false,'cf',true,...
                          'dm',true,'dmGen',1);
        staticControls = struct('paralle',false,'numWorkers',4,...
                                'nSteps',10);
    end
    methods
        % constructor
        function obj = EdRom(name,osFern)
            obj = obj@Rom(name,osFern);
        end
                
        % Create modal derivatives basis set
        function [v] = BasisCreate_DM(obj, phi, a, M)
            % Construct a basis using dual modes. Options are:
            %
            % - firstGen [true]: "First generation" dual modes
            % - secondGen [true]: "Second generation" dual modes
            %
            % Based on a function written by Rob Kuether and Lorraine
            % Guerin.
                        
            % Create psuedo inverse of basis set
            phiInv = phi.'*M;
            
            % Creat loadings
            obj.controls.tf = false; obj.controls.df = true;
            obj.controls.mf = true;
            [P,F] = icloading(sqrt(diag(obj.Kbb))/2/pi,phi,...
                a,M,[],obj.controls);
            
            % Build 1st Generation Dual modes
            v1 = zeros(size(phi));dlist = [];
            for kk = 1:size(phi,2)
                [~,t1] = ismember([kk 0 0],P,'rows');
                if ~isempty(t1)
                    u_i = obj.static.SolveMultiple(10,F(:,t1));
                    u_modal = phiInv(kk,:)*u_i*phi(:,kk);
                    w_i = u_i - u_modal;
                    v1(:,kk) = w_i;
                    dlist = [dlist; kk 0 0];
                end
            end
            v = v1;
            
            % Build 2nd Generation Dual modes
            if obj.controls.dmGen > 1
                dum = 1; v2 = zeros(size(phi,1),size(phi,2)^2);
                for jj = 1:size(phi,2)-1
                    for kk = jj+1:size(phi,2)
                        [~,t1] = ismember([jj kk 0],P,'rows');
                        if ~isempty(t1)
                            u_i = obj.static.SolveMultiple(10,F(:,t1));
                            u_modal = phiInv(jj,:)*u_i*phi(:,jj)+ ...
                                phiInv(kk,:)*u_i*phi(:,kk);
                            w_i = u_i - u_modal;
                            v2(:,dum) = w_i; dum = dum+1;
                            dlist = [dlist; jj kk 0];
                        end
                    end
                end
                v2 = v2(:,1:dum);
                v = [v v2];
            end
            
            % Build 3rd Generation Dual modes
            if obj.controls.dmGen > 2
                dum = 1; v3 = zeros(size(phi,1),size(phi,2)^3);
                for ii = 1:size(phi,2)-2
                    for jj = ii+1:size(phi,2)-1
                        for kk = jj+1:size(phi,2)
                            [~,t1] = ismember([ii jj kk],P,'rows');
                            if ~isempty(t1)
                                u_i = obj.static.SolveMultiple(10,F(:,t1));
                                u_modal = phiInv(ii,:)*u_i*phi(:,ii)+ ...
                                    phiInv(jj,:)*u_i*phi(:,jj)+ ...
                                    phiInv(kk,:)*u_i*phi(:,kk);
                                w_i = u_i - u_modal;
                                v3(:,dum) = w_i; % RJK added
                                dum = dum+1;
                                dlist = [dlist; ii jj kk];
                            end
                        end
                    end
                end
                v3 = v3(:,1:dum);
                v = [v v3];
            end
            
        end
        
        % ---- Abstract Methods ---- %
        
        % Build Enforced Displacements rom
        function Build(obj, modeIndices,modeScalings)
            % Construct a basis of modal derivatives. Options are:
            %
            % - crossModes [true]: If true, includes cross-derivative modes in the
            % basis.
            
            
            % Create linear normal mode basis set
            obj.BasisCreate_LNM(modeIndices);
            phi = obj.basis; phiFree = phi(obj.freeDof,:);
            
            % Build mass matrix
            M = obj.osFern.GetMassMatrix('All');
            
            % Build linear stiffness matrix
            K = obj.osFern.GetStiffnessMatrix('All'); 
            
            % Create static object if it doesnt exist
            if isempty(obj.static)
                obj.MakeStatic()
            end
            
            % Generate ed loadings 
            [P,U]=edloading_struct(phi,modeScalings,[],obj.controls);
            
            % Perform static solutions
            [F,Kt] = obj.static.RunMultipleEnforcedDisp(U);
            
            % Create dual modes - default mode scaling is a factor of 10
            % lower
            [v] = BasisCreate_DM(obj, phi, modeScalings./10, M);
            obj.dual_m = v;
            
            % Orthogonalize
            [obj.basis, obj.Kbb] = obj.Orthogonalize(phi,...
                                   v, K, M);
            
            % Generate modal static data
            mFitControls.quad = 1; mFitControls.triple = 1;
            [uModal,fModal,tlistTemp]=GenStaticModalData(obj.Kbb,...
                    phi,F,U,mFitControls,...
                    [],phi'*M);
                
            % If doing basic least squares
            KnlTemp = (uModal\fModal)';    
            
            % Generate tlist for dual mode set   
            [~,~,obj.tlist]=GenStaticModalData(obj.Kbb,...
                    obj.basis,F,U,mFitControls,...
                    [],obj.basis'*M);

            % Need to expand Knl to full rom size
            obj.Knl = obj.ExpandKnl(KnlTemp,tlistTemp,obj.tlist);
            
            % Convert to nash form
            [N1,N2,Nlist]=Knl2Nash(obj.Knl,obj.tlist);
            obj.NashForm.N1 = N1; obj.NashForm.N2 = N2;
            obj.NashForm.Nlist = Nlist;
           
            % Store training data
            obj.training.uModal = uModal;
            obj.training.fModal = fModal;
            obj.training.F = F; obj.training.U = U;
%             obj.training.staticSolveTime = tStatic; 
            
            % Store data
            obj.m = size(obj.basis, 2);                   
            obj.Mbb = eye(obj.m); obj.Cbb = zeros(obj.m);
            
        end
        
        % Assemble nonlinear stiffness matric
        function [Knl,varargout] = Get_NlStiffness(obj, dNew)
            % Initialzie matrices
            N1_u=zeros(obj.m,obj.m); N2_u=zeros(obj.m,obj.m);

            % Generate quadractic combinations
            d_d = dNew(obj.NashForm.Nlist(:,1)).*dNew(obj.NashForm.Nlist(:,2));
            for jj=1:obj.m
                N1_u(:,jj)= obj.NashForm.N1(:,:,jj) * dNew;
                N2_u(:,jj)= obj.NashForm.N2(:,:,jj) * d_d;
            end
            
            % Nonlinear stiffness
            Knl = N1_u+N2_u;
            
            % Nonlinear internal force
            if nargout > 1
                varargout{1}=(0.5*N1_u+(1/3)*N2_u)*dNew;
            end
        end
        
        % Assemble nonlinear internal force
        function [fnl,varargout] = Get_NlForce(obj,dNew)
            
            % Initialzie matrices
            N1_u=zeros(obj.m,obj.m); N2_u=zeros(obj.m,obj.m);

            % Generate quadractic combinations
            d_d = dNew(obj.NashForm.Nlist(:,1)).*dNew(obj.NashForm.Nlist(:,2));
            for jj=1:obj.m
                N1_u(:,jj)= obj.NashForm.N1(:,:,jj) * dNew;
                N2_u(:,jj)= obj.NashForm.N2(:,:,jj) * d_d;
            end
            
            % Nonlinear internal force
            fnl=(0.5*N1_u+(1/3)*N2_u)*dNew;

            % Nonlinear stiffness
            if nargout > 1
                varargout{1}=(N1_u+N2_u);
            end
        end
        
        % Assemble energy
        function [Enl] = Get_Energy(obj, dNew)
            % Initialzie matrices
            N1_u=zeros(obj.m,obj.m); N2_u=zeros(obj.m,obj.m);

            % Generate quadractic combinations
            d_d = dNew(obj.NashForm.Nlist(:,1)).*dNew(obj.NashForm.Nlist(:,2));
            for jj=1:obj.m
                N1_u(:,jj)= obj.NashForm.N1(:,:,jj) * dNew;
                N2_u(:,jj)= obj.NashForm.N2(:,:,jj) * d_d;
            end
            
            % Nonlinear stiffness
            Enl = (1/6)*dNew'*N1_u*dNew+(1/12)*dNew'*N2_u*dNew;
        end
    end
    
    methods (Static)
        function KnlOut = ExpandKnl(KnlIn,tlistIn,tlistOut)
            % This function takes a rom nonlinear stiffness terms
            % and expands them out to a larger rom.
            
            %
            maxIndex = max(max(tlistIn));
            KnlOut = zeros(max(max(tlistOut)),length(tlistOut));
            for jj = 1:maxIndex
                for ii = 1:size(tlistIn,1)
                    for kk = 1:length(tlistOut)
                        if tlistIn(ii,:) == tlistOut(kk,:)
                            KnlOut(jj,kk) = KnlIn(jj,ii);
                        end
                    end
                end
            end
        end
    end
end

