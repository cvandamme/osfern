classdef Component < handle 
    % Component is a class that holds the FE information for s specific component
    % of a MatFem object. This allows for flexibility in the model building, 
    % assembly and step procedures to allow for aribitrary rom/fem configurations.
    
    
    properties
        name % component name
        cid  % component id #
        nDOF % total DOF
        cDOF % constrainted DOF 
        fDOF % free DOF 
        
        dofVec % dof vector 
        fdofVec % freeDof Vector
        cdofVec % constraint vector
        
    end
    
    methods
        % Initialize Component
        function  obj = Component()
            
        end
        
        % Set methods
        function obj = SetName(obj,mname)
            obj.name = mname;
        end
        function obj = SetComponentNumber(obj,mcid)
            obj.cid = mcid;
        end
        
    end
    methods (Abstract)
        % Update the component
        [gdvOut,gnvOut,obj] = Update(obj,gdvIn,gnvIn)
      
        % Build Mass
        mfTripletVec = GetMass(obj)
        
        % Build Stiffness
        mfTripletVec = GetStiffness(obj)
                
        % Build Tangent Stiffness 
        mfTripletVec = GetTangentStiffness(obj)
        
        % Get internal state
        mfInternalForce = GetInternalForce(obj) 
    end
    
end

