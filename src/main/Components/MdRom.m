classdef MdRom < Rom & handle
    % This class reperesents a Modal Derivative Reduced Order Model
    % More detailed explanation to come soon
    
    properties
        md_vecs
        nlOpt = true;
        options = struct('crossModes',true)
        fdOpts = struct('domain','complex','step',1e-10);
    end
    methods
        % constructor
        function obj = MdRom(name,osFern)
            obj = obj@Rom(name,osFern);
        end
        
        % Create modal derivatives basis set
        function theta = BasisCreate_MD(obj, phi, M, K)
           
            % Linear frequencies
            lambda = diag(obj.Kbb);
            
            % Create dual mode basis set
            if obj.options.crossModes
                theta = zeros(obj.osFern.nDof, obj.m*(obj.m + 1)/2);
            else
                theta = zeros(obj.osFern.nDof, obj.m);
            end
            nDerivatives = size(theta, 2); count = 0;
            
            % Loop through the modes
            for j = 1:obj.m
                % if using cross modes
                if obj.options.crossModes
                    list = j:obj.m;
                else
                    list = j;
                end
                for k = list
                    count = count + 1;
                    
                    [~, Kt] = obj.osFern.GetTangentStiffness(...
                        1i*obj.fdOpts.step*phi(:, k)/norm(phi(:, k)));
                    dKtdj = imag(Kt)/obj.fdOpts.step;
                    
%                     theta(obj.osFern.freeDof, count) = -K\dKtdj*phi(obj.osFern.freeDof, j);
                    
                    m1 = [K-lambda(j)*M, -M*phi(obj.osFern.freeDof, j);...
                         (-M*phi(obj.osFern.freeDof, j))', 0];
                    v1 = m1\[dKtdj*phi(obj.osFern.freeDof,j);0];
                    theta(obj.osFern.freeDof, count) = v1(1:end-1);
%                     theta(obj.osFern.freeDof, count) = -K\dKtdj*phi(obj.osFern.freeDof, j);
                    
                    fprintf('Derivative (%i, %i) computed [%i of %i]\n',...
                            j, k, count, nDerivatives);
                    
                end
            end
        end
        
        % ---- Abstract Methods ---- %
        
        % Build the modal derivatives rom
        function Build(obj, modeIndices)
            % Construct a basis of modal derivatives. Options are:
            %
            % - crossModes [true]: If true, includes cross-derivative modes in the
            % basis.
            
            
            % Create linear normal mode basis set
            obj.BasisCreate_LNM(modeIndices); phi = obj.basis;
            
            % Build mass matrix
            M = obj.osFern.GetMassMatrix();
            
            % Build linear stiffness matrix
            K = obj.osFern.GetStiffnessMatrix(); 
            
            % Create dual modes
            theta = obj.BasisCreate_MD(phi, M, K); 
            obj.md_vecs = theta;
            
            % Orthogonalize
            [freeBasis, obj.Kbb] = obj.Orthogonalize( ...
                                   phi(obj.osFern.freeDof, :), ...
                                   theta(obj.osFern.freeDof, :), ...
                                   K, M);
                               
            % Store data
            obj.m = size(freeBasis, 2);                   
            obj.basis = zeros(obj.osFern.nDof, obj.m);
            obj.basis(obj.osFern.freeDof, :) = freeBasis;
            obj.Mbb = eye(obj.m); obj.Cbb = zeros(obj.m);
            
        end
        
        % Assemble nonlinear stiffness matric
        function [Ktr,varargout] = Get_NlStiffness(obj, dNew)
            
            if obj.nlOpt
               dFull = obj.basis*dNew;
               [Kn,Kt] = obj.osFern.GetTangentStiffness(dFull);
               Knl = obj.basis(obj.freeDof,:)'*Kn*obj.basis(obj.freeDof,:);
               Ktr = obj.basis(obj.freeDof,:)'*Kt*obj.basis(obj.freeDof,:);
            else
                Knl = zeros(size(obj.Kbb));
                Ktr = zeros(size(obj.Kbb));
            end
            
            if nargout > 1
                varargout{1} = 0.5*Knl*dNew;
            end
        end
        
        % Assemble nonlinear internal force
        function [fnl,varargout] = Get_NlForce(obj,dNew)
            if obj.nlOpt
                dFull = obj.basis*dNew;
                [Kn,Kt] = obj.osFern.GetTangentStiffness(dFull);
                Knl = obj.basis(obj.freeDof,:)'*Kt*obj.basis(obj.freeDof,:);
            else
                Knl = zeros(size(obj.Kbb));
            end
%             fnl2 = obj.basis(obj.freeDof,:)'*Kn*obj.basis(obj.freeDof,:)*dNew;
            fnl = 0.5*Knl*dNew;
            if nargout > 1
                varargout{1} = Knl;
            end
        end
        
    end
  
end

