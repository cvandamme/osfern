classdef ComponentSe
    % This class represents a superelement object that represents 
    % a component of a finite element assembly. The superelement 
    % can be geometrically nonlinear if desired. The superelement can be
    % used in full system methods. 
    % 
    % 

    properties
        osFern   % osfern parent fe model 
        name     % name of the component
        elements % elements included within the component
        nodes    % nodes included within the component
        modeIndices % which fixed interface modes are included
        oSet = struct('nodes',[],'dof',[]) % internal physical dof
        bSet = struct('nodes',[],'dof',[]) % boundary definition
        qSet = struct('dof',[]) % modal dof
        aSet = struct('dof',[]) % modal dof
        M,C,K    % linear mass, damping and stiffness of component
        phi      % fixed interface modes
        psi      % constraint modes shapes
        fn       % constraint modes 
        dof
    end
    
    methods
        function obj = ComponentSe(osFern,name)
            %UNTITLED Construct an instance of this class
            %   Detailed explanation goes here
            obj.osFern = osFern;
        end
        
        % Add features
        function obj = AddElements(obj,eles)
        end
        function obj = AddNodes(obj,nodes)
            
        end
        function obj = AddBoundary(obj,nodes,dof)
        end
        
        % Build model
        function obj = Build(obj,modeIndices)
            
            % Build required physical matrices matrix 
            Mfull = obj.osFern.BuildMassMatrix();
            Kfull = obj.osFern.BuildStiffnessMatrix();
            
            % Build dof 
            for n = 1:length(obj.nodes)
                
            end
            for n = 1:length(obj.bSet.nodes)
                
            end
            
            % Build required matrices
            Kii = Kfull(iDof, iDof); Mii = Mfull(iDof, iDof);
            Kib = Kfull(iDof, bDof); Mib = Mfull(iDof, bDof);
            Mbb = Mfull(bDof, bDof); Kbb = Kfull(bDof, bDof);

            % Build constraint modes
            obj.psi  = -Kii\Kib;
            
            % Compute the fixed interface modes
            fprintf('Finding %i modes for %s\n', nModes, obj.name);
            [phiFI, fn] = eigs(Kii, Mii, nModes, 'SM');
            fn = sqrt(diag(fn))/2/pi;
            [fn, sortInd] = sort(fn);
            obj.phi = phiFI(:, sortInd);
            
            % Save modal matrices to model
            obj.normFactor      = max(abs(phiFI), [], 1); % Normalization factor for each mode
            obj.linModel.phiFI  = phiFI;
            obj.linModel.omegaN = omegaN;
            obj.linModel.psiIB  = psiIB;
            
            % Build mass "hat" matrices (Craig-Bampton reduced mass matrices)
            MkbHat  = obj.phi.'*(Mii*obj.psi + Mib);
            MbbHat  = obj.psi.'*Mii*obj.psi + Mib.'*obj.psi +...
                        obj.psi.'*Mib + Mbb;
            
            % Build stiffness "hat" matrix (Craig-Bampton reduced stiffness matrices)
            KbbHat  = obj.psi.'*Kii*obj.psi + Kib.'*obj.psi +...
                        obj.psi.'*Kib + Kbb;
            
            % craig bampton mass matrix for component
            obj.M = [eye(length(obj.fn)),obj.MkbHat;
                     obj.MkbHat',        obj.MbbHat];
            
            % zero matrix for stiffness terms
            Z   = zeros(size(obj.MkbHat));
                
            % craig bamping stiffness matrix for component
            obj.K = [diag((obj.fn*2*pi).^2),    Z; 
                      Z',             obj.KbbHat];
            obj.seDof   = [true(length(obj.fn),1); false(size(obj.psi,1),1)];

        end
    end
end

