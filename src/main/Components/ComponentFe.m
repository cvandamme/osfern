classdef ComponentFe < Component & handle
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        vNodes
        vElements
        vConstraints
        vForces
        property
        state
    end
    
    methods
        function obj = ComponentFe(name)
            obj = obj@Component(name);
        end
        function obj = Update()
        end
        function obj = AddNode(nodeObj)
        end
        function obj = AddElement(eleObj)
        end
        
        % Build Mass
        function mfTripletVec = GetMassTriplets(obj)
        end
        % Build Stiffness
        function mfTripletVec = GetStiffnessTriplets(obj)
        end       
        % Build Tangent Stiffness 
        function mfTripletVec = GetTangentStiffnessTriplets(obj)
        end
        % Get internal state
        function mfInternalForce = GetInternalForce(obj) 
        end
    end
    
end

