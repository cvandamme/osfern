classdef HotMdRom < MdRom & handle
    % This class reperesents a Modal Derivative Reduced Order Model
    % More detailed explanation to come soon
    
    properties
        thermal
    end
    methods
        % constructor
        function obj = HotMdRom(name,osFern,thermal)
            obj = obj@MdRom(name,osFern);
            obj.thermal = thermal;
            
        end
        
        % Create modal derivatives basis set
        function theta = BasisCreate_MD(obj, phi, M, K)
           
            % Linear frequencies
            lambda = diag(obj.Kbb);
            
            % Create dual mode basis set
            if obj.options.crossModes
                theta = zeros(obj.osFern.nDof, obj.m*(obj.m + 1)/2);
            else
                theta = zeros(obj.osFern.nDof, obj.m);
            end
            nDerivatives = size(theta, 2); count = 0;
            
            % Loop through the modes
            for j = 1:obj.m
                % if using cross modes
                if obj.options.crossModes
                    list = j:obj.m;
                else
                    list = j;
                end
                for k = list
                    count = count + 1;
                    
                    [~, Kt] = obj.osFern.GetTangentStiffness(...
                        1i*obj.fdOpts.step*phi(:, k)/norm(phi(:, k)));
                    dKtdj = imag(Kt)/obj.fdOpts.step;
                                        
                    m1 = [K-lambda(j)*M, -M*phi(obj.osFern.freeDof, j);...
                         (-M*phi(obj.osFern.freeDof, j))', 0];
                    v1 = m1\[dKtdj*phi(obj.osFern.freeDof,j);0];
                    theta(obj.osFern.freeDof, count) = v1(1:end-1);
                    
                    fprintf('Derivative (%i, %i) computed [%i of %i]\n',...
                            j, k, count, nDerivatives);
                    
                end
            end
        end
        
        % ---- Abstract Methods ---- %
        
        % Build the modal derivatives rom
        function Build(obj, modeIndices)
            % Construct a basis of modal derivatives. Options are:
            %
            % - crossModes [true]: If true, includes cross-derivative modes in the
            % basis.
            
            obj.freeDof = obj.osFern.freeDof;
            
            % Create a basis using linear modes only to start
            obj.modeIndices = sort(modeIndices);
            obj.m = length(modeIndices);
            p = modeIndices(end);
            
            % Solve nonlinear static thermal equilibrium
            obj.thermal.Solve(10);
            obj.initialDisplacement = obj.thermal.finalDisplacement;
            
            % Eigenvalue about thermal equilibrium
            obj.thermal.Frequency(p); 
            fn = obj.thermal.fn; phi = obj.thermal.phi;
            
            % Set basis, mass, and stiffness matrices
            obj.basis = phi(:, modeIndices);
            obj.Kbb = diag((2*pi*fn(modeIndices)).^2);
            obj.Mbb = eye(obj.m);  obj.Cbb = zeros(obj.m);
            
            % Build mass matrix
%             M = sparse(obj.osFern.nDof,obj.osFern.nDof);
%             M(obj.freeDof,obj.freeDof) = obj.osFern.GetMassMatrix();            
            M = obj.osFern.GetMassMatrix();
            % Build linear stiffness matrix + thermal stress stiffness
%             K = sparse(obj.osFern.nDof,obj.osFern.nDof);
%             K(obj.freeDof,obj.freeDof) = obj.thermal.Kss + ...
%                                          obj.osFern.GetStiffnessMatrix();             
            K = obj.thermal.Kss + obj.osFern.GetStiffnessMatrix();
            
            % Create dual modes
            theta = obj.BasisCreate_MD(phi, M, K); 
            obj.md_vecs = theta;
            
            % Orthogonalize
            [freeBasis, obj.Kbb] = obj.Orthogonalize( ...
                                   phi(obj.osFern.freeDof, :), ...
                                   theta(obj.osFern.freeDof, :), ...
                                   K, M);
                               
            % Store data
            obj.m = size(freeBasis, 2);                   
            obj.basis = zeros(obj.osFern.nDof, obj.m);
            obj.basis(obj.osFern.freeDof, :) = freeBasis;
            obj.Mbb = eye(obj.m); obj.Cbb = zeros(obj.m);
            
        end
        
        % Assemble nonlinear stiffness matric
        function [Ktr,varargout] = Get_NlStiffness(obj, dNew)
            
            if obj.nlOpt
               dFull = obj.basis*dNew;
               [Kn,Kt] = obj.osFern.GetTangentStiffness(dFull);
               Knl = obj.basis(obj.freeDof,:)'*Kn*obj.basis(obj.freeDof,:);
               Ktr = obj.basis(obj.freeDof,:)'*Kt*obj.basis(obj.freeDof,:);
            else
                Knl = zeros(size(obj.Kbb));
                Ktr = zeros(size(obj.Kbb));
            end
            
            if nargout > 1
                varargout{1} = 0.5*Knl*dNew;
            end
        end
        
        % Assemble nonlinear internal force
        function [fnl,varargout] = Get_NlForce(obj,dNew)
            if obj.nlOpt
                dFull = obj.basis*dNew;
                [Kn,Kt] = obj.osFern.GetTangentStiffness(dFull);
                Knl = obj.basis(obj.freeDof,:)'*Kt*obj.basis(obj.freeDof,:);
            else
                Knl = zeros(size(obj.Kbb));
            end
%             fnl2 = obj.basis(obj.freeDof,:)'*Kn*obj.basis(obj.freeDof,:)*dNew;
            fnl = 0.5*Knl*dNew;
            if nargout > 1
                varargout{1} = Knl;
            end
        end
        
    end
  
end

