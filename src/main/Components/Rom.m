classdef Rom < handle
    %   This class is used to represent a ROM for geometric nonlinear
    %   analysis. The ROMs are generated from finite element models within
    %   the OsFern finite element code. The following ROMs are supported
    %   IcRom - Implicit Condensation Rom
    %   MdRom - Modal Derivatives Rom
    %   SmRom = Shi and Mei Rom
    %   EdRom - Enforced Displacements Rom
    
    properties
        osFern        % osfern FE class object
        name          % name of rom, what it will be saved as
        basis;        % Reduction basis
        freeDof;      % Free degrees of freedom
        Kbb;          % Reduced stiffness matrix
        Mbb;          % Reduced mass matrix
        Cbb;          % Reduced dampin matrix
        m;            % Number of modes in basis
        modeIndices;  % Indices of linear modes in basis (if applicable)
        damping = struct('alpha',0.0,'beta',0.0);
        initialDisplacement % displacement that defines the equilibrium the
                            % rom is made about
        matrix      % matrix step object
        modal       % modal step object
        static      % nonlinear static step object
        dynamic     % nonlinear dynamic step object
    end
    
    methods
        % Constructor
        function obj = Rom(name,osFern)
            obj.osFern = osFern;
            obj.name = name;
            obj.freeDof = obj.osFern.freeDof;
%             obj = obj@Component(name);
        end
        
        % Create linear basis set
        function obj = BasisCreate_LNM(obj,modeIndices)
            % Create a basis using linear modes only.
            obj.modeIndices = sort(modeIndices);
            obj.m = length(modeIndices);
            p = modeIndices(end); % Number of modes to obtain
            
            % Make Frequency object if not present
            if isempty(obj.modal)
                obj.MakeModal();
            end
            obj.modal.Solve(p);
            
            % Set basis, mass, and stiffness matrices
            obj.basis = obj.modal.phi(:, modeIndices);
            obj.Kbb = diag((2*pi*obj.modal.fn(modeIndices)).^2);
            obj.Mbb = eye(obj.m);  obj.Cbb = zeros(obj.m);
        end
        
        % Mass/Stiffness full FE functions
        function MakeMatrix(obj,varargin)
            % If additional arguments are used then have abaqus solve the
            % load cases
            if nargin > 1
                % Grab inputs
                feModel = varargin{1}; feDir = varargin{2};

                % Create abaqus interface
                abqObj = AbqInterface(obj.name,feModel,feDir,cd,BATCHcommands);

                % Perform modal and store similiar to osfern format
                abqObj.GetModal();
                obj.modal.fn = abqObj.fn; obj.modal.phi = abqObj.phi;
                abqObj.GetMK();
                obj.modal.M = M; obj.modal.K = K;
            else
                % Generate modal object and add in any step constraints
                obj.modal = Frequency('modal', obj.osFern, obj.initialDisplacement);
            end
        end
            
        % Modal full FE functions 
        function MakeModal(obj,varargin)
            
            
            % If additional arguments are used then have abaqus solve the
            % load cases
            if nargin > 1
                % Grab inputs
                feModel = varargin{1}; feDir = varargin{2};

                % Create abaqus interface
                abqObj = AbqInterface(obj.name,feModel,feDir,cd,BATCHcommands);

                % Perform modal and store similiar to osfern format
                abqObj.GetModal();
                obj.modal.fn = abqObj.fn; obj.modal.phi = abqObj.phi;
                abqObj.GetMK();
                obj.modal.M = M; obj.modal.K = K;
            else
                % Generate modal object and add in any step constraints
                obj.modal = Frequency('modal', obj.osFern, obj.initialDisplacement);
            end

            
            
        end
        
        % Static full FE functions
        function MakeStatic(obj,varargin)
            
            % If additional arguments are used then have abaqus solve the
            % load cases
            if nargin > 1
                % Grab inputs
                feModel = varargin{1}; feDir = varargin{2};

                % Create abaqus interface
                abqObj = AbqInterface(obj.name,feModel,feDir,cd,BATCHcommands);

                % Create function handle to static force
                obj.static = @(F) abqObj.StaticForce(F);    
            else
                % Generate modal object and add in any step constraints
                obj.static = NLStatic('modal', obj.osFern, obj.initialDisplacement);
                
            end
            
        end
        
        % Dynamic full FE functions 
        function SetDynamic(obj,dynamicObj)
            obj.dynamic = dynamicObj;
        end
        function MakeDynamic(obj,varargin)
            
            % Generate modal object and add in any step constraints
            obj.dynamic = Dynamic('modal', obj.osFern, obj.initialDisplacement);
            
            
        end
        
        % Get methods
        function [f,varargout] = Get_Force(obj, dNew)
            
            % Get nonlinear portion
            [fnl,Knl] = obj.Get_NlForce(dNew);
            
            % Add linear portion
            f = obj.Kbb*dNew + fnl;
            
            % Output stiffness if desired
            if nargout > 1
                varargout{1} = obj.Kbb + Knl;
            end
        end
        function [K,varargout] = Get_Stiffness(obj, dNew)
            
            % Get nonlinear portion
            [fnl,Knl] = obj.Get_NlForce(dNew);
            
            % Add linear portion
            K = obj.Kbb + Knl;
            
            % Output stiffness if desired
            if nargout > 1
                varargout{1} = obj.Kbb*dNew + fnl; 
            end
        end
        function [E] = Get_Energy(obj, dNew)
            
            % Get nonlinear portion
            [Enl] = obj.Get_NlEnergy(dNew);
            
            % Add linear portion
            E = 0.5*dNew'*obj.Kbb*dNew + Enl;

        end
        
        % Display information
        function disp(obj)
            
            fprintf('Rom Object\n');
            
            fprintf('\tName: %s\n', obj.name);
            
            fprintf('\t%i modes\n', obj.m);
%             fprintf('\t%i DOF\n', obj.nDof);
%             fprintf('\t%i elements\n', obj.nElement);
            
            
            
        end
        
        % Save the object
        function save(obj)
           save(obj.name,'obj') 
        end
        
        % Plot the basis set
        function PlotBasis(obj)
            figure; 
            fn = sqrt(diag(obj.Kbb))/2/pi;
            for ii = 1:obj.m
                ax{ii} = subplot(2,ceil(obj.m/2),ii);
                obj.osFern.Plot(obj.basis(:,ii)/norm(obj.basis(:,ii)));
                try
                    title(['Mode ',num2str(obj.modeIndices(ii)),...
                           ' , f = ',num2str(round(fn(ii),2)),' Hz'])
                catch
                    title(['Additional basis',num2str((ii)),...
                           ' , f = ',num2str(round(fn(ii),2)),' Hz'])   
                end
                axis off;
            end
        end
    end
    
    methods(Static)
        function [basis, Kbb] = Orthogonalize(phi, theta, K, M)
            % Orthogonalize additional shapes in theta with respect to phi
            % through the mass matrix.
            
            m = size(phi, 2);
            V = [phi, theta];
            [n, k] = size(V);
            basis = zeros(n,k);
            basis(:,1:m) = phi;
            
            for i = (m + 1):k
                basis(:,i) = V(:,i);
                for j = 1:i-1
                    basis(:,i) = basis(:,i) - ( basis(:,i)'*M*basis(:,j) )/( basis(:,j)'*M*basis(:,j) )*basis(:,j);
                end
                % Mass normalize
                basis(:,i) = basis(:,i)/sqrt(basis(:,i)'*M*basis(:,i));
            end
            
            Kbb = basis'*K*basis;
            
        end
    end
    
    methods(Abstract)
        
        % Build the rom
        Build(obj);
        
        % Assemble nonlinear stiffness matric
        Get_NlStiffness(obj, dNew);
        
        % Assemble nonlinear internal force
        Get_NlForce(obj,dNew)
        
    end
end

