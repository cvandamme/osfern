classdef ComponentAssembly
    % This class assembles individual components, either full models or
    % superelements, that will later be used in analyses. 
    % 
    
    properties
        components = {}
        B, L % craig bampton transformation matrices
        bSet = struct('nodes',[],'dof',[],count,0)
        aSet = struct('dof',[],'type',[])
        M,C,K % assembled mass, damping and stiffness matrices
    end
    
    methods
        function obj = ComponentAssembly(osFern)
            %UNTITLED2 Construct an instance of this class
            %   Detailed explanation goes here
            obj.Property1 = inputArg1 + inputArg2;
        end
        function obj = AddComponents(obj,component)
            nComp = length(obj.components);
            obj.components{nComp + 1} = component;
        end
        function obj = Build_BnL(obj)
            
            bNodes = [obj.components(:).bSet.nodes];
            bDof = [obj.components(:).bSet.dof];
            
            % If any of the boundaries are not repeated then they are
            % connected to the residual structure
            [bConn2Res,bIds] = unique(bNodes);
            
            % Now take the leftovers which should be connected to residual
            bConn2Part = bNodes;
            
            % Find the boundaries of each component
            for i = 1:length(obj.components)
                
                
            end
            % Perform preliminary system substructuring for both the CB and 
            % the CC substructuring techniques. Constructs a
            % signed binary B matrix of DOF constraints then uses SVD to
            % find the null space of B, yielding the boolean assembly
            % matrix L. L is then used as a transformation from a block
            % diagonal matrix of each CB component matrix to the final
            % system CB matrix.
            % The "makeConnections"-method of CBSS-class writes connected 
            % nodes in right order in cell array 'obj.connect': let's
            % assume that part 1 and part 3 are connected; then the node
            % of part 1 corresponding to the k-th row of obj.connect{1,3}
            % is connected to the node of part 3 corresponding to the
            % k-th row of obj.connect{3,1}
            
            % 1) Build the B matrix. First, we need a list of the system
            % DOF number of each boundary DOF in the system. This also
            % determines the total number of boundary DOF in the system.
            % (We don't know otherwise).
            obj.bSetRelations = containers.Map('KeyType', 'char', 'ValueType', 'double');
            currDOF = 0; % Running count of "current" DOF in loop
            for i = 1:length(obj.components) % Run through each part
                
                % Add modal dofs to current dof counter 'currDOF'
                currDOF = currDOF + length(obj.components{i}.qSet);
                
                % Need to get a UNIQUE list of DOF that are actually attached
                connSet = []; 
                obj.parts(i).nBoundaryDOF = 0;
                for j = 1:obj.npart % Run through each connection set
                        if (i == j); continue; end      % Skip boundary sets
                        setA = obj.connect{i, j};
                        if isempty(setA); continue; end % Skip empty sets

                        % Change to node.dof format (note transpose)                        
                        setA = bsxfun(@plus, setA(:, 1), bsxfun(@times, setA(:, 2:7),(1:6)/10))';
                        setA = setA(:); 
                        setA((setA - floor(setA) == 0)) = []; % FG: I think the brackets are wrong, but does not matter since the minus-operation is evaluated first
                        % ('0' is not active, '1' is active)
                        
                        connSet = [connSet; setA];
                        % Also handle the connectivity, noting that a node
                        % may have more than 1 attaching DOF

                end

                connSet = unique(connSet, 'stable'); % Only unique DOF
                nConDof = length(connSet);
                % Now, assign to each of these DOF an absolute system DOF
                % number
                for j = 1:length(connSet);
                    % DOF on different parts need unique ID; use a string
                    dofStr = sprintf('part(%i).%.1f', i, connSet(j));
                    obj.boundaryRelations(dofStr) = currDOF + j; % Write in Map-object 'boundaryRelations'
                end
                currDOF = currDOF + nConDof;
                obj.parts(i).nBoundaryDOF = obj.parts(i).nBoundaryDOF + nConDof;
                obj.nBoundaryDOF = obj.nBoundaryDOF + nConDof;
            end
            
            

            % Now we know the absolute DOF number of all boundary DOF in
            % the model. We have to do the same thing, but this time go
            % through and keep track of DOF connectivity.
            currConstr = 1; % Current constraint number
            obj.B = sparse(1, obj.nModalDOF + obj.nBoundaryDOF);
            for i = 1:obj.npart % Run through each part
                for j = (i + 1):obj.npart % Run through each connection set
                    
                    % Now we can go through again, but since we actually
                    % know where the DOF are in the model, we can build the
                    % B matrix.
                    setA = obj.connect{i, j};   % Get connetion nodes of part i
                    setB = obj.connect{j, i};   % Get connetion nodes of part j
                    if isempty(setA); continue; end

                    % Change dofs of both parts to node.dof format
                    setA = bsxfun(@plus, setA(:, 1), bsxfun(@times, setA(:, 2:7),(1:6)/10))';
                    setA = setA(:);
                    setA((setA - floor(setA) == 0)) = []; % Why is this needed?
                    setB = bsxfun(@plus, setB(:, 1), bsxfun(@times, setB(:, 2:7),(1:6)/10))';
                    setB = setB(:);
                    setB((setB - floor(setB) == 0)) = [];

                    if (length(setA) ~= length(setB))
                        error('Incompatible connection sets for parts %i and %j', i, j);
                    end
                    
                    % NOW we can build the B-matrix
                    for k = 1:length(setA)
                        dofStrI = sprintf('part(%i).%.1f', i, setA(k));
                        dofStrJ = sprintf('part(%i).%.1f', j, setB(k));
                        try
                            constrI = obj.boundaryRelations(dofStrI);
                            constrJ = obj.boundaryRelations(dofStrJ);
                        catch err
                            keyboard
                        end
                        obj.B(currConstr, constrI) = 1;
                        obj.B(currConstr, constrJ) = -1;
                        currConstr = currConstr + 1;
                        % MSA: As long as B is boolean, we can probably just
                        % build L analytically as well??
                    end
                end
            end
            assert(size(obj.B, 2) == (obj.nBoundaryDOF + obj.nModalDOF), ...
                'Incompatible size of B Matrix');
            % Get total number of constraints
            obj.nConstr = currConstr - 1;
            obj.L = null(obj.B, 'r');
            

        end
        
        function [M, K] = AssembleLinear(obj)
            % This function assembles the linear mass and stiffness
            % matrices of a structure by assembling craig bampton
            % superelements. 
            
            % Build the craig bampton transformation matrices
            if isempty(obj.L)
                obj = obj.Build_BnL();
            end
            
            % Finally, assemble the full system and transform it
            Msys = sparse([]); Ksys = sparse([]); ssInd = [];
            
            % Run through each part
            for i = 1:length(obj.components) 
                                 
                % Add to system matrices
                Msys    = blkdiag(Msys, Mcb);
                Ksys    = blkdiag(Ksys, Kcb);
                ssInd   = [ssInd; i*ones(size(Mcb,1),1)];
                
                % Assemble omitted dof (ones represented by modal dof) and
                % boundary dof
                               
            end
            
            % Now add any contributions from residual structure
            
            % Assemble the mass and stiffness matrices
            M = obj.L'*full(Msys)*obj.L;
            K = obj.L'*full(Ksys)*obj.L;
            
            % Modal analysis of assembled matrices
%             [phiLocal, lambda] = eigs(Kassem, Maseem, nModes, 'SM');
%             obj.phi = obj.L*real(phiLocal);
%             obj.fn = real(sqrt(diag(lambda))/2/pi);
            
        end
        
        function [fNl,varargout] = AssembleNonLinear(obj,def)
            % This function assembles the nonlinear internal force at the
            % system level of a structure by assembling craig bampton
            % superelements. Also the tangent and nonlinear stiffness can
            % be outputted. 
            % The nonlinear representation of the superelement depends on
            % the modeling type. The following are supprted:
            % 1) MdSe - Modal derivatives superelement
            %         - this fully assembles the associated component
            %           nonlinear stiffness matrices then projects onto the
            %           superelement 
            % 2) IcSe - Implicit Condensation superelement
            %
            % 3) SmSe - Shi and Mei superelement
            % 
            % Inputs : 
            %           obj = ComponentAssembly object
            %           def = deformation, this is in the se cb space
            %           for each component, if the component is linear then
            %           it is ignored
            % Outputs :
            %           fNl = nonlinear internal restoring force
            %           varargout{1} = nonlinear stiffness
            %           varargout{2} = tangent stiffness
            
            % Finally, assemble the full system and transform it
            KnlSys = sparse([]); Ksys = sparse([]); ssInd = [];
            
            % Run through each part
            for i = 1:length(obj.components) 
                
                % zero matrix for stiffness terms
                Z   = zeros(size(obj.components{i}.MkbHat));
                
                % Evaluate modal nonlinear forces
                [fNl,kNl,kT] = obj.component(i).GetFint(def_se);
                
                % Evaluate physical (boundary) forces
                
                
                % craig bamping stiffness matrix for ith component
                Kbl = [diag((obj.components{i}.fn*2*pi).^2),    Z; 
                       Z',                       obj.components{i}.KbbHat];
                   
                % Add to system matrices
                KnlSys    = blkdiag(KnlSys, Mcb);
                Ktsys    = blkdiag(Ktsys, Kcb);
                fNlsys = [fNlsys;fnl];
                ssInd   = [ssInd; i*ones(size(Mcb,1),1)];
                
                % Assemble omitted dof (ones represented by modal dof) and
                % boundary dof
                               
            end
            
            % Assemble the mass and stiffness matrices
            fNlAssem = obj.L'*full(fNl);
            KnlAssem = obj.L'*full(Knl)*obj.L; 
            KtAssem = obj.L'*full(Kt)*obj.L;
            
            % Output additional information if desired
            if nargout > 1
                varargout{1} = KnlAssem;
            elseif nargout > 2
                varargout{2} = KtAssem;
            end
        end
    end
end

