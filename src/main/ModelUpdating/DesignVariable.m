classdef DesignVariable < handle
    % This class is used to model design variables such that they
    % are updated across the entire model. They can be used simply as
    % values for a parameter as normal. 
    
    properties
        val;
        name; 
        eleId;
        nl= true; % this is used to say whether this is nonlinear or not
    end
    
    methods
        function obj = DesignVariable(val,name)
            obj.val = val;
            obj.name = name;
        end
        function obj = set(obj,valIn)
            obj.val = valIn;
        end
        function valOut = GetVal(obj)
            valOut = obj.val;
        end
        function obj = SetName(obj,name)
            obj.name = name;
        end
        function obj = SetVal(obj,valIn)
            obj.val = valIn;
        end
        % overloaded operations
        function valOut = plus(obj1,obj2)
            a = double(obj1);
            b = double(obj2);
            valOut = a + b;
        end
        function valOut = minus(obj1,obj2)
            a = double(obj1);
            b = double(obj2);
            valOut = a - b;
        end
        function valOut = times(obj1,obj2)
            a = double(obj1);
            b = double(obj2);
            valOut = a*b;
        end
        function valOut = mtimes(obj1,obj2)
            a = double(obj1);
            b = double(obj2);
            valOut = a*b;
        end
        function valOut = rdivide(obj1,obj2)
            a = double(obj1);
            b = double(obj2);
            valOut = a./b;
        end
        function valOut = mrdivide(obj1,obj2)
            a = double(obj1);
            b = double(obj2);
            valOut = a/b;
        end
        function valOut = power(obj1,obj2)
            a = double(obj1);
            b = double(obj2);
            valOut = a.^b;
        end
        function valOut = mpower(obj1,obj2)
            a = double(obj1);
            b = double(obj2);
            valOut = a.^b;
        end
        function valOut = double(obj)
            valOut = obj.val;
        end
        function valOut = imag(obj)
            valOut = imag(obj.val);
        end
        function valOut = real(obj)
            valOut = real(obj.val);
        end
    end
    
end

