classdef LinearModelUpdate < handle
    % This class contains routines in which to update an OsFern
    % finite element model to match some provided low level test data. Primarily the
    % data of concern is modal test data i.e. natural frequcnies. Currently
    % the code is limited to matching modes based on mode indices.
    % Identification of modes by MAC or CO is not yet supported.
    
    properties
        name     % name of object
        target = struct('modeIndices',[],'fn',[],'phi',[]);
        paramVec % vectof of design variable objects
        J   % cost function value
        dJ  % gradients
        P   % parameter values (normalized) during updating
        p0  % initial design parameters
        fnError % matrix of errors between target and model frequncies
        alpha % optimization routine step size
        iter  % iteration number
        % Controls
        A         % state variable weight scaling
        B         % design parameter weight scaling
        gamma = 0 % total design weight scaling
        bounds = [] % bounds on parameters
        
        % Initial and final values
        initial = struct('p',[],'J',[],'dJ',[],'fn',[],'phi',[])
        final = struct('p',[],'J',[],'dJ',[],'fn',[],'phi',[])
        
        % OsFren and step objects
        osFern     % OsFern Object
        modalObj   % Modal Object
        staticObj  % Static Object
        dynamicObj % Dynamic Object
        
        % Plot controls
        fHandle

        % Finite difference options
        fdOpts = struct('domain','complex','step',1e-12);
        
        % Parallel options
        parOpts = struct('logic',false,'numWorkers',2);
        
        % Optimization options
        optOpts = struct('alg','interior-point',...
                         'gradients',false,...
                         'stepTol',1e-6,'gradTol',1e-6,'funTol',1e-6)
        
        % Plotting options
        plotOpts = struct('sp11','parameter','sp12','cost',...
                          'sp21','gradient','sp22','freq')
                      
        % Apply displacement field (for linearized modal about non-zero
        % equilibrium)
        dispField
        
    end
    
    methods
        % Initialization Function
        function obj = LinearModelUpdate(name,osFern,paramVec)
            obj.name = name; obj.osFern = osFern;
            obj.paramVec = paramVec;
        end
        
        % Specify target data to be used
        function obj = SetTargetData(obj,modeIds,fn,varargin)
            
            % Store mode indicies and frequncies (Hz)
            obj.target.modeIndices = modeIds;
            obj.target.fn = fn;
            
            % If mode shapes are used then included within model updating
            if nargin > 3
                obj.target.phi = varargin{1};
            end
        end
        
        % Set weights
        function obj = SetWeights(obj,A,B,gamma)
            obj.A = A;
            obj.B = B;
            obj.gamma = gamma;
        end
        
        % Set displacement field
        function obj = SetDisplacement(obj,dispField)
            % This function sets the displacement field for which to perform
            % the eigenvalue problem about. If multiple states are
            % specified then an eigenvalue computation will be performed
            % about each state. The user must then supply a matrix of
            % frequcnies [nf x nStates] in the target data.
            
            % Check sie and number of cases
            [nDof,nLoads] = size(dispField);
            % Check size of field
            if nDof == size(obj.osFern.freeDof)
                
            elseif nDof == sum(obj.osFern.freeDof)
                
            else
                error('Displacement field size is incorrect')
                
            end
            
            % Store into the object
            obj.dispField = dispField;
        end
        
        % Start the optimization routine
        function [obj] = Start(obj,varargin)
            
            
            % Create modal object if empty
            obj.modalObj = Frequency([obj.osFern.name,'_modal'],obj.osFern);
            
            % Check if weights are supplied
            if isempty(obj.A); obj.A = eye(length(obj.target.fn)); end
            if isempty(obj.B); obj.B = eye(length(obj.paramVec));  end
            
            % Check if bounds are supplied
            if isempty(obj.bounds)
                lb = []; ub = [];
            else
                lb = obj.bounds(:,1); ub = obj.bounds(:,2);
            end
            
            % Optimization options
            options = optimoptions(@fmincon,...
                     'Algorithm',obj.optOpts.alg,...
                     'Display','iter-detailed',...
                     'StepTolerance',obj.optOpts.stepTol,...
                     'OptimalityTolerance',obj.optOpts.gradTol,...
                     'FunctionTolerance',obj.optOpts.funTol,...
                     'SpecifyObjectiveGradient',obj.optOpts.gradients,...
                     'CheckGradients',true,...
                     'OutputFcn',@obj.OutFun);
 
            % Linear Constraints 
            Aineq = []; bineq = []; Aeq = []; beq = [];
            
            % Provide initial predicion
            pStart = ones(length(obj.paramVec),1);
            
            % Grab initial values
            for ii = 1:length(obj.paramVec)
                obj.initial.p(ii) = obj.paramVec{ii}.GetVal();
            end
            obj.p0 = obj.initial.p;
            
            % Evaluate the problem with nominal parameters
            [obj.initial.J,obj.initial.dJ] = obj.MinFreqRes(pStart);         
            obj.initial.fn = obj.modalObj.fn(obj.target.modeIndices);
            
            % Assign objective function
            objfun = @(p)obj.MinFreqRes(p);
            
            % Call fmincon
            [pF,J,exitFlag,output,lambda,djVal]=fmincon(objfun,pStart,...
                Aineq,bineq,Aeq,beq,lb,ub,...
                [],options);
            
            % Evaluate the problem with final parameters
            [obj.final.J,obj.final.dJ] = obj.MinFreqRes(pF);    
            obj.final.p = pF;
            obj.final.fn = obj.modalObj.fn(obj.target.modeIndices);
            % Update design variables
            for ii = 1:length(pF)
                obj.paramVec{ii}.SetVal(obj.p0(ii)*pF(ii));
            end
            
            % Update the object
            obj.osFern = obj.osFern.Update(true);
        end
        
        % Objective function
        function [J,varargout] = MinFreqRes(obj,pC)
            % This function updates the parameters of a FE model in order to obtain
            % a prescriebd set of nonlinear coefficients through the IC method.
            % Inputs :
            %       pC = Current parameters used in the optimization procedure
            %       Klin_target = Linear stiffness targets - matrix of vector of
            %                     diagonals
            %       paramVec = Vector of design variables
            %       feObj = finite element object
            %       mind = modes to use
            %       p0 = initial parameter values
            %       varargin{1} ; vector of weighting coefficients for the modes
            % Outputs :
            %       J = objective function value
            %       dJ = gradient vrctor
            
            % Compute parameter change
            dP = ones(size(pC)) - pC;
   
            % Update the design variables
            for ii = 1:length(pC)
                obj.paramVec{ii}.SetVal(pC(ii)*obj.p0(ii));
            end
            
            % Update the fe model
            obj.osFern.Update(true);
           
            % Create modal object and solve
            obj.modalObj.Solve(max(obj.target.modeIndices));
            
            % Compute the error
            dY = (obj.target.fn - obj.modalObj.fn(obj.target.modeIndices))./obj.target.fn;
            
            % Compute the objective function
            J = dY'*obj.A*dY + obj.gamma*dP'*obj.B*dP;
            
            % normalize objective function
            if ~isempty(obj.initial.J)
                J = J/obj.initial.J;
            end
            
            % Compute gradient if desired
            if nargout > 1
                dfdp = -1*obj.GradientCalc_LinearFreq(pC);
                varargout{1} = 2*(dfdp'*obj.A*dY + obj.gamma*obj.B*dP); %.*obj.p0';
%                 if ~isempty(obj.initial.J)
%                     varargout{1} = varargout{1}/norm(obj.initial.dJ);
%                 end
            end
            
            % Try to save current value
            try
                obj.fnError(:,obj.iter+1) = dY;
            catch
                obj.fnError(:,1) = dY;
            end
        end
                
        % Gradient of frequency wrt to parameters
        function [dwdp,varargout] = GradientCalc_LinearFreq(obj,pC)
            % This function computes the sensitivity of linear natural
            % frequcnies to changes the parameters of a FE model.
            % 
            % Inputs :  
            %         phiC = current mode shape vectors
            %         wC = current eigenvalues (rad/sec)
            %         pC = current design variables
            % Outputs :
            %         dwdp = gradient of frequencies wrt to design
            %         parameters

            % Grab freedof vector
            fDof = obj.osFern.freeDof;
           
            % Ggrab the modal object results 
            w = 2*pi*obj.modalObj.fn; w = w(obj.target.modeIndices);
            phi = obj.modalObj.phi; phi = phi(:,obj.target.modeIndices);
            
            % Calculate Gradients 
            if strcmp(obj.fdOpts.domain,'complex')
                [dMdP,dKdP] = obj.GradientCalc_dMKdP_c(pC); % [n x n x p]
            else
                [dMdP,dKdP] = obj.GradientCalc_dMKdP_r(pC); % [n x n x p]
            end
            
            % Loop through the modes of interest
            dwdp = zeros(length(w),length(obj.paramVec));
            for ii = 1:length(w)
                for jj = 1:length(dMdP)
                    dwdp(ii,jj) = (1/(4*pi*w(ii)))*((phi(fDof,ii)'*(dKdP{jj}-...
                                    w(ii)^2*dMdP{jj}))*phi(fDof,ii));
                end
            end
            
            % Check if additional ouput arguments are requested
            if nargout > 1
                varargout{1} = dMdP; varargout{2} = dKdP;
            end
            
        end
              
        % Linear mass and stiffness gradients
        function[dMdP,dKdP] = GradientCalc_dMKdP_c(obj,pC)
            
            
            % Initialzie matrix
            dMdP = cell(length(obj.paramVec),1);
            dKdP = cell(length(obj.paramVec),1);
            
            % Loop through the parameters
            for ii = 1:length(obj.paramVec)
                
                    % Perturb the value and update model
                    obj.paramVec{ii}.SetVal(obj.p0(ii)*pC(ii) + 1i*obj.fdOpts.step);
                    
                    % Check if either full influence or local influence
                    if isempty(obj.paramVec{ii}.eleId)
                        obj.osFern.Update(true);
                        % Assemble matrices and take complex part
                        [Mnew] = obj.osFern.GetMassMatrix();
                        [Knew] = obj.osFern.GetStiffnessMatrix();
                    else
                        obj.osFern.Update(true,obj.paramVec{ii}.eleId);
                        
                        % Assemble matrices and take complex part
                        [~,~,~,Mmat] = obj.osFern.GetElement_MassMatrix(...
                                       obj.paramVec{ii}.eleId);
                        Mnew = Mmat(obj.osFern.freeDof,...
                                    obj.osFern.freeDof);
                                
                        % Assemble matrices and take complex part
                        [~,~,~,Kmat]  = obj.osFern.GetElement_StiffnessMatrix(...
                                obj.paramVec{ii}.eleId);
                        Knew = Kmat(obj.osFern.freeDof,...
                                    obj.osFern.freeDof);        
                    end
                    
                    % Take imaginary part
                    dMdP{ii} = (imag(Mnew)/obj.fdOpts.step)*obj.p0(ii);
                    dKdP{ii} = (imag(Knew)/obj.fdOpts.step)*obj.p0(ii);
                    
                    % Return values to nominal
                    obj.paramVec{ii}.SetVal(obj.p0(ii)*pC(ii));
            end
            
            % Update the model back to normal
            obj.osFern.Update(true);
            
        end
        function[dMdP,dKdP] = GradientCalc_dMKdP_r(obj,pC)

            
            % Initialzie matrix
            dKdP = cell(length(obj.paramVec),1);
            dMdP = cell(length(obj.paramVec),1);
            
            % Loop through the parameters
            for ii = 1:length(obj.paramVec)
                % Perturb the value and update model
                obj.paramVec{ii}.SetVal(obj.p0(ii)*pC(ii) + obj.fdOpts.step);
                obj.osFern.Update(true,obj.paramVec{ii}.eleId);

                % Check if either full influence or part influence
                if isempty(obj.paramVec{ii}.eleId)
                    [Kmat]  = obj.osFern.GetStiffnessMatrix();
                else
                    % Assemble matrices and take complex part
                    [~,~,~,Kmat]  = obj.osFern.GetElement_StiffnessMatrix(...
                            obj.paramVec{ii}.eleId);
                        
                    % Assemble matrices and take complex part
                    [~,~,~,Mmat]  = obj.osFern.GetElement_MassMatrix(...
                            obj.paramVec{ii}.eleId);
                end

                % Find indices of updated components
                [row,col,val] = find(Kmat);

                % Update new matrix 
                Knew = obj.K0;
                for kk = 1:length(val)
                    Knew(row(kk),col(kk)) = val(kk);
                end

                % Forward finite difference
                dKdP{ii} = (Knew-obj.K0)/obj.fdOpts.step;

                % Return values to nominal
                obj.paramVec{ii}.SetVal(obj.p0(ii)*pC(ii));
                obj.osFern.Update(true);
            end
            
            % Update the model back to normal
            obj.osFern.Update(true);
        end
        
        % Plot functions
        function [stop] = OutFun(obj,xIn,optimValues,state)
            
            stop = false; 
            
            % Initialize the figure
            switch state
                 case 'init'
                     
                    % Call initialization function
                    [obj,sp_data,sp_axes] = obj.InitializePlot();
                    
                case 'iter'
                     % Add to iteration count
                    if optimValues.iteration == 0
                        obj.iter = 1;
                    else
                        obj.iter = obj.iter + 1;
                    end
                    
                    % Store values
                    obj.J(obj.iter) = optimValues.fval;
                    obj.dJ(:,obj.iter) = optimValues.gradient;
                    obj.P(:,obj.iter) = xIn;
                    
                    % Track stepsize
                    if isempty(optimValues.stepsize)
                        obj.alpha(obj.iter) = 0;
                    else
                        obj.alpha(obj.iter) = optimValues.stepsize;
                    end
                    
                    % Grab figure and axes
                    sp_data=getappdata(gcf,'Subplot_data');
                    sp_axes=getappdata(gcf,'Subplot_axes');
                    
                    % Add an annotation of step size
%                     annotation('textbox',[.85 .5 .1 .2],'String',...
%                        {['Current Step Size : ', num2str(obj.alpha(obj.iter))],...
%                        'Itartion # : outside the axes'},'EdgeColor','k')
                   
                case 'done'
                    sp_data=getappdata(gcf,'Subplot_data');
                    sp_axes=getappdata(gcf,'Subplot_axes');
                case 'interrupt'
                    sp_data=getappdata(gcf,'Subplot_data');
                    sp_axes=getappdata(gcf,'Subplot_axes');
            end
            
            % Subplot (1,1)
            switch obj.plotOpts.sp11
                case 'parameter'
                    [obj,sp_axes{1},sp_data{1}] = ParameterPlot(...
                                         obj,sp_axes{1},sp_data{1},...
                                         xIn,optimValues,state);
                case 'cost'
                    [obj,sp_axes{1},sp_data{1}] = CostPlot(...
                                         obj,sp_axes{1},sp_data{1},...
                                         xIn,optimValues,state);
                case 'gradient'
                    [obj,sp_axes{1},sp_data{1}] = GradientPlot(...
                                         obj,sp_axes{1},sp_data{1},...
                                         xIn,optimValues,state);
                case 'fe'
                    [obj,sp_axes{1},sp_data{1}] = FePlot(...
                                         obj,sp_axes{1},sp_data{1},...
                                         xIn,optimValues,state);
                case 'mac'
                    [obj,sp_axes{1},sp_data{1}] = MacPlot(...
                                         obj,sp_axes{1},sp_data{1},...
                                         xIn,optimValues,state);
                case 'freq'
                    [obj,sp_axes{1},sp_data{1}] = FreqPlot(...
                                         obj,sp_axes{1},sp_data{1},...
                                         xIn,optimValues,state);
            end
            
            % Initialie subplot (1,2)
            switch obj.plotOpts.sp12
                case 'parameter'
                    [obj,sp_axes{2},sp_data{2}] = ParameterPlot(...
                                        obj,sp_axes{2},sp_data{2},...
                                          xIn,optimValues,state);
                case 'cost'
                    [obj,sp_axes{2},sp_data{2}] = CostPlot(...
                                        obj,sp_axes{2},sp_data{2},...
                                          xIn,optimValues,state);
                case 'gradient'
                    [obj,sp_axes{2},sp_data{2}] = GradientPlot(...
                                        obj,sp_axes{2},sp_data{2},...
                                          xIn,optimValues,state);
                case 'fe'
                    [obj,sp_axes{2},sp_data{2}] = FePlot(...
                                        obj,sp_axes{2},sp_data{2},...
                                          xIn,optimValues,state);
                case 'mac'
                    [obj,sp_axes{2},sp_data{2}] = MacPlot(...
                                        obj,sp_axes{2},sp_data{2},...
                                          xIn,optimValues,state);
                case 'freq'
                    [obj,sp_axes{2},sp_data{2}] = FreqPlot(...
                                        obj,sp_axes{2},sp_data{2},...
                                         xIn,optimValues,state);
                otherwise
            end
            
            % Initialie subplot (2,1)
            switch obj.plotOpts.sp21
                case 'parameter'
                    [obj,sp_axes{3},sp_data{3}] = ParameterPlot(...
                                        obj,sp_axes{3},sp_data{3},...
                                          xIn,optimValues,state);
                case 'cost'
                    [obj,sp_axes{3},sp_data{3}] = CostPlot(...
                                        obj,sp_axes{3},sp_data{3},...
                                          xIn,optimValues,state);
                case 'gradient'
                    [obj,sp_axes{3},sp_data{3}] = GradientPlot(...
                                        obj,sp_axes{3},sp_data{3},...
                                          xIn,optimValues,state);
                case 'fe'
                    [obj,sp_axes{3},sp_data{3}] = FePlot(...
                                        obj,sp_axes{3},sp_data{3},...
                                          xIn,optimValues,state);
                case 'mac'
                    [obj,sp_axes{3},sp_data{3}] = MacPlot(...
                                        obj,sp_axes{3},sp_data{3},...
                                          xIn,optimValues,state);
                case 'freq'
                    [obj,sp_axes{3},sp_data{3}] = FreqPlot(...
                                        obj,sp_axes{3},sp_data{3},...
                                        xIn,optimValues,state);
                otherwise
            end
            
            % Initialie subplot (2,2)
            switch obj.plotOpts.sp22
                case 'parameter'
                    [obj,sp_axes{4},sp_data{4}] = ParameterPlot(...
                                        obj,sp_axes{4},sp_data{4},...
                                          xIn,optimValues,state);
                case 'cost'
                    [obj,sp_axes{4},sp_data{4}] = CostPlot(...
                                        obj,sp_axes{4},sp_data{4},...
                                          xIn,optimValues,state);
                case 'gradient'
                    [obj,sp_axes{4},sp_data{4}] = GradientPlot(...
                                      obj,sp_axes{4},sp_data{4},...
                                          xIn,optimValues,state);
                case 'fe'
                    [obj,sp_axes{4},sp_data{4}] = FePlot(...
                                        obj,sp_axes{4},sp_data{4},...
                                          xIn,optimValues,state);
                case 'mac'
                    [obj,sp_axes{4},sp_data{4}] = MacPlot(...
                                        obj,sp_axes{4},sp_data{4},...
                                          xIn,optimValues,state);
                case 'freq'
                    [obj,sp_axes{4},sp_data{4}] = FreqPlot(obj,sp_axes{4},...
                                            sp_data{4},...
                                            xIn,optimValues,state);
                otherwise
            end
            
            % Set the data and axes
            setappdata(obj.fHandle,'Subplot_data',sp_axes)
            setappdata(obj.fHandle,'Subplot_data',sp_data)
            drawnow;
        end
        function [obj,sp_data,sp_axes] = InitializePlot(obj)
            % This function initializes the modeling updating gui interface
            %
            
            
            % Initialize Figure
            obj.fHandle = figure('Visible','on','Renderer','zbuffer','NumberTitle','off', ...
                'Name','Linear Model Updating in OsFern');
            set(obj.fHandle,'uni','nor','position',[0.05 0.1 0.9 0.8],'Color',[1 1 1],'toolbar','figure');
            
            % Subplot (1,1)
            sp_axes{1} = subplot(2,2,1);
            set(sp_axes{1},'Units','normalized','Position',[0.10 0.55 0.35 0.35],'Box','on');
            set(sp_axes{1},'XScale','linear','YScale','linear','FontSize',11,'XGrid','on','YGrid','on');
            
            % Subplot (1,2)
            sp_axes{2} = subplot(2,2,2);
            set(sp_axes{2},'Units','normalized','Position',[0.55 0.55 0.35 0.35],'Box','on');
            set(sp_axes{2},'XScale','linear','YScale','linear','FontSize',11,'XGrid','on','YGrid','on');
            
            % Subplot (2,1)
            sp_axes{3} = subplot(2,2,3);
            set(sp_axes{3},'Units','normalized','Position',[0.10 0.10 0.35 0.35],'Box','on');
            set(sp_axes{3},'XScale','linear','YScale','linear','FontSize',11,'XGrid','on','YGrid','on');
            
            % Subplot (2,2)
            sp_axes{4} = subplot(2,2,4);
            set(sp_axes{4},'Units','normalized','Position',[0.55 0.10 0.35 0.35]);
            set(sp_axes{4},'XScale','linear','YScale','linear','FontSize',11,'XGrid','on','YGrid','on');
            
            % Set application data
            setappdata(obj.fHandle,'Subplot_axes',sp_axes)
            drawnow
            sp_data = cell(4,1);
            
        end
        function [obj,aHandle,dHandle] = ParameterPlot(obj,aHandle,dHandle,xIn,optimValues,state)
            
            % Initialize or udpate plot
            switch state
                case 'init'
                    dHandle = plot(nan(1,100),...
                        nan(length(obj.paramVec),100),...
                        'LineStyle','-','LineWidth',2,...
                        'Marker','o','MarkerSize',18,...
                        'Parent',aHandle);
                    set(aHandle,'XScale','linear','YScale','linear',...
                        'FontSize',11,'XGrid','on','YGrid','on');
                    set(get(aHandle,'XLabel'),'string',...
                        'Iteration #','FontSize',16);
                    set(get(aHandle,'YLabel'),'string',...
                        'Paramter Values','FontSize',16);
                    set(get(aHandle,'Title'),'string',...
                        'Paramter Plot','FontSize',16);
                    paramLegend = cell(length(obj.paramVec),1);
                    for ii=1:length(obj.paramVec)
                        try
                            paramLegend{ii}=obj.paramVec(ii).name;
                        catch
                            paramLegend{ii}=obj.paramVec{ii}.name;
                        end
                    end
                    legend(aHandle,paramLegend);
                case 'iter'
                    % Update plot 1 : parameter plot
                    for k = 1:length(obj.paramVec)
                        set(dHandle(k),'XData',1:obj.iter,'YData',...
                            obj.P(k,1:obj.iter))
                    end
                otherwise
                    
            end
        end
        function [obj,aHandle,dHandle] = CostPlot(obj,aHandle,dHandle,xIn,optimValues,state)
            % Initialize or udpate plot
            switch state
                case 'init'
                    dHandle = plot(0,optimValues.fval,'LineStyle','-',...
                        'LineWidth',2,'Marker','o','MarkerSize',18,...
                        'Parent',aHandle);
                    set(aHandle,'XScale','linear','YScale','linear',...
                        'FontSize',11,'XGrid','on','YGrid','on');
                    set(get(aHandle,'XLabel'),'string',...
                        'Iteration #','FontSize',16);
                    set(get(aHandle,'YLabel'),'string',...
                        'Cost Function Value','FontSize',16);
                    set(get(aHandle,'Title'),'string',...
                        'Cost Function Plot','FontSize',16);
                case 'iter'
                    set(dHandle, 'XData',1:obj.iter, 'YData',obj.J(1:obj.iter)/obj.J(1));
                otherwise
            end
            
        end
        function [obj,aHandle,dHandle] = GradientPlot(obj,aHandle,dHandle,xIn,optimValues,state)
            
            % Initialize or udpate plot
            switch state
                case 'init'
                    dHandle = bar(rand(length(obj.paramVec),1),...
                                  'Parent',aHandle);
                    set(aHandle,'XScale','linear',...
                        'YScale','linear','FontSize',...
                        11,'XGrid','on','YGrid','on');

                    set(get(aHandle,'XLabel'),'string',...
                            'Parameter','FontSize',16);
                    set(get(aHandle,'YLabel'),'string',...
                            'Gradients','FontSize',16);
                    set(get(aHandle,'Title'),'string',...
                        'Gradient Plot','FontSize',16);
                    paramLegend = cell(length(obj.paramVec),1);
                    for ii=1:length(obj.paramVec)
                        try
                            paramLegend{ii}=obj.paramVec(ii).name;
                        catch
                            paramLegend{ii}=obj.paramVec{ii}.name;
                        end
                    end
                    xticklabels(aHandle,paramLegend)
                    xticks(aHandle,1:length(paramLegend))
                    xtickangle(aHandle,45)
                case 'iter'
                    set(dHandle,'XData',1:length(obj.paramVec),...
                        'YData',(optimValues.gradient));
                otherwise
            end
            

                        
                        
        end
        function [obj,aHandle,dHandle] = FePlot(obj,aHandle,dHandle,xIn,optimValues,state)
            % Initialize or udpate plot
            switch state
                case 'init'
                    dHandle = bar([rand(obj.target.nSols,1),...
                                   rand(obj.target.nSols,1)],...
                                  'Parent',aHandle);
                    set(get(aHandle,'XLabel'),'string',...
                            'Solution #','FontSize',16);
                    set(get(aHandle,'YLabel'),'string',...
                            'Residual','FontSize',16);
                    legend(aHandle,{'State Error','Frequency Error'});
                case 'iter'
                    dW = (obj.target.w - obj.NNM{obj.iter}.w)./obj.target.w;
                    dSn = zeros(1,size(obj.NNM{obj.iter}.dS,2));
                    for ii = 1:size(obj.NNM{obj.iter}.dS,1)
                        dSn(1,ii) = norm(obj.NNM{obj.iter}.dS(ii,:));
                    end
                    set(dHandle(1),'XData',1:obj.target.nSols,...
                        'YData',abs([dSn']));
                    set(dHandle(2),'XData',1:obj.target.nSols,...
                        'YData',abs([dW']));
                otherwise
            end
        end
        function [obj,aHandle,dHandle] = MacPlot(obj,aHandle,dHandle,xIn,optimValues,state)
            % Initialize or udpate plot
            switch state
                case 'init'
                    hold(aHandle,'on')
                    x2plot = zeros(obj.target.nSols,length(obj.target.dof2plot));
                    for jj = 1:obj.target.nSols
                        x2plot(jj,:) = obj.target.x{jj}(obj.target.dof2plot);
                    end
                    plot(obj.target.w/2/pi,x2plot,...
                                    'Color','k','LineStyle','-',...
                                    'LineWidth',2,'Marker','s',...
                                    'MarkerSize',18,'Parent',aHandle);
                    set(get(aHandle,'YLabel'),'string',...
                        'Amplitude(-)','FontSize',16);
                    set(get(aHandle,'XLabel'),'string',...
                        'Frequency (Hz)','FontSize',16);
                    set(get(aHandle,'Title'),'string',...
                        'Displacement vs. Frequency Plot','FontSize',16);
                    xMat = cell2mat(obj.initial.nnm.xInterp');
                    
                    % Transpose for plotting if it is only a single point
                    if size(xMat,1) == 1 || size(xMat,2) == 1
                        x2plot = xMat;
                    else
                        x2plot = xMat(obj.target.dof2plot,:);
                    end
                    
                    plot(obj.initial.nnm.wInterp/2/pi,x2plot,...
                        '-rs','MarkerSize',15,'Parent',aHandle)
                case 'iter'
%                     x2plot = zeros(obj.target.nSols,length(obj.target.dof2plot));
%                     
%                     if isa(obj,'WiscModelUpdateFem')
%                         for jj = 1:obj.target.nSols
%                             x2plot(jj,:) = obj.NNM{obj.iter}.xInterp{jj}(obj.target.dof2plot,1);
%                         end
%                     else
%                         for jj = 1:obj.target.nSols
%                             x2plot(jj,:) = obj.NNM{obj.iter}.x{jj}(obj.target.dof2plot,1);
%                         end
%                     end
                    
                    % Grab displacement
                    xMat = cell2mat(obj.NNM{obj.iter}.xInterp');
                    
                    % Transpose for plotting if it is only a single point
                    if size(xMat,1) == 1 || size(xMat,2) == 1
                        x2plot = xMat;
                    else
                        x2plot = xMat(obj.target.dof2plot,:);
                    end
                    
                    % Plot the frequency vs displacement
                    plot(obj.NNM{obj.iter}.wInterp/2/pi,x2plot,...
                        '-rs','MarkerSize',15,'Parent',aHandle)
                otherwise
            end
        end
        function [obj,aHandle,dHandle] = FreqPlot(obj,aHandle,dHandle,xIn,optimValues,state)
            
            % Initialize or udpate plot
            switch state
                case 'init'
                    dHandle = bar(rand(length(obj.paramVec),1),...
                                  'Parent',aHandle);
                    set(aHandle,'XScale','linear',...
                        'YScale','linear','FontSize',...
                        11,'XGrid','on','YGrid','on');
                    set(get(aHandle,'XLabel'),'string',...
                            'Mode Index','FontSize',16);
                    set(get(aHandle,'YLabel'),'string',...
                            'Frequency Error (%)','FontSize',16);
                    set(get(aHandle,'Title'),'string',...
                        'Frequency Error Plot','FontSize',16);
                    xticklabels(aHandle,num2cell(obj.target.modeIndices))
                    xticks(aHandle,1:length(obj.target.modeIndices))
                    xtickangle(aHandle,45)
                case 'iter'
                    set(dHandle,'XData',1:length(obj.target.modeIndices),...
                        'YData',100*(obj.fnError(:,obj.iter)));
                otherwise
            end
             
        end
    end
    
end

