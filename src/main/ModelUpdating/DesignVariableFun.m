classdef DesignVariableFun < DesignVariable & handle
    % This class is used to model design variables as function handles
    % They are updated across the entire model. They can be used simply as
    % values for a parameter as normal. 
    
    properties
%         val;
%         name; 
%         eleId;
        fun; % matlab function value that must return scalar value
             % used to parameterize fields of nodes
        paramVal; % This is the scalar value used to parameterize the 
                  % function
    end
    
    methods
        function obj = DesignVariableFun(paramVal,name,funHandle)
            obj = obj@DesignVariable(paramVal,name);
            
            % Store the parameter value, fun handle and actual value
            obj.paramVal = paramVal;
            obj.fun = funHandle;
            obj.val = obj.fun(paramVal);
            
            
        end
        function obj = set(obj,valIn)
            obj.paramVal = valIn;
            obj.val = obj.fun(valIn);
        end
        function valOut = GetVal(obj)
            valOut = obj.val;
        end
        function valOut = GetParamVal(obj)
            valOut = obj.paramVal;
        end
        function obj = SetName(obj,name)
            obj.name = name;
        end
        function obj = SetVal(obj,valIn)
            obj.paramVal = valIn;
            obj.val = obj.fun(valIn);
        end
        % overloaded operations
        function valOut = plus(obj1,obj2)
            a = double(obj1);
            b = double(obj2);
            valOut = a + b;
        end
        function valOut = minus(obj1,obj2)
            a = double(obj1);
            b = double(obj2);
            valOut = a - b;
        end
        function valOut = times(obj1,obj2)
            a = double(obj1);
            b = double(obj2);
            valOut = a*b;
        end
        function valOut = mtimes(obj1,obj2)
            a = double(obj1);
            b = double(obj2);
            valOut = a*b;
        end
        function valOut = rdivide(obj1,obj2)
            a = double(obj1);
            b = double(obj2);
            valOut = a./b;
        end
        function valOut = mrdivide(obj1,obj2)
            a = double(obj1);
            b = double(obj2);
            valOut = a/b;
        end
        function valOut = power(obj1,obj2)
            a = double(obj1);
            b = double(obj2);
            valOut = a.^b;
        end
        function valOut = mpower(obj1,obj2)
            a = double(obj1);
            b = double(obj2);
            valOut = a.^b;
        end
        function valOut = double(obj)
            obj.val = obj.fun(obj.paramVal);
            valOut = obj.val;
        end
        function valOut = sqrt(obj)
            obj.val = obj.fun(obj.paramVal);
            valOut = sqrt(obj.val);
        end
        function valOut = imag(obj)
            valOut = imag(obj.val);
        end
        function valOut = real(obj)
            valOut = real(obj.val);
        end
    end
    
end

