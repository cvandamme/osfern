

classdef SubspaceDynamic < ReducedStep
    
    properties
        
        response;
        t;
        
    end
    
    properties(Constant)
        
        tol = 1E-3;
        
    end
    
    methods
        
        function obj = SubspaceDynamic(name, matFem, initialDisplacement, initialVelocity)
            
            obj = obj@ReducedStep(name, matFem);
            obj.finalDisplacement = zeros(obj.matFem.nDof, 1);
            obj.finalVelocity = zeros(obj.matFem.nDof, 1);
            
            if nargin > 2
                obj.initialDisplacement = initialDisplacement;
            end
            
            if nargin > 3
                obj.initialVelocity = initialVelocity;
            end
            
            assemOPTs.assemble = 'SERIAL';
            assemOPTs.mass = true;
            assemOPTs.stiff = true;
%             obj.assembler = NLAssem(obj.matFem, assemOPTs);
            
        end
        
        function Knm = GetNLStiffnessMatrix(obj, displacement)
            % Assemble nonlinear stiffness matrix at the supplied
            % displacement level.
            
            % Update full-space tangent force matrix
            Kn = obj.matFem.GetTangentStiffness(obj.basis*displacement);
            
            % Convert to subspace
            Knm = obj.basis(obj.matFem.freeDof, :)'*Kn*obj.basis(obj.matFem.freeDof, :);
            
        end
%         
%         function obj = assemble(obj)
%             
%             obj.assembler.assemble();
%             obj.assembler.assembleNL(obj.initialDisplacement);
%             
%             
%         end
        
        
    end
    
end