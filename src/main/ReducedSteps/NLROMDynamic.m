

classdef NLROMDynamic < ReducedStep
    
    properties
        
        response;
        t;
        
        K1m;
        K2m;  % Tangent stiffness matrix; d(fnl)/dx = Kt
        K1h;  % Expanded forms for tensor integration
        K2h;
        
    end
    
    properties(Constant)
        
        % Integrator settings
        tol = 1E-3;
        
    end
    
    methods
        
        function obj = NLROMDynamic(name, matFem, initialDisplacement, initialVelocity)
            
            obj = obj@ReducedStep(name, matFem);
            obj.finalDisplacement = zeros(obj.matFem.nDof, 1);
            obj.finalVelocity = zeros(obj.matFem.nDof, 1);
            
            if nargin > 2
                obj.initialDisplacement = initialDisplacement;
            end
            
            if nargin > 3
                obj.initialVelocity = initialVelocity;
            end
            
            assemOPTs.assemble = 'SERIAL';
            assemOPTs.mass = true;
            assemOPTs.stiff = true;
            
        end
        
        function Kn = GetNLStiffnessMatrix(obj, displacement)
            
            % Apply displacements to nonlinear coefficients
            m = size(obj.basis, 2);
            dmat = repmat(eye(m),1,m);
            dind = dmat == 1;
            v1 = ones(m,1);
            dd = v1*displacement';
            dmat(dind) = dd;
            
            Kn = dmat*obj.K1h + dmat*obj.K2h*dmat';
            
            
        end
        
        
        function AssembleNLROM(obj)
            
            
            % Allocate memory
            nEL = length(obj.matFem.element);
            m = size(obj.basis, 2);
            
            % allocate memory for set of N.L. modal stiffness matrices
            obj.K2m = zeros(m, m, m, m);
            obj.K1m = zeros(m, m, m);
            
            % double loop to form mxm set of nonlinear modal stiffness matrices, Km
            for i = 1:m
                for j = 1:m
                    
                    % loop to assemble matrices, K1m and K2m for modes (i,j)
                    modestr = [num2str(i),':',num2str(j)];
                    disp(['Assembling Km for modes ',modestr]);
                    
                    % Loop through element-by-element
                    for ii = 1:nEL
                        
                        % Obtain element-local deformation of each mode
                        globalDof = obj.matFem.element{ii}.dofVec;
                        xi = obj.basis(globalDof, i);
                        xj = obj.basis(globalDof, j);
                        
                        % Need to pull deformations from both modes.
                        
                        % Make some kind of return flag here if the element
                        % doesn't have a nonlinear stiffness to continue
                        % the loop to the next iteration
                        [K1, K2] = obj.matFem.element{ii}.BuildNLmodal(xi, xj);
                        
                        
                        % modalize the element nonlinear stiffness matrices and add them
                        % to the global matrices (for beam and plate elements only)
                        if i == j
                            obj.K1m(:,:,i) = obj.K1m(:,:,i)+ ...
                                obj.basis(globalDof,:)'*K1*obj.basis(globalDof,:);
                        end
                        obj.K2m(:,:,i,j) = obj.K2m(:,:,i,j)+ ...
                            obj.basis(globalDof,:)'*K2*obj.basis(globalDof,:);
                        
                        
                    end
                    
                end
            end
            
            
            % convert K1m and K2m to 2D block format for computation speed
            obj.K1h = zeros(m^2, m);
            obj.K2h = zeros(m^2);
            nm1 = m - 1;
            for r = 1:m
                irow = m*r - nm1:m*r;
                obj.K1h(irow, :) = obj.K1m(:, :, r);
                for s = 1:m
                    icol = m*s - nm1:m*s;
                    obj.K2h(irow, icol) = obj.K2m(:, :, r, s);
                end
            end
            
        end
        
        
    end
    
end