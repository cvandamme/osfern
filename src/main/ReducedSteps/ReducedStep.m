classdef ReducedStep < DirectStep
    % Class for reduced-basis analysis procedures. 
    
    properties
        response;
        % Integrator settings
        integrator = struct('quasiRate',1,'tol',1e-6,'adaptiveStepSize',false,...
                            'maxIters',100,'optIters',3,'stepMin',1e-5,...
                            'stepMax',0.01,'stepScale',3);
        romObj;
        t
        damping
    end
    
    methods
        
        % Constructor
        function obj = ReducedStep(name, romObj)
            
            obj = obj@DirectStep(name, romObj.osFern);
            obj.romObj = romObj;
            % Zero out initial displacement by default
            obj.initialDisplacement = zeros(obj.osFern.nDof, 1);
            
        end
        
        % Static Solution
        function obj = Static(obj, nStep)
            % This solves for the static solution of a ROM
            
            % Obtain stiffness matrix and force vector
            Kbb = obj.romObj.Kbb;
            Fphys = obj.AssembleForce(0);
            basisFree = obj.romObj.basis(obj.romObj.freeDof,:);
            Fmodal = basisFree'*Fphys;
            
            % Solve
            steps = (1/nStep):(1/nStep):1; %linspace(0, 1, nStep);
            
            % compute the linear static response at the first load step
            qold = Kbb\Fmodal*steps(1);
            
            % loop to compute solution at each load step
            icount = 0;
            obj.solution.solution = zeros(obj.osFern.nDof, length(steps));
            x_nli = obj.romObj.basis*qold;
            for ii = 2:length(steps)
                derr = 1; ferr = 1;
                disp([' load step',num2str(ii)]);
                
                % Update the load (follower load if pressure)
%                 Fphys = obj.AssembleForce([],x_nli);
%                 Fmodal = basisFree'*Fphys;
                
                % iterate the nonlinear solution
                nIter = 1; dtol = 1E-6; ftol = 1E-6;
                beta = 0.6;
                while (derr > dtol) || (ferr > ftol)
                    
                    % compute the nonlinear stiffness matrices at the current state
                    [Fnl,Kn] = obj.romObj.Get_NlForce(qold);
                    
                    % form and factor the tangent stiffness matrix
                    Ktan = Kbb + Kn;
                    
                    % compute the effective load
                    Fnew = Kbb*qold + Fnl;
                    
                    % compute the residual force
                    Fres = steps(ii)*Fmodal - Fnew;
                    
                    % compute the incremental nonlinear static response
                    del_q = Ktan\Fres;
                    
%                     q_nli = Kbb\Fres;
                    if nIter == 1; del_q0 = del_q; end
                    
                    % update the displacement vector
                    q_nli = qold + beta*del_q;

                    % apply underrelaxation
%                     q_nli = beta*del_q+(1-beta)*qold;
    
                    % compute the displacement error for all DOF
                    derr = norm(del_q)/norm(del_q0);
                    
                    % compute the force error
                    ferr = norm(Fres)/norm(Fmodal);
                    
                    % print the error values
                    if obj.printOpts.logic
                        fprintf('   displ error = %8.3e, force error = %8.3e\n',[derr ferr]);
                    end
                    
                    % test for non-convergence
                    if nIter > obj.integrator.maxIters
                        disp(['Fatal Error - Reached Maximum Number of Iterations : ', num2str(obj.integrator.maxIters),' without convergence'])
                        disp([' at time step ',num2str(ii)])
                        disp(['Final displacement will be a zero vector'])
                        obj.finalDisplacement = zeros(obj.osFern.nDof,1);
                        return
                    end
                    
                    qold = q_nli; x_nli = obj.romObj.basis*q_nli;
                    nIter = nIter+1; icount = icount+1;
                end
                
                % save the converged displacement vector
                obj.solution.solution(obj.osFern.freeDof,ii) = basisFree*q_nli;
            end

            obj.finalDisplacement = zeros(obj.osFern.nDof,1);
            obj.finalDisplacement(obj.osFern.freeDof) = basisFree*q_nli;
            
            % Obtain response DOF
            responseDof = [];
            for i = 1:length(obj.monitor)
                responseDof = unique([responseDof, obj.monitor(i).dof]);
            end
            
            obj.response = obj.finalDisplacement(responseDof);
            
        end
        
        % Dynamic integration
        function obj = Dynamic(obj, T, dt)
            % This is still almost directly copied from the old Bobtran
            % "nlstat" function.
            % A big thing to note is that I'm writing everything using a
            % fully unconstrained solution vector. This is for ease of
            % indexing, especially with the NL assembly. It has to come at
            % some sort of performance cost, but until we can quantify
            % exactly what that penalty is, I don't want to bother messing
            % with it. (Right now, the vast majority of the solution time
            % is spent in element assembly.)
            % TODO: Profile, rewrite, and add automatic timestepping.
            
            warning('Integration routine under developement and validation')
            
            % Shorthand for initial velocity and displacement
            if isempty(obj.initialDisplacement)
                x0 = zeros(obj.osFern.nDof, 1);
            else
                x0 = obj.initialDisplacement;
            end
            
            if isempty(obj.initialVelocity)
                v0 = zeros(obj.osFern.nDof, 1);
            else
                v0 = obj.initialVelocity;
            end
                       
            % Obtain response DOF
            responseDof = [];
            for i = 1:length(obj.monitor)
                responseDof = unique([responseDof, obj.monitor(i).dof]);
            end
            
            % Obtain time vector
            obj.t = 0:dt:T;
            dt2 = dt^2;
            nstep = length(obj.t);
            
            % compute the linear static response at the first load step
            obj.solution.displacement = zeros(obj.osFern.nDof, nstep);
            obj.solution.velocity = zeros(obj.osFern.nDof, nstep);
            obj.solution.acceleration = zeros(obj.osFern.nDof, nstep);
            
            obj.solution.displacement(:, 1) = x0;
            obj.solution.velocity(:, 1) = v0;
            
            % Form transformed stiffness and damping matrices
            nmodes = obj.romObj.m;
            Km = obj.romObj.Kbb;
            Mm = obj.romObj.Mbb;
            
            % Setup damping
            if isempty(obj.damping)
                Cm = zeros(size(Mm));
            elseif isstruct(obj.damping)
                Cm = obj.damping.alpha*Mm+obj.damping.beta*Km;
            elseif ismatrix(obj.damping)
                if size(obj.damping) == size(Mm)
                    Cm = obj.damping; 
                else
                    Cm = diag(2*obj.damping.*diag(Km).^(1/2));
                end
            end
            % compute constant terms in Keff
            Keff0 = 4*Mm/dt2 + 2*Cm/dt;
            fold = obj.romObj.basis(obj.osFern.freeDof, :)'*...
                   obj.AssembleForce(0);
            
            % initialize temporary vectors of modal displacements, dold,
            % velocities, vold, and accelerations, aold
            dold = obj.romObj.basis(obj.osFern.freeDof, :)\x0(obj.osFern.freeDof);
            vold = obj.romObj.basis(obj.osFern.freeDof, :)\v0(obj.osFern.freeDof);
            
            % Assemble tangent stiffness
            [fNl,Kn] = obj.romObj.Get_NlForce(dold);
            Ktold = Km + Kn;
            
            % compute initial modal accelerations
            aold = Mm\(fold - Cm*vold - Ktold*dold - fNl);
            
            % allocate matrix to save modal displacements
            q = [dold, zeros(nmodes, nstep - 1)];
            qd = [vold, zeros(nmodes, nstep - 1)];
            dnew = dold;
            
           % perform the integration loop for (npts - 1) time points
            for ii = 2:nstep
                err = 1;  dtemp = dold;
                
                % Assemble new force vector
                Fnew = obj.romObj.basis(obj.osFern.freeDof, :)'*...
                       obj.AssembleForce(obj.t(ii));  
                
                % compute the effective load
                Reff = Fnew + fNl + Mm*4*(dold + dt*vold + (dt2/4)*aold)/dt2 +...
                        Cm*(2*dold/dt + vold);
                
                % loop to converge on dnew
                niter = 0;
                while err > obj.integrator.tol
                    niter = niter + 1;
                    
                    % test for non-convergence
                    if niter > obj.integrator.maxIters
                        disp(['Fatal Error - Reached Maximum Number of Iterations : ', num2str(obj.integrator.maxIters),' without convergence'])
                        disp([' at time step ',num2str(ii)])
                        return
                    end
                    
                    % build the nonlinear modal stiffness terms
                    if mod(niter,obj.integrator.quasiRate) == 0 || niter == 0
                        [fNl,Kn] = obj.romObj.Get_NlForce(dold);
                        Ktnew = Km + Kn;
                        Keff = Keff0 + Ktnew;
                    end
                    
                    % compute new displacements
                    dnew = Keff\Reff;
                    
                    % compute the error between dnew and dold
                    err = max(abs((dnew - dtemp)./dnew));
                    
                    dtemp = dnew;
                    
                end 
                
                % Output message
                if obj.printOpts.logic && mod(ii,obj.printOpts.frameRate) == 0
                    fprintf('Increment %i of %i complete in %i iterations\n', ii, nstep, niter);
                end
                
                % compute new values for velocity and acceleration
                vnew = -vold+2*(dnew-dold)/dt;
                anew = 4*(dnew-dold)/dt2-4*vold/dt-aold;
                
                % udpate 
                dold = dnew; vold = vnew; aold = anew;
                
                % save the current modal displacement vector
                q(:, ii) = dnew;
                qd(:, ii) = vnew;
                
                obj.solution.displacement(:, ii) = obj.romObj.basis*dnew;
                obj.solution.velocity(:, ii) = obj.romObj.basis*vnew;
                obj.solution.acceleration(:, ii) = obj.romObj.basis*anew;
                                
            end
            
            obj.finalDisplacement = obj.romObj.basis*q(:, end);
            obj.finalVelocity = obj.romObj.basis*qd(:, end);
            obj.response = obj.romObj.basis(responseDof, :)*q;
            
        end
        
        % Generate damping
        function obj = SetDamping(obj, alpha, beta)
            % Currently only proportional damping is used
            obj.damping.alpha = alpha;
            obj.damping.beta = beta;
        end
        
        % Just here currently as a  place holder
        function obj = Solve(obj)
        end
        
        % Display the object
        function disp(obj)
            
            fprintf('%s Object\n', class(obj));
            
            fprintf('\tName: %s\n', obj.name);
            
            disp(obj.osFern);

            disp(obj.romObj);
            
        end
    end

end
