classdef StressField < handle
    % This class is used to represent a stress field applied to an osFern
    % fe model. The field can be defined by the user by simply providing
    % stresses in each element or by using a thermal field to generate the
    % stress field. 
    % The stress field is currently not time dependent. 
    
    properties
        name     % name of the force
        nodes    % vector of node objects the field is applied to 
        elements % vector of elements the stress field is applied to
        val      % value of field at the node 
        amplitude = 1 % by default amplitude is one otherwise can assign 
        osFern   % osFern model object 
        sigma    % stress for each element [sx, sy, sz]
        force
        epsilon
        Kss
    end
    
    methods
        function obj = StressField(name,osFernObj,val)
            obj.name = name;
            obj.osFern = osFernObj;
            obj.val = val;
        end
        function AddElements(obj, eles)
            % Add elements to the stress field object. 
            %
            % 3 input options: 
            %       a) cell array of element objects
            %       b) single element object
            %       c) vector of element ids
            
            
            % 2 input options: nodes (cell array) or node ID (regular
            % array)
            if iscell(eles) && isa(eles{1},'Element') % Cell array of node objects
                for i = 1:length(eles)
                    obj.eles{end+1} = eles{i};
                end
            elseif isa(eles,'Node')
                    obj.eles{end+1} = eles;
            else
                for i = 1:length(eles)
                    obj.eles{end+1} = obj.osFern.eles{i};
                end
            end
                
                
        end
        function AddAmplitude(obj, amp)
            % Add ampliude objects to specfiied amplitude name,
            % creating it if it doesn't exist.
            
            if ~isa(amp,'Amplitude')
                error('Must use the amplitude object');
            else
                obj.amplitude = amp;
            end
        end
        function Temperatue2StressField(obj,thermalObj,varargin)
            
            
            % Check if displacement field is provided for the assembly
            if narargin > 2
               xi = varargin{1}; 
            else
               xi = zeros(obj.osFern.nDof);
            end
            
            % Take the thermal object and create the stress field
            % properties
            [f,s,e] = thermalObj.AssembleThermalField(xi);
           
            % Creaet sparse vector 
            obj.force = f; obj.sigma = s; obj.epsilon = e;
            
            % Assemble stress stiffness matrix
            obj.Kss = obj.osFern.GetStressStiffness(s,xi);
            
        end
        function [F,Se,Ee] = AssembleStiffness(obj,ti,varargin)
            
            % Initialzie variables (stress, strain and nodal loads)
            S = zeros(length(obj.osFern.element),3);
            E = zeros(length(obj.osFern.element),3);
            P = zeros(obj.osFern.nDof,1);
            
            % Grab ids of nodes within this field
            nVec = [obj.nodes{:}]; nids = [nVec.id];
            
            % Check if displacement is supplied
            if nargin > 2
                xIn = varargin{1};
            else
                xIn = zeros(obj.osFern.nDof,1);
            end
            
            % Check which elements to look for 
            
            % Loop through element-by-element
            for ii = 1:length(obj.osFern.element)
                nodeVec = ([obj.osFern.element{ii}.nodes{:}]);
                nodalTempVec = zeros(size(nodeVec));
                [~,ia,~] = intersect([nodeVec.id],nids);
               
                % Check if no intersection is present (i.e. element is not
                % within heated area)
                if isempty(ia)
                    Sele = zeros(1,3); Eele = zeros(1,3);
                    Pele = zeros(size([nodeVec.globalDof]))';
                else
                    nodalTempVec(ia) = obj.val;
                    xEle = xIn([nodeVec.globalDof]);
                    eleAveTemp = mean(nodalTempVec);
%                     obj.osFern.element{ii}.SetTemperature(eleAveTemp);
                    [Sele,Eele,Pele] = obj.osFern.element{ii}.ComputeThermalLoads(nodalTempVec',xEle);
                end
                
                S(ii,:) = Sele; E(ii,:) = Eele;
                P([nodeVec.globalDof]) = P([nodeVec.globalDof]) + Pele;
            end
            
            % If no time is provided then use unscaled force
            if isempty(ti) || ~isa(obj.amplitude,'Amplitude') 
                amp = 1;
            else
                amp = obj.amplitude(ti);
            end
            
            % Creaet sparse vector
            F = amp*P; Se = amp*S; Ee = amp*E;
        end
    end
    
end

