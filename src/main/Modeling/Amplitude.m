classdef Amplitude
    % This is an object that is used to model amplitudes throughout dynamic
    % integration schemes. 
    
    properties
        
        name
        time 
        amplitude
        type 
        
    end
    
    methods
        function obj = Amplitude(name,time,amplitude)
            obj.name = name;
            obj.time = time;
            obj.amplitude = amplitude;
        end
        function ampOut = subsref(obj,tInterp)
            % This subsindex function is used to interpolate the
            % amplitude object at the prescribed time
            % 
            % Inputs : 
            %       obj = AMplitude Object
            %       tInterp = time of interpolation
            
            % Interpolate the value
            ampOut = interp1(obj.time,obj.amplitude,tInterp.subs{1},'pchip');
            
            % Check that the interpolation worked 
            if isnan('ampOut')
                error('Interpolation caused out')
            end
        end
    end
    
    methods (Static)
       function [signal, time, inputVar] = GenRandAmp(psd, fs, T, No, seed)
            % [signal, time, inputVar] = randSignal(psd, fs, T, No, [seed])
            %
            % Generate a random signal corresponding to an arbitrary single-sided PSD
            % input.
            %
            % Inputs:
            %
            % psd:  Anonymous function of frequency that provides the SINGLE-SIDED PSD
            %       amplitude as a function of circular frequency [rad/s]. Should return
            %       a scalar value.
            % fs:   Scalar value; desired sample rate of the resulting signals.
            % T:    Scalar value; desired time interval of the resulting signals.
            % No:   [Optional] Scalar value; number of output signals desired. Defaults
            %       to 1.
            % seed: [Optional] Seed for the RNG. If not supplied, uses seed = now as
            %       the RNG seed.
            %
            % Outputs:
            %
            % For the outputs below; N = fs*T is the number of samples in the time
            % history.
            %
            % signal:   An [No x N] vector of the generated random signal.
            %
            % time:     A [1 x N] vector of the generated time history.
            %
            % inputVar: Theoretical variance of the input PSD, based on a trapezoidal
            %           integration of the PSD in the frequency domain.
            %
            
            %%%% Input processing %%%%
            % Verify 'psd' accepts a non-index input frequency and returns a scalar value:
            try
                psd(0.5);
            catch err
                warning('Ensure psd is a function of frequency');
                err.rethrow();
            end
            
            if length(fs) > 1; error('Input fs must be a scalar'); end
            if length(T) > 1; error('Input T must be a scalar'); end
            
            if nargin < 4 % If no number of outputs supplied
                No = 1;
            end
            
            if nargin < 5 % If no seed is supplied
                rng('shuffle'); % Shuffle the RNG based on current time
            else
                rng(seed); % Apply seed to RNG
            end
            
            % Determine time/frequency characteristics of signal; these must be
            % modified in order to use Hz or rad/s. (Currently setup for Hz).
            dOmega = 1/T;              % Frequency resolution
            omegaMax = fs;             % Maximum frequency
            % To use units of rad/s:
            % dOmega = pi/T;
            % omegaMax = 2*pi*fs;
            
            % Generate PSD vector at each frequency interval in the domain, and a
            % corresponding vetor of uniform random phase angles
            targetOmega = 0:dOmega:(omegaMax - dOmega);
            try
                targetPsd = psd(targetOmega); % Ideally this is vectorized
            catch err
                warning(['Input PSD is not vectorizable. This significantly impacts'...
                    , ' performance. Vectorize the PSD if possible.']);
                targetPsd = zeros(No, length(targetOmega));
                for i = 1:length(targetOmega);
                    targetPsd(i) = psd(targetOmega(i));
                end
            end
            
            psiVec = 2*pi*rand(No, length(targetPsd));
            
            % Set any NaN elements to 0
            mask = isnan(targetPsd);
            targetPsd(mask) = 0;
            
            % Perform FFT operation and generate signal
            S = bsxfun(@times, sqrt(2*targetPsd), exp(1i*psiVec));
            s = fft(S, [], 2);
            signal = sqrt(dOmega)*real(s);
            
            % Optional outputs
            if nargout > 1
                dT = 1/fs;
                time = 0:(dT):(T - dT); % Vector of time points
            end
            if nargout > 2
                inputVar = trapz(targetOmega, targetPsd);
            end
            
        end 
    end
    
end

