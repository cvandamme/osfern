classdef Pressure < handle
    % This class is used to represent a pressure field applied to a FE 
    % model. It currently only works for shell elements and maintains a 
    % constant value over all specified elements in the class. The pressure
    % can be time varying by supplying an amplitude object. If looking for
    % nonuniform pressures over a surface then one must use many of these
    % pressure objects.
    
    properties
        name     % name of the force
        fe       % fe model object 
        pressure % pressure value
        eles     % vector of element objects the pressure is applied to 
        val      % val of the consistent forces based on pressure value   
        amplitude = 1 % by default amplitude is one otherwise can assign 
    end
    
    methods
        function obj = Pressure(name,feObj,val)
            obj.name = name;
            obj.fe = feObj;
            obj.pressure = val;obj.val = val;
        end
        function obj = AddFieldVector(obj, fVec)
            % Add a specified pressure field to the model through a vector
            % of values. It is assumed the vector has the same number of
            % elements of the enitre mode. 
            
            % Check to make sure size of input vector correspons with model
            if length(fVec) == length(obj.fe.element)
                obj.eles = obj.fe.eles; 
                obj.val = fVec;
            else
                error(['Field vector provided is not consistent with ',...
                       'total # of elements'])
            end
        end
        function obj = AddElements(obj, elements)
            % Add elements to pressure field by supplying a vector. The 
            % vector must be either element ids or cell array of element 
            % objects 
            
            % 2 input options: 
            % nodes (cell array) or element ID (regular array)
            if iscell(elements) && isa(elements{1},'Element')
                for i = 1:length(elements)
                    obj.eles{end+1} = elements{i};
                end
            elseif isa(elements,'Element')
                obj.eles{end+1} = elements;
            end
           
            
        end
        function obj = AddAmplitude(obj, amp)
            % Add ampliude objects to specfiied amplitude name,
            % creating it if it doesn't exist.
            
            if ~isa(amp,'Amplitude')
                error('Must use the amplitude object');
            else
                obj.amplitude = amp;
            end
        end
        function F = AssembleForce(obj, ti)
            % Assemble the force vector at time ti. If no time is provided
            % then the vector is scaled by unity. If a time value is
            % provided then the force is scaled by the amplitude at ti. 
            
           % Compute nodal loads based upon provided pressure
           
           % Initialzie variables (stress, strain and nodal loads)
           F = zeros(obj.fe.nDof,1);
           
           % Loop through element-by-element
            for ii = 1:length(obj.eles)
                tempNodeVec = ([obj.eles{ii}.nodes{:}]);
%                 xEle = xIn([tempNodeVec.globalDof]);
%                 pEle = pIn([tempNodeVec.id]);
                fEle = obj.eles{ii}.ComputePressureLoads(obj.val);
                F([tempNodeVec.globalDof]) = F([tempNodeVec.globalDof]) + fEle;
            end
            
        
            % If no time is provided then use unscaled force
            if isempty(ti) || ~isa(obj.amplitude,'Amplitude') 
                amp = 1;
            else
                amp = obj.amplitude(ti);
            end
            
            % Scale by amplitude
            F = F*amp;

        end
    end
    
end

