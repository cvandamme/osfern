classdef Material < handle
    
    % Material definition class. I wrote this mainly to make it easier to
    % handle temperature-varying properties. It is a handle class so that
    % it can be modified in-place in the MatFEM and still propogate changes
    % to any associated assembler models.
    
    properties(GetAccess = 'private', SetAccess = 'private')
        
        name  % Material name
        props % Map of property names and values
        
    end
    
    methods
        
        function obj = Material(name)
            
            obj.name = name;
            obj.props = containers.Map('KeyType', 'char', 'ValueType', 'any');
            
        end
        
        function obj = setProp(obj, property, values, temps)
            
            if nargin < 4
                temps = [];
                if length(values) > 1
                    error('Property must have single value if no temperature vector supplied.');
                end
            elseif length(values) ~= length(temps)
                    error('Temperature and property values must be same length');
            end
            
            obj.props(property) = struct('values', values, 'temps', temps);
            
        end
        
        function value = getProp(obj, property, temp)
            
            prop = obj.props(property);
            
            if nargin < 3
%                 if ~isempty(prop.temps)
%                     error('Must supply temp value with temp-dependent property');
%                 end
                value = prop.values(1);
            else
                value = interp1(prop.temps, prop.values, temp);
            end    
            
        end
        
        function disp(obj)
            params = keys(obj.props);
            for ii = 1:length(obj.props)
                disp([params{ii} ,' = ',  num2str(obj.props(params{ii}).values)]);
            end
        end
    end
end
            
            
            
                
            
            
                