classdef OsFernFemParser
    % This class reads in data from other finie element formats and outputs
    % an osfern object. 
    %
    % In progress
    % Supported types
    %    1) Abaqus  - .inp
    %    2) Nastrain/Optistruct .fem
    
    properties
        filePath
        fileType
        fileId
    end
    
    methods
        function obj = OsFernFemParser(filePath)
            
            pPos = strfind(filePath,'.');
            fType = filePath(pPos+1:end);
            fid = fopen(filePath);

        end
        
        function obj = ReadInp(obj)
            fid = fopen('D:\FERN\src\models\curvedBeam.inp');
            tline = fgetl(fid);
            while ischar(tline)
                disp(tline)
                tline = fgetl(fid);
                
                % Check if new heading is found
                if strcmp(tline(1),'*') && ~strcmp(tline(1:2),'**')
                    cIndex = strfin
                    
                else
                end
            end
        end
    end
    
end

