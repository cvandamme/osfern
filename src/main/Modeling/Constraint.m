classdef Constraint < handle
    % This class is used to represent nodal constrains applied to a FE
    % model throughout an analysis
    
    properties
        name     % name of the force
        nodes    % vector of node objects the constraint is applied to 
        dof      % dof of the constraint
        val      % value of constraint at the dof 
        amplitude = 1 % by default amplitude is one otherwise can assign 
        fe       % fe model object 
    end
    
    methods
        function obj = Constraint(name,feObj)
            obj.name = name;
            obj.fe = feObj;
        end
        function obj = AddForceVector(obj, fVec)
            % Add forces to model by supplying a vector. The vector must be 
            % either consistent wih the total number of dof or with the
            % number of freedof. 
            
            if length(fVec) == obj.fe.nDof
                obj.dof = [obj.dof ;1:obj.fe.nDof]; 
                obj.val = [obj.val ;fVec];
            elseif length(fVec) == sum(obj.fe.freeDof)
                obj.dof = [obj.dof ; find(obj.fe.freeDof)]; 
                obj.val = [obj.val ; fVec]; 
            else
                error(['Force vector provided is not consistent with ',...
                      'total # of dof or # of free dof'])
            end
        end
        function obj = AddForce(obj, nodes, dof, value)
            % Add forces to model. Can be multiple nodes and DOF; value can
            % be either scalar or must be size n*m for n nodes and m DOF.
            % Amplitude must be a single anonymous function, an empty
            % variable, or omitted. For static steps, the amplitude is
            % not used.
            
            
            % 2 input options: nodes (cell array) or node ID (regular
            % array)
            if iscell(nodes) && isa(nodes{1},'Node') % Cell array of node objects
                for i = 1:length(nodes)
                    obj.nodes{end+1} = nodes{i};
                    nodeDof = nodes{i}.GetGlobalDof();
                    fDof = nodeDof(dof(i));
                    % Add to the current vectors
                    obj.dof = [obj.dof fDof]; obj.val = [obj.val value];
                end
            else
                for i = 1:length(nodes)
                    obj.nodes{end+1} = obj.fe.nodes{i};
                    fDof = obj.fe.nodes{i}.GetGlobalDof(dof(i));
                    
                    % Add to the current vectors
                    obj.dof = [obj.dof fDof]; obj.val = [obj.val value];
                end
            end
                
                
        end
        function obj = AddAmplitude(obj, amp)
            % Add ampliude objects to specfiied amplitude name,
            % creating it if it doesn't exist.
            
            if ~isa(amp,'Amplitude')
                error('Must use the amplitude object');
            else
                obj.amplitude = amp;
            end
        end
        function F = AssembleForce(obj, ti)
            % Assemble the force vector at time ti. If no time is provided
            % then the vector is scaled by unity. If a time value is
            % provided then the force is scaled by the amplitude at ti. 
            
            % If no time is provided then use unscaled force
            if isempty(ti) || ~isa(obj.amplitude,'Amplitude') 
                amp = 1;
            else
                amp = obj.amplitude(ti);
            end
            
            % Creaet sparse vector
            F = sparse(obj.dof, 1, obj.val, obj.fe.nDof, 1);
            
            % Scale by amplitude
            F = F*amp;

        end

    end
    
end
