classdef Node < handle
    % This is a class that represents a general nodal coordiante for finite
    % elements. 
    %   Detailed explanation goes here
    
    % TODO: Remove "obj = " for all method calls and verify that this still works. 
    
    properties
        id    % node id number 
        index % node index in FEM (can update)
        x     % node x position
        y     % node y position
        z     % node z position
        autospc = true
        globalDof % global dof 
        forces = nan;   % forces applied to dof of node
        % cDof      % (OBSOLETE) active constraint dof, true or false
        constraints % displacement constraint value
        temp  = nan; % temperaute of node
        fInt % internal forces
        
        NUM_DOF = 6; % Number of displacment DOF. Treated as a constant for now; potentially 
                     % could be adjusted later for additional types of
                     % elements. cvd - this needs to change now that solid
                     % element is implemented.
        elements
                 % vector of structures indicating elements this node is
                 % is associated with. used to constrain dof if on a solid
                 % element. maybe change to cell array of element objects?
    end
    
    methods
        % Initialization
        function obj = Node(nid, x, y, z)
            
            % Node id #
            if round(nid) ~= nid
                error('Require integer for node ID');
            end
            obj.id = nid;
            
            % xyz locations
            obj.x = x; obj.y = y; obj.z = z;
            
            % Global Dof Indices
            obj.globalDof = (nid-1)*6+1:nid*6;
           
        end
        
        % Set Functions
        function obj = SetIndex(obj, index)
            % Set the node's absolute index in the model.
            obj.index = index;
        end
        
        function obj = SetGlobalDof(obj, prevGlobalDof)
            % Set the node's global DOF in the model.
            obj.globalDof = prevGlobalDof + obj.GetLocalDof();
        end
        
        function obj = SetForce(obj, forceVec, varargin)
            % Set a force applied to a node, either through direct vector
            % specification
            % varagin{1} would be if only applying to s specific index
            if nargin > 2
                obj.forces = nan(obj.NUM_DOF, 1);
                obj.forces(varargin{1}) = forceVec;
            else
                if length(forceVec) ~= obj.NUM_DOF
                    error('Incorrect length of specified force vector');
                end
                obj.forces = forceVec;
            end
        end
        
        function obj = SetDisplacement(obj,dispVec,varargin)
            % Set a displacement applied to a node, either through direct
            % vector specification (1 argument) or with a (value, dof)
            % combination (2 arguments).
            if nargin > 2
                obj.constraints = nan(obj.NUM_DOF, 1);
                obj.constraints(varargin{1}) = dispVec;
            else
                if length(dispVec) ~= obj.NUM_DOF
                    error('Incorrect length of specified displacement vector');
                end
                obj.constraints = dispVec;
            end
        end
        
        function obj = SetFixed(obj, varargin)
            % Convenience function to set zero-valued boundary conditions
            % on a node or its DOF.
            if (nargin > 1) && ~isempty(varargin{1})
                obj.constraints = nan(obj.NUM_DOF, 1);
                obj.constraints(varargin{1}) = 0;
            else
                obj.constraints = zeros(obj.NUM_DOF, 1);
            end
        end
        
        function obj = SetFree(obj, varargin)
            % Convenience function to release zero-valued boundary
            % conditions on a node or its DOF.
            if (nargin > 1) && ~isempty(varargin{1})
                obj.constraints(varargin{1}) = nan;
            else
                obj.constraints = nan(obj.NUM_DOF, 1);
            end
        end
        
        function obj = SetTemp(obj, tempIn)
            % Set the Node's temperature DOF.
            obj.temp = tempIn;
        end
        
        function obj = SetXYZ(obj,x,y,z)
            obj.x = x;
            obj.y = y;
            obj.z = z;
        end   
        
        function obj = SetDependentElement(obj,elementObj)
            
            if isempty(obj.elements)
                numEles = 0;
            else
                numEles = length(obj.elements.type);
            end
            
            obj.elements{numEles + 1} = elementObj;
        end
        
        function obj = SetElementIds(obj,id)
            % add the element id to the node
            if ~ismember(id,obj.elements)
                obj.elements(end+1) = id;
            end
        end
        
        % Access functions
        function xyz = GetXYZ(obj)
            try
                xyz = [double(obj.x); double(obj.y); double(obj.z)];
            catch
                 xyz = [obj.x; obj.y; obj.z];
            end
        end            
        
        function nid = GetId(obj)
            nid = obj.id;
        end
        
        function index = GetIndex(obj)
            index = obj.index;
        end
        
        function dof = GetGlobalDof(obj,varargin)
            
            if nargin > 1
                dof = obj.globalDof(varargin{1});
            else
                dof = obj.globalDof;
            end
        end
        
        function dof = GetLocalDof(obj)
            dof = 1:obj.GetNumDof();
        end
        
        function nDOF = GetNumDof(obj)
%             nDOF = length(obj.globalDof); % assume 6 for now
              % Check if a solid element is associated with the node
%               if any(isa(obj.elements{:},'H8'))
%                   nDOF = 3;
%               else
                 nDOF = obj.NUM_DOF;
%               end
        end
        
        function tempOut = GetTemperature(obj)
            tempOut = obj.temp;
        end
        
        function constrainedDof = GetLocalConstraints(obj)
            % Return the active constraints on this node, in node's local
            % DOF ordering.
            if isnan(obj.constraints)
                constrainedDof = [];
            else
                constrainedDof = find(obj.constraints == 0);
            end
        end
        
        function constrainedDof = GetGlobalConstraints(obj)
            % Return the active constraints on this node, in node's global DOF
            % ordering.
            constrainedDof = obj.globalDof(obj.GetLocalConstraints);
        end
        
        % Utility functions
        function obj = RemoveDisplacement(obj,varargin)
            if nargin > 1
                obj.constraints(varargin{1}) = nan;
            else
                obj.constraints = nan;
            end
        end
        
        function obj = RemoveForce(obj,varargin)
            obj.forces = nan;
            
        end
        
        function obj = RemoveTemp(obj,varargin)
            obj.temp = nan;
        end
        
        
    end
    
end

