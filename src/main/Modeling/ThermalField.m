classdef ThermalField < handle
    % This class is used to represent a thermal field applied to a FE
    % model. The field will define temperature values at specified nodes
    % in the model. 
    % Class is also used to generate the loads from the thermal field. 
    %
    % TFobj = ThermalField('name',FEObj,tempVal);
    %    This specifies a uniform thermal field with temperature "tempVal",
    %    which is applied to the OSFern FEM in "FEObj".
    %    By default a uniform temperature field is created, i.e. tfvec
    %    below is tfvec=ones(length(FEObj.nodes))
    %   TFobj.AddFieldVector(tfvec);
    %    where tfvec is a vector the same size as the number of nodes in
    %    the FEM which defines the temperature field. The resulting
    %    temperature at each node is:
    %    T=tfvec*tempVal
    %    The vector tfvec should be ordered consistent with the order of
    %    the nodes in the FEM, i.e. FEObj.GetNodesArray = [Node#, x,y,z].
    %
    
    properties
        name     % name of the force
        nodes    % vector of node objects the field is applied to 
        elements % vector of active elements (to speed up assembly)
        val      % Temperature scale factor.  Temperature at each node "i" is
                 % given by: T=tempDist(i)*val
        tempDist % Shape of temperature field.  Must contain one entry for
                 % every node in the FEM.
        amplitude = 1 % by default amplitude is one otherwise can assign 
        fe       % fe model object 
    end
    
    methods
        function obj = ThermalField(name,feObj,val)
            % Creation function
            obj.name = name;
            obj.fe = feObj;
            obj.val = val; % Temperature value
        end
        function obj = AddFieldVector(obj, tempDist)
            % Add forces to model by supplying a vector. The vector must be 
            % either consistent with the total number of dof or with the
            % number of freedof. 
            
            if length(tempDist) == length(obj.fe.node)
                % Add all nodes to the object
                obj.nodes = obj.fe.node; 
                % Add all elements as active since all nodes are used
                obj.elements = obj.fe.element;
                obj.tempDist=tempDist; % Shape of temperature distribution.
                    % For now we will assume this is passed in consistent
                    % with the order of the nodes in the FEM, so the field
                    % vector corresponds with each row of obj.fe.GetNodesArray.
            else
                error(['Field vector provided is not consistent with ',...
                       'total # of nodes'])
            end
        end
        function obj = AddNodes(obj, nodes)
            % Add nodes to the thermal field object. 
            %
            % 3 input options: 
            %       a) cell array of node objects
            %       b) single node object
            %       c) vector of node ids
            
            % Check which option is provided
            if iscell(nodes) && isa(nodes{1},'Node') 
                for i = 1:length(nodes)
                    obj.nodes{end+1} = nodes{i};
                end
            elseif isa(nodes,'Node')
                    obj.nodes{end+1} = nodes;
            else
                for i = 1:length(nodes)
                    obj.nodes{end+1} = obj.fe.node{i};
                end
            end
            
            % Recheck which elements are active within this set anytime
            % more nodes are added
            nVec = [obj.nodes{:}]; eleVec = unique([nVec.elements]);
            obj.elements = obj.fe.element{eleVec};
            
        end
        function obj = AddAmplitude(obj, amp)
            % Add ampliude objects to specfiied amplitude name,
            % creating it if it doesn't exist.
            
            if ~isa(amp,'Amplitude')
                error('Must use the amplitude object');
            else
                obj.amplitude = amp;
            end
        end
        function [tField] = AssembleTemperatureField(obj,ti,varargin)
            % MSA - this function seems to be incomplete - not sure if it
            % works or actually does anything.
            
            % Initialzie variables (stress, strain and nodal loads)
            tField = zeros(length(obj.fe.element),1);
            
            % Grab ids of nodes within this field
            nVec = [obj.nodes{:}]; nids = [nVec.id];
            
            % Loop through element-by-element
            for ii = 1:length(obj.elements)
                nodeVec = ([obj.fe.element{ii}.nodes{:}]);
                nodalTempVec = zeros(size(nodeVec));
                [~,ia,~] = intersect([nodeVec.id],nids);
               
                % Check if no intersection is present (i.e. element is not
                % within heated area)
                if isempty(ia)
                    Tele = 0;
                else
                    nodalTempVec(ia) = obj.val; % ########################
                    warning('This hasn''t been updated to use non-uniform temperature field');
                    Tele = mean(nodalTempVec);
                end
                
                tField(ii) = Tele; 
            end
            
            % If no time is provided then use unscaled force
            if isempty(ti) || ~isa(obj.amplitude,'Amplitude') 
                amp = 1;
            else
                amp = obj.amplitude(ti);
            end
            
%             % Create sparse vector
%             F = amp*P; % Nodal loads
%             Se = amp*S; % Stress
%             Ee = amp*E; % Strain
        end
        function [F,Se,Ee] = AssembleForce(obj,ti,varargin)
            % 
            
            % Initialzie variables (stress, strain and nodal loads)
            S = zeros(length(obj.fe.element),3); % Stress
            E = zeros(length(obj.fe.element),3); % Strain
            P = zeros(obj.fe.nDof,1); % Nodal Loads
            
            % Grab ids of nodes within this field
            nVec = [obj.nodes{:}]; nids = [nVec.id];
            
            % Check if displacement is supplied
            if nargin > 2
                xIn = varargin{1};
            else
                xIn = zeros(obj.fe.nDof,1);
            end
            
            % Loop through element-by-element
            for ii = 1:length(obj.elements)
                nodeVec = ([obj.elements{ii}.nodes{:}]);
%                 nodalTempVec = zeros(size(nodeVec));
                nodalTempVec = obj.tempDist([nodeVec.id])*obj.val;
                [~,ia,~] = intersect([nodeVec.id],nids);
               
                % Check if no intersection is present (i.e. element is not
                % within heated area)
                if isempty(ia) % If not within a heated region
                    Sele = zeros(1,3); Eele = zeros(1,3);
                    Pele = zeros(size([nodeVec.globalDof]))';
                else
%                     nodalTempVec(ia) = obj.val; %###########################
                    xEle = xIn([nodeVec.globalDof]);
                    [Sele,Eele,Pele] = obj.elements{ii}.ComputeThermalLoads(nodalTempVec(:),xEle);
                end
                
                S(obj.elements{ii}.id,:) = Sele;
                E(obj.elements{ii}.id,:) = Eele;
                P([nodeVec.globalDof]) = P([nodeVec.globalDof]) + Pele;
            end
            
            % If no time is provided then use unscaled force
            if isempty(ti) || ~isa(obj.amplitude,'Amplitude') 
                amp = 1;
            else
                amp = obj.amplitude(ti);
            end
            
            % Creaet sparse vector (stress, strain and nodal loads)
            F = amp*P; Se = amp*S; Ee = amp*E;
        end
    end
    
end

