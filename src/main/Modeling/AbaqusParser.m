classdef AbaqusParser
    % This class reads in data from other finie element formats and outputs
    % an osfern object.
    %
    % In progress
    % Supported types
    %    1) Abaqus  - .inp
    %    2) Nastrain/Optistruct .fem
    
    properties
        fileDir
        fileName
        filePath
        fileId
        osFern
    end
    
    methods
        function obj = AbaqusParser(filePath,osFern)
            % This is the finitialization function for the abaqus parser
            % class.
            % Inputs :
            %       filePath : location of the .inp file with the extensino
            %                  included i.e. dir/femodel.inp
            %       osfernObj: osfern object
            % Outputs:
            %       obj = abaqus parser object
            
            
            % Print out warning
            warning(['When exporting the abaqus .inp file you must ensure ',...
                'that parts and assemblies are not written. Use element',...
                'or node sets to define different components'])
            
            
            % Extract some info
            pPos = strfind(filePath,'.');
            sPos = strfind(filePath,'.');
            obj.filePath = filePath;
            obj.fileName = filePath(sPos+1:pPos-1);
            obj.fileDir = filePath(1:sPos-1);
            
            % Store the osfern object into the class
            obj.osFern = osFern;
            
        end
        
        function obj = ReadInp(obj)
            
            % Open the file
            fid = fopen(obj.filePath);
            tline = fgetl(fid);
            
            % Continue reading until end of file
            while ischar(tline)
                
                % Print the line - for debugging
                disp(tline)
                
                % Grab the line
                tline = fgetl(fid);
                                
                % Break if not a character
                if ~ischar(tline); break; end

                
                % Check if tline is empty or a comment
                if isempty(tline) || strcmp(tline(1:2),'**')
                    % disp('empty line')
                    % skipLine = true;
                else
                    
                    % Check if new heading is found
                    if strcmp(tline(1),'*')
                        
                        % Extract the header and options
                        headers = split(tline(1:end),',');
                        header = headers{1}; disp(header)
                        if length(headers) > 1
                            opts = headers(2:end);
                        else
                            opts = [];
                        end
                       
                        
                        % Continue to read until the next header 
                        star = false;
                        while ~star;
                            disp(tline)
                            tline = fgetl(fid);
                            if isempty(tline); break; end
                            if ~ischar(tline); break; end
                            if strcmp(tline(1:2),'**'); break; end
                            switch header
                                case '*NODE'
                                    % Grab each parameter
                                    nodeParams = split(tline,',');
                                    
                                    % Create node object
                                    tempNode = Node(str2double(nodeParams{1}),...
                                                    str2double(nodeParams{2}),...
                                                    str2double(nodeParams{3}),...
                                                    str2double(nodeParams{4}));
                                    
                                    % Add node object to osfern model
                                    obj.osFern.AddNodeObject(tempNode);
                                    
                                case '*ELEMENT'
                                    
                                    % Grab each parameter
                                    eleParams = split(tline,',');
                                    if strcmp(strip(opts{1}),'TYPE=B31')
                                        
                                        % Create the beam element
                                        tempEle = B2(str2double(eleParams{1}));
                                        
                                        % Grab nodes used to define element
                                        nodeIds = [str2double(eleParams{2}),str2double(eleParams{3})];
                                            
                                        % If third option is supplidd then
                                        % it is used to define the cross section
                                        tempEle.Build(obj.osFern.GetNodes(nodeIds),...
                                                      tempMat);
                                    elseif strcmp(opts{1},'TYPE=B21')
                                        error('2-Dimensional Beam Elements are not allowed')
                                    elseif strcmp(opts{1},'S4') || strcmp(opts{1},'S4R')
                                        % Create the shell element
                                        tempEle = S4S(str2double(eleParams{1}));
                                        
                                        % Grab nodes used to define element
                                        nodeIds = [str2double(eleParams{2}),...
                                                   str2double(eleParams{3}),...
                                                   str2double(eleParams{4}),...
                                                   str2double(eleParams{5})];
                                            
                                        % it is used to define the cross section
                                        tempEle.Build(obj.osFern.GetNodes(nodeIds),...
                                                      tempMat);
                                    else
                                        error('This element is not supported')
                                    end
                                case 'Section'
                                    
                                case 'Msterial'
                                    
                                case 'Nset'
                                    
                                case 'Elset'
                                    
                            end
                        end
                        
                        % Jump back one line
%                         ferr = fseek(fid, -1, 0);
                        
                    end
                    
                    % This assumes that the following will be read until
                    % another header or comment is found
                
                end
            end
        end
        
        function obj = ReadNodes(obj)
        end
        
        function obj = ReadShell(obj)
        end
    end
    methods (Static)
        function [header,opts] = CheckHeader(stringIn)
            
            % Find all the commas in the header
            commas = strfind(stringIn,',');
            
            if isempty(commas)
                header = stringIn; opts = [];
            else
                % Grab header - first string
                header = strip(stringIn(1:commas-1));
                
                % Break the string into those components - first is most
                % important
                opts = cell(length(commas));
                commas(end+1) = length(stringIn) + 1;
                for ii = 1:length(commas)-1
                    opts{ii} = strip(stringIn(commas(ii)+1:commas(ii+1)-1));
                end
                
            end
        end
    end
end

