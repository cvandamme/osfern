classdef Field < handle
    % This class is used to represent fields applied to a FE
    % model throughout an analysis.
    % Primary use is as a thermal field
    
    properties
        name     % name of the force
        nodes    % vector of node objects the field is applied to 
        val      % value of field at the node 
        amplitude = 1 % by default amplitude is one otherwise can assign 
        fe       % fe model object 
    end
    
    methods
        function obj = Field(name,feObj)
            obj.name = name;
            obj.fe = feObj;
        end
        function obj = AddFieldVector(obj, fVec)
            % Add forces to model by supplying a vector. The vector must be 
            % either consistent wih the total number of dof or with the
            % number of freedof. 
            
            if length(fVec) == length(obj.fe.node)
                obj.nodes = obj.fe.node; 
                obj.val = fVec;
            else
                error(['Field vector provided is not consistent with ',...
                       'total # of nodes'])
            end
        end
        function obj = AddField(obj, nodes, value)
            % Add forces to model. Can be multiple nodes and DOF; value can
            % be either scalar or must be size n*m for n nodes and m DOF.
            % Amplitude must be a single anonymous function, an empty
            % variable, or omitted. For static steps, the amplitude is
            % not used.
            
            
            % 2 input options: nodes (cell array) or node ID (regular
            % array)
            if iscell(nodes) && isa(nodes{1},'Node') % Cell array of node objects
                if length(nodes) ~= length(value)
                    for i = 1:length(nodes)
                        obj.nodes{end+1} = nodes{i};
                        obj.val = [obj.val value(i)];
                    end
                else
                   error('Inconsistent sized between nodes and values');
                end
            elseif isa(nodes,'Node')
                    obj.nodes{end+1} = nodes;
                    obj.val = [obj.val value];
            else
                if length(nodes) ~= length(value)
                    for i = 1:length(nodes)
                        obj.nodes{end+1} = obj.fe.node{i};
                        obj.val = [obj.val value];
                    end
                else
                    error('Inconsistent sized between nodes and values');
                end
            end
                
                
        end
        function obj = AddAmplitude(obj, amp)
            % Add ampliude objects to specfiied amplitude name,
            % creating it if it doesn't exist.
            
            if ~isa(amp,'Amplitude')
                error('Must use the amplitude object');
            else
                obj.amplitude = amp;
            end
        end
    end
    
    methods (Abstract)
        F = AssembleField(obj,ti)
    end
end

