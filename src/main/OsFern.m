classdef OsFern < handle
    % OsFern is the primary object containing finite element information
    % about the model.
    % 
    % It conatins : 
    %       - nodes, node sets 
    %       - elements, element sets
    %       - components (in progress)
    %       - free dof information
    %       - control structures
    % It has access / can build
    %       - Linear Mass, Damping, Stiffness Matrix
    %       - Tangent Stiffness, Stress Stiffened Matrix
    %       - Strains, Stresses
    %       - Model Mass
    %       - Model TimeStep
    %
    %
    % Add Liscense : BSD
    % 
    % Copyright : Christopher Van Damme - cvandamme@wisc.edu,
    %             Joseph Schoneman - joe.schoneman@ata-e.com
    % 
    % Version 2018.01
    % 
    % Some of the available methods that may be of interet:
    %
    %   OsFern.Plot(x,...) - Plot deformation field 'x'
    %
    %   OsFern.uimplot(x,...) - Interactive plot of field 'x'
    %
    %   OsFern.GetNodesArray - Extract 'nodes' array compatible with
    %       Hollkamp's routines
    %
    %   OsFern.GetElesArray - Extract 'eles' array compatible with
    %       Hollkamp's routines such as uimplot5
    %
    %   OsFern.findNODEfromXYZ - Find a node in the model closest to a
    %       certain set of xyz coordinates.
    % ...
    % For a somewhat complete list of all methods, type: 'doc OsFern' to
    % open the help browser with a list of all methods, properties, etc.
    %
    properties(GetAccess = 'public', SetAccess = 'public')
        
        % Meta properties (most for convenience).
        % Note that all property names are singular.
        name;
        nNode = 0;
        nDof = 0;
        nElement = 0;
        nComponents = 0;
        nMatrixEntries = 0;  % number of values in global matrices
        
        % Components 
        components = {}; % cell array of components (can be rom or fem)
        
        % Main properties.
        node = {} % cell array of node objects

        % Given a node ID, obtain the node index. 
        nodeId2Ind = [];%  = containers.Map('KeyType', 'double', 'ValueType', 'double');
        freeDof = []; % Boolean array containing free DOF in the model

        % Store element properties here, but not the actual element
        element = {} % cell array of element objects
        
        % Given an element ID, obtain the element index. 
        elementId2Ind = []; % containers.Map('KeyType', 'double', 'ValueType', 'double');

        % Set and surface definitions.
        nset = containers.Map('KeyType', 'char', 'ValueType', 'any'); % Node sets
        elset = containers.Map('KeyType', 'char', 'ValueType', 'any'); % Element sets
        
        % Section property and material definitions 
        property = containers.Map('KeyType', 'char', 'ValueType', 'any');
        material = containers.Map('KeyType', 'char', 'ValueType', 'any');
                      
        % Field and amplitude definitions
        field = struct('name', [], 'data', []);
        amplitude = struct('name', [], 'data', []);
        
        % Parallel computing options (default is no)
        parallelOpts = struct('logic', false,'numWorkers', 2,'pPool',[]);
        
        % Plot Options Structure
        plotOpts = struct('constraints',false,'forces',false);
        
        end
    
    methods
        % ---- Initialization ----- %
        function obj = OsFern(name)
            %    obj = MatFEM(name)
            obj.name = name;
            
            % Explicitly initialize all Map containers
            obj.nodeId2Ind = containers.Map('KeyType', 'double', 'ValueType', 'double');
            obj.elementId2Ind = containers.Map('KeyType', 'double', 'ValueType', 'double');
            
            obj.nset = containers.Map('KeyType', 'char', 'ValueType', 'any'); % Node sets
            obj.elset = containers.Map('KeyType', 'char', 'ValueType', 'any'); % Element sets
            
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%% MODEL BUILDING METHODS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function obj = AddNodeObject(obj, nodeObj, nset)
            %    obj = AddNodeObject(obj, nodeObj, nset)
            %
            % MatFEM.AddNodeObject adds a node using an existing Node object.
            
            % TODO: This has to interact with
            % components somehow, though, because you can have duplicate
            % ID's on different components. 
            
            
            if obj.nodeId2Ind.isKey(nodeObj.id)
                error('Duplicate node ID %i supplied to MatFEM', nodeObj.id);
            end
            
            obj.node{end + 1} = nodeObj; 
            obj.nDof = obj.nDof + nodeObj.GetNumDof();
            obj.nNode = obj.nNode + 1;
            obj.nodeId2Ind(nodeObj.id) = obj.nNode;
            
            % Add node to specified nset if argument supplied
            if nargin > 2
                if nset
                    obj.Add2Nset(nset, nodeObj);
                end
            end
            
        end
                
        function obj = AddElementObject(obj, eleObj, elset)
            %    obj = AddElementObject(obj, eleObj, elset)
            %

            % TODO: This has to interact with
            % components somehow, though, because you can have duplicate
            % ID's on different components. 
            % Adds a node using an existing Node object.
            
            if obj.elementId2Ind.isKey(eleObj.id)
                error('Duplicate element ID %i supplied to MatFEM', eleObj.id);
            end
            
            obj.element{end + 1} = eleObj; 
            obj.nElement = obj.nElement + 1;
            obj.elementId2Ind(eleObj.id) = obj.nElement;
            
            % Add element to specified elset if argument supplied
            if nargin > 2
                if elset
                    obj.Add2Elset(elset, eleObj);
                end
            end
            
        end
                
        function Add2Nset(obj, nset, node)
            %    Add2Nset(obj, nset, node)
            %
            % Add Node objects to specfiied nset, creating it if it doesn't
            % exist.
            
            if obj.nset.isKey(nset)
                % Had to do it this way; cell arrays are kind of dumb like
                % that. 
                tempArray = obj.nset(nset);
                tempArray{end + 1} = node;
                obj.nset(nset) = tempArray;
            else
                obj.nset(nset) = {node};
            end
        end
                
        function Add2Elset(obj, elset, element)
            %    Add2Elset(obj, elset, element)
            %
            % Add Element objects to specfiied elset; creating it if it doesn't
            % exist.
            
            if obj.elset.isKey(elset)
                % Had to do it this way; cell arrays are kind of dumb like
                % that. 
                tempArray = obj.elset(elset);
                tempArray{end + 1} = element;
                obj.elset(elset) = tempArray;
            else
                obj.elset(elset) = {element};
            end
        end
        
        function obj = AddConstraint(obj, varargin)
            %    obj = AddConstraint(obj, nodes, constrainedDof, ... )
            %
            % Add zero-BC constraints to model. Sets specified constraints
            % on Node objects.
            % Two possible input pattens:
            %
            % (1) AddConstraint({nodeCellArray}, [constrainedDof], ...)
            % (2) AddConstraint([nodeIds], [constrainedDof], ... )
            
            if obj.nNode == 0
                error('addConstraint requires node definitions in MatFEM object');
            end
            
            n = length(varargin);

            for i = 1:n/2
                nodeArg = varargin{2*(i - 1) + 1};
                dofArg = varargin{2*i};
                
                % Pattern 1: Supply cell array of nodes
                if iscell(nodeArg)
                    for j = 1:length(nodeArg)
                        nodeArg{j}.SetFixed(dofArg);
                    end
                % Pattern 2: Supply array of node IDs
                else
                    for j = 1:length(nodeArg)
                        ind = obj.GetNodeIndex(nodeArg(j));
                        obj.node{ind}.SetFixed(dofArg);
                    end
                end

            end

        end        
        
        function freeDof = BuildConstraintVec(obj, varargin)
            % This function builds the constraint vector for global dof
            % based upon constrained nodes during generation. 
            % TODO: Write capability for optional input of additional
            % constraints from specific steps.
            obj.freeDof = logical(ones(obj.nDof, 1, 'uint8')); %#ok (can't do ones with logical; dumb)
            for ii = 1:obj.nNode
                nodeConstraints = obj.node{ii}.GetGlobalConstraints();
                obj.freeDof(nodeConstraints) = false;
            end
            
            if nargout == 1
                freeDof = obj.freeDof;
            end
            
        end
        
        function obj = BuildDof(obj)
            % Update all nodal and elemental DOF.
            
            % Nodes
            prevDof = 0;
            for ii = 1:obj.nNode
                obj.node{ii}.SetGlobalDof(prevDof);
                prevDof = prevDof + obj.node{ii}.GetNumDof();
            end  
            
            nPrevMatrixEntries = 0;
            for ii = 1:length(obj.element)
                obj.element{ii}.SetGlobalDof(nPrevMatrixEntries);
                nPrevMatrixEntries = nPrevMatrixEntries + obj.element{ii}.GetNumMatrixEntries;
            end
                        
        end
        
        function obj = Update(obj,varargin)
            % Updates all dof, constraints, assembler, etc
            % This should be ran anytime anything has changed on the model
            % itself, not for specific step procedures. 
            
            % 
            if nargin > 1
                reBuild = varargin{1};
                if reBuild
%                     disp('Rebuilding Finite Element Model')
                end
            else
                reBuild = false;
            end
            
            % Assign correct global DOF to all nodes
            obj.BuildDof();
            
            % Update constraint vectors using constraints currently active
            % on model.
            obj.BuildConstraintVec();
            
            % Obtain the correct number of matrix entries for the problem
            obj.nMatrixEntries = 0;
            for ii = 1:length(obj.element)
                if reBuild
                    obj.element{ii}.ReBuild();
                end
                obj.nMatrixEntries = obj.nMatrixEntries + obj.element{ii}.GetNumMatrixEntries();
            end
            
        end
        
        % State Assembly Methods
        function [M] = GetMassMatrix(obj,varargin)
            
            % Allocate memory
            Mv = zeros(obj.nMatrixEntries, 3);
            
            % Loop through element-by-element
            for ii = 1:length(obj.element)
                Mv(obj.element{ii}.ind,:) = [obj.element{ii}.rid, obj.element{ii}.cid, obj.element{ii}.BuildMass()];
            end
            
            % Build sparse matrix
            M = sparse(Mv(:, 1), Mv(:, 2), Mv(:, 3), obj.nDof, obj.nDof);

            % If additional argument is supplied output full matrix
            if nargin < 2
                M = M(obj.freeDof, obj.freeDof);
            end
                
        end
        
        function [M] = GetDiagonalMassMatrix(obj,varargin)
            
            % Allocate memory
            Mv = zeros(obj.nMatrixEntries, 3);
            
            % Loop through element-by-element
            for ii = 1:length(obj.element)
                Mv(obj.element{ii}.ind,:) = [obj.element{ii}.rid, obj.element{ii}.cid, obj.element{ii}.BuildMassDiag()];
            end
            
            % Build sparse matrix
            M = sparse(Mv(:, 1), Mv(:, 2), Mv(:, 3), obj.nDof, obj.nDof);

            % If additional argument is supplied output full matrix
            if nargin < 2
                M = M(obj.freeDof, obj.freeDof);
            end
            
        end
        
        function [K] = GetStiffnessMatrix(obj,varargin)
            
            % Allocate memory
            Kv = zeros(obj.nMatrixEntries, 3);
            
            % Loop through element-by-element
            for ii = 1:length(obj.element)
                Kv(obj.element{ii}.ind,:) = [obj.element{ii}.rid, obj.element{ii}.cid, obj.element{ii}.BuildStiff([])];
            end
            
            % Build sparse matrix
            K = sparse(Kv(:, 1), Kv(:, 2), Kv(:, 3), obj.nDof,obj.nDof);
            
            % If additional argument is supplied output full matrix
            if nargin < 2
                K = K(obj.freeDof, obj.freeDof);
            end
                
        end
        
        function [Kn,Kt] = GetTangentStiffness(obj, xIn, varargin)
            % Assemble the nonlinear stiffness matrices at a given
            % deformation
            % DEFINE Kn and Kt !!!!!!!!!!!!!!!!!!!!!!!!!!!
            %
            
            % Allocate memory
            Knv = zeros(obj.nMatrixEntries, 3);
            Ktv = zeros(obj.nMatrixEntries, 3);
            
            % Loop through element-by-element
            tStart = tic;
            for ii = 1:length(obj.element)
                tempNodeVec = ([obj.element{ii}.nodes{:}]);
                xEle = xIn([tempNodeVec.globalDof]);
                [KnEle,KtEle] = obj.element{ii}.BuildNLStiff(xEle);
                Knv(obj.element{ii}.ind,:) = [obj.element{ii}.rid,...
                    obj.element{ii}.cid, KnEle];
                Ktv(obj.element{ii}.ind,:)  = [obj.element{ii}.rid,...
                    obj.element{ii}.cid, KtEle];
            end
            tAssembly = toc(tStart);
            
            % Build sparse matrices
            Kn = sparse(Knv(:, 1), Knv(:, 2), Knv(:, 3), obj.nDof, obj.nDof);
            Kt = sparse(Ktv(:, 1), Ktv(:, 2), Ktv(:, 3), obj.nDof, obj.nDof);
            
            % If additional argument is supplied output full matrix
            if nargin < 3
                Kn = Kn(obj.freeDof, obj.freeDof);
                Kt = Kt(obj.freeDof, obj.freeDof);
            end
            
            
            
        end
        
        function [fIntNl,Kn,Kt] = GetFintNl(obj, xIn, varargin)
            % Assemble the nonlinear stiffness matrices at a given
            % deformation
            
            % Allocate memory
            Knv = zeros(obj.nMatrixEntries, 3);
            Ktv = zeros(obj.nMatrixEntries, 3);
            fIntNl = zeros(obj.nDof, 1);
            
            % Loop through element-by-element
            tStart = tic;
            for ii = 1:length(obj.element)
                tempNodeVec = ([obj.element{ii}.nodes{:}]);
                xEle = xIn([tempNodeVec.globalDof]);
                [fIntNle,KnEle,KtEle] = obj.element{ii}.GetFintNl(xEle);
                fIntNl(obj.element{ii}.rid(1:obj.element{ii}.nDof)) = ...
                    fIntNl(obj.element{ii}.rid(1:obj.element{ii}.nDof)) + fIntNle;
                Knv(obj.element{ii}.ind,:) = [obj.element{ii}.rid,...
                    obj.element{ii}.cid, KnEle];
                Ktv(obj.element{ii}.ind,:)  = [obj.element{ii}.rid,...
                    obj.element{ii}.cid, KtEle];
            end
            tAssembly = toc(tStart);
            
            % Build sparse matrices
            Kn = sparse(Knv(:, 1), Knv(:, 2), Knv(:, 3), obj.nDof, obj.nDof);
            Kt = sparse(Ktv(:, 1), Ktv(:, 2), Ktv(:, 3), obj.nDof, obj.nDof);
            
            % If additional argument is supplied output full matrix
            if nargin < 3
                Kn = Kn(obj.freeDof, obj.freeDof);
                Kt = Kt(obj.freeDof, obj.freeDof);
                fIntNl = fIntNl(obj.freeDof);
            end
            
            
            
        end

        function [Me,rid,cid,Mmat] = GetElement_MassMatrix(obj,eleIds)
            % This function is used to update the mass martix
            % of a structure. This is meant to be used in the case that
            % there has only been a local change to an element. This avoids
            % reassmbly the entire mass matrix
            
            % Allocate memory
            Mc = cell(length(eleIds),1); ridc = Mc; cidc = Mc;

            % Loop through element-by-element
            for ii = 1:length(eleIds)
                Mc{ii} = obj.element{eleIds(ii)}.BuildMass();
                ridc{ii} = obj.element{eleIds(ii)}.rid;
                cidc{ii} = obj.element{eleIds(ii)}.cid;
            end
             
            % Covnert to vectors 
            Me = cell2mat(Mc); rid = cell2mat(ridc); cid = cell2mat(cidc);
            
            % Build sparse matrix
            M= sparse(rid, cid, Me, obj.nDof,obj.nDof);
            Mmat = M(obj.freeDof, obj.freeDof);
        end
        function [Ke,rid,cid,Kmat] = GetElement_StiffnessMatrix(obj,eleIds)
            % This function is used to update the stiffness martix
            % of a structure. This is meant to be used in the case that
            % there has only been a local change to an element. This avoids
            % reassmbly the entire stiffness matrix
            
            % Allocate memory
            Kc = cell(length(eleIds),1); ridc = Kc; cidc = Kc;

            % Loop through element-by-element
            for ii = 1:length(eleIds)
                Kc{ii} = obj.element{eleIds(ii)}.BuildStiff([]);
                ridc{ii} = obj.element{eleIds(ii)}.rid;
                cidc{ii} = obj.element{eleIds(ii)}.cid;
            end
             
            % Covnert to vectors 
            Ke = cell2mat(Kc); rid = cell2mat(ridc); cid = cell2mat(cidc);
            
            % Build sparse matrix
            K = sparse(rid, cid,Ke, obj.nDof,obj.nDof);
            Kmat = K; %K(obj.freeDof, obj.freeDof);

        end
        function [Kne,Kte,rid,cid,Kn,Kt] = GetElement_TangentStiffness(obj, xIn, eleIds)
            % This function is used to update the tangent stiffness martix
            % of a structure. This is meant to be used in the case that
            % there has only been a local change to an element. This avoids
            % reassmbly the entire tangent stiffness matrix
            
            % Allocate memory
            Knc = cell(length(eleIds),1); Ktc = cell(length(eleIds),1);
            ridc = cell(length(eleIds),1); cidc = cell(length(eleIds),1);
            
            % Loop through element-by-element
            for ii = 1:length(eleIds)
                tempNodeVec = ([obj.element{eleIds(ii)}.nodes{:}]);
                xEle = xIn([tempNodeVec.globalDof]);
                [Knc{ii},Ktc{ii}] = obj.element{eleIds(ii)}.BuildNLStiff(xEle);
                ridc{ii} = obj.element{eleIds(ii)}.rid;
                cidc{ii} = obj.element{eleIds(ii)}.cid;
            end
             
            % Covnert to vectors 
            Kne = cell2mat(Knc); Kte = cell2mat(Ktc);
            rid = cell2mat(ridc); cid = cell2mat(cidc);
            
            % Build sparse matrix
            Kn = sparse(rid, cid, Kne, obj.nDof,obj.nDof);
            Kt = sparse(rid, cid, Kte, obj.nDof,obj.nDof);
            Kn = Kn(obj.freeDof, obj.freeDof);
            Kt = Kt(obj.freeDof, obj.freeDof);
        end
      
        function [Kn,Kt] = GetTangentStiffnessPar(obj, xIn,varargin)
            
            % Allocate memory
            Knv = cell(length(obj.element),1); Ktv = Knv;
            
            % Add to parallel pool or recover parallel pool
            % this removes sending over the elements each time which
            % reduces communication overhead
            if isempty(obj.parallelOpts.pPool)
                sharedObj = parallel.pool.Constant(obj.element());
                obj.parallelOpts.pPool = sharedObj;
            else
                sharedObj = obj.parallelOpts.pPool;
            end
            eV = obj.element;
            
            % Loop through element-by-element in parallel
            tStart = tic;
            parfor (ii = 1:length(eV), obj.parallelOpts.numWorkers)
                tempNodeVec = ([eV{ii}.nodes{:}]);
                xEle = xIn([tempNodeVec.globalDof]); %#ok<PFBNS>
                [KnEle,KtEle] = eV{ii}.BuildNLStiff(xEle);
                Knv{ii} = [eV{ii}.rid, eV{ii}.cid, KnEle];
                Ktv{ii} = [eV{ii}.rid, eV{ii}.cid, KtEle];
            end
            tAssembly = toc(tStart);
            
            % Expand to matrix
            Knm = cell2mat(Knv);  Ktm = cell2mat(Ktv);
            
            % Build sparse matrices
            Kn = sparse(Knm(:,1),Knm(:,2),Knm(:,3), obj.nDof, obj.nDof);
            Kt = sparse(Ktm(:,1),Ktm(:,2),Ktm(:,3), obj.nDof, obj.nDof);
            
            % If additional argument is supplied output full matrix
            if nargin < 3
                Kn = Kn(obj.freeDof, obj.freeDof);
                Kt = Kt(obj.freeDof, obj.freeDof);
            end
            
        end
        
        function [Kss] = GetStressStiffness(obj,s0,varargin)
            % Assemble the nonlinear stiffness matrices at a given
            % deformation
            
            % Allocate memory
            kVec = zeros(obj.nMatrixEntries, 3);
            
            % If displacement is provided, update stress stiffness 
            % about deformed state
            if nargin > 2 
                xIn = varargin{1};
            else
                xIn = zeros(obj.nDof,1);
            end
            
            % Loop through element-by-element
            for ii = 1:length(obj.element)
                sEle = s0(ii,:);
                tempNodeVec = ([obj.element{ii}.nodes{:}]);
                xEle = xIn([tempNodeVec.globalDof]);
                if isa(obj.element{ii},'Spring')
                    kEle = zeros(size(obj.element{ii}.ind));
                else
                    [kEle] = obj.element{ii}.BuildStressStiff(sEle',xEle);
                end
                kVec(obj.element{ii}.ind,:) = [obj.element{ii}.rid,...
                        obj.element{ii}.cid, kEle];
            end
            
            % Build sparse matrices
            Kss = sparse(kVec(:, 1), kVec(:, 2), kVec(:, 3), obj.nDof, obj.nDof);
            
            % If additional argument is supplied output full matrix,
            % otherwise ouput only at the free DOF.
            if nargin < 4
                Kss = Kss(obj.freeDof, obj.freeDof);
            end
            
            
        end
      
        function [S,E,P] = ComputeThermalLoads(obj,xIn,tIn,varargin)
           % Compute thermal loads of a FE model
           
           % Initialzie variables (stress, strain and nodal loads)
           S = zeros(length(obj.element),3);
           E = zeros(length(obj.element),3);
           P = zeros(obj.nDof,1);
           
           % Loop through element-by-element
            for ii = 1:length(obj.element)
                tempNodeVec = ([obj.element{ii}.nodes{:}]);
                xEle = xIn([tempNodeVec.globalDof]);
                tEle = tIn([tempNodeVec.id]);
                if isa(obj.element{ii},'Spring')
                    Sele = zeros(1,3); Eele = zeros(1,3);
                    Pele = zeros(obj.element{ii}.nDof,1);
                else
                    if ~isempty(varargin)
                        [Sele,Eele,Pele] = obj.element{ii}.ComputeThermalLoads(tEle,xEle,varargin);
                    else
                        [Sele,Eele,Pele] = obj.element{ii}.ComputeThermalLoads(tEle,xEle);
                    end
                end
                S(ii,:) = Sele; E(ii,:) = Eele;
                P([tempNodeVec.globalDof]) = P([tempNodeVec.globalDof]) + Pele;
            end
            
        end

        function [S] = ComputeStress(obj,xIn)
           % Compute stress of the finite element model given a
           % displacement field. 
           
           % Initialzie variables (stress, strain and nodal loads)
           S = zeros(length(obj.element),3);
           
           % Loop through element-by-element
           for ii = 1:length(obj.element)
               tempNodeVec = ([obj.element{ii}.nodes{:}]);
               xEle = xIn([tempNodeVec.globalDof]);
               [Sele] = obj.element{ii}.ComputeStress(xEle);
               S(ii,:) = Sele;
           end
           
        end
        function [S] = ComputeStressFromStrain(obj,xIn)
           % Compute stress of the finite element model given a
           % displacement field. 
           
           % Initialzie variables (stress, strain and nodal loads)
           S = zeros(length(obj.element),3);
           
           % Loop through element-by-element
           for ii = 1:length(obj.element)
               tempNodeVec = ([obj.element{ii}.nodes{:}]);
               xEle = xIn([tempNodeVec.globalDof]);
               [Sele] = obj.element{ii}.ComputeStressFromStrain(xEle);
               S(ii,:) = Sele;
           end
           
        end
        function [E] = ComputeStrain(obj,xIn)
           % Compute strains of the finite element model given a
           % displacement field.
           
           % Initialzie variable
           E = zeros(length(obj.element),9);
           
           % Loop through element-by-element
           for ii = 1:length(obj.element)
               tempNodeVec = ([obj.element{ii}.nodes{:}]);
               xEle = xIn([tempNodeVec.globalDof]);
               [Eele] = obj.element{ii}.ComputeStrain(xEle);
               E(ii,:) = Eele;
           end
           
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%% "GET" METHODS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        function [mass,varargout] = GetMass(obj)
           % This function computes the mass of the finite element model
           % Loop through element-by-element
           
            massVec = zeros(length(obj.element),1);
            for ii = 1:length(obj.element)
                massVec(ii) = obj.element{ii}.GetMass();
            end
            mass = sum(massVec);
            
            if nargout > 1
                varargout{1} = massVec;
            end
        end
        function [vol,varargout] = GetVolume(obj)
           % This function computes the volume of the finite element model
           % Loop through element-by-element
            volVec = zeros(length(obj.element),1);
            for ii = 1:length(obj.element)
                volVec(ii) = obj.element{ii}.GetVolume();
            end
            vol = sum(volVec);
            if nargout > 1
                varargout{1} = volVec;
            end
        end
        function areas = GetArea(obj)
            areas = zeros(length(obj.element),1);
            for ii = 1:length(obj.element)
                areas(ii) = obj.element{ii}.GetArea();
            end
        end
        function dvdt = GetVolGradient(obj)
            dvdt = zeros(length(obj.element),1);
            for ii = 1:length(obj.element)
                dvdt(ii) = obj.element{ii}.material.getProp('rho')*obj.element{ii}.GetArea();
            end
        end 
        function nodeArray = GetNodes(obj, nodeIds)
            % Return a cell array containing the requeted node IDS.
            nodeIndices = obj.GetNodeIndex(nodeIds);
            nodeArray = deal(obj.node(nodeIndices));
        end
        
        function nodeId = GetNodeId(obj, nodeIdx)
            %    nodeId = GetNodeId(nodeIdx)
            %
            % Given absolute node numbers for this part, return actual node
            % numbers. 
            % 
            % For example, if the actual node numbers of a part
            % are [1333, 1335, 1337], their absolute node numbers are [1,
            % 2, 3]. The call to "obj.getNodeId(2)" returns "1335."
            %
            % INPUT
            %
            % absNodeNumber: Absolute node numbers of interest.
            %
            % OUTPUT
            %
            % nodeNum: Actual node number corresponding to "absNodeNumber"
            
            [m, n] = size(nodeIdx); 
            if (m > 1) && (n > 1); error('nodeIdx must be a vector'); end
            nodeId = zeros(size(nodeIdx));
            for i = 1:length(nodeId)
                % Write this way to extend to struct. 
                nodeId(i) = obj.node{i}.id; 
            end

        end
        
        function nodeIndex = GetNodeIndex(obj, nodeId)
            %    nodeIndex = getNodeIndex(nodeId)
            %
            % Given actual node numbers for this part, return absolute node
            % numbers.
            %
            % For example, if the actual node numbers of a part
            % are [1333, 1335, 1337], their absolute node numbers are [1,
            % 2, 3]. The call to "obj.getAbsNodeNumber(1335)" returns "2."
            %
            % INPUT
            %
            % nodeId: Actual node numbers of interest.
            %
            % OUTPUT
            %
            % nodeIndex: Absolute node number corresponding to "nodeId"
            
            [m, n] = size(nodeId); 
            if (m > 1) && (n > 1); error('nodeId must be a vector'); end
            nodeIndex = zeros(size(nodeId));
            for i = 1:length(nodeId)
                nodeIndex(i) = obj.nodeId2Ind(nodeId(i));
            end
        end
        
        function dofId = GetDofId(obj, dofIndex)
            % Given a length-n vector of DOF Indices, return an n x 2
            % vector of [node, DOF] entries. 
            error('Not implemented');
            [m, n] = size(dofIndex); 
            if (m > 1) && (n > 1); error('nodeIds must be a vector'); end
            
            n = length(dofIndex);
            dofId = zeros(n, 2);
            
            for i = 1:n
                dofId(i, :) = obj.dof(dofIndex(i), :);
            end
        end
            
        function dofIndex = GetDofIndex(obj, nodeIds, dofs)
            % Given a length-n vector of node IDs, return a length(dofs)*n
            % vector of global DOF indices. If dofs is not supplied then all 6
            % nodal DOF are returned. 
            
            % Input checks
            if (nargin < 3)
                dofs = 1:6;
            end
            
            [m, n] = size(nodeIds); 
            if (m > 1) && (n > 1); error('nodeIds must be a vector'); end
            [m, n] = size(dofs); 
            if (m > 1) && (n > 1); error('dofs must be a vector'); end
            
            
            n = length(nodeIds); m = length(dofs);
            dofIndex = zeros(m*n, 1);
            inds = 1:m;
            
            for i = 1:n
                nodeInd = obj.nodeId2Ind(nodeIds(i));
                globalDof = obj.node{nodeInd}.GetGlobalDof();
                dofIndex((i - 1)*m + inds) = globalDof(dofs);
            end
        end
        function dof = GetSetDof(obj,setName)
            nSet = obj.nset(setName);
            nSet = [nSet{:}];
            dof = [nSet.globalDof];
        end
        function dof = GetFixedDof(obj)
            dof = find(obj.freeDof==0);
        end

        
        function Plot(obj, x, normMethod, varargin)
            
            % Overloaded call to plot function. Additional name/value plot
            % property pairs will be carried forward.
            %
            % obj.Plot(x, normMethod, varargin)
            %   "x" is the displacement field to plot
            %   "normMethod" = 'norm' 'x' 'y' 'z'...
            %   varargin{1}=gca or a force field, not documented yet, see
            %   code...
            %   
            %
            %
            % i.e. Plot
            
            % Use "patch" to make the plot. Key tasks are stripping out all
            % empty values from the element connectivity matrix, and
            % re-ordering the values in the element connectivity matrix to
            % correspond to a sorted order of nodes.
            
            % varargin: Name/value pairs corresponding to the "patch"
            % command. (MSA - this doesn't seem to work.)
            
            % Make empty arrays of vertices and elements
            vertices = zeros(obj.nNode, 3);
            shellEles = zeros(length(obj.element), 4);
            springEles = zeros(length(obj.element), 2);
            beamEles = zeros(length(obj.element), 2);
            
            % Check if field is provided,, if it is check if it is 
            % dof based or element based
            if nargin < 2 % 
                x = zeros(obj.nDof, 1); def = reshape(x,6,[]);
            elseif length(x) == obj.nDof
                fData = 'dof'; def = reshape(x,6,[]);
            elseif length(x) == obj.nElement
                fData = 'element';
            end
            
            % Normalization method
            if nargin < 3; normMethod = 'norm'; end
            
            % Update axes handle
            if nargin > 3 
                axHandle = varargin{1}; 
            else
                axHandle = gca;
            end
               
            % Determine if plotting constraints
            clampedVertices = [];
            for i = 1:obj.nNode
                gdof = obj.node{i}.globalDof;
                vertices(i, :) = obj.node{i}.GetXYZ()' + x(gdof(1:3))';
                if any(obj.node{i}.constraints==0)
                    clampedVertices = [clampedVertices; obj.node{i}.GetXYZ()'];
                end
            end
            
            % Determine if plotting forces
            if nargin > 3 && obj.plotOpts.forces
                forceVec = varargin{1};
                if size(forceVec) ~= obj.nDOF
                    if size(forceVec,2) == 2
                        forceVal = forceVec(:,1);
                        forceDof = forceVec(:,2);
                    else
                        error('Inccorrect force vector size')
                    end
                else
                    
                end
            end
            
            % Grab element indicies for plotting
            shellEleId = 1; springEleId = 1; beamEleid = 1;
            for j = 1:length(obj.element)
                if isa(obj.element{j},'S4S') || isa(obj.element{j},'S4BDI')
                    tempEleVec = ([obj.element{j}.nodes{1:end}]);
                    shellEles(j, :)  = [tempEleVec.id];
                    shellEleId = shellEleId + 1;
                elseif isa(obj.element{j},'Spring')
                    tempEleVec = ([obj.element{j}.nodes{1:end}]);
                    springEles(j,:) = [tempEleVec.id];
                    springEleId = springEleId + 1;
                elseif isa(obj.element{j},'B2')
                    tempEleVec = ([obj.element{j}.nodes{1:end}]);
                    beamEles(j,:) = [tempEleVec.id];
                    beamEleid = beamEleid + 1;
                end
            end
            
            % Remove zeros
            [x2,~] = find(shellEles);  shellEles = shellEles(x2(1:end/2),:);
            [x3,~] = find(springEles); springEles = springEles(x3(1:end/2),:);
            [x4,~] = find(beamEles); beamEles = beamEles(x4(1:end/2),:);
            
            % Get a length scale for plotting purposes
            h = patch('Vertices', vertices,...
                      'Faces', shellEles,'Parent',axHandle);
            
            % Formatting
            set(h, 'MarkerEdgeColor',[0 0 0], ...
                    'Marker','None', ...
                    'FaceColor',[0.6 0.6 1], ...
                    'FaceAlpha', 0.5, ...
                    'EdgeColor',[0 0 0.3]);
            axHandle.XLabel.String = '\bfX';
            axHandle.YLabel.String = '\bfY';
            axHandle.ZLabel.String = '\bfZ';
%             hold on
%             axis equal
            
            
            % Plot Springs
            for jj = 1:size(springEles,1)
                springDef = def(:,springEles(jj,:));
                xData = [1; 1].*[vertices(springEles(jj,1),1),...
                                 vertices(springEles(jj,2),1)];
                yData = [1; 1].*[vertices(springEles(jj,1),2),...
                                 vertices(springEles(jj,2),2)];
                zData = [1; 1].*[vertices(springEles(jj,1),3),...
                                vertices(springEles(jj,2),3)];
                switch normMethod
                    case {'X', 'x'}
                        cData = [1; 1].*springDef(1,:);
                    case {'Y', 'y'}
                        cData = [1; 1].*springDef(2,:);
                    case {'Z', 'z'}
                        cData = [1; 1].*springDef(3,:);
                    case {'norm', 'mag'}
                        cData = dot([springDef(1,1), springDef(2,1),...
                                    springDef(3,1)],...
                                    [springDef(1,2), springDef(2,2),...
                                    springDef(3,2)], 2).^(1/2);
                        cData = ones(2).*cData;
                    otherwise
                        error('Norm method %s not recognized', string(normMethod));
                end
                s{jj} = surface(xData,yData,zData,real(cData),...
                        'facecol','no','edgecol','interp','linew',2,...
                        'Marker','s','MarkerSize',15,'Parent',axHandle);
            end

            
            % Plot beam elements 
            for jj = 1:size(beamEles,1)
                beamDef = def(:,beamEles(jj,:));
                xData = [1; 1].*[vertices(beamEles(jj,1),1),...
                                 vertices(beamEles(jj,2),1)];
                yData = [1; 1].*[vertices(beamEles(jj,1),2),...
                                 vertices(beamEles(jj,2),2)];
                zData = [1; 1].*[vertices(beamEles(jj,1),3),...
                                vertices(beamEles(jj,2),3)];
                switch normMethod
                    case {'X', 'x'}
                        cData = [1; 1].*beamDef(1,:);
                    case {'Y', 'y'}
                        cData = [1; 1].*beamDef(2,:);
                    case {'Z', 'z'}
                        cData = [1; 1].*beamDef(3,:);
                    case {'norm', 'mag'}
                        cData = dot([beamDef(1,1), beamDef(2,1), beamDef(3,1)],...
                                    [beamDef(1,2), beamDef(2,2), beamDef(3,2)], 2).^(1/2);
                        cData = ones(2).*cData;
                    otherwise
                        error('Norm method %s not recognized', string(normMethod));
                end
                b{jj} = surface(xData,yData,zData,real(cData),...
                        'facecol','no','edgecol','interp','linew',2,...
                        'Marker','o','MarkerSize',8,'Parent',axHandle);
            end
            
            % Handle colors, if displacement was supplied
            if nargin >= 2 && ~isempty(shellEles)
                if strcmp(fData,'dof')
                    nd = 6;
                    switch normMethod
                        case {'X', 'x'}
                            cdata = x(1:nd:end);
                        case {'Y', 'y'}
                            cdata = x(2:nd:end);
                        case {'Z', 'z'}
                            cdata = x(3:nd:end);
                        case {'norm', 'mag'}
                            cdata = dot([x(1:nd:end), x(2:nd:end), x(3:nd:end)],...
                                [x(1:nd:end), x(2:nd:end), x(3:nd:end)], 2).^(1/2);
                        otherwise
                            error('Norm method %s not recognized', string(normMethod));
                    end
                    set(h, 'FaceVertexCData', cdata, 'FaceColor', 'Interp');
                elseif strcmp(fData,'element')
                    set(h, 'CData', x, 'FaceColor', 'Interp');
                end
                    
            end
            
            % Plot constraints if any 
            if ~isempty(clampedVertices) && obj.plotOpts.constraints
                plot3(clampedVertices(:,1),...
                    clampedVertices(:,2),...
                    clampedVertices(:,3),...
                    'r^','MarkerSize',8,'MarkerFaceColor','r',...
                    'Parent',axHandle)
            end
            
            % Plot forces if any
            
        end
        
        function uimplot(obj, x,varargin)
            
            % Send displacement field 'x' to uimplot5 along with model data
            % to create an interactive plot.
            %
            % obj.uimplot(x)
            %   "x" is the displacement field to plot
            %
            % obj.uimplot(phi,fn)
            %   "phi" is the displacement field to plot
            %   "fn" contains the frequencies to label each deformation.
            %
            % Gordon & Hollkamp's uimplot5 script must be on the path.
            %
            % Warning - this doesn't check against the DOF vector.  If the
            % model doesn't have 6 DOF per node then it may not work
            % correctly.
            
            if nargin>2
                fn=varargin{1};
            else
                fn=[];
            end
            % Get compatible nodes array
            [nodes] = GetNodesArray(obj);
            % Get compatible eles array
            [eles] = GetElesArray(obj);
            uimplot5({nodes,eles,fn,x});

        end
        
        % --- Misc Functions ---
        function [nodeid] = findNODEfromXYZ(obj,xyz)
            % This function takes in a set of xyz coordinates and finds the
            % corresponding nodeid.
            %
            % Inputs :
            %           obj
            %           xyz = matrix of nodal coordinates [x(:),y(:),z(:)]
            % 
            % CVD edited to find closest distance rather than exact node
            
            % Initialization
            ncoords = size(xyz,1);  nodeid = zeros(ncoords,1);
            
            % Grab nodes and extract coordinates
            tempNodes = [obj.node{:}]; tempXYZ = tempNodes.GetXYZ()';	
            % Loop through and use logical indexing to find corrresponding nodes
            for ii = 1:ncoords
                nodeidtemp = find(tempXYZ(:,1)==xyz(ii,1) & tempXYZ(:,2)==xyz(ii,2) & tempXYZ(:,3)==xyz(ii,3), 1);
                if isempty(nodeidtemp)
                    warning([' X-Y-Z Coordinates Provided Do Not Match with Existing Nodes. ',...
                            'Lookin For Closest Node'])
                    xyzDist = tempXYZ - xyz(ii,:); minNormDist = inf;
                    for jj = 1:length(tempXYZ)
                        normDist = norm(xyzDist(jj,:));
                        if normDist < minNormDist
                            nodeidtemp = jj; minNormDist = normDist;
                        end
                    end
                    nodeid(ii) = nodeidtemp;
                else
                    nodeid(ii) = nodeidtemp;
                end
            end
            
            
        end
        function [nodeId] = GetNodeFromDof(obj,dofNum)
            % This function takes in a set of xyz coordinates and finds the
            % corresponding nodeid.
            %
            % Inputs :
            %           obj
            %           dofNum = matrix of nodal coordinates [x(:),y(:),z(:)]
            %
            % MSA: This function seems to be obsolete - can we delete it?
            
            % 
            vecInd = find(obj.dof(:,2)==dofNum);
            nodeId = obj.dof(1,vecInd); %#ok<FNDSB>
        end
        function [vertices] = GetNodeVertices(obj)
            % output all the vertices of the model
            vertices = zeros(obj.nNode,3);
            for i = 1:obj.nNode
                vertices(i, :) = obj.node{i}.GetXYZ()';
            end
        end
        function [nodes] = GetNodesArray(obj)
            % Output a matrix of node index and xyz coordinates compatible
            % with Hollkamp's routines such as uimplot5
            % 
            % [nodes] = GetNodesArray;
            %
            % nodes = [ID, x, y, z;
            %       ID, x, y, z;...]
            %
            nodes = zeros(obj.nNode,4);
            for i = 1:obj.nNode
                nodes(i,1) = obj.node{i}.id;
                nodes(i,2:4) = obj.node{i}.GetXYZ()';
            end
        end
        
        function [eles] = GetElesArray(obj)
            % Output a matrix of element definitions that is compatible
            % with Hollkamp's routines such as uimplot5
            % 
            % [eles] = GetElesArray
            %
            % eles = (element#  element-type  N1  N2  N3  N4  N5  N6  N7  N8)
            %   Allowable element types are:
            %   tria=1, line=2, quad=9, hex=12, pent=13.
            %
            eles = zeros(obj.nElement,10);
            for i = 1:obj.nElement
                eles(i,1) = obj.element{i}.id;
                if obj.element{i}.nNode==3
                    eles(i,2)=1;
                elseif obj.element{i}.nNode==2
                    eles(i,2)=2;
                elseif obj.element{i}.nNode==4
                    eles(i,2)=9;
                elseif obj.element{i}.nNode==8
                    eles(i,2)=12;
                elseif obj.element{i}.nNode==5
                    eles(i,2)=13;
                end
                for k=1:obj.element{i}.nNode
                   eles(i,2+k) = obj.element{i}.nodes{k}.id;
                end
            end
        end
        
        % Overload functions
        function plot(obj, x, normMethod, varargin)
            %    plot(obj, x, normMethod, varargin)
            %
            % Redirects to Plot call
            obj.Plot(x, normMethod, varargin);
        end
        
        function disp(obj)
            
            fprintf('OsFern Object\n');
            
            fprintf('\tName: %s\n', obj.name);
            
            fprintf('\t%i nodes\n', obj.nNode);
            fprintf('\t%i DOF\n', obj.nDof);
            fprintf('\t%i elements\n', obj.nElement);
            
            % TODO: Update to find actual size
            % From: https://www.mathworks.com/matlabcentral/answers/14837-how-to-get-size-of-an-object
            % Do you mean something like this: 
            % function GetSize(this) 
            % props = properties(this); totSize = 0; 
            % for ii=1:length(props) 
            % currentProperty = getfield(this, char(props(ii))); 
            % s = whos('currentProperty'); 
            % totSize = totSize + s.bytes;	
            % end fprintf(1, '%d bytes\n', totSize);	
            % end
            byteSize = whos('obj'); byteSize = byteSize.bytes;
            fprintf('\tSize of %.3f mb\n', byteSize/1024^2);
            fprintf('To see all available data/properties type: fieldnames(%s)\n',obj.name);
            fprintf('To see list of available functions (methods) type help OsFern');
            
        end
          
        function delete(obj)
           % Clean up some specific items on deletion.
            
           delete(obj.nodeId2Ind);
           delete(obj.elementId2Ind);
           
        end
            
    end
    
end
