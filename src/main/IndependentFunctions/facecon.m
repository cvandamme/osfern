function [faces,iface] = facecon(xyz,ele,irem);
% function [faces,iface] = facecon(xyz,ele,irem);
% R Gordon   8/15/97
%           12/22/00 non-sequential node numbers
%             1/5/04 line elements & remove shared HEX faces
%
% Generates a matrix, faces, of element face node connectivity for 
% MPLOT5.M. Converts HEXs and PENTs into 6 and 5 face elements,
% respectively. Optionally, faces shared by adjacent solid elements
% are removed. Line (beam/bar) elements are included as 2-node faces.
% Also returns a row vector, iface, relating each face to its element
% row number in matrix, ele.
%
% Input arguments:
%   xyz   - matrix of node coordinates
%   ele   - matrix of element node connectivity
%   irem  - switch for removal of shared HEX element faces,
%           1 = on (default), 0 = off
%
% Output arguments:
%   faces - matrix of planar face element connectivity
%   iface - vector of element row numbers in matrix, ele, corresponding
%           to faces
%           
% Useage:  edges = solidege(xyz,ele,faces,iface);
%
if nargin < 3
  irem = 1;
end

nnode = size(xyz,1);
hex_ind = [1 2 3 4 ;1 5 6 2; 5 8 7 6 ;2 6 7 3; 1 4 8 5 ;3 7 8 4];
pent_ind = [1 2 3 1;4 5 6 4;1 2 5 4;2 3 6 5;3 1 4 6];

iline = find(ele(:,2)==2);  % lines
nline = length(iline);
itri = find(ele(:,2)==1);  % triangles
ntri = length(itri);
iquad = find(ele(:,2)==9 | ele(:,2)==20);  % quads and fluid4s
nquad = length(iquad);
ipent = find(ele(:,2)==13);  % pentas
npent = length(ipent);
ihex = find(ele(:,2)==12 | ele(:,2)==21);  % hexs and fluid8s
nhex = length(ihex);
ntt = nline+ntri;
ntq = ntt+nquad;
faces = zeros(ntq,4);
iface = zeros(1,ntq);

irow = zeros(1,max(xyz(:,1)));
irow(xyz(:,1)) = 1:nnode;

% add line faces (lines)
faces(1:nline,1:4) = irow(ele(iline,[3 4 3 4]));
iface(1:nline) = iline;

% add triangle faces
faces(nline+1:ntt,1:4) = irow(ele(itri,[3 4 5 3]));
iface(nline+1:ntt) = itri;

% add quad faces
faces(ntt+1:ntq,:) = irow(ele(iquad,3:6));
iface(ntt+1:ntq) = iquad;

% add solid element faces
nth = 6*nhex;
nts = nth+5*npent;
ftmp = zeros(nts,4);
itmp = zeros(1,nts);

% hex faces
for i=1:6
  ftmp(i:6:nth-6+i,:) = irow(ele(ihex,hex_ind(i,:)+2));
  itmp(i:6:nth-6+i) = ihex;
end

% penta faces
for i=1:5
  ftmp(nth+i:5:nts-5+i,:) = irow(ele(ipent,pent_ind(i,:)+2));
  itmp(nth+i:5:nts-5+i) = ipent;
end

if irem  % eliminate shared pent & hex faces
  [ftnew,itnew] = facerem(ftmp,itmp);
  faces = [faces;ftnew];
  iface = [iface itnew];
else
  faces = [faces;ftmp];
  iface = [iface itmp];
end

