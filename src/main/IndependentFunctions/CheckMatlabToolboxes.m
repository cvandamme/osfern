function [] = CheckMatlabToolboxes()

verInfo = ver;

% Required toolboxes
parBoxCheck = false;
optBoxCheck = false; 
statBoxCheck = false; 
sigBoxCheck = false; 


for ii = 1:length(verInfo)
    
    switch verInfo(ii).Name
        case {'Parallel Computing Toolbox'}
            parBoxCheck = true;
        case {'Optimization Toolbox'}
            optBoxCheck = true; 
        case {'Statistics and Machine Learning Toolbox'}
            statBoxCheck = true;
        case {'Signal Processing Toolbox'}
            sigBoxCheck = true;
        otherwise
    end
end


if ~parBoxCheck
    warning(['Wont be able to use any parallelization utilities, they',... 
            'rely on the parallelization toolbox'])
end
if ~optBoxCheck
    warning(['Wont be able to use model uptating script, they rely on',... 
            'optimization toolbox'])
end

end
