function J = MatchModes(pC,fnTarget,paramVec,feObj,mind,p0,varargin)
% This function updates the parameters of a FE model in order to obtain
% a prescriebd set of nonlinear coefficients through the IC method.
% Inputs : 
%       pC = Current parameters used in the optimization procedure
%       Klin_target = Linear stiffness targets - matrix of vector of
%                     diagonals
%       Knl_target = Nonlinear stiffness targets - matrix
%       paramVec = Vector of design variables
%       feObj = finite element object
%       mind = modes to use 
%       p0 = initial parameter values
%       varargin{1} ; vector of weighting coefficients for the modes
% Outputs :
%       J = objective function value
%       dJ = gradient value;

% Grab weighting if supplied
if nargin > 6
    W = diag(varargin{1});
else
    W = eye(length(fnTarget));
end

% Update the design variables
for ii = 1:length(pC)
    paramVec{ii}.SetVal(pC(ii)*p0(ii));
end

% Update the fe model
feObj.Update(true);

% Update the fe model
% figure; feObj.Plot();

% Create modal object and solve
modal = Frequency('freq',feObj); modal.Solve(20);

% Compute the error
errVec = ((fnTarget - modal.fn(mind)).^2)./fnTarget;

% Compute the objective function 
J = errVec'*W*errVec;

end