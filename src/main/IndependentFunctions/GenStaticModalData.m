function [uModal,fModal,tlist]=GenStaticModalData(freq,shapes,force,disp,controls,varargin)
%
%  function [Knl,tlist]=noregress(freq,shapes,force,disp,controls,flist);
%
%  function [Knl,tlist]=noregress(freq,phi,force,disp,controls,flist,phiTM);
%
%   Fit a McEwan type of model without regression.
%
%   INPUTS:
%     freq or K			stiffness: either a list of Modal Frequencies [Hz]
%							(for modes of interest), or a linear stiffness
%							matrix K if freq --(no. modes x 1)  if K
%							(no. modes X no. modes) if (1 X 1) stiffness is
%							assumed to be in form of freq
%     shapes			Matrix of Mode Shapes (phi) (no. nodes X no. modes)
%     force				Static Force Cases (no. nodes X no. cases)
%     disp				Static Displacements (no. nodes x no. cases);
%     controls.quad		if quad==1; use quads in the fit (default=0);
%     controls.triple	if triple==1; use 123 type cubic terms (default=0);
%     flist				fixed list of terms, same format as tlist; (default=[])
%     phiTM				Matrix of phi.'*M where M is the mass matrix
%							(no. modes X no. nodes)
%
%   Note that there may be some inaccuracy associated with calling the
%   function using shapes instead of phiTM, but that is allowed for cases
%   where the FEA mass matrix is inaccessible.
%
%   OUTPUT
%     Knl		nonlinear coefficients (1 X nterms);
%     tlist		nonlinear term list  (nterms X 3)
%               	i.e. [1 1 0; 1 1 1; 1 1 2; ...]	means Knl are
%					coefficients in front of u1^2, u1^3, u2*u1^2, etc
%
%   version 3.0   JJH  original 8/21/02; add 123 cubic cross terms 8/12/05
%                      add option for K instead of freq 1/16/08
%   Updated by MSA 10/9/2014, added phiTM, added fit check.
%   Updated by Chris Van Damme 04/19/2019 to only output the training data
%   uModal and fModal. This way it can later be used with more advanced
%   training routines.

% Defaults
if ~isfield(controls,'quad'); controls.quad = 1; end
if ~isfield(controls,'triple'); controls.triple = 1; end

if nargin > 5
    flist=varargin{1};
else
    flist=[];
end

if nargin > 6
    phiTM=varargin{2};
else
    % MSA: Here a pinv is being used when we should use phi.'*M.  This could
    % introduce inaccuracy!!!  I've added an option to correct it.
    % phiTM=(shapes/(shapes'*shapes))';
    phiTM= pinv(shapes); % this is approximate
end

% Fit Parameters
nmodes=size(shapes,2);	% Number of Shapes input
ncases=size(force,2);	% Number of Load Cases to use in fit

% Check if freq or K was input
if min(size(freq)) == 1
    K=diag((2*pi*freq(1:nmodes)).^2);
else
    K=freq(1:nmodes,1:nmodes);
end

%  Make the term list
tlist=flist; qlist=[];
if isempty(tlist)
    if controls.triple == 0	% No 3 mode combinations ~(q1,q2,q3)
        clist=cubic_combinations([1:nmodes],0);
        
    else					% All possible combinations of 3 modes
        clist=cubic_combinations([1:nmodes],nmodes+1);
    end
    
    if controls.quad==1		% All possible combinations of 2 modes
        qlist=quad_combinations([1:nmodes]);
    end
    tlist=[qlist' clist']';	% concatenate all possible term listings
end
nterms=size(tlist,1);

%  first calculate the modal contributions to the force and disp
p=phiTM*disp;             % Dim: (nmodes x ncases)
mforce=shapes.'*force;    %(nmodes x ncases) Modal Force

% Form data matrix with all the combinations present
data_matrix=zeros(ncases,nterms);
for ii=1:ncases
    for jj=1:nterms
        if tlist(jj,3) == 0		% Quadratic Terms
            data_matrix(ii,jj)=p(tlist(jj,1),ii)*p(tlist(jj,2),ii);
            
        else					% Cubic Terms
            data_matrix(ii,jj)=p(tlist(jj,1),ii)*p(tlist(jj,2),ii)*p(tlist(jj,3),ii);
        end
    end
end

% Solve static probelm for nonlinear force
Fnl = mforce - K * p;
fModal = Fnl';						% Dim: (ncases X nmodes)
uModal = data_matrix;
end