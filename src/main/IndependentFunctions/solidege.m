 function edges = solidege(xyz,ele,faces,iface,angle)
% SOLIDEGE.M - Find visible edges of solid models
% edges = solidege(xyz,ele,faces,iface,angle);
% R Gordon  1/8/04
%
% Compute the matrix of visible edge connectivity, edges, for a patch
% object of TRIA, QUAD, PENTA and HEX elements.  Used by UIMPLOT5.M 
% to provide rudimentary solid model viewing.  Input arguments are
% the node coordinate matrix, xyz, the element connectivity matrix, ele,
% and the face connectivity, faces/iface (from FACECON.M).
%
% Input arguments:
%   xyz   - matrix of node coordinates
%   ele   - matrix of element node connectivity
%   faces - matrix of planar face element connectivity
%   iface - vector of element row numbers in matrix, ele, corresponding
%           to faces
%   angle - angle between normal vectors of adjacent planar elements
%           above which the shared edge should be visible (default = 30)
%
% Output arguments:
%   edges - matrix of visible edge line connectivity (No. of lines X 2)
%           
% Useage:  edges = solidege(xyz,ele,faces,iface);
%
% To plot the resulting edge lines for a solid object,
%  h1 = patch('Vertices',xyz(:,2:4),'Faces',edges);
%

t0 = clock;

if nargin < 5  % set the default angle threshold
  angle = 30;
end
cosangle = cos(pi*angle/180);

% eliminate face rows which are line elements
elrow = find(ele(:,2) ~= 2);
tmp = ismember(iface,elrow);
elind = find(tmp);

if ~isempty(elind)  % some non-line elements present
  faces = faces(elind,:);
  iface = iface(elind);

  % break down face elements into edges
  nface = size(faces,1);
  l = zeros(nface*4,4);
  l(:,1:2) = [faces(:,1:2)' faces(:,2:3)' faces(:,3:4)' faces(:,[1 4])']';

  % add iface vector as 3rd column
  l(:,3) = [iface iface iface iface]';

  % add vector indexing each edge (row) to row in faces matrix
  l(:,4) = [1:nface 1:nface 1:nface 1:nface]';

  % do some sorting
  [r,c] = size(l);
  [y,i1] = sort(l(:,1));
  ls = l(i1,:);

  ls2=(sort(ls(:,1:2)'))';
  ls2 = [ls2 ls(:,3:4)];

  [y2,i2]=sort(ls2(:,1));
  ls3 = ls2(i2,:);

  ls4 = sortrows(ls3);

  % eliminate repeated edges
  lind = find(ls4(1:r-1,1)-ls4(2:r,1) ==0 ...
              & ls4(1:r-1,2)-ls4(2:r,2) == 0 ...
              & ls4(1:r-1,3)-ls4(2:r,3) == 0);

  edges = ls4(setdiff(1:r,lind),:);

  % generate index vectors of edges shared by 1-4 elements
  nr = size(edges,1);
  e2 = edges(1:nr-1,1:2)-edges(2:nr,1:2);
  i2 = find(e2(:,1)==0 & e2(:,2)==0)';
  e3 = edges(1:nr-2,1:2)-edges(3:nr,1:2);
  i3 = find(e3(:,1)==0 & e3(:,2)==0)';
  e4 = edges(1:nr-3,1:2)-edges(4:nr,1:2);
  i4 = find(e4(:,1)==0 & e4(:,2)==0)';

  j4 = i4;
  i4x = [j4 j4+1 j4+2 j4+3];
  j3 = setdiff(i3,i4x);
  i3x = [j3 j3+1 j3+2];
  j2 = setdiff(i2,[i3x i4x]);
  i2x = [j2 j2+1];
  j1 = setdiff(1:nr,[i2x i3x i4x]);

  % initialize matrix of edges for display
  pedges = zeros(nr,2);

  % turn on edges shared by 1 element
  pedges(1:length(j1),:) = edges(j1,1:2);
  ct = length(j1)+1;

  % turn on edges shared by 2 elements
  % first compute the normal vector for each face
  npan = size(faces,1);
  nvec = zeros(npan,3);
  for kk = 1:npan
    nvec(kk,:) = f_norm(xyz,faces(kk,:));
  end

  for ii = 1:length(j2)  % test each egge shared by 2 faces
    f1 = faces(edges(j2(ii),4),:);
    f2 = faces(edges(j2(ii)+1,4),:);
    if isequal(f1,f2)  % faces are coincident, add to list
      pedges(ct,:) = edges(j2(ii),1:2); % add to list
      ct = ct+1;
    else  % faces sharing edge not coincident, compare normals
      % find cos of angle between element surface normals
      n1 = nvec(edges(j2(ii),4),:);
      n2 = nvec(edges(j2(ii)+1,4),:);
      cos_n = n1*n2';
      if abs(cos_n) < cosangle
        pedges(ct,:) = edges(j2(ii),1:2); % add to list
        ct = ct+1;
      end
    end
  end

  % turn on edges shared by 3 elements
  pedges(ct:ct+length(j3)-1,:) = edges(j3,1:2);
  ct = ct+length(j3);

  % turn on edges shared by 4 elements
  for ii = 1:length(j4)
    edge_ele = ele(edges(j4(ii):j4(ii)+3,3)',2)';
    if ~isequal(edge_ele,[12 12 12 12])  % not four hexs
      if max(edge_ele) < 13 | sum(edge_ele) == 44 % no pents / 2Qs+2Ps
        pedges(ct,:) = edges(j4(ii),1:2); % add to list
        ct = ct+1;
      end
    end
  end

  % remove the unused rows
  edges = pedges(1:ct-1,:);

else
  edges = [];
end

% add in line elements
linrow = find(ele(:,2) == 2);
edges = [edges;ele(linrow,3:4)];

%disp(['Computation Time = ',num2str(etime(clock,t0)),' sec']);
