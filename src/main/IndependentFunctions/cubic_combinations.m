function clist=cubic_combinations(mlist,pset)
%
%        clist=cubic_combinations(mlist,pset);            % jjh 10-2-2001
%
%        mlist   modelist -- mode indices to find cubic combinations for
%        pset > max(mlist)  all combinations (default)
%        pset = 0           all combinations except 3 mode terms (McEwan, Cooper, et al.)
%        pset = any mode    keep only combinations with that mode present in comb.
%        pset = -any mode   keep only comb with any mode & eliminate 3 mode terms
%         
%  EXAMPLE: clist=cubic_combinations([1:4]);
%
if nargin < 2; pset=max(mlist)+1; end;
pset=round(pset);

mlist=sort(mlist);
nm=length(mlist);
if length(mlist) > 2
  nc=nm*nm+nchoosek(nm,3);
 else
  nc=nm*nm;
end
clist=zeros(nc,3);
if length(mlist) > 2
  clist(nm*nm+1:nc,:)=nchoosek(mlist,3);
end;

for ii=1:nm
 clist((ii-1)*nm+1:ii*nm,1:2)=mlist(ii);
 clist(ii:nm:nm*nm,3)=mlist(ii);
end

% whittle down the set 

if pset > max(mlist); return; end;        % keep all modes
if pset < 1; clist=clist(1:nm*nm,:);end     % eliminate 3 mode terms
if abs(pset) > 0;                         % keep only comb with a certain mode 
  p=abs(pset);
  ii=find(clist(:,1) == p | clist(:,2) == p | clist(:,3) == p);
  clist=clist(ii,:);
end