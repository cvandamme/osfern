function r = eqStatic_shallowShell(q, P, Gammax)
% Static equation of equilibrium for a shallow-shell Galerkin model. Static
% structural terms of Eq. 2.12 from Dowell.

n = length(q);

r = zeros(n, 1);

for s = 1:n
    qs = q(s);
    r(s) = 0.5*qs*(s*pi)^4 - P*(1 - (-1)^s)/(s*pi);
    nls = 0.5*qs*(s*pi)^2 + Gammax*(1 - (-1)^s)/(s*pi);
    
    for m = 1:n
        qm = q(m);
        nlm = 3*(m*pi)^2*qm^2 + 12*Gammax*qm*(1 - (-1)^m)/(m*pi);
        r(s) = r(s) + nls*nlm;
    end
end
    



