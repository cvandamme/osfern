function [P,Dshapes]=edloading_struct(shapes,deflection,dof_interest,controls)
%
%    [P,Dshapes]=edloading(shapes,deflection,dof_interest,controls);
%
%    Calculate the load cases for an enforced displacement ROM strategy.
%
%    Version 2.0 R Kuether 4-18-14
%    Version 3.0 C VanDamme 4-11-2019 - converted controls to structure   
%     NOTE: the modal decomposition is assumed for unity modal mass
%
%   Inputs:
%     shapes           mode shapes                     (ndof X nmodes)
%     deflection       vector of deflections           (nmodes X 1)     [if a single number is given, that is used each]
%     dof_interest     vector of dofs-of-interest in terms of a the global dof for each mode (nmodes X 1)
%                           [if a single dof is given, that dof is used for all]
%     controls.sf      singles_flag: set to 1 to include 1 0 0 type of loading, [Default = 1];
%     controls.df      doubles_flag: set to 1 to have 1 1 0 type of loading, [Default = 1];
%     controls.tf      triples_flag: set to 1 to have 1 2 3 type of loading, [Default = 1];
%     controls.rf      reduction_flag: set to 1 to cut doubles by 1/2, triples by 1/3;
%                      [Default = 0];
%     controls.mf      minimum_flag: if this flag is 1 - use the minimum set of loading in
%                      doubles (3 sets instead of 4) and triples (1 set instead of 8)
%                      [Default = 0]
%     controls.cf      constantforce_flag: if this flag is 1 - use the
%                      constant scaling method for the first mode in
%                      shapes(:,1)
%                      [Default = 0, which is constant displacement, uses
%                       displacements defined for all modes]
%
%   Outputs:
%     P                Permutations of load cases (nloadss x nmodes)
%     Dshapes          matrix of generated displacements  (ndof X nloads)

field_flag=2;
nmodes=size(shapes,2);

if nargin < 3 || isempty(dof_interest)
    temp=abs(shapes); temp(4:6:end,:)=0; temp(5:6:end,:)=0; temp(6:6:end,:)=0;
    [junk,dof_interest]=max(temp); dof_interest=dof_interest';
end

% Defaults
if ~isfield(controls,'sf'); controls.sf = 1; end
if ~isfield(controls,'df'); controls.df = 1; end
if ~isfield(controls,'tf'); controls.tf = 1; end
if ~isfield(controls,'rf'); controls.rf = 0; end
if ~isfield(controls,'mf'); controls.mf = 0; end
if ~isfield(controls,'cf'); controls.cf = 0; end

if max(size(dof_interest)) == 1; dof_interest=dof_interest*ones(nmodes,1); end
if max(size(deflection)) == 1;  deflection=deflection*ones(nmodes,1); end

a=zeros(1,nmodes);
if controls.cf == 0 % Constant Displacement
    for ii=1:nmodes
        a(ii) = abs(deflection(ii)/shapes(dof_interest(ii),ii)); % Modified for sign issue
    end
elseif controls.cf == 1 % Constant Scale
    for ii = 1:nmodes
        a(ii) = deflection(1)/shapes(dof_interest(1),1);
    end
else
    error('Need to define either constant force or constant displacement');
end

%
% form the permutation matrix
%

if controls.sf == 1
    singles=nchoosek([1:nmodes],1);nsingles=size(singles,1);
else
    nsingles=0;
end
if nmodes == 1
    ndoubles = 0;
    controls.df = 0;
    doubles = [];
    ntriples = 0;
    controls.tf = 0;
    triples = [];
else
    if controls.df == 1
        doubles=nchoosek([1:nmodes],2);ndoubles=size(doubles,1);
    else
        ndoubles = 0;
    end
    if controls.tf == 1
        triples=nchoosek([1:nmodes],3);ntriples=size(triples,1);
    else
        ntriples=0;
    end
end

if controls.mf == 0
    P=zeros(2*nsingles+4*ndoubles+8*ntriples,3);
else
    P=zeros(2*nsingles+3*ndoubles+ntriples,3);
end

% build the single permutations

pend=0;

if controls.sf == 1
    P(1:nsingles,1)=singles; pend=nsingles;
    P(pend+1:pend+nsingles,1)=-singles; pend=pend+nsingles;
end

single_marker=pend;

% the double permutations

if controls.df == 1
    P(pend+1:pend+ndoubles,1)=doubles(:,1);  P(pend+1:pend+ndoubles,2)=doubles(:,2);  pend=pend+ndoubles;
    P(pend+1:pend+ndoubles,1)=doubles(:,1);  P(pend+1:pend+ndoubles,2)=-doubles(:,2); pend=pend+ndoubles;
    P(pend+1:pend+ndoubles,1)=-doubles(:,1); P(pend+1:pend+ndoubles,2)=doubles(:,2);  pend=pend+ndoubles;
    
    if controls.mf==0
        P(pend+1:pend+ndoubles,1)=-doubles(:,1); P(pend+1:pend+ndoubles,2)=-doubles(:,2); pend=pend+ndoubles;
    end
end
double_marker=pend;

% the triple permutations

if controls.tf == 1
    P(pend+1:pend+ntriples,1)= triples(:,1); P(pend+1:pend+ntriples,2)= triples(:,2); P(pend+1:pend+ntriples,3)= triples(:,3); pend=pend+ntriples;
    
    if controls.mf== 0
        P(pend+1:pend+ntriples,1)= triples(:,1); P(pend+1:pend+ntriples,2)= triples(:,2); P(pend+1:pend+ntriples,3)=-triples(:,3); pend=pend+ntriples;
        P(pend+1:pend+ntriples,1)= triples(:,1); P(pend+1:pend+ntriples,2)=-triples(:,2); P(pend+1:pend+ntriples,3)= triples(:,3); pend=pend+ntriples;
        P(pend+1:pend+ntriples,1)= triples(:,1); P(pend+1:pend+ntriples,2)=-triples(:,2); P(pend+1:pend+ntriples,3)=-triples(:,3); pend=pend+ntriples;
        P(pend+1:pend+ntriples,1)=-triples(:,1); P(pend+1:pend+ntriples,2)= triples(:,2); P(pend+1:pend+ntriples,3)= triples(:,3); pend=pend+ntriples;
        P(pend+1:pend+ntriples,1)=-triples(:,1); P(pend+1:pend+ntriples,2)= triples(:,2); P(pend+1:pend+ntriples,3)=-triples(:,3); pend=pend+ntriples;
        P(pend+1:pend+ntriples,1)=-triples(:,1); P(pend+1:pend+ntriples,2)=-triples(:,2); P(pend+1:pend+ntriples,3)= triples(:,3); pend=pend+ntriples;
        P(pend+1:pend+ntriples,1)=-triples(:,1); P(pend+1:pend+ntriples,2)=-triples(:,2); P(pend+1:pend+ntriples,3)=-triples(:,3); pend=pend+ntriples;
    end
    
end


fprintf('\nThere are %g permutations of loading. \n', size(P,1))
for ii=1:size(P,1)
    fprintf('Subcase=%g Loading %g %g %g\n',[ii P(ii,1:3)])
end

% construct the loading

Dshapes=zeros(size(shapes,1),size(P,1));


for ii=1:size(P,1)
    
    scale_factor=1;
    if controls.rf == 1
        if ii < single_marker+1
            scale_factor=1.0;
        elseif ii < double_marker+1
            scale_factor=0.5;
        else
            scale_factor=0.3333333;
        end
    end
    
    for jj=1:3
        if P(ii,jj) ~=0
            Dshapes(:,ii)=Dshapes(:,ii) + sign(P(ii,jj)) * scale_factor * a(abs(P(ii,jj))) * shapes(:,abs(P(ii,jj)));
        end
        
    end
end
end
