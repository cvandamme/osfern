function [N1,N2,Nlist]=Knl2Nash(Knl,tlist);
%
%    [N1,N2,Nlist]=Knl2Nash(Knl,tlist);
%    
%    convert Knl to N1 and N2 in Nash's format
%    VERSION 2.0 Date: 6/22/11
%
%  INPUT:
%    Knl           nonlinear coefficients  (Neqn X Nnlterms)
%    tlist         nonlinear term list  (Nnlterms X 3)
%                    i.e. [1 1 0; 1 1 1; 1 1 2; ...] 
%                    means Knl are coefficients in front of q1^2, q1^3, q2*q1^2, ...
%  OUTPUT:
%    Nlist         list of the quadratic nonlinear coefficients combinations (Neqn X 2)
%    N1            quadratic NL matrix in Nash's format (Neqn,Neqn,Neqn)
%    N2            cubic NL matrix in Nash's format (Neqn,(Neqn+nchoosek(Neqn,2)),Neqn)
%                  
%   In theory:
%    Knl(u)= [1/2 * N1(u)+ 1/3 * N2(u,u)] * u
%     and
%    dKnl(u) = [N1(u) + N2(u,u)];
%      where N1(u) is N1 evaluated at u 
%      where N2(u,u) is N2 evaluated at the Nlist quad combos of u
%
%    EXAMPLE evaluation of N1(u) and N2(u,u):
%     u_nl = u(Nlist(:,1)).*u(Nlist(:,2));
%     for jj=1:Neqn;
%       N1_u(:,jj)= N1(:,:,jj) * u;
%       N2_u(:,jj)= N2(:,:,jj) * u_nl;
%     end;
%
[Neqn,Npar]=size(Knl);

% find which terms are quadratic and which are cubic in Knl

qindex=find(tlist(:,3) == 0); Nq=length(qindex);
cindex=find(tlist(:,3) ~= 0); Nc=length(cindex);
Nlist=quad_combinations([1:Neqn]);
% Nn=length(Nlist); % RJK changed, didn't work for 1 mode models
Nn=size(Nlist,1);
N1=zeros(Neqn,Neqn,Neqn); N2=zeros(Neqn,Nn,Neqn);

% set up a look_up table for the Nlist vector

look_up=zeros(Neqn,Neqn);
for jh=1:Nn;
  look_up(Nlist(jh,1),Nlist(jh,2))=jh;
  look_up(Nlist(jh,2),Nlist(jh,1))=jh;
end;

% find the matchs in between tlist and qlist

match1=zeros(Nq,1);
for jh=1:Nq
  match1(jh)=findrow_ALL(Nlist,tlist(qindex(jh),:));
end;

% set up N1

if Nq > 0;
 for jh=1:Nq;
  ih=match1(jh);
  N1(:,Nlist(ih,2),Nlist(ih,1))= N1(:,Nlist(ih,2),Nlist(ih,1))+...
         Knl(:,qindex(jh));
  N1(:,Nlist(ih,1),Nlist(ih,2))= N1(:,Nlist(ih,1),Nlist(ih,2))+...
         Knl(:,qindex(jh));
 end;
end;

% set up N2

 if Nc > 0
  for jh=1:Nc;
   N2(:,look_up(tlist(cindex(jh),2),tlist(cindex(jh),3)),tlist(cindex(jh),1))= ...
     N2(:,look_up(tlist(cindex(jh),2),tlist(cindex(jh),3)),tlist(cindex(jh),1))+...
      Knl(:,cindex(jh));
   N2(:,look_up(tlist(cindex(jh),1),tlist(cindex(jh),3)),tlist(cindex(jh),2))= ...
     N2(:,look_up(tlist(cindex(jh),1),tlist(cindex(jh),3)),tlist(cindex(jh),2))+...
      Knl(:,cindex(jh));
   N2(:,look_up(tlist(cindex(jh),1),tlist(cindex(jh),2)),tlist(cindex(jh),3))= ...
     N2(:,look_up(tlist(cindex(jh),1),tlist(cindex(jh),2)),tlist(cindex(jh),3))+...
      Knl(:,cindex(jh));
  end
 end

Nlist=Nlist(:,[1 2]);   % trim Nlist
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function clist=quad_combinations(mlist,pset)
%
%        clist=quad_combinations(mlist,pset);            % jjh 10-2-2001
%
%        pset > max(mlist)  all combinations (default)
%        pset = 0           eliminate cross terms
%        pset = any mode    keep only combinations with that mode present in comb.
%        pset = -any mode   keep only combinations with that mode present in comb.
%                           and eliminate cross terms 
%     
if nargin < 2; pset=max(mlist)+1; end;
pset=round(pset);

mlist=sort(mlist);
nm=length(mlist);
if nm ==1;
 clist=[1 1 0];
 return
end;
nc=nm+nchoosek(nm,2);
clist=zeros(nc,3);
clist(nm+1:nc,1:2)=nchoosek(mlist,2);

for ii=1:nm
 clist(ii,1:2)=mlist(ii);
end

% whittle down the set 

if pset > max(mlist); return; end;        % keep all modes
if pset < 1; clist=clist(1:nm,:);end     % eliminate 3 mode terms
if abs(pset) > 0;                         % keep only comb with a certain mode 
  p=abs(pset);
  ii=find(clist(:,1) == p | clist(:,2) == p);
  clist=clist(ii,:);
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function index=findrow_ALL(list,row);
%
%        find if row is in the matrix list
%        use all combinations of row

% combinations=PERMS(row);
combinations=perms(row);
combinations=flipud(combinations);

for ii=1:size(combinations,1)
  index=findrow(list,combinations(ii,:));
  if ~isempty(index);
   return
  end
end
return  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function n = findrow(A,x)
% n = findrow(A,x)
% R Gordon  8/28/02,  modified 1/30/03 JJH version 4.0
% 
% Finds the row number at which a row vector, x, occurs as a row in the 
% matrix, A.  Vector, x, must have the same number of elements as
% matrix, A, has columns.

n=[];
for ii = 1:size(A,1)
  if max(abs(A(ii,:)-x)) == 0
    n = ii;
    return
  end
end
return