function clist=quad_combinations(mlist,pset)
%
%        clist=quad_combinations(mlist,pset);            % jjh 10-2-2001
%
%        pset > max(mlist)  all combinations (default)
%        pset = 0           eliminate cross terms
%        pset = any mode    keep only combinations with that mode present in comb.
%        pset = -any mode   keep only combinations with that mode present in comb.
%                           and eliminate cross terms 
%     
if nargin < 2; pset=max(mlist)+1; end;
pset=round(pset);

mlist=sort(mlist);
nm=length(mlist);
if nm ==1;
 clist=[1 1 0];
 return
end;
nc=nm+nchoosek(nm,2);
clist=zeros(nc,3);
clist(nm+1:nc,1:2)=nchoosek(mlist,2);

for ii=1:nm
 clist(ii,1:2)=mlist(ii);
end

% whittle down the set 

if pset > max(mlist); return; end;        % keep all modes
if pset < 1; clist=clist(1:nm,:);end     % eliminate 3 mode terms
if abs(pset) > 0;                         % keep only comb with a certain mode 
  p=abs(pset);
  ii=find(clist(:,1) == p | clist(:,2) == p);
  clist=clist(ii,:);
end