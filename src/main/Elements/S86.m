classdef S86 < Element
% Quadratic shell/membrane element.


    properties(GetAccess = 'private', SetAccess = 'private')
        
        % Derived Section Properties
        sectionProps
        % A, Iyy, Izz, J, Ky, Kz
        elementProps
        % L, X, S
        % Y[y1, y2, y3, y4] , PHIY
        % Z[z1, z2, z3, z4] , PHIZ
        Te
    end
    
    % --- Constant Properties Cannot Be Changed 
    properties(Constant, GetAccess = 'private')
        %

        ndof = 48; % Number of DOF in shell
        irot = [6, 12, 18, 24, 30, 36, 42, 48];  
        mi = [1, 2, 7, 8, 13, 14, 19, 20, 25, 26, 31, 32, 37, 38, 43, 44]; 
        bsi = [3, 4, 5, 9, 10, 11, 15, 16, 17, 21, 22, 23, 27, 28, 29, ...
              33, 34, 35, 39, 40, 41, 45, 46, 47]
        bi = [3, 9, 15, 21, 27, 33, 39, 45];
%         
        QUADpointsBM = [-sqrt(0.6), 0, sqrt(0.6)]; % Quadrature Points; bending/membrane
        QUADweightsBM = [5/9, 8/9, 5/9]; % Quadrature Weights; bending/membrane
        QUADpointsS = [-sqrt(0.6), 0, sqrt(0.6)]; % Quadrature Points; shear
        QUADweightsS = [5/9, 8/9, 5/9]; % Quadrature Weights; shear
        
        % Reduced integration?
%         QUADpointsBM = [-1/sqrt(3), 1/sqrt(3)]; % Quadrature Points; bending/membrane
%         QUADweightsBM = [1, 1]; % Quadrature Weights; bending/membrane
%         QUADpointsS = [-1/sqrt(3), 1/sqrt(3)]; % Quadrature Points; bending/membrane
%         QUADweightsS = [1, 1]; % Quadrature Weights; bending/membrane
        
        
%         QUADpointsS = 0; % Quadrature Points; shear
%         QUADweightsS = 2; % Quadrature Weights; shear
        
    end 



    methods

        function obj = S86(nodeCoord, material, section)

            obj = obj@Element(nodeCoord, material, section);

        end

        function obj = build(obj)
            
            % Build Tranfromation Matrix
            obj.buildTransform();
            
            % Build Beam Section Properties
            obj.buildSection();
            
            % Build Beam Shape (Element Properties)
%             obj.buildShape();

        end

        
                % ------ Element Generation Functions ------ 
        function [Mout] = buildMass(obj)
            % This function builds the element mass matrix of a C1
            % Continous Beam Element in the element coordinate system.
            % Inputs :
            %
            % Outputs :
            %
            %
            
            
            % Extract Nodes (can be reomved in future for debugging)
%             c1 = obj.nodes.coord(1,:)+off1; % 1st Nodal Coordinate
%             c2 = obj.nodes.coord(2,:)+off2; % 2nd Nodal Coordinate
%             c3 = obj.nodes.coord(3,:)+off1; % 3rd Nodal Coordinate
%             c4 = obj.nodes.coord(4,:)+off2; % 4th Nodal Coordinate
%             c = [c1 c2 c3 c4]; % All nodal cooridinates 
            
            % Initialize Matrices
            Mel = zeros(obj.ndof);
            
            % Extract Projected X and Y (inplane) Nodal Coordinates
            X = obj.localCoords(:, 1);
            Y = obj.localCoords(:, 2);
            
            % Full integration (2 x 2 Quadrature) for membrane and bending terms
            % -- Membrane, Bending and Shear Indicies are S4 Constant Properties 
            % -- Quadrature Points and Weights are S4 Constant Properties
            for ii = 1:length(obj.QUADpointsBM)
                % Assign Points and Weights 
                xi = obj.QUADpointsBM(ii);
                wi = obj.QUADweightsBM(ii);
                for jj = 1:length(obj.QUADpointsBM)
                    
                    % Assign Points and Weights 
                    eta = obj.QUADpointsBM(jj);
                    wj = obj.QUADweightsBM(ii);
                    
                    % Evaluate Shape Functions (hopefully can be removed)
                    [detJ, ~, ~, Nm, Nb] = obj.evalMBN(xi, eta, X, Y);
                  
                    % Generate mass
                    Mel(obj.mi, obj.mi) = Mel(obj.mi, obj.mi) + obj.material.getProp('rho')*obj.section.t*wi*wj*(Nm'*Nm)*detJ;
                    Mel(obj.bsi, obj.bsi) = Mel(obj.bsi, obj.bsi) + obj.material.getProp('rho')*obj.section.t*wi*wj*(Nb'*Nb)*detJ;
                    
                end
            end
            
            % Tranform to Global System 
            Mel = obj.transform*Mel*obj.transform';
            
            % Change Ouput to Vector
            Mout = Mel(:);
            
        end
        
        function [Kout] = buildStiff(obj, nonsense)
            
            % Initialize Matrices
            Kel = zeros(obj.ndof);
            
            % Extract Projected X and Y (inplane) Nodal Coordinates
            X = obj.localCoords(:, 1);
            Y = obj.localCoords(:, 2);
            
            % Full integration (2 x 2 Quadrature) for membrane and bending terms
            % -- Membrane, Bending and Shear Indicies are Constant Properties
            % -- Quadrature Points and Weights are Constant Properties
            for ii = 1:length(obj.QUADpointsBM)
                % Assign Points and Weights
                xi = obj.QUADpointsBM(ii);
                wi = obj.QUADweightsBM(ii);
                for jj = 1:length(obj.QUADpointsBM)
                    
                    % Assign Points and Weights
                    eta = obj.QUADpointsBM(jj);
                    wj = obj.QUADweightsBM(ii);
                    
                    % Evaluate Shape Functions (hopefully can be removed)
                    [detJ, Bm, Bb, ~, ~] = obj.evalMBN(xi, eta, X, Y);
                    
                    % Generate Stiffness
                    Kel(obj.mi, obj.mi) = Kel(obj.mi, obj.mi) + wi*wj*Bm'*obj.elementProps.Dmembrane*Bm*detJ;
                    Kel(obj.bsi, obj.bsi) = Kel(obj.bsi, obj.bsi) + wi*wj*Bb'*obj.elementProps.Dbending*Bb*detJ;
                    
                end
            end
            
            % Reduced integration for the shear terms
            for i = 1:length(obj.QUADpointsS)
                xi = obj.QUADpointsS(i);
                wi = obj.QUADweightsS(i);
                for j = 1:length(obj.QUADpointsS)
                    eta = obj.QUADpointsS(j);
                    wj = obj.QUADweightsS(j);
                    [detJ, Bs] = obj.evalS(xi, eta, X, Y);
                    Kel(obj.bsi, obj.bsi) = Kel(obj.bsi, obj.bsi) + wi*wj*Bs'*obj.elementProps.Dshear*Bs*detJ;
                end
            end
            
            % Rotational Terms
%             keyboard
%             warning('No rotational stiffness engaged');
            Kel(obj.irot,obj.irot) = obj.elementProps.alpha*obj.material.getProp('E')*obj.elementProps.vol*(4/3*eye(8) - ones(8)/3);
%             Kel(irot,irot) = alpha*E*vol*(4/3*eye(8) - ones(8)/3);
                        
            % Tranform to Global System
            Kel = obj.transform*Kel*obj.transform';
            
            % Change Ouput to Vector
            Kout = Kel(:);
            
        end
        
        
        
         function [Kn, Kt] = buildNLStiff(obj, x)
            
            X = obj.localCoords(:, 1);
            Y = obj.localCoords(:, 2);
            
            xLocal = obj.transform'*x;
            
            K1nm = zeros(4);
            K1bm = zeros(4, 8);
            K2b = zeros(4);
            
            % Full integration (2 x 2 Quadrature) for all matrices
            % points = [-1/sqrt(3), 1/sqrt(3)];
            % weights = [1, 1];
            points = [-sqrt(0.6), 0, sqrt(0.6)];
            weights = [5/9, 8/9, 5/9];
            
            for i = 1:length(points)
                xi = points(i);
                wi = weights(i);
                for j = 1:length(points)
                    eta = points(j);
                    wj = weights(j);
                    [detJ, G, Nm, C, Bm] = obj.evalNL(xi, eta, X, Y, xLocal(obj.mi), xLocal(obj.bi), obj.elementProps.Dmembrane);
                    
                    K1nm = K1nm + 0.5*wi*wj*G'*Nm*G*detJ;
                    K1bm = K1bm + 0.5*wi*wj*G'*C'*obj.elementProps.Dmembrane*Bm*detJ;
                    K2b = K2b + 0.5*wi*wj*G'*C'*obj.elementProps.Dmembrane*C*G*detJ;
                    
                end
            end
            
            Kn = [K1nm + K2b,  K1bm; K1bm.', zeros(8)];
            Kt = [2*K1nm + 3*K2b, 2*K1bm; 2*K1bm.', zeros(8)];
            reorder = [obj.bi, obj.mi];
            
            Kne = zeros(24);
            Kte = zeros(24);
            
            Kne(reorder, reorder) = Kn;
            Kte(reorder, reorder) = Kt;
            
            Kn = obj.transform*Kne*obj.transform';
            Kt = obj.transform*Kte*obj.transform';
            
            Kn = Kn(:);
            Kt = Kt(:);

         end
         
         function [KNLout] = buildNLmodal(obj,def)
             warning('Not Implemented yet')
             KNLout = obj + def;
         end
         
         
         
         
         %%%% Helper Functions %%%%
         function [obj] = buildTransform(obj)
             % This function builds the transformation matrix of the beam
             % element to rotate from local CS to global CS.
             %
             % Inputs :
             %           obj
             % Outputs :
             %           obj
             
             % Extract Offsets (removed for now)
             off1 = [0 0 0];
             off2 = [0 0 0];
             off3 = [0 0 0];
             off4 = [0 0 0];
             
             % Extract Nodes
             c1 = obj.nodes(1).coord+off1; % 1st Nodal Coordinate
             c2 = obj.nodes(2).coord+off2; % 2nd Nodal Coordinate
             c3 = obj.nodes(3).coord+off3; % 3rd Nodal Coordinate
             c4 = obj.nodes(4).coord+off4; % 4th Nodal Coordinate
             c5 = obj.nodes(5).coord+off4; % 4th Nodal Coordinate
             c6 = obj.nodes(6).coord+off4; % 4th Nodal Coordinate
             c7 = obj.nodes(7).coord+off4; % 4th Nodal Coordinate
             c8 = obj.nodes(8).coord+off4; % 4th Nodal Coordinate
             
             
             
             c = [c1; c2; c3; c4; c5; c6; c7; c8]; % All nodal cooridinates
             
             % Compute Unit Vectors (e1 & e2)
             e1 = (c2 - c1)/norm(c2 - c1);
             e2 = (c4 - c1)/norm(c4 - c1);
             e3 = cross(e1,e2);
             
             % Compute Unit Vector (e13 & e42)
             e13 = (c3 - c1)/norm(c3 - c1);
             e24 = (c2 - c4)/norm(c2 - c4);
             
             % Generate Element Local Coordinate System Unit Vectors
             % ** cvd - need to check this **
             obj.Te = zeros(3);
             obj.Te(1,:) = e13 + e24; % Add together ?? why ??
             obj.Te(3,:) = Element.cross2(obj.Te(1,:), e13);
             obj.Te(2,:) = Element.cross2(obj.Te(3,:), obj.Te(1,:));
             
             % Normalize Matrix
             obj.Te(1,:) = obj.Te(1,:)/norm(obj.Te(1,:));
             obj.Te(2,:) = obj.Te(2,:)/norm(obj.Te(2,:));
             obj.Te(3,:) = obj.Te(3,:)/norm(obj.Te(3,:));
%              global R
%              keyboard
             
             % Assemble Transformation Matrix
             T = zeros(24);
             T(1:3,1:3) = obj.Te';
             T(4:6,4:6) = obj.Te';
             T(7:9,7:9) = obj.Te';
             T(10:12,10:12) = obj.Te';
             T(13:15,13:15) = obj.Te';
             T(16:18,16:18) = obj.Te';
             T(19:21,19:21) = obj.Te';
             T(22:24,22:24) = obj.Te';
             
             % Store Transformation
             obj.transform = blkdiag(T, T);
             %             keyboard
             
             % Build Local Coordinates
             % Compute node point coords in element coordinate system
             % ** cvd not confident about this **
             norg = sum(c)/8;
             u = [c(:,1)'-norg(1);c(:,2)'-norg(2);c(:,3)'-norg(3)]';
             obj.localCoords = u*obj.Te(1:2,:)';
             
%              keyboard
             
         end
         
         function [obj] = buildSection(obj)
            % This function computes the derived sectional properties of a
            % beam from a given cross-section shape and parameters.
            %
            % Inputs  :
            %           obj
            % Outputs :
            %
            %
            
            % Assign Material if not specified 
            E = obj.material.getProp('E');
            nu = obj.material.getProp('nu');
            G =  E/(2*(1 + nu));
            
            obj.material.setProp('G', G);
            
            % Form constituitive matrices
            obj.elementProps.Dmembrane = E*obj.section.t/(1 - nu^2)*[1, nu, 0;
                                         nu,  1, 0;
                                         0,  0, 0.5*(1 - nu)];
            obj.elementProps.Dbending = E*obj.section.t^3/(12*(1 - nu^2))*[1, nu, 0;
                                        nu,  1, 0;
                                        0,  0, 0.5*(1 - nu)];
            obj.elementProps.Dshear = 5/6*G*obj.section.t*[1, 0; 0, 1];
          
            % Compute x and y lengths 
            dx = .5*(obj.localCoords(2,1)-obj.localCoords(1,1)+obj.localCoords(3,1)-obj.localCoords(4,1));
            dy = .5*(obj.localCoords(4,2)-obj.localCoords(1,2)+obj.localCoords(3,2)-obj.localCoords(2,2));
            
            % Store Properties 
            obj.elementProps.vol = dx*dy*obj.section.t;
            obj.elementProps.alpha = 0.01;
            
        end


        function [detJ, Bm, Bb, Nm, Nb] = evalMBN(obj, xi, eta, X, Y)
            % Evaluate B membrane and bending matrices, along with shape functions themselves,
            % at the coordinate 'xi' and 'eta'
            
            % Write shape functions
            N5 = 0.5*(1 - xi^2)*(1 - eta); N6 = 0.5*(1 + xi)*(1 - eta^2);
            N7 = 0.5*(1 - xi^2)*(1 + eta); N8 = 0.5*(1 - xi)*(1 - eta^2);
            
            N1 = 0.25*(1 - xi)*(1 - eta) - 0.5*(N5 + N8);
            N2 = 0.25*(1 + xi)*(1 - eta) - 0.5*(N5 + N6);
            N3 = 0.25*(1 + xi)*(1 + eta) - 0.5*(N6 + N7);
            N4 = 0.25*(1 - xi)*(1 + eta) - 0.5*(N7 + N8);
            
            % Write shape function derivatives
            N5xi = -xi*(1 - eta); N6xi = 0.5*(1 - eta^2); N7xi = -xi*(1 + eta); N8xi = -0.5*(1 - eta^2);
            N5eta = -0.5*(1 - xi^2); N6eta = -eta*(1 + xi); N7eta = 0.5*(1 - xi^2); N8eta = -eta*(1 - xi);
            
            N1xi = -0.25*(1 - eta) - 0.5*(N5xi + N8xi); N2xi = 0.25*(1 - eta) - 0.5*(N5xi + N6xi);
            N3xi = 0.25*(1 + eta) - 0.5*(N6xi + N7xi); N4xi = -0.25*(1 + eta) - 0.5*(N7xi + N8xi);
            
            N1eta = -0.25*(1 - xi) - 0.5*(N5eta + N8eta); N2eta = -0.25*(1 + xi) - 0.5*(N5eta + N6eta);
            N3eta = 0.25*(1 + xi) - 0.5*(N6eta + N7eta); N4eta = 0.25*(1 - xi) - 0.5*(N7eta + N8eta);
            
            J =  [N1xi,  N2xi,  N3xi,  N4xi, N5xi, N6xi, N7xi, N8xi;
                N1eta, N2eta, N3eta, N4eta, N5eta, N6eta, N7eta, N8eta]*[X(:), Y(:)];
            
            detJ = J(1, 1)*J(2, 2) - J(2, 1)*J(1, 2);
            
            % Define "gamma" products
            Gamma11 = J(2, 2)/detJ;
            Gamma12 = -J(1, 2)/detJ;
            Gamma21 = -J(2, 1)/detJ;
            Gamma22 = J(1, 1)/detJ;
            
            % Write shape function derivatives wrt x
            N1x = Gamma11*N1xi + Gamma12*N1eta; N1y = Gamma21*N1xi + Gamma22*N1eta;
            N2x = Gamma11*N2xi + Gamma12*N2eta; N2y = Gamma21*N2xi + Gamma22*N2eta;
            N3x = Gamma11*N3xi + Gamma12*N3eta; N3y = Gamma21*N3xi + Gamma22*N3eta;
            N4x = Gamma11*N4xi + Gamma12*N4eta; N4y = Gamma21*N4xi + Gamma22*N4eta;
            N5x = Gamma11*N5xi + Gamma12*N5eta; N5y = Gamma21*N5xi + Gamma22*N5eta;
            N6x = Gamma11*N6xi + Gamma12*N6eta; N6y = Gamma21*N6xi + Gamma22*N6eta;
            N7x = Gamma11*N7xi + Gamma12*N7eta; N7y = Gamma21*N7xi + Gamma22*N7eta;
            N8x = Gamma11*N8xi + Gamma12*N8eta; N8y = Gamma21*N8xi + Gamma22*N8eta;
            
            % Write each B submatrix
            Bm1 = [N1x, 0;  0, N1y;  N1y, N1x];
            Bm2 = [N2x, 0;  0, N2y;  N2y, N2x];
            Bm3 = [N3x, 0;  0, N3y;  N3y, N3x];
            Bm4 = [N4x, 0;  0, N4y;  N4y, N4x];
            Bm5 = [N5x, 0;  0, N5y;  N5y, N5x];
            Bm6 = [N6x, 0;  0, N6y;  N6y, N6x];
            Bm7 = [N7x, 0;  0, N7y;  N7y, N7x];
            Bm8 = [N8x, 0;  0, N8y;  N8y, N8x];
            
            Bb1 = [0, N1x, 0;  0, 0, N1y;  0, N1y, N1x];
            Bb2 = [0, N2x, 0;  0, 0, N2y;  0, N2y, N2x];
            Bb3 = [0, N3x, 0;  0, 0, N3y;  0, N3y, N3x];
            Bb4 = [0, N4x, 0;  0, 0, N4y;  0, N4y, N4x];
            Bb5 = [0, N5x, 0;  0, 0, N5y;  0, N5y, N5x];
            Bb6 = [0, N6x, 0;  0, 0, N6y;  0, N6y, N6x];
            Bb7 = [0, N7x, 0;  0, 0, N7y;  0, N7y, N7x];
            Bb8 = [0, N8x, 0;  0, 0, N8y;  0, N8y, N8x];
            
            
            % Return full B matrix
            Bm = [Bm1, Bm2, Bm3, Bm4, Bm5, Bm6, Bm7, Bm8];
            Bb = [Bb1, Bb2, Bb3, Bb4, Bb5, Bb6, Bb7, Bb8];
            
            % Shape functions
            Nm = [N1,  0, N2,  0, N3,  0, N4, 0, N5, 0, N6, 0, N7, 0, N8, 0;
                0, N1,  0, N2,  0, N3,  0, N4, 0, N5, 0, N6, 0, N7, 0, N8];
            
            Nb = [N1,  0,  0, N2,  0,  0, N3,  0,  0, N4,  0,  0, N5,  0,  0, N6,  0,  0, N7,  0,  0, N8,  0,  0;
                0, N1,  0,  0, N2,  0,  0, N3,  0,  0, N4,  0,  0, N5,  0,  0, N6,  0,  0, N7,  0,  0, N8,  0;
                0,  0, N1,  0,  0, N2,  0,  0, N3,  0,  0, N4,  0,  0, N5,  0,  0, N6,  0,  0, N7,  0,  0, N8];
            
        end
        
        function [detJ, Bs] = evalS(obj, xi, eta, X, Y)
            % Evaluate B shear matrix at the coordinate 'xi' and 'eta'
            
            % Write shape functions
            N5 = 0.5*(1 - xi^2)*(1 - eta); N6 = 0.5*(1 + xi)*(1 - eta^2);
            N7 = 0.5*(1 - xi^2)*(1 + eta); N8 = 0.5*(1 - xi)*(1 - eta^2);
            
            N1 = 0.25*(1 - xi)*(1 - eta) - 0.5*(N5 + N8);
            N2 = 0.25*(1 + xi)*(1 - eta) - 0.5*(N5 + N6);
            N3 = 0.25*(1 + xi)*(1 + eta) - 0.5*(N6 + N7);
            N4 = 0.25*(1 - xi)*(1 + eta) - 0.5*(N7 + N8);
            
            % Write shape function derivatives
            N5xi = -xi*(1 - eta); N6xi = 0.5*(1 - eta^2); N7xi = -xi*(1 + eta); N8xi = -0.5*(1 - eta^2);
            N5eta = -0.5*(1 - xi^2); N6eta = -eta*(1 + xi); N7eta = 0.5*(1 - xi^2); N8eta = -eta*(1 - xi);
            
            N1xi = -0.25*(1 - eta) - 0.5*(N5xi + N8xi); N2xi = 0.25*(1 - eta) - 0.5*(N5xi + N6xi);
            N3xi = 0.25*(1 + eta) - 0.5*(N6xi + N7xi); N4xi = -0.25*(1 + eta) - 0.5*(N7xi + N8xi);
            
            N1eta = -0.25*(1 - xi) - 0.5*(N5eta + N8eta); N2eta = -0.25*(1 + xi) - 0.5*(N5eta + N6eta);
            N3eta = 0.25*(1 + xi) - 0.5*(N6eta + N7eta); N4eta = 0.25*(1 - xi) - 0.5*(N7eta + N8eta);
            
            J =  [N1xi,  N2xi,  N3xi,  N4xi, N5xi, N6xi, N7xi, N8xi;
                N1eta, N2eta, N3eta, N4eta, N5eta, N6eta, N7eta, N8eta]*[X(:), Y(:)];
            
            detJ = J(1, 1)*J(2, 2) - J(2, 1)*J(1, 2);
            
            % Define "gamma" products
            Gamma11 = J(2, 2)/detJ;
            Gamma12 = -J(1, 2)/detJ;
            Gamma21 = -J(2, 1)/detJ;
            Gamma22 = J(1, 1)/detJ;
            
            % Write shape function derivatives wrt x
            N1x = Gamma11*N1xi + Gamma12*N1eta; N1y = Gamma21*N1xi + Gamma22*N1eta;
            N2x = Gamma11*N2xi + Gamma12*N2eta; N2y = Gamma21*N2xi + Gamma22*N2eta;
            N3x = Gamma11*N3xi + Gamma12*N3eta; N3y = Gamma21*N3xi + Gamma22*N3eta;
            N4x = Gamma11*N4xi + Gamma12*N4eta; N4y = Gamma21*N4xi + Gamma22*N4eta;
            N5x = Gamma11*N5xi + Gamma12*N5eta; N5y = Gamma21*N5xi + Gamma22*N5eta;
            N6x = Gamma11*N6xi + Gamma12*N6eta; N6y = Gamma21*N6xi + Gamma22*N6eta;
            N7x = Gamma11*N7xi + Gamma12*N7eta; N7y = Gamma21*N7xi + Gamma22*N7eta;
            N8x = Gamma11*N8xi + Gamma12*N8eta; N8y = Gamma21*N8xi + Gamma22*N8eta;
            
            % Write each B submatrix
            Bs1 = [-N1x, N1, 0;  -N1y, 0, N1];
            Bs2 = [-N2x, N2, 0;  -N2y, 0, N2];
            Bs3 = [-N3x, N3, 0;  -N3y, 0, N3];
            Bs4 = [-N4x, N4, 0;  -N4y, 0, N4];
            Bs5 = [-N5x, N5, 0;  -N5y, 0, N5];
            Bs6 = [-N6x, N6, 0;  -N6y, 0, N6];
            Bs7 = [-N7x, N7, 0;  -N7y, 0, N7];
            Bs8 = [-N8x, N8, 0;  -N8y, 0, N8];
            
            % Return full B matrix
            Bs = [Bs1, Bs2, Bs3, Bs4, Bs5, Bs6, Bs7, Bs8];
            
        end
        
    end 
end

  
  
