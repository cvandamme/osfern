function s4r = S4R
% Alias function for an S4 shell element. Returns whatever particular
% implenetation is the current baseline element in use.
s4r = S4S();

end