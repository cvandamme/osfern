classdef Element < handle
% Element superclass.

    properties(GetAccess = 'public', SetAccess = 'public')
        id     % Element ID number
        index  % Element index number in MatFEM object
        rid    % Row Vector Identification for Global Matrix 
        cid    % Column Vector Identification for Global Matrix
        ind    % Entry index # for Global Matrix
        dofVec % Vector of dof indicies
    end
    
    properties(GetAccess = 'public', SetAccess = 'protected')

        nodes;  % Node coords in global CS
        localCoords; % Node coordinates in local element CS

        material;    % Material ID key from parent object
        section;     % Section ID key from parent object

        transform; % Coordinate + skew transform from element to global coordinates
        shapeFun;  % Shape functions, derivatives, and Jacobian info (structure array)
        
        mass, volume

        temperature = []; % temperature of element
    end

    properties (GetAccess = 'private', SetAccess = 'private')
        
    end
    
    % These properties must be defined by all Element implementations.
    properties(Abstract, Constant)
       nNode;
       activeDof;
    end
    
    properties(Constant)
%         TYPE_INDEX = 'uint32'; % This causes problems for some reason
        TYPE_INDEX = 'double';
    end
    
    methods

        function obj = Element(id)
            % Initialize element so that abstract properties can be
            % obtained.
            obj.id = id;
        end

        function [Kn, Kt] = BuildNL(obj, displacement)
            % Return nonlinear stiffness matrix and tangent stiffness matrix.
            Kn = zeros(obj.nDof^2, 1);
            Kt = zeros(obj.nDof^2, 1);
        end

        function Km = BuilldNLModal(obj, displacement1, displacement2)
            % Return modalized nonlinear stiffness matrix. (Not sure what outputs we really need yet)
            Km = zeros(obj.nDof, obj.nDof);
        end
        
        function nDof = GetNumDof(obj)
            % Returns number of DOF in the element.
            % TODO: Make this rely on the stores Node objects in the
            % element rather than class properties.
            nDof = obj.nNode*obj.activeDof;
        end
        
        function nMatrixEntries = GetNumMatrixEntries(obj)
            nMatrixEntries = obj.GetNumDof()^2;
        end
        
        function SetIndex(obj, index)
            obj.index = index;
        end
        
        function SetGlobalDof(obj, nPrevMatrixEntries)
            % First, get the global DOF of the element
            obj.dofVec = zeros(obj.activeDof, obj.nNode, obj.TYPE_INDEX);
            for i = 1:obj.nNode
                obj.dofVec(:, i) = obj.nodes{i}.GetGlobalDof();
            end
            obj.dofVec = obj.dofVec(:);
            
            % Populate rid, cid, and vector entries
            nDof = obj.GetNumDof(); % Maybe this should be a class property
            obj.rid = zeros(nDof*nDof, 1, obj.TYPE_INDEX);
            obj.cid = zeros(nDof*nDof, 1, obj.TYPE_INDEX);
            obj.ind = zeros(nDof*nDof, 1, obj.TYPE_INDEX);
            
            obj.rid(:) = repmat(obj.dofVec, nDof, 1);
            cidTemp = repmat(obj.dofVec, 1, nDof)';
            obj.cid(:) = cidTemp(:);
            obj.ind(:) = nPrevMatrixEntries + (1:nDof^2)';
            
        end

        function SetTemperature(obj,temp)
           obj.temperature = temp; 
        end
        
        function SetElement2NodeId(obj)
            for ii = 1:length(obj.nodes)
                obj.nodes{ii}.SetElementIds(obj.id);
            end
        end
    end

        % These must be implemented by all subclasses of Element
        methods(Abstract)

        % Build and store coordinate transformation and shape function matrices. These
        % Should never be constructed again in any element call. This should also set
        % the appropriate AUTOSPC flag for all nodes present in the element. 
        obj = Build(obj, nodeCoords, material, section)    

        % Return element mass matrix as sparse vector. 
        M = BuildMass(obj)

        % Return element stiffness matrix as sparse vector.
        K = BuildStiff(obj)
        
        % Return Mass
        m = GetMass(obj)
        vol = GetVolume(obj)
        end
    
        methods(Static)
            % Static methods for use by all subclasses
            
            function c = cross2(a, b)
                % Compute the cross product of vectors "a" and "b"

                c = [a(2)*b(3) - a(3)*b(2); 
                    -a(1)*b(3) + a(3)*b(1);
                     a(1)*b(2) - a(2)*b(1)];
            end
            
        end

end
