function s4 = S4
% Alias function for an S4 shell element. Returns whatever particular
% implenetation is the current baseline element in use.
s4 = S4BDI();

end