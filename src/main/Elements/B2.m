classdef B2 < Element & handle
    % This class is used to generate the mass, stifffness (linear and
    % nonlinear) of beam elements. The formulation is based upon :
    % 3-D Timeshenko Beam Element from 
    % "Concepts and Applications of Finite Element Analysis" - Cook,
    % Malkis, Plesha, Witt - 4th Ed., Page 27.
    % 
    % Nonlinear Beam Model is from ...
    % 
    % Part of OSFern FE Code ...
    % 
    % Christopher Van Damme - 06/21/2018
    % 
    properties (GetAccess = 'public', SetAccess = 'private')
        % Beam Options
        formulation = 'E'; % 'Euler' ('E') or 'Timoshenko' ('T')
        continuity = 1; % element continuity
        % Derived Section Properties
        sectionProps 
        % A, Iyy, Izz, J, Ky, Kz
        elementProps
        % L, X, S
        % Y[y1, y2, y3, y4] , PHIY
        % Z[z1, z2, z3, z4] , PHIZ
        csVec
        offSet = zeros(3,2);
        offSetTransform
        dB % element derivative matrix
        normal;
    end
    
    % --- Constant Properties Cannot Be Changed
    properties (Constant)
        nNode = 2;
        activeDof = 6;
        nDof = 12
        indx = [1 2 3 5 6 7 8 9 11 12]; % All dof besides rotation about x
        linQuadPoitns =[-1/sqrt(3) 1/sqrt(3)]
        linQuadWeights = [1 1];
        nlQuadPoints = [-0.906179846 -0.53846931 0,...
                         0.53846931 0.906179846]
        nlQuadWeights = [0.236926885 0.47862967 0.5688888889,...
                         0.47862967 0.236926885]
    end
    methods
        % ----- Initialization ----- % 
        function obj = B2(id)

            obj = obj@Element(id);
%             obj = obj@Element(parent, nodes, material, section);

        end
        
        % ----- Abstract Methods that Must Be Used ----- 
        function [obj] = Build(obj, node, material, section, csVec, varargin)
            % This function is called to generate the beam section
            % properties and transformation matrix for a beam element object
            
            obj.nodes = node;
            obj.material = material;
            obj.section = section;
            obj.csVec = csVec;
            
            % Build Beam Section Properties
            obj.BuildSection();
            
            % Build Beam Shape (Element Properties)
            obj.BuildShape();
            
            % Check if offset if provided
            if nargin > 5
                if size(varargin{1}) == [3,1]
                    offSet1 = varargin{1};
                elseif size(varargin{1}) == [1,3]
                    offSet1 = varargin{1}';
                else
                    offSet1 = [0;0;0];
                    warning('Offset at Node 1 is incorrect size .. ignoring')
                end
                if size(varargin{2}) == [3,1]
                    offSet2 = varargin{2};
                elseif size(varargin{2}) == [1,3]
                    offSet2 = varargin{2}';
                else
                    offSet2 = [0;0;0];
                    warning('Offset at Node 1 is incorrect size .. ignoring')
                end
                obj.offset = [offSet1, offSet2];
            end
            
            % Build Tranfromation Matrix
            obj.localCoords = [0 , 1 , 0]; % Unit length along x-axis
            obj.BuildTransform();
            
            % Adds the element id to each node (speeds up assembly later)
            obj.SetElement2NodeId()
        end
        function [obj] = ReBuild(obj)
            
            % Build Beam Section Properties
            obj.BuildSection();
            
            % Build Beam Shape (Element Properties)
            obj.BuildShape();
            
            % Build Tranfromation Matrix
            obj.BuildTransform();
        end
        function [Mout] = BuildMassDiag(obj)
            % This function builds the element mass matrix of a C1
            % Continous Beam Element in the element coordinate system.
            % Inputs : 
            %
            % Outputs : 
            
            % Initialize
            Mel = zeros(12);
            
            % Define Constants 
            cy = (1+obj.elementProps.PHIY)^2;
            cz = (1+obj.elementProps.PHIZ)^2;
            rly = obj.sectionProps.Izz/(obj.sectionProps.A*obj.elementProps.L^2);
            rlz = obj.sectionProps.Iyy/(obj.sectionProps.A*obj.elementProps.L^2);
            
            % From Cook
          
            % First Quadrant 
            M11_1 = zeros(6);
            M11_2 = M11_1;
            M11_1(1,1) = 1/3;
            M11_1(2,2) = (13/35+.7*obj.elementProps.PHIY+obj.elementProps.PHIY^2/3)/cy;
            M11_1(3,3) = (13/35+.7*obj.elementProps.PHIZ+obj.elementProps.PHIZ^2/3)/cz;
            M11_1(4,4) = (obj.sectionProps.Iyy+obj.sectionProps.Izz)/(3*obj.sectionProps.A);
            M11_1(5,5) = ((1/105+obj.elementProps.PHIZ/60+obj.elementProps.PHIZ^2/120)*obj.elementProps.L^2)/cz;
            M11_1(6,6) = ((1/105+obj.elementProps.PHIY/60+obj.elementProps.PHIY^2/120)*obj.elementProps.L^2)/cy;
            M11_1(6,2) = (11/210+11*obj.elementProps.PHIY/120+obj.elementProps.PHIY^2/24)*obj.elementProps.L/cy;
            M11_1(2,6) = M11_1(6,2);
            M11_1(5,3) = -(11/210+11*obj.elementProps.PHIZ/120+obj.elementProps.PHIZ^2/24)*obj.elementProps.L/cz;
            M11_1(3,5) = M11_1(5,3);
            M11_2(2,2) = 6*rly/(5*cy);
            M11_2(3,3) = 6*rlz/(5*cz);
            M11_2(5,5) = (2/15+obj.elementProps.PHIZ/6+obj.elementProps.PHIZ^2/3)*obj.elementProps.L^2*rlz/cz;
            M11_2(6,6) = (2/15+obj.elementProps.PHIY/6+obj.elementProps.PHIY^2/3)*obj.elementProps.L^2*rly/cy;
            M11_2(6,2) = (.1-obj.elementProps.PHIY/2)*obj.elementProps.L*rly/cy;
            M11_2(2,6) = M11_2(6,2);
            M11_2(5,3) = -(.1-obj.elementProps.PHIZ/2)*obj.elementProps.L*rlz/cz;
            M11_2(3,5) = M11_2(5,3);
            M11 = M11_1+M11_2;
            
            % Fourth Quadrant 
            M22 = M11;
            M22(6,2) = -M11(6,2);
            M22(2,6) = M22(6,2);
            M22(5,3) = -M11(5,3);
            M22(3,5) = M22(5,3);
            
            M21_1 = zeros(6);
            M21_2 = M21_1;
            M21_1(1,1) = 1/6;
            M21_1(2,2) = (9/70+.3*obj.elementProps.PHIY+obj.elementProps.PHIY^2/6)/cy;
            M21_1(3,3) = (9/70+.3*obj.elementProps.PHIZ+obj.elementProps.PHIZ^2/6)/cz;
            M21_1(4,4) = (obj.sectionProps.Iyy+obj.sectionProps.Izz)/(6*obj.sectionProps.A);
            M21_1(5,5) = -(1/140+obj.elementProps.PHIZ/60+obj.elementProps.PHIZ^2/120)*obj.elementProps.L^2/cz;
            M21_1(6,6) = -(1/140+obj.elementProps.PHIY/60+obj.elementProps.PHIY^2/120)*obj.elementProps.L^2/cy;
            M21_1(6,2) = -(13/420+3*obj.elementProps.PHIY/40+obj.elementProps.PHIY^2/24)*obj.elementProps.L/cy;
            M21_1(2,6) = -M21_1(6,2);
            M21_1(5,3) = (13/420+3*obj.elementProps.PHIZ/40+obj.elementProps.PHIZ^2/24)*obj.elementProps.L/cz;
            M21_1(3,5) = -M21_1(5,3);
            M21_2(2,2) = -6*rly/(5*cy);
            M21_2(3,3) = -6*rlz/(5*cz);
            M21_2(5,5) = (-1/30-obj.elementProps.PHIZ/6+obj.elementProps.PHIZ^2/6)*obj.elementProps.L^2*rlz/cz;
            M21_2(6,6) = (-1/30-obj.elementProps.PHIY/6+obj.elementProps.PHIY^2/6)*obj.elementProps.L^2*rly/cy;
            M21_2(6,2) = -(.1-obj.elementProps.PHIY/2)*obj.elementProps.L*rly/cy;
            M21_2(2,6) = -M21_2(6,2);
            M21_2(5,3) = (.1-obj.elementProps.PHIZ/2)*obj.elementProps.L*rlz/cz;
            M21_2(3,5) = -M21_2(5,3);
            M21 = M21_1+M21_2;
            
            % Assemble Together
            Mel(1:6,1:6) = M11;
            Mel(7:12,1:6) = M21;
            Mel(1:6,7:12) = M21';
            Mel(7:12,7:12) = M22;
%             Mel([4:6,10:12],:) = 0; Mel(:,[4:6,10:12]) = 0;
            % Scale by overall mass 
            Mout = obj.material.getProp('rho')*obj.sectionProps.A*obj.elementProps.L*Mel;
            Mout = diag(diag(Mout)); % poor mans diagonalization
            
            % Transform 
            Mout = obj.transform*Mout*obj.transform';
            Mout = Mout(:);
        end
        function [Mout] = BuildMass(obj)
            % This function builds the element mass matrix of a C1
            % Continous Beam Element in the element coordinate system.
            % Inputs : 
            %
            % Outputs : 
            
            % Initialize
            Mel = zeros(12);
            
            % Define Constants 
            cy = (1+obj.elementProps.PHIY)^2;
            cz = (1+obj.elementProps.PHIZ)^2;
            rly = obj.sectionProps.Izz/(obj.sectionProps.A*obj.elementProps.L^2);
            rlz = obj.sectionProps.Iyy/(obj.sectionProps.A*obj.elementProps.L^2);
            
            % From Cook
            
            % First Quadrant 
%             M11 = zeros(6);
%             M11(1,1) = 1/3; M11(2,2) = 13/35; M11(3,3) = 13/35; 
%             M11(4,4) = (obj.sectionProps.Iyy + obj.sectionProps.Izz)/(3*obj.sectionProps.A); 
%             M11(5,5) = (obj.elementProps.L^2)/105;
%             M11(6,6) = (obj.elementProps.L^2)/105;
%             M11(3,5) = -(11*obj.elementProps.L^2)/210; M11(5,3) = M11(3,5);
%             M11(2,6) = (11*obj.elementProps.L^2)/210; M11(6,2) = M11(2,6);
%             
%             % Second Quadrant
%             M12 = zeros(6);
%             M12(1,1) = 1/6; M11(2,2) = 9/70; M11(3,3) = 9/70;
%             M12(4,4) = (obj.sectionProps.Iyy + obj.sectionProps.Izz)/(6*obj.sectionProps.A); 
%             M12(5,5) = (-obj.elementProps.L^2)/140;
%             M12(6,6) = (-obj.elementProps.L^2)/140;
%             M12(3,5) = (13*obj.elementProps.L^2)/420; M12(5,3) = M11(3,5);
%             M12(2,6) = -(13*obj.elementProps.L^2)/420; M12(6,2) = M11(2,6);
%             
%             
%             % Fourth Quadrant 
%             M22 = M11;
%             M22(6,2) = -M11(6,2);
%             M22(2,6) = M22(6,2);
%             M22(5,3) = -M11(5,3);
%             M22(3,5) = M22(5,3);
%             
%             % Assemble Together
%             Mel(1:6,1:6) = M11;
%             Mel(7:12,1:6) = M12';
%             Mel(1:6,7:12) = M12;
%             Mel(7:12,7:12) = M22;
%             
            % First Quadrant 
            M11_1 = zeros(6);
            M11_2 = M11_1;
            M11_1(1,1) = 1/3;
            M11_1(2,2) = (13/35+.7*obj.elementProps.PHIY+obj.elementProps.PHIY^2/3)/cy;
            M11_1(3,3) = (13/35+.7*obj.elementProps.PHIZ+obj.elementProps.PHIZ^2/3)/cz;
            M11_1(4,4) = (obj.sectionProps.Iyy+obj.sectionProps.Izz)/(3*obj.sectionProps.A);
            M11_1(5,5) = ((1/105+obj.elementProps.PHIZ/60+obj.elementProps.PHIZ^2/120)*obj.elementProps.L^2)/cz;
            M11_1(6,6) = ((1/105+obj.elementProps.PHIY/60+obj.elementProps.PHIY^2/120)*obj.elementProps.L^2)/cy;
            M11_1(6,2) = (11/210+11*obj.elementProps.PHIY/120+obj.elementProps.PHIY^2/24)*obj.elementProps.L/cy;
            M11_1(2,6) = M11_1(6,2);
            M11_1(5,3) = -(11/210+11*obj.elementProps.PHIZ/120+obj.elementProps.PHIZ^2/24)*obj.elementProps.L/cz;
            M11_1(3,5) = M11_1(5,3);
            M11_2(2,2) = 6*rly/(5*cy);
            M11_2(3,3) = 6*rlz/(5*cz);
            M11_2(5,5) = (2/15+obj.elementProps.PHIZ/6+obj.elementProps.PHIZ^2/3)*obj.elementProps.L^2*rlz/cz;
            M11_2(6,6) = (2/15+obj.elementProps.PHIY/6+obj.elementProps.PHIY^2/3)*obj.elementProps.L^2*rly/cy;
            M11_2(6,2) = (.1-obj.elementProps.PHIY/2)*obj.elementProps.L*rly/cy;
            M11_2(2,6) = M11_2(6,2);
            M11_2(5,3) = -(.1-obj.elementProps.PHIZ/2)*obj.elementProps.L*rlz/cz;
            M11_2(3,5) = M11_2(5,3);
            M11 = M11_1+M11_2;
            
            % Fourth Quadrant 
            M22 = M11;
            M22(6,2) = -M11(6,2);
            M22(2,6) = M22(6,2);
            M22(5,3) = -M11(5,3);
            M22(3,5) = M22(5,3);
            
            M21_1 = zeros(6);
            M21_2 = M21_1;
            M21_1(1,1) = 1/6;
            M21_1(2,2) = (9/70+.3*obj.elementProps.PHIY+obj.elementProps.PHIY^2/6)/cy;
            M21_1(3,3) = (9/70+.3*obj.elementProps.PHIZ+obj.elementProps.PHIZ^2/6)/cz;
            M21_1(4,4) = (obj.sectionProps.Iyy+obj.sectionProps.Izz)/(6*obj.sectionProps.A);
            M21_1(5,5) = -(1/140+obj.elementProps.PHIZ/60+obj.elementProps.PHIZ^2/120)*obj.elementProps.L^2/cz;
            M21_1(6,6) = -(1/140+obj.elementProps.PHIY/60+obj.elementProps.PHIY^2/120)*obj.elementProps.L^2/cy;
            M21_1(6,2) = -(13/420+3*obj.elementProps.PHIY/40+obj.elementProps.PHIY^2/24)*obj.elementProps.L/cy;
            M21_1(2,6) = -M21_1(6,2);
            M21_1(5,3) = (13/420+3*obj.elementProps.PHIZ/40+obj.elementProps.PHIZ^2/24)*obj.elementProps.L/cz;
            M21_1(3,5) = -M21_1(5,3);
            M21_2(2,2) = -6*rly/(5*cy);
            M21_2(3,3) = -6*rlz/(5*cz);
            M21_2(5,5) = (-1/30-obj.elementProps.PHIZ/6+obj.elementProps.PHIZ^2/6)*obj.elementProps.L^2*rlz/cz;
            M21_2(6,6) = (-1/30-obj.elementProps.PHIY/6+obj.elementProps.PHIY^2/6)*obj.elementProps.L^2*rly/cy;
            M21_2(6,2) = -(.1-obj.elementProps.PHIY/2)*obj.elementProps.L*rly/cy;
            M21_2(2,6) = -M21_2(6,2);
            M21_2(5,3) = (.1-obj.elementProps.PHIZ/2)*obj.elementProps.L*rlz/cz;
            M21_2(3,5) = -M21_2(5,3);
            M21 = M21_1+M21_2;
            
            % Assemble Together
            Mel(1:6,1:6) = M11;
            Mel(7:12,1:6) = M21;
            Mel(1:6,7:12) = M21';
            Mel(7:12,7:12) = M22;
%             Mel([4:6,10:12],:) = 0; Mel(:,[4:6,10:12]) = 0;
            % Scale by overall mass 
            Mout = obj.material.getProp('rho')*obj.sectionProps.A*obj.elementProps.L*Mel;
            
            % Transform 
            Mout = obj.transform*Mout*obj.transform';
            Mout = Mout(:);
        end
        function [m] = GetMass(obj)
            m = obj.material.getProp('rho')*obj.elementProps.L*obj.sectionProps.A;
        end
        function [Kout] = BuildStiff(obj,x)
            
                    
            % From Cook 
            K11 = diag([obj.elementProps.X,obj.elementProps.Y(1),...
                        obj.elementProps.Z(1),obj.elementProps.S,...
                        obj.elementProps.Z(3),obj.elementProps.Y(3)]); 
            K11(3,5) = -obj.elementProps.Z(2); K11(5,3) = K11(3,5);
            K11(2,6) = obj.elementProps.Y(2);  K11(6,2) = K11(2,6);
            
            K12 = diag([-obj.elementProps.X,-obj.elementProps.Y(1),...
                        -obj.elementProps.Z(1),-obj.elementProps.S,...
                        obj.elementProps.Z(4),obj.elementProps.Y(4)]); 
            K12(3,5) = -obj.elementProps.Z(2); K12(5,3) = -K12(3,5);
            K12(2,6) = obj.elementProps.Y(2); K12(6,2) = -K12(2,6);
            
            K22 = K11;
            K22(3,5) = -K11(3,5); K22(5,3) = K22(3,5);
            K22(2,6) = -K11(2,6); K22(6,2) = K22(2,6);

            % Assemble Together
            Kel = zeros(12);
            Kel(1:6,1:6) = K11;
            Kel(7:12,1:6) = K12';
            Kel(1:6,7:12) = K12;
            Kel(7:12,7:12) = K22;
            
            % Transform 
            Kel = obj.transform*Kel*obj.transform';
            Kout = Kel(:);
            
        end
        function [Kn,Kt] = BuildNLStiff(obj,def,varargin)
            % 
            
            % Extract Properties (remove in future)
            E = obj.material.getProp('E');
            J = obj.elementProps.L/2; L = obj.elementProps.L;
            
            %  Check that Deflctions is Correct 
            if size(def,1) ~= 12 || size(def,2) ~= 1
                error('The size of the provided deflections is incorrect')
            end
            
            % Transform deformed shape(s) from global to local coords
            dr = obj.transform'*def; dr = dr(obj.indx);
            
            % Compute element stiffness matrix, Kel, in element coord system
            K1bm = zeros(10);  K2b = zeros(10);  K1m = zeros(10);
            
            % Perform Gauss Quadrature - Cook, p. 170
            for ii = 1:length(obj.nlQuadPoints)
                x = (1+obj.nlQuadPoints(ii))*L/2; z = x/L;
                Bu = [-1/L 0 0 0 0 1/L 0 0 0 0];
                Bw = [0, (-6*z+6*z^2)/L, (-6*z+6*z^2)/L, -(1-4*z+3*z^2), 1-4*z+3*z^2 ...
                      0, (6*z-6*z^2)/L, (6*z-6*z^2)/L, -(-2*z+3*z^2), -2*z+3*z^2];
                
                dudx_r = Bu*dr;
                dwdx_r = Bw*dr;
                
                K1bm = K1bm + 0.5*E*obj.sectionProps.A*dwdx_r*Bu'*Bw*J*obj.nlQuadWeights(ii);
                K1m = K1m + 0.5*E*obj.sectionProps.A*dudx_r*Bw'*Bw*J*obj.nlQuadWeights(ii);
                K2b = K2b + 0.5*E*obj.sectionProps.A*dwdx_r*dwdx_r*Bw'*Bw*J*obj.nlQuadWeights(ii);
            end
            
            % Assemble Matrices
            Kne = zeros(12);  Kte = Kne;
            Kne(obj.indx,obj.indx) = K1m+K1bm+K1bm'+K2b;
            Kte(obj.indx,obj.indx) = 2*K1m+2*K1bm+2*K1bm'+3*K2b;
            
            % Transform Kne and Kte to Global Coordinates
            Kne = obj.transform*Kne*obj.transform';
            Kte = obj.transform*Kte*obj.transform';
            
            % Output element matrices rather than vector if desired
            Kn = Kne(:);
            Kt = Kte(:); 
        end
        function [K1,K2] = BuildNLmodal(obj,phi1,phi2)
            % This script is adapted from BobTran function : 
            % 
            %  [K1e,K2e] = beam_n13(xyz,mat,ele,prop,modepr,iel);
            %  R Gordon   8/8/00
            %           11/19/02 corrected for sign errors in [Bw]
            %            1/30/08 updated for complex modes
            %
            %  Computes the finite element nonlinear stiffness matrices, K1bm, K1m,
            %  and K2b, for a 3-D C1 beam element.  This element is based on
            %  work by P. Dumir and A. Bhaskar, 1988.  This function is
            %  called by the nonlinear matrix assembler functions, ASSEM_NF.M
            %  and ASSEM_NM.M.
            % 
            %  Inputs :
            %       obj = B2 element object
            %       phi1 = first mode
            %       phi2 = second mode
            %  Outputs :
            %       K1
            %       K2
            
            % Extract Properties (remove in future)
            E = obj.material.getProp('E'); L = obj.elementProps.L; J = L/2;
            A = obj.sectionProps.A;
            
            % transform deformed shape/s from global to local coords
            dr = obj.transform'*phi1;  dr = dr(obj.indx);
            ds = obj.transform'*phi2;  ds = ds(obj.indx);
            
            %  compute element stiffness matrix, Kel, in element coord system
            K1bm = zeros(10); K2b = zeros(10); K2u = zeros(10);
            
            % loop to perform Gauss quadrature
            for ii = 1:length(obj.nlQuadPoints)
                x = (1+obj.nlQuadPoints(ii))*L/2; z = x/L;
                Bu = [-1/L 0 0 0 0 1/L 0 0 0 0];
                Bw = [0, (-6*z+6*z^2)/L, (-6*z+6*z^2)/L, -(1-4*z+3*z^2), 1-4*z+3*z^2 ...
                      0, (6*z-6*z^2)/L, (6*z-6*z^2)/L, -(-2*z+3*z^2), -2*z+3*z^2];

                dudx_r = Bu*dr;
                dwdx_r = Bw*dr;
                dwdx_s = Bw*ds;
                
                K1bm = K1bm + .5*E*A*dwdx_r*Bu.'*Bw*J*obj.nlQuadWeights(ii);
                K2u = K2u + .5*E*A*dudx_r*Bw.'*Bw*J*obj.nlQuadWeights(ii);
                K2b = K2b + .5*E*A*dwdx_r*dwdx_s*Bw.'*Bw*J*obj.nlQuadWeights(ii);
            end
            
            % Initialize full matrix
            K1e = zeros(12); K2e = K1e;
            
            % Fill full matrix
            K1e(obj.indx,obj.indx) = K1bm+K1bm.'+K2u;
            K2e(obj.indx,obj.indx) = K2b;
            
            % Transform K1e & K2e to global coordinates
            K1 = obj.transform*K1e*obj.transform';
            K2 = obj.transform*K2e*obj.transform';
            
            % Turn into vector
%             K1 = K1e(:); K2 = K2e(:);
        end
        function [fInt] = GetFint(obj,x)
            
            % Build linear stiffness matrix
            [K] = BuildStiff(obj, []);
            
            % Reshape and multiply by displacement
            fInt = reshape(K,obj.nDof,obj.nDof)*x;   
            
            
        end
        function [fIntNl,Kt,Kn] = GetFintNl(obj,x)
            
            % Build linear stiffness matrix
            [Kn,Kt] = BuildNLStiff(obj, x);
            
            % Reshape and multiply by displacement
            fIntNl = reshape(Kn,obj.nDof,obj.nDof)*x;  
        end
        function [tMin] = GetTimeStep(obj)
            
            % Material constant
            c = sqrt(obj.material.getProp('E')/...
                     obj.material.getProp('rho'));
            
            % 
            tMin = obj.elementProps.L/c;
            
        end
        
        % ----- Stress/Strain Generation Functions ---
        function [Kss] = BuildStressStiff(obj,sigmaVec,xEle)
            % Compute the stress stiffness matrix of a C0 continous
            % beam element. The only stress used is the axial stress of the
            % beam element.
            % Inputs : 
            %           obj = B2 Object
            %           sigmaVec = stress vector [Sx,Sy,Sz]
            % Ouputs :
            %           Kss = stress stiffness matrix
            
            % Only need axial stress
            sigmaAxial = sigmaVec(1);
            
            % Grab length
            l = obj.elementProps.L;
            
            %  compute element stress stiffness matrix, Kss, in element coords
            Ksse = zeros(12); indy = [2 6 8 12]; indz = [3 5 9 11];     
            Ksse(indy,indy) = sigmaAxial*obj.sectionProps.A...
                            /(30*l)*[36    3*l  -36   3*l; ...
                                   3*l  4*l^2 -3*l -l^2; ...
                                   -36   -3*l   36  -3*l; ...
                                   3*l   -l^2 -3*l 4*l^2];
            Ksse(indz,indz) = sigmaAxial*obj.sectionProps.A...
                            /(30*l)*[36   3*l  -36  -3*l; ...
                                   3*l  4*l^2  3*l -l^2; ...
                                    -36   3*l  36  -3*l; ...
                                   -3*l  -l^2  -3*l 4*l^2];
            
            %  transform Kss to global coordinates
            Kss = obj.transform()*Ksse*obj.transform()';
            Kss = Kss(:);
        end
        function [Sele,Eele,Pele] = ComputeThermalLoads(obj,varargin)
            % This functions computes the thermal effects of applied
            % temperature to a B2 element.
            
            % Check if nodal temperatues are set directly
            
            % Check provided information
            if nargin > 3
                nodalTemp = obj.material.getProp('tRef')*ones(obj.nNode,1);
                nodalDef = varargin{2};
            elseif nargin > 2 % nodal temps and deformations supplied
                nodalTemp = varargin{1};
                nodalDef = varargin{2};
            elseif nargin > 1 % only nodal temps provided
                nodalTemp = varargin{1};
                nodalDef = zeros(obj.nDof,1);
            else % use nodal temps set in nodeObj (for 
%                 nodalTemp = 
            end
            
            % Transform to local coordinates
            ind = [1:3 7:9];
            def = nodalDef(ind);
%             [obj] = obj.BuildTransform(def);
            
            xLocal = obj.GetNodalCoords()+reshape(nodalDef(ind),3,2);
            norg = sum(xLocal,2)/4;


            % Grab reference temperature, thermal expansion and section
            refTemp = obj.material.getProp('tRef');
            alpha = obj.material.getProp('alpha');
            E = obj.material.getProp('E');
            
            % compute the average element temperature
            avgTemp = mean(nodalTemp);
           
            % Difference from provided reference temp
            thermalGrad = avgTemp-refTemp;
            
            % compute thermal stresses
            Ee = alpha*thermalGrad;
            Se = E*Ee;
            
            % Nodal thermal loads
            Pel = zeros(12,1); 
            Pel([1 7]) = [-1 1].*obj.sectionProps.A*E*alpha*thermalGrad;
            
            % Initialize and set variables
            Sele = zeros(3,1); Eele = zeros(3,1);
            Sele(1) = -Se; Eele(1) = -Ee;

            %  transform Pel to global coordinates
            Pele = obj.transform*Pel;
            
        end
        function [s,varargout] = ComputeStress(obj,xIn,varargin)
            % This function computes the stresses the 2-node beam element
            % additionally if the strains are desired to be outputed they 
            % can as well.
            
            % Check inputs
            if nargin > 2
                % Compute strains
                [e] = ComputeStrain(obj,xIn,varargin); 
            else
                % Compute strains
                [e] = ComputeStrain(obj,xIn); 
            end
            
            % Convert strains to stresses - axial only
            s = zeros(1,3);
            s(1) = obj.material.getProp('E')*sum(e);
            
            % Ouput strains if desired
            if nargout > 1
                varargout{1} = e;
            end
            
        end
        function [e] = ComputeStrain(obj,xIn,varargin)
            
            % Check for output - default is at surface of beam
            if nargin > 2 
                cy = varargin{1}(1);
                cz = varargin{1}(2);
            else
                % Compute section props
                cy = obj.sectionProps.t/2; cz = 0; 
            end
                        
            % tranform to local coordinate
            localDef = obj.transform'*xIn;
            
            % Strain computation
            if obj.continuity == 0
                e(1) = obj.dB.u*localDef;
                e(2) = .5*(obj.dB.wy*localDef)^2;
                e(3) = .5*(obj.dB.wz*localDef)^2;
                e(4) = -cy*obj.dB.tz*localDef;
                e(5) =  cz*obj.dB.ty*localDef;
            elseif obj.continuity == 1
                e(1) = obj.dB.u*localDef;
                e(2) = 0.25*((obj.dB.wy1*localDef)^2+(obj.dB.wy2*localDef)^2);
                e(3) = 0.25*((obj.dB.wz1*localDef)^2+(obj.dB.wz2*localDef)^2);
                e(4) = -0.5*cy*(obj.dB.tz1*localDef+obj.dB.tz2*localDef);
                e(5) = 0.5*cz*(obj.dB.ty1*localDef+obj.dB.ty2*localDef);
            end
            
            
        end
        
        % ---- Additional build functions --- %
        function [obj,varargout] = BuildTransform(obj,varargin)
            % This function builds the transformation matrix of the beam
            % element to rotate from local CS to global CS.
            %
            % Inputs :
            %           obj
            %           varargin{1} = deflection
            % Outputs :
            %           obj
            %           varargout{1} = transformation about deformed state
            
            
            % Grab coordinates
            n1 = obj.nodes{1}.GetXYZ();  n2 = obj.nodes{2}.GetXYZ();
            n1 = n1 + obj.offSet(:,1); n2 = n2 + obj.offSet(:,2);
            
            % If deformation is applied include in transformatoin
            if nargin > 1
                def = reshape(varargin{1},3,2)';
                n1 = n1 + def(1,:)'; n2 = n2 + def(2,:)';
            end
            
            % Compute length
            l = norm(n1-n2);
            
            % Local Coordinates
            xbi = (n2'-n1')/l;
            %             ybi = cross(obj.csVec,xbi);
            ybi2 = null(xbi); ybi = ybi2(:,1)';
            zbi = cross(xbi,ybi);
            obj.normal = zbi;
            
            % Individual matrix
            T = [xbi; ybi; zbi];
            
            % Build transformation
            TT = zeros(12);
            TT(1:3,1:3) = T'; TT(4:6,4:6) = T';
            TT(7:9,7:9) = T'; TT(10:12,10:12) = T';
            
            % Check arguments
            if nargout > 1
                varargout{1} = TT;
            else
                obj.transform = TT;
            end
            
        end
        function [obj] = BuildOffSetTransformation(obj)
            
            TToff = zeros(12);
            T1off = eye(6);
            T2off = T1off;
            T1off(1,5) = -xyz(n1,4)+c1(3);
            T1off(2,4) = -T1off(1,5);
            T1off(1,6) = xyz(n1,3)-c1(2);
            T1off(3,4) = -T1off(1,6);
            T1off(2,6) = -xyz(n1,2)+c1(1);
            T1off(3,5) = -T1off(2,6);
            T2off(1,5) = -xyz(n2,4)+c2(3);
            T2off(2,4) = -T2off(1,5);
            T2off(1,6) = xyz(n2,3)-c2(2);
            T2off(3,4) = -T2off(1,6);
            T2off(2,6) = -xyz(n2,2)+c2(1);
            T2off(3,5) = -T2off(2,6);
            TToff(1:6,1:6) = T1off;
            TToff(7:12,7:12) = T2off;
            
        end
        function [obj] = BuildSection(obj)
            % This function computes the derived sectional properties of a
            % beam from a given cross-section shape and parameters.
            %
            % Inputs  : 
            %           obj
            % Outputs : 
            %
            %       
                        
            % Assign Material if not specified
            E = obj.material.getProp('E');
            nu = obj.material.getProp('nu');
            G =  E/(2*(1 + nu));
            
            obj.material.setProp('G', G);
            
            % Extract Section Properties from Parent Element
            obj.sectionProps.t = obj.section.t;
            obj.sectionProps.w = obj.section.w;
            
            % Calculate Derived Section Properties
            obj.sectionProps.A = obj.section.t*obj.section.w;
            obj.sectionProps.Iyy = (obj.section.t^3*obj.section.w)/12;
            obj.sectionProps.Izz = (obj.section.t*obj.section.w^3)/12;
            obj.sectionProps.J = (obj.section.t^2*obj.section.w^2)/12;
            obj.sectionProps.Ky = (obj.section.t^2*obj.section.w^2)/12;
            obj.sectionProps.Kz = (obj.section.t^2*obj.section.w^2)/12;
            obj.sectionProps.K = (obj.section.t^2*obj.section.w^2)/12;
            
            % Calculate Element Length
            obj.elementProps.L = norm(obj.nodes{1}.GetXYZ()-obj.nodes{2}.GetXYZ());
            
             % X - Properties 
            obj.elementProps.X = (obj.sectionProps.A*E)/obj.elementProps.L;
            obj.elementProps.S = (obj.sectionProps.K*G)/obj.elementProps.L;
            
            % Y - Properties
            obj.elementProps.PHIY = (12*obj.sectionProps.Izz*E*obj.sectionProps.Ky)/(obj.sectionProps.A*G*obj.elementProps.L^2);
            obj.elementProps.Y(1) = (12*obj.sectionProps.Izz*E)/((1+obj.elementProps.PHIY)*obj.elementProps.L^3);
            obj.elementProps.Y(2) = (6*obj.sectionProps.Izz*E)/((1+obj.elementProps.PHIY)*obj.elementProps.L^2);
            obj.elementProps.Y(3) = ((4+obj.elementProps.PHIY)*obj.sectionProps.Izz*E)/((1+obj.elementProps.PHIY)*obj.elementProps.L);
            obj.elementProps.Y(4) = ((2-obj.elementProps.PHIY)*obj.sectionProps.Izz*E)/((1+obj.elementProps.PHIY)*obj.elementProps.L);
            
            % Z - Properties
            obj.elementProps.PHIZ = (12*obj.sectionProps.Iyy*E*obj.sectionProps.Kz)/(obj.sectionProps.A*G*obj.elementProps.L^2);
            obj.elementProps.Z(1) = (12*obj.sectionProps.Iyy*E)/((1+obj.elementProps.PHIZ)*obj.elementProps.L^3);
            obj.elementProps.Z(2) = (6*obj.sectionProps.Iyy*E)/((1+obj.elementProps.PHIZ)*obj.elementProps.L^2);
            obj.elementProps.Z(3) = ((4+obj.elementProps.PHIZ)*obj.sectionProps.Iyy*E)/((1+obj.elementProps.PHIZ)*obj.elementProps.L);
            obj.elementProps.Z(4) = ((2-obj.elementProps.PHIZ)*obj.sectionProps.Iyy*E)/((1+obj.elementProps.PHIZ)*obj.elementProps.L);
            
        end 
        function [obj] = BuildShape(obj)
            % These are the generalization of a shape function for a 3D
            % beam. 
            % Inputs  : 
            %           obj = 
            
            % Outputs : 
            %
            %
            
            % Grab for ease
            l = obj.elementProps.L;
            
            % Define derivatives of shape functions
            if obj.continuity == 0
               obj.dB.u = [-1/l 0 0 0 0 0 1/l 0 0 0 0 0];
               obj.dB.wy = [0 -1/l 0 0 0 0 0 1/l 0 0 0 0];
               obj.dB.wz = [0 0 -1/l 0 0 0 0 0 1/l 0 0 0];
               obj.dB.ty = [0 0 0 0 -1/l 0 0 0 0 0 1/l 0];
               obj.dB.tz = [0 0 0 0 0 -1/l 0 0 0 0 0 1/l];
            elseif obj.continuity == 1
               obj.dB.u = [-1/l 0 0 0 0 0 1/l 0 0 0 0 0];
               obj.dB.wy1 = [0 0 0 0 0 1 0 0 0 0 0 0];
               obj.dB.wz1 = [0 0 0 0 1 0 0 0 0 0 0 0];
               obj.dB.wy2 = [0 0 0 0 0 0 0 0 0 0 0 1];
               obj.dB.wz2 = [0 0 0 0 0 0 0 0 0 0 1 0];
               obj.dB.ty1 = [0 0 -6/l^2 0 -4/l 0 0 0 6/l^2 0 -2/l 0];
               obj.dB.tz1 = [0 -6/l^2 0 0 0 -4/l 0 6/l^2 0 0 0 -2/l];
               obj.dB.ty2 = [0 0 6/l^2 0 2/l 0 0 0 -6/l^2 0 4/l 0];
               obj.dB.tz2 = [0 6/l^2 0 0 0 2/l 0 -6/l^2 0 0 0 4/l];
            end
            
        end
        function [fEle] = ComputePressureLoads(obj,pVal,varargin)
            
            % Check provided information
            if nargin > 2
                nodalDef = varargin{2};
            else
                nodalDef = zeros(obj.nDof,1);
            end
            
            % Transform to local coordinates
            indx = [1:3 7:9];
            xLocal = obj.GetNodalCoords()+reshape(nodalDef(indx),3,2);
            norg = sum(xLocal,2)/2;
            u = [xLocal(1,:)'-norg(1) xLocal(2,:)'-norg(2) xLocal(3,:)'-norg(3)];
%             xLocal = u*obj.Te(1:2,:)';
            
            pVec = obj.elementProps.L*obj.sectionProps.w*pVal*obj.normal;
            
            fEle = zeros(obj.nDof,1);
            
            % Ad hoc procedure
            % Create pressure loads for translational dof
            fEle(1:3,:) = 0.5*pVec;
            fEle(7:9,:) = 0.5*pVec;
        
        end
        
        % Utility functions
        function coords = GetNodalCoords(obj)
            
            c1 = obj.nodes{1}.GetXYZ(); % 1st Nodal Coordinate
            c2 = obj.nodes{2}.GetXYZ(); % 2nd Nodal Coordinate
 
            coords = [c1, c2]; % All nodal cooridinates
        end
        function A = GetArea(obj)
           A =  obj.sectionProps.t*obj.elementProps.L;
        end
        function V = GetVolume(obj)
            V =  obj.sectionProps.t*obj.sectionProps.t*obj.elementProps.L;
        end
    end
    methods (Static)
        function [N] = Shape(xi)
                % 1D - Shape functions for a 2-Node C1-Continous beam element
                % these are all normalized to unit length
                %
                % inputs :
                %           xi - normalized axial position
                % Outputs :
                %           N
                % 
                
                % Axial Shape functions (displacement and rotation)
%                 N1 = 1-xi; N1xi = -1; 
%                 N2 = xi; N2xi = 1;
                
                % Write bending shape functions (u, v, ru, rv)
                N = zeros(1,12);
                N(1) = (1-xi); N(7) = xi;   % Axial a, b
                N(4) = (1-xi); N(10) = xi;  % Torsion a, b (not accurate)
                N(2) = 1 - 3*xi^2 + 2*xi^3; % Bending 1 - a 
                N(3) = 1 - 3*xi^2 + 2*xi^3; % Bending 2 - a 
                N(5) = xi - 2*xi^2 + xi^3;  % Rotation 1 - a 
                N(6) = xi - 2*xi^2 + xi^3;  % Rotation 2 - a 
                N(8) = 3*xi^2 - 2*xi^3;     % Bending 1 - b 
                N(9) = 3*xi^2 - 2*xi^3;     % Bending 2 - b 
                N(11) = - xi^2 + xi^3;      % Rotation 1 - b 
                N(12) = - xi^2 + xi^3;      % Rotation 2 - b 
                % Write bending shape function derivatives
%                 B = zeros()
%                 B(1) = -6 + 12*xi; 
%                 B(2)  = -4 + 6*xi; 
%                 B(3)  = 6-12*xi; 
%                 B(4)  = -2 + 6*xi;

                % Assembly B matrix
%                 B = [1 N1xi -N1xi 1 N2xi N3xi N4xi];

        end
    end
end

