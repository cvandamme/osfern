classdef Spring < Element
    % This class represents a 3D spring element in which the stiffness is
    % only along the local axis of the spring. 
    % 
    % The spring will contain a single stiffness (and damping) value. 
    % Additionally a nonlinaer stiffness and damping values may be provided
    % as a function handle. 
    % A mass can be added to the spring that is evenly divided between the
    % ends of the spring.
    % 
    % Torsional Spring : 
    % Only a single node is needed. Plotted as a ...
    % 
    % Supply the socal coordinate system (csVec) with a negative value along the
    % axis of rotation of the spring.
    % 
    
    properties
        sLength % spring original length 
        kLin  % Linear stiffness [kx,ky,kx,krx,kry,kz]
        fNl   % Nonlinear force function handle
        kNl   % Nonlinear Stiffness Function (function handle)
        cLin  % Linear damping 
        cNl   % Nonlinear damping Function (function handle)
        massVal % discete mass added at nodal points
        csVec % Local beam coordinate system
        delta_pl = 0;  % Enforced initial displacement for modeling preload
        sDof = []; % This is 
    end
    
    properties (Constant)
        nDof = 12;
        nNode = 2;
        activeDof = 6;
    end
    
    methods
        % Constructor function
        function [obj] = Spring(id)
            
            % Call to superclass
            obj = obj@Element(id);
            
        end
        
        % Build the element
        function [obj] = Build(obj, nodes, kLinIn, cLinIn, massVal,...
                              csVec,varargin)
            
            % Check provided nodes
            if length(nodes) ~= 2
                error(' Specified number of nodes is incorrect, must be 2')
            else
                obj.nodes = nodes;
            end
            
            % Check inputs 
            if length(kLinIn) == 1
                obj.kLin = kLinIn;
            else
                error('Currently must supply entire stiffness')
            end
            
            % Check inputs
            if length(cLinIn) == 1 || isempty(cLinIn)
                obj.cLin = cLinIn;
            else
                error('Currently must supply entire damping')
            end
           
            % Check inputs
            if length(massVal) == 1 || isempty(massVal)
                obj.massVal = massVal;
            else
                error('Currently must supply entire damping')
            end
            
            % Cross section vector
            obj.csVec = csVec;
            
            % Check if enforcing that the spring is applied based on dof
            % and not location
            if nargin > 6
                obj.sDof = varargin{1};
            end
            
            % Build section 
%             obj.BuildSection();
            
            % Build Transformation
            obj.BuildTransformation();
            
        end
        
        % Rebuild the element
        function [obj] = ReBuild(obj)
        end
        
        % Build Stiffness
        function [kOut] = BuildStiff(obj,x)
            
            % For stiffness simply apply to local axial direction
            kEle = zeros(12);
            
            % Check if a single node spring or a two node
            if isempty(obj.sDof)
                kEle(1,1) = obj.kLin; kEle(7,7) = obj.kLin;
                kEle(1,7) = -1*obj.kLin; kEle(7,1) = -1*obj.kLin;
                % Tranform to global coordinates
                kOut = obj.transform*kEle*obj.transform';
            else
                kEle(obj.sDof,obj.sDof) = obj.kLin;
                kEle(obj.sDof+6,obj.sDof+6) = obj.kLin;
                kEle(obj.sDof,obj.sDof+6) = -1*obj.kLin;
                kEle(obj.sDof+6,obj.sDof) = -1*obj.kLin;
                % Does not use a transformation, assumes applied to global
                % dof 
                kOut = kEle;
            end
                        
            kOut = kOut(:);
        end
        function [kOut] = BuildStressStiffness(obj)
            % Check if preload is present
            if ~isempty(obj.delta_pl)
                % Check if a single node spring or a two node
                if isempty(obj.sDof)
                    kEle(1,1) = obj.kLin; kEle(7,7) = obj.kLin;
                    kEle(1,7) = -1*obj.kLin; kEle(7,1) = -1*obj.kLin;
                    % Tranform to global coordinates
                    kOut = obj.transform*kEle*obj.transform';
                else
                    kEle(obj.sDof,obj.sDof) = obj.kLin;
                    kEle(obj.sDof+6,obj.sDof+6) = obj.kLin;
                    kEle(obj.sDof,obj.sDof+6) = -1*obj.kLin;
                    kEle(obj.sDof+6,obj.sDof) = -1*obj.kLin;
                    % Does not use a transformation, assumes applied to global
                    % dof 
                    kOut = kEle;
                end
            end
        end
        
        % Build Mass
        function [mOut] = BuildMass(obj)
            
            % Currently no mass is added
            mEle = zeros(12);
            mEle(1,1) = obj.massVal/2; mEle(7,7) = obj.massVal/2;
            
            % Tranform to gloval coordinates
            kOut = obj.transform*mEle*obj.transform';
            mOut = kOut(:);
        end
        function [mOut] = BuildMassDiag(obj)
            
            % Currently no mass is added
            mEle = zeros(12);
            mEle(1,1) = obj.massVal/2; mEle(7,7) = obj.massVal/2;
            
            % Tranform to gloval coordinates
            kOut = obj.transform*mEle*obj.transform';
            mOut = kOut(:);
        end
        
        % Build Damping
        function [cOut] = BuildDamping(obj)
            
            % For damping simply apply to local axial direction
            cEle = zeros(12);
            cEle(1,1) = obj.cLin; cEle(7,7) = obj.cLin;
            cEle(1,7) = -obj.cLin; cEle(7,1) = -obj.cLin;
            
            % Tranform to gloval coordinates
            cOut = obj.transform*cEle*obj.transform';
            
            cOut = cOut(:);
        end
        
        % Build Nonlinear Stiffness
        function [Kn,Kt] = BuildNLStiff(obj,x)
            % This function generates the nonlinear stiffness term of the
            % spring element based upon a user provided function handle
            % that must accept the displacement (scalar) of the spring
            % along its local axis.
            % Inputs :
            %       obj
            %       x = nodal displacements
            % Notes : 
            %        if trying to use an explicity force-displacement curve
            %        either fit a polynomial to the curve and create
            %        function handle based on that fit or use function 
            %        handle that interpolates the data.
            % Example 1 :
            %       allpha = 1; beta =2;
            %       Kt1 = @(d) 2*alpha*d + 3*beta*d^2;
            % Example 2 : 
            %       x = linspace(0,1); y = 2*alpha*x + 3*beta*x.^2;
            %       Kt2 = @(d) interp1(x,y,d);

            
            
            % If no nonlinear function is provided then the output is zero
            if isempty(obj.kNl)
%                 warning(['No Nonlienar function provided will assume no', ...
%                     'nonlinearity'])
                obj.kNl = @(d) 0*d;
            end
            
            
            % Transform the displacements to local coordinate system
            xLocal = obj.transform'*x;
            
            % Only Interested in local axial deformation
            d = norm(xLocal(1:3) - xLocal(7:9));
            
            % For stiffness simply apply to local axial direction
            kEle = zeros(12); kVal = obj.kNl(d(1));
            kEle(1,1) = kVal; kEle(7,7) = kVal;
            kEle(1,7) = -kVal; kEle(7,1) = -kVal;
            
            % Tranform to gloval coordinates
            kOut = obj.transform*kEle*obj.transform';
            
            Kn = kOut(:); Kt = kOut(:);
                
        end
        
        % Build Nonlinear Damping
        function [cOut,cOut2] = BuildNlDamping(obj,v)
            
            % If no nonlinear function is provided then the output is zero
            if isempty(obj.cNl)
%                 warning(['No Nonlienar function provided will assume no', ...
%                     'nonlinearity'])
                obj.kNl = @(d) 0*d;
            end
            
            
            % Transform the displacements to local coordinate system
            vLocal = obj.transform'*v;
            
            % Only Interested in local axial deformation
            d = norm(vLocal(1:3) - vLocal(7:9));
            
            % For stiffness simply apply to local axial direction
            cEle = zeros(12); kVal = obj.kNl(d(1));
            cEle(1,1) = kVal; cEle(7,7) = kVal;
            cEle(1,7) = -kVal; cEle(7,1) = -kVal;
            
            % Tranform to gloval coordinates
            cOut = obj.transform*cEle*obj.transform';
            
            cOut = cOut(:);       
            cOut2 = cOut;
        end
        
        % Get Internal force of element
        function [fInt,Kn,Kt] = GetFintNl(obj,x)
            
            
            [Kn,Kt] = obj.BuildNLStiff(x);
            kMat = reshape(Kn,obj.nDof,obj.nDof);
            fInt = 0.33*kMat*x; 
        end
        
        function [Sele,Eele,Pele] = ComputeThermalLoads(obj,varargin)
            % This functions computes the thermal effects of applied
            % temperature to a spring element. Spring element is
            % essentially a bar element so will use unaxial extension. 
            
            % Check if nodal temperatues are set directly
            
            % Check provided information
            if nargin > 3
                nodalTemp = obj.material.getProp('tRef')*ones(obj.nNode,1);
                nodalDef = varargin{2};
            elseif nargin > 2 % nodal temps and deformations supplied
                nodalTemp = varargin{1};
                nodalDef = varargin{2};
            elseif nargin > 1 % only nodal temps provided
                nodalTemp = varargin{1};
                nodalDef = zeros(obj.nDof,1);
            else % use nodal temps set in nodeObj (for 
%                 nodalTemp = 
            end
            
            % Transform to local coordinates
%             ind = [1:3 7:9];
%             xLocal = obj.GetNodalCoords()+reshape(nodalDef(ind),3,2);
% 
%             % Grab reference temperature, thermal expansion and section
%             refTemp = 0; %obj.material.getProp('tRef');
%             alpha = 1.22E-005; %obj.material.getProp('alpha');
%             
%             % compute the average element temperature
%             avgTemp = mean(nodalTemp);
%            
%             % Difference from provided reference temp
%             thermalGrad = avgTemp-refTemp;
%             
%             % compute thermal stresses
%             Ee = alpha*thermalGrad;
%             Se = E*Ee;
%             
%             % Nodal thermal loads
%             Pel = zeros(12,1); 
%             Pel([1 7]) = [-1 1].*obj.kLin*alpha*thermalGrad/obj.L;
%             
%             % Initialize and set variables
            Sele = zeros(3,1); Eele = zeros(3,1);
%             Sele(1) = -Se; Eele(1) = -Ee;
% 
%             %  transform Pel to global coordinates
%             Pele = obj.transform*Pel;
            Pele = zeros(obj.nDof,1);
            
        end
        function [Eele] = ComputeStrain(obj,xIn)
            
            % Strain is only along axis of spring
            xLocal = obj.transform*xIn;
            xStrain = (xLocal(7) - xLocal(1))/1;
            Eele = zeros(3,1);
            Eele(1) = xStrain;
        end
        function [Sele] = ComputeStress(obj,xIn)
            
            warning('No Stress for the spring, will output strain')
            [Eele] = ComputeStrain(obj,xIn);
            % Strain is only along axis of spring
            Sele = Eele;
        end
        
        % Build Transformation Matrix
        function [obj] = BuildTransformation(obj)
            
            % Grab coordinates
            n1 = obj.nodes{1}.GetXYZ();  n2 = obj.nodes{2}.GetXYZ();
            
            % Compute length
            l = norm(n1-n2);
            
            % Local Coordinates
            xbi = (n2'-n1')/l;
            ybi = cross(obj.csVec,xbi);
            zbi = cross(xbi,ybi);
            
            % Build Transformation
            T = [xbi; ybi; zbi];
            obj.transform = zeros(12);
            obj.transform(1:3,1:3) = T'; 
            obj.transform(4:6,4:6) = T'; 
            obj.transform(7:9,7:9) = T';
            obj.transform(10:12,10:12) = T';
        end
        function fEle = ComputePressureLoads(obj,pVal)
            fEle = zeros(obj.nDof,1);
        end
        
        % Set Functions
        function [obj] = SetLinearStiffness(obj,kLinIn,varargin)
            % Changes the linear stiffness of the spring 
            % Recommended to use DesignVariable object instead
            obj.kLin = kLinIn;
        end
        function [obj] = SetNonlinearForce(obj,fNlFun,varargin)
            
            % Add a nonlinear stiffness portion to the spring 
            obj.fNl = fNlFun;
            
            % For now, force the user to specify the nonlinear stiffness
            % function
            if nargin > 2
                obj.kNl = varargin{1};
            else
                error(['User must supply nonlinear force and nonlinear stiffness',...
                      'functions.'])
                obj.kNl = [];
            end
            
        end
        function [obj] = SetNonlinearStiffness(obj,kNlFun,varargin)
            % Add a nonlinear stiffness portion to the spring 
            obj.kNl = kNlFun;
            
        end
        function [obj] = SetLinearDamping(obj,cLinIn,varargin)
            % Changes the linear damping of the spring 
            % Recommended to use DesignVariable object instead
            obj.cLin = cLinIn;
        end
        function [obj] = SetNonlinearDamping(obj,cNlFun,varargin)
             % Add a nonlinear damping portion to the spring 
            obj.cNl = cNlFun;
        end
        function [obj] = SetPreload_Displacement(obj,delta_pl,varargin)
            % Set a prescribed preload to the model defined as a prescribed
            % displacement. F_pl = K*delta_pl
            % Inputs :
            %         delta_pl = varargin
            %         varargin = 
            % Ouputs :
            %         obj = Spring Object
            
            % Check input
            if length(x) ~= 1
                error('Preload must be a scalar')
            end
            
            % Assign displacement to object
            obj.delta_pl = delta_pl;
            
            % Calculate preload force
            obj.f_pl = obj.kLin*delta_pl;
            
        end
        function [obj] = SetPreload_Force(obj,f_pl,varargin)
            % Set a prescribed preload to the model defined as a prescribed
            % displacement. 
            %                f_pl = K*delta_pl -> delta_pl = K/f_pl
            % Inputs :
            %         delta_pl = varargin
            %         varargin = 
            % Ouputs :
            %         obj = Spring Object
            
            % Check input
            if length(x) ~= 1
                error('Preload must be a scalar')
            end
            
            % Assign force to object
            obj.f_pl = f_pl;
            
            % Calculate preload displacement
            obj.delta_pl = f_pl/obj.kLin;
            
        end

        % Get Functions
        function [kLinOut] = GetLinearStiffness(obj,varargin)
        end
        function [kNlOut] = GetNonlinearStiffness(obj,varargin)
        end
        function [cLinOut] = GetLinearDamping(obj,varargin)
        end
        function [cNlOut] = GetNonlinearDamping(obj,varargin)
        end
        function [dt] = GetTimeStep(obj)
            
            % Estimate largest frequency
            w = sqrt(obj.kLin/(0.25*obj.massVal));
          
            % Estimate the largest timestep
            if ~isempty(obj.cLin)
                dt = (2/w)*(sqrt(1+obj.cLin^2)-obj.cLin);
            else
                dt = 2/w;
            end
        end
        function [m] = GetMass(obj)
            m = obj.mass;
        end
        function [v] = GetVolume(obj)
            v = obj.volume;
        end
        function coords = GetNodalCoords(obj)
            
            c1 = obj.nodes{1}.GetXYZ(); % 1st Nodal Coordinate
            c2 = obj.nodes{2}.GetXYZ(); % 2nd Nodal Coordinate
 
            coords = [c1, c2]; % All nodal cooridinates
        end
        
        % Complex step finite differnce estimation
        function dfds = ComputeDerivative(obj,funHandle,s0)
            % This function computes the derivative of a function using the
            % complex step finite difference method.
            
            % Initialize matrix
            dfds = zeros(length(s0)); stp = 1e-10;
            
            % Loop through each free variable
            for ii = 1:length(s0)
                
                % Perturb the ith dof in the complex plane
                si = s0; si(ii) = s0(ii) + 1i*stp;
                
                % Take the complex part and divide by step size
                try
                    dfds(:,ii) = imag(funHandle(si))/stp;
                catch
                    % If complex doesn't work (i.e. for interpolation)
                    si(ii) = s0(ii) + stp;
                    dfds(:,ii) = (funHandle(si)-funHandle(s0))/stp;
                end
            end
        end
        
    end
    

end

