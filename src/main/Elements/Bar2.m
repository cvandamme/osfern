classdef Bar2 < Element
    % This class represents a 2 node bar element in which the stiffness is
    % only along the axis of the element. The bar will contain a single
    % stiffness (and damping) value. 
    % Additionally a nonlinaer stiffness value may be provided as a
    % function handle. 
    
    properties
        sLength % spring original length 
        kLin  % Linear stiffness [kx,ky,kx,krx,kry,kz]
        kNl   % Nonlinear Stiffness Function (function handle)
        cLin  % Linear damping 
        cNl   % Nonlinear damping Function (function handle)
        massVal % discete mass added at nodal points
        csVec % Local beam coordinate system
    end
    
    properties (Constant)
        nDof = 12;
        nNode = 2;
        activeDof = 6;
    end
    
    methods
        % Constructor function
        function [obj] = Bar2(id)
            
            % Call to superclass
            obj = obj@Element(id);
            
        end
        
        % Build the element
        function [obj] = Build(obj, nodes, material, A, csVec)
            
            % Check provided nodes
            if length(nodes) ~= 2
                error(' Specified number of nodes is incorrect, must be 2')
            else
                obj.nodes = nodes;
            end
            
            % Save Section
            obj.section.A = A;
            
            % Check inputs 
            if isa(material,'material')
                obj.material = material;
            else
                error('Currently must supply entire stiffness')
            end
            
            % Check inputs
            if length(cLinIn) == 1 || isempty(cLinIn)
                obj.cLin = cLinIn;
            else
                error('Currently must supply entire damping')
            end
           
            % Cross section vector
            obj.csVec = csVec;
            
            % Build section 
            obj.BuildSection();
            
            % Build Transformation
            obj.BuildTransformation();
            
        end
        
        % Rebuild the element
        function [obj] = ReBuild(obj)
        end
        
        % Build Stiffness
        function [kOut] = BuildStiff(obj,x)
            
            % For stiffness simply apply to local axial direction
            kEle = zeros(12);
            kEle(1,1) = obj.kLin; kEle(7,7) = obj.kLin;
            kEle(1,7) = -1*obj.kLin; kEle(7,1) = -1*obj.kLin;
            
            % Tranform to gloval coordinates
            kOut = obj.transform*kEle*obj.transform';
            
            kOut = kOut(:);
        end
        
        % Build Mass
        function [mOut] = BuildMass(obj)
            
            % Currently no mass is added
            mEle = zeros(12);
            mEle(1,1) = obj.massVal/2; mEle(7,7) = obj.massVal/2;
            
            % Tranform to gloval coordinates
            kOut = obj.transform*mEle*obj.transform';
            mOut = kOut(:);
        end
        function [mOut] = BuildMassDiag(obj)
            
            % Currently no mass is added
            mEle = zeros(12);
            mEle(1,1) = obj.massVal/2; mEle(7,7) = obj.massVal/2;
            
            % Tranform to gloval coordinates
            kOut = obj.transform*mEle*obj.transform';
            mOut = kOut(:);
        end
        
        % Build Damping
        function [cOut] = BuildDamping(obj)
            
            % For damping simply apply to local axial direction
            cEle = zeros(12);
            cEle(1,1) = obj.cLin; cEle(7,7) = obj.cLin;
            cEle(1,7) = -obj.cLin; cEle(7,1) = -obj.cLin;
            
            % Tranform to gloval coordinates
            cOut = obj.transform*cEle*obj.transform';
            
            cOut = cOut(:);
        end
        
        % Build Nonlinear Stiffness
        function [kOut,kOut2] = BuildNLStiff(obj,x)
            % This function generates the nonlinear stiffness term of the
            % spring element based upon a user provided function handle
            % that must accept the displacement (scalar) of the spring
            % along its local axis.
            % Inputs :
            %       obj
            %       x = nodal displacements
            % Notes : 
            %        if trying to use an explicity force-displacement curve
            %        either fit a polynomial to the curve and create
            %        function handle based on that fit or use function 
            %        handle that interpolates the data.
            % Example 1 :
            %       allpha = 1; beta =2;
            %       Kt1 = @(d) 2*alpha*d + 3*beta*d^2;
            % Example 2 : 
            %       x = linspace(0,1); y = 2*alpha*x + 3*beta*x.^2;
            %       Kt2 = @(d) interp1(x,y,d);

            
            
            % If no nonlinear function is provided then the output is zero
            if isempty(obj.kNl)
%                 warning(['No Nonlienar function provided will assume no', ...
%                     'nonlinearity'])
                obj.kNl = @(d) 0*d;
            end
            
            
            % Transform the displacements to local coordinate system
            xLocal = obj.transform'*x;
            
            % Only Interested in local axial deformation
            d = norm(xLocal(1:3) - xLocal(7:9));
            
            % For stiffness simply apply to local axial direction
            kEle = zeros(12); kVal = obj.kNl(d(1));
            kEle(1,1) = kVal; kEle(7,7) = kVal;
            kEle(1,7) = -kVal; kEle(7,1) = -kVal;
            
            % Tranform to gloval coordinates
            kOut = obj.transform*kEle*obj.transform';
            
            kOut = kOut(:);
            
            kOut2 = kOut;
                
        end
        
        % Build Nonlinear Damping
        function [cNlOut] = buildNlDamping(obj,v)
            
        end
        
        % Get Internal force of element
        function [fInt] = GetFintNl(obj,x)
            
            [kOut,kOut2] = obj.BuildNLStiff(x);
            kOut = reshape(kOut,obj.nDof,obj.nDof);
            fInt = kOut*x; 
        end
        
        % Build Transformation Matrix
        function [obj] = BuildTransformation(obj)
            
            % Grab coordinates
            n1 = obj.nodes{1}.GetXYZ();  n2 = obj.nodes{2}.GetXYZ();
            
            % Compute length
            l = norm(n1-n2);
            
            % Local Coordinates
            xbi = (n2'-n1')/l;
            ybi = cross(obj.csVec,xbi);
            zbi = cross(xbi,ybi);
            
            % Build Transformation
            T = [xbi; ybi; zbi];
            obj.transform = zeros(12);
            obj.transform(1:3,1:3) = T'; 
            obj.transform(4:6,4:6) = T'; 
            obj.transform(7:9,7:9) = T';
            obj.transform(10:12,10:12) = T';
        end
        
        % Set Functions
        function [obj] = SetLinearStiffness(obj,kLinIn,varargin)
        end
        function [obj] = SetNonlinearStiffness(obj,kNlFun,varargin)
            
            obj.kNl = kNlFun;
            
        end
        function [obj] = SetLinearDamping(obj,cLinIn,varargin)
        end
        function [obj] = SetNonlinearDamping(obj,cNlFun,varargin)
        end
        
        % Get Functions
        function [kLinOut] = GetLinearStiffness(obj,varargin)
        end
        function [kNlOut] = GetNonlinearStiffness(obj,varargin)
        end
        function [cLinOut] = GetLinearDamping(obj,varargin)
        end
        function [cNlOut] = GetNonlinearDamping(obj,varargin)
        end
        
        function [dt] = GetTimeStep(obj)
            
            % Estimate largest frequency
            w = sqrt(obj.kLin/(0.25*obj.massVal));
          
            % Estimate the largest timestep
            if ~isempty(obj.cLin)
                dt = (2/w)*(sqrt(1+obj.cLin^2)-obj.cLin);
            else
                dt = 2/w;
            end
        end
    end
    

end

