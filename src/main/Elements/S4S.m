classdef S4S < Element
    % S4 shell element with selectively reduced integration for the shear
    % stiffness terms. 
    
    properties (GetAccess = 'private', SetAccess = 'private')
        
        % Derived Section Properties
        sectionProps
        % A, Iyy, Izz, J, Ky, Kz
        elementProps
        % L, X, S
        % Y[y1, y2, y3, y4] , PHIY
        % Z[z1, z2, z3, z4] , PHIZ
        Te
        normal
        nlShape = struct('G', [], 'DBm', [], 'Ghat', []);
    end
    
    % --- Constant Properties Cannot Be Changed
    properties (Constant)
        
        nNode = 4;     % Number of nodes in this element
        activeDof = 6;  % Number of active DOF per node for this element
        nDof = 24;
        irot = [6, 12, 18, 24];  % In-plane rotation indices
        ir = [4, 5, 10, 11, 16, 17, 22, 23];  % Out-of-plane rotation indices
        mi = [1, 2, 7, 8, 13, 14, 19, 20]; % Membrane indices
        bsi = [3, 4, 5, 9, 10, 11, 15, 16, 17, 21, 22, 23]; % Bending/shear indices
        bi = [3, 9, 15, 21];
        QUADpointsBM = [-1/sqrt(3), 1/sqrt(3)]; % Quadrature Points; bending/membrane
        QUADweightsBM = [1, 1]; % Quadrature Weights; bending/membrane
        %         QUADpointsS = [-1/sqrt(3), 1/sqrt(3)]; % Quadrature Points; bending/membrane
        %         QUADweightsS = [1, 1]; % Quadrature Weights; bending/membrane
        blendFactor = 1; % Blending ratio of 1x1 shear quadrature to 2x2 shear quadrature
        % Used to avoid spurious rigid body modes in a flat plate mesh.
        %         blendFactor = 0;
        QUADpointsS = 0; % Quadrature Points; shear
        QUADweightsS = 2; % Quadrature Weights; shear
        
        QUADpointsNL = [-sqrt(0.6), 0, sqrt(0.6)];
        QUADweightsNL = [5/9, 8/9, 5/9];
        
        
    end
    
    methods
        % ----- Initialization ----- %
        function obj = S4S(id)
            
            obj = obj@Element(id);
            
        end
        
        % ----- Abstract Method Implementations -----
        function [obj] = Build(obj, node, material, section)
            % This function is called to generate the shell properties
            % based upon using node objects rather than structures.
            % Requires changing all of the subfunctions to use node object
            % methods. In Progress.
            
            obj.nodes = node;
            obj.material = material;
            obj.section = section;
            
            % Build Tranfromation Matrix
            obj.BuildTransform();
            
            % Build Section Properties
            obj.BuildSection();
            
            % Build shapes
            obj.BuildShapeNL();
            
            % Adds the element id to each node (speeds up assembly later)
            obj.SetElement2NodeId()
        end
        function [obj] = ReBuild(obj)
            % Build Tranfromation Matrix
            obj.BuildTransform();
            
            % Build Section Properties
            obj.BuildSection();
            
            % Build shapes
            obj.BuildShapeNL();
        end
        
        % ------ Element Generation Functions ------
        function [Mout] = BuildMass(obj)
            % This function builds the element mass matrix of a C1
            % Continous Beam Element in the element coordinate system.
            % Inputs :
            %
            % Outputs :
            %
            %
            
            
            % Extract Nodes (can be reomved in future for debugging)
            %             c1 = obj.nodes.coord(1,:)+off1; % 1st Nodal Coordinate
            %             c2 = obj.nodes.coord(2,:)+off2; % 2nd Nodal Coordinate
            %             c3 = obj.nodes.coord(3,:)+off1; % 3rd Nodal Coordinate
            %             c4 = obj.nodes.coord(4,:)+off2; % 4th Nodal Coordinate
            %             c = [c1 c2 c3 c4]; % All nodal cooridinates
            
            % Initialize Matrices
            Mel = zeros(24);
            
            % Extract Projected X and Y (inplane) Nodal Coordinates
            X = obj.localCoords(:, 1); Y = obj.localCoords(:, 2);
            
            % Full integration (2 x 2 Quadrature) for membrane and bending terms
            % -- Membrane, Bending and Shear Indicies are S4 Constant Properties
            % -- Quadrature Points and Weights are S4 Constant Properties
            for ii = 1:length(obj.QUADpointsBM)
                % Assign Points and Weights
                xi = obj.QUADpointsBM(ii); wi = obj.QUADweightsBM(ii);
                for jj = 1:length(obj.QUADpointsBM)
                    
                    % Assign Points and Weights
                    eta = obj.QUADpointsBM(jj); wj = obj.QUADweightsBM(ii);
                    
                    % Evaluate Shape Functions (hopefully can be removed)
                    [detJ, ~, ~, Nm, Nb] = obj.EvalMembraneBending(xi, eta, X, Y);
                    
                    % Membrane terms
                    Mel(obj.mi, obj.mi) = Mel(obj.mi, obj.mi) +...
                        obj.section.t*wi*wj*(Nm'*Nm)*detJ;

                    % Bending-translation terms
                    Nbb = Nb(:, 1:3:end);
                    Mel(obj.bi, obj.bi) = Mel(obj.bi, obj.bi) + ...
                        obj.section.t*wi*wj*(Nbb'*Nbb)*detJ; 
                    
                    % Bending-rotational terms
                    Nr = Nb(:,[2,3,5,6,8,9,11,12]);
                    Mel(obj.ir,obj.ir) = Mel(obj.ir,obj.ir)+...
                        (Nr'*Nr)*detJ*(obj.section.t^3)/12;
    
                end
            end
            Mel(12,12) = 1e-12; Mel(24,24) = 1e-12;
            
            % Tranform to Global System
            Mel = obj.material.getProp('rho')*obj.transform*Mel*obj.transform';
            
            % Change Ouput to Vector
            Mout = Mel(:);
            
        end
        function [Mout] = BuildMassDiag(obj)
            % This function builds the element mass matrix of a C1
            % Continous Beam Element in the element coordinate system.
            % Inputs :
            %
            % Outputs :
            %
            %
            
            
            % Extract Nodes (can be reomved in future for debugging)
            %             c1 = obj.nodes.coord(1,:)+off1; % 1st Nodal Coordinate
            %             c2 = obj.nodes.coord(2,:)+off2; % 2nd Nodal Coordinate
            %             c3 = obj.nodes.coord(3,:)+off1; % 3rd Nodal Coordinate
            %             c4 = obj.nodes.coord(4,:)+off2; % 4th Nodal Coordinate
            %             c = [c1 c2 c3 c4]; % All nodal cooridinates
            
            % Initialize Matrices
            Mel = zeros(24);
            
            % Extract Projected X and Y (inplane) Nodal Coordinates
            X = obj.localCoords(:, 1);
            Y = obj.localCoords(:, 2);
            
            % Full integration (2 x 2 Quadrature) for membrane and bending terms
            % -- Membrane, Bending and Shear Indicies are S4 Constant Properties
            % -- Quadrature Points and Weights are S4 Constant Properties
            for ii = 1:length(obj.QUADpointsBM)
                % Assign Points and Weights
                xi = obj.QUADpointsBM(ii);
                wi = obj.QUADweightsBM(ii);
                for jj = 1:length(obj.QUADpointsBM)
                    
                    % Assign Points and Weights
                    eta = obj.QUADpointsBM(jj);
                    wj = obj.QUADweightsBM(ii);
                    
                    % Evaluate Shape Functions (hopefully can be removed)
                    [detJ, ~, ~, Nm, Nb] = obj.EvalMembraneBending(xi, eta, X, Y);
                    
                    % Generate mass
                    Mel(obj.mi, obj.mi) = Mel(obj.mi, obj.mi) + obj.material.getProp('rho')*obj.section.t*wi*wj*(Nm'*Nm)*detJ;
                    Mel(obj.bsi, obj.bsi) = Mel(obj.bsi, obj.bsi) + obj.material.getProp('rho')*obj.section.t*wi*wj*(Nb'*Nb)*detJ;
                    
                    % This is suspect and needs further examination. 
                    Nbb = Nb(:, 1:3:end); % Put on bending nodes only??
                    Mel(obj.bi, obj.bi) = Mel(obj.bi, obj.bi) + obj.material.getProp('rho')*obj.section.t*wi*wj*(Nbb'*Nbb)*detJ; 
                end
            end
            
            % Tranform to Global System
            Mel = obj.transform*Mel*obj.transform';
            Mel = diag(diag(Mel)); % poor mans diagonalization
            % Change Ouput to Vector
            Mout = Mel(:);
            
        end
        function [Kout] = BuildStiff(obj, x)
            
            % Initialize Matrices
            Kel = zeros(24);
            
            % Extract Projected X and Y (inplane) Nodal Coordinates
            X = obj.localCoords(:, 1); Y = obj.localCoords(:, 2);
            
            % Full integration (2 x 2 Quadrature) for membrane and bending terms
            % -- Membrane, Bending and Shear Indicies are Constant Properties
            % -- Quadrature Points and Weights are Constant Properties
            for ii = 1:length(obj.QUADpointsBM)
                % Assign Points and Weights
                xi = obj.QUADpointsBM(ii); wi = obj.QUADweightsBM(ii);
                for jj = 1:length(obj.QUADpointsBM)
                    
                    % Assign Points and Weights
                    eta = obj.QUADpointsBM(jj); wj = obj.QUADweightsBM(ii);
                    
                    % Evaluate Shape Functions (hopefully can be removed)
                    [detJ, Bm, Bb, ~, ~] = obj.EvalMembraneBending(xi, eta, X, Y);
                    [~, Bs] = obj.EvalShear(xi, eta, X, Y);
                    
                    % Generate Stiffness
                    Kel(obj.mi, obj.mi) = Kel(obj.mi, obj.mi) + wi*wj*Bm'*obj.elementProps.Dmembrane*Bm*detJ;
                    Kel(obj.bsi, obj.bsi) = Kel(obj.bsi, obj.bsi) + wi*wj*Bb'*obj.elementProps.Dbending*Bb*detJ;
                    Kel(obj.bsi, obj.bsi) = Kel(obj.bsi, obj.bsi) + (1 - obj.blendFactor)*wi*wj*Bs'*obj.elementProps.Dshear*Bs*detJ;
                end
            end
            
            % Reduced integration for the shear terms
            for i = 1:length(obj.QUADpointsS)
                xi = obj.QUADpointsS(i);
                wi = obj.QUADweightsS(i);
                for j = 1:length(obj.QUADpointsS)
                    eta = obj.QUADpointsS(j);
                    wj = obj.QUADweightsS(j);
                    [detJ, Bs] = obj.EvalShear(xi, eta, X, Y);
                    Kel(obj.bsi, obj.bsi) = Kel(obj.bsi, obj.bsi) + obj.blendFactor*wi*wj*Bs'*obj.elementProps.Dshear*Bs*detJ;
                end
            end
            
            % Grab current modulus term 
            if isempty(obj.temperature)
                E = obj.material.getProp('E');
            else
                E = obj.material.getProp('E',obj.temperature);
            end
            
            % Rotational Terms
            Kel(obj.irot,obj.irot) = obj.elementProps.alpha*E*...
                    obj.volume*(diag([4/3,4/3,4/3,4/3])-ones(4)/3);
            
            % Hacky function to get rid of spurious zero-energy modes
            % 1E-7 seems to work
            Kel = S4S.stabilization(Kel, X, Y, 1E-7);

            % Tranform to Global System
            Kel = obj.transform*Kel*obj.transform';
            
            % Change Ouput to Vector
            Kout = Kel(:);
            
            % If a displacement is supplied, also return the nonlinear tangent
            % stiffness matrix
            if ~isempty(x)
                [~, Kt] = obj.buildNLStiff(x);
                Kout = Kout + Kt;
            end
            
        end
        function [Kn, Kt] = BuildNLStiff(obj, x)
            % Build Element Stiffness Matrices
            % DEFINE Kn and Kt !!!!!!!!!!!!!!!!
            % 
            %
            
            
            xLocal = obj.transform'*x;
            xm = xLocal(obj.mi);
            xb = xLocal(obj.bi);
            
            K1nm = zeros(4);
            K1bm = zeros(4, 8);
            K2b = zeros(4);
            n = length(obj.QUADweightsNL);
            
            for i = 1:n
                for j = 1:n
                    
                    %                     [detJ, G, Nm, C, Bm] = obj.evalNL(xi, eta, X, Y, xLocal(obj.mi), xLocal(obj.bi), obj.elementProps.Dmembrane);
                    %                     [Nm, C] = obj.evalNL(i, j, xm, xb);
                    
                    nm = obj.nlShape(i, j).DBm*xm;
                    Nm = [nm(1), nm(3); nm(3), nm(2)];
                    
                    dw = obj.nlShape(i, j).G*xb;
                    C = [dw(1), 0;  0, dw(2);  dw(2), dw(1)];
                    
                    K1nm = K1nm + obj.nlShape(i, j).Ghat*Nm*obj.nlShape(i, j).G;
                    K1bm = K1bm + obj.nlShape(i, j).Ghat*C.'*obj.nlShape(i, j).DBm;
                    K2b = K2b + obj.nlShape(i, j).Ghat*C.'*obj.elementProps.Dmembrane*C*obj.nlShape(i, j).G;
                    
                    
                end
            end
            
            Kn = [K1nm + K2b,  K1bm; K1bm.', zeros(8)];
            Kt = [2*K1nm + 3*K2b, 2*K1bm; 2*K1bm.', zeros(8)];
            reorder = [obj.bi, obj.mi];
            
            Kne = zeros(24);
            Kte = zeros(24);
            
            Kne(reorder, reorder) = Kn;
            Kte(reorder, reorder) = Kt;
            
            Kne = obj.transform*Kne*obj.transform.';
            Kte = obj.transform*Kte*obj.transform.';
            
            Kn = Kne(:);
            Kt = Kte(:);
            
        end
        function [fInt] = GetFint(obj, x)
            
            % Build linear stiffness matrix
            [K] = BuildStiff(obj, []);
            
            % Reshape and multiply by displacement
            fInt = reshape(K,obj.nDof,obj.nDof)*x;     
            
        end
        function [fIntNl,Kn,Kt] = GetFintNl(obj, x)
            

            % Build linear stiffness matrix
            [Kn,Kt] = BuildNLStiff(obj, x);
            
            % Reshape and multiply by displacement
            fIntNl = reshape(Kn,obj.nDof,obj.nDof)*x; 
            
        end

        function [Kss] = BuildStressStiff(obj,sigmaVec,xIn)
            % Compute the stress stiffness matrix based upong provided
            % stress values
            
            
            % Build stress matrix
            sigmaMat = [sigmaVec(1) sigmaVec(3);...
                        sigmaVec(3) sigmaVec(2)];
            
            % Section thickness
            t = obj.section.t;
            
%             % Build Local Coordinates (about deformed state)
%             ind = [1:3, 7:9, 13:15, 19:21];
%             uDef = reshape(xIn(ind),3,4);
%             c = obj.GetNodalCoords().' + uDef.'; norg = sum(c)/4;
%             u = [c(:,1)'-norg(1);c(:,2)'-norg(2);c(:,3)'-norg(3)]';
%             xLocal = (u)*obj.Te(1:2,:)';
%             % Extract local cooridantes 
%             xLocal = obj.localCoords;
            
            % Transform to local coordinates
            indx = [1:3 7:9 13:15 19:21];
            xLocal = obj.GetNodalCoords()+reshape(xIn(indx),3,4);
            norg = sum(xLocal,2)/4;
            u = [xLocal(1,:)'-norg(1) xLocal(2,:)'-norg(2) xLocal(3,:)'-norg(3)];
            xLocal = u*obj.Te(1:2,:)';
            
            %  2x2 Gauss quadrature
            Ksse = zeros(24);

            for ii = 1:length(obj.QUADpointsBM)
              nxi = obj.QUADpointsBM(ii);
              for jj = 1:length(obj.QUADpointsBM)
                neta = obj.QUADpointsBM(jj);
                [~,Btheta,~,~,Jd]=obj.shape2_n0(nxi,neta,xLocal);
                Ksse(obj.bi,obj.bi) = Ksse(obj.bi,obj.bi)+...
                                      Btheta'*sigmaMat*Btheta*Jd*1; % *t
              end
            end

            %  transform Kss to global coordinates
            Kss = obj.transform()*Ksse*obj.transform()';
            Kss = Kss(:);
        end
        
        function [K1, K2] = BuildNLmodal(obj, phi1, phi2)
            
            xLocal = obj.transform'*phi1;
            xm1 = xLocal(obj.mi);
            xb1 = xLocal(obj.bi);
            
            xLocal = obj.transform'*phi2;
            xb2 = xLocal(obj.bi);
            
            K1nm = zeros(4);
            K1bm = zeros(4, 8);
            K2b = zeros(4);
            n = length(obj.QUADweightsNL);
            
            for i = 1:n
                for j = 1:n
                    
                    nm = obj.nlShape(i, j).DBm*xm1;
                    Nm1 = [nm(1), nm(3); nm(3), nm(2)];
                    dw = obj.nlShape(i, j).G*xb1;
                    C1 = [dw(1), 0;  0, dw(2);  dw(2), dw(1)];
                    
                    dw = obj.nlShape(i, j).G*xb2;
                    C2 = [dw(1), 0;  0, dw(2);  dw(2), dw(1)];
                    
                    K1nm = K1nm + obj.nlShape(i, j).Ghat*Nm1*obj.nlShape(i, j).G;
                    K1bm = K1bm + obj.nlShape(i, j).Ghat*C1.'*obj.nlShape(i, j).DBm;
                    K2b = K2b + obj.nlShape(i, j).Ghat*C1.'*obj.elementProps.Dmembrane*C2*obj.nlShape(i, j).G;
                    
                end
            end
            
            K1e = [K1nm,  K1bm; K1bm.', zeros(8)];
            K2e = [K2b, zeros(4, 8); zeros(8, 4), zeros(8)];
            
            reorder = [obj.bi, obj.mi];
            
            K1 = zeros(24);
            K2 = zeros(24);
            
            K1(reorder, reorder) = K1e;
            K2(reorder, reorder) = K2e;
            
            %  transform Kne and Kte to global coordinates
            K1 = obj.transform*K1*obj.transform';
            K2 = obj.transform*K2*obj.transform';
            
            
        end
        
        % Estimate Time Step
        function [tMin] = GetTimeStep(obj)
            % This may be incorrect
            
            % Grab current modulus term 
            if isempty(obj.temperature)
                E = obj.material.getProp('E');
                rho = obj.material.getProp('rho');
                nu = obj.material.getProp('nu');
            else
                E = obj.material.getProp('E',obj.temperature);
%                 E = obj.material.getProp('rho',obj.temperature);
%                 E = obj.material.getProp('nu',obj.temperature);
            end
            
            % Material constant
            c = sqrt(E/((1-nu^2)*rho));
            
            % Estimate critical lengths
            coords = obj.GetNodalCoords();
            
            % Side Lengths
            s1 = norm(coords(:,1)-coords(:,2));
            s2 = norm(coords(:,2)-coords(:,3));
            s3 = norm(coords(:,3)-coords(:,4));
            s4 = norm(coords(:,4)-coords(:,1));
            
            % Diagonal Lengths
            d1 = norm(coords(:,1)-coords(:,3));
            d2 = norm(coords(:,2)-coords(:,4));
            
            
            
            % Calculate areas (6)
            % Compute x and y lengths
            dx = .5*(obj.localCoords(2,1)-obj.localCoords(1,1)+obj.localCoords(3,1)-obj.localCoords(4,1));
            dy = .5*(obj.localCoords(4,2)-obj.localCoords(1,2)+obj.localCoords(3,2)-obj.localCoords(2,2));
            
            % Side and diagonal critical length
            l1 = dx*dy/max([s1;s2;s3;s4]);
            l2 = dx*dy/max([d1;d2]);
            
            a(1) = dx*dy;
            a(2) = dx*obj.section.t;
            a(3) = dy*obj.section.t;
            
            % Estimate timestep 
            tMin = dx*dy*obj.section.t/(c*max(a));
%             tMin  = min(l1,l2)/c;
            
        end
        
        % ----- Stress/Strain Generation Functions ---
        function [eleStress] = ComputeStress(obj,x)
            
            % Assume worst stress (outer fiber)
            dz = obj.section.t/2;
            
            % Transform to local coordinates
            xLocal = obj.transform*x;
            
            % Grab local coordinates
            c = obj.localCoords;
            ir = [4:5,10:11,16:17,22:23];

            % Evaluate linear strain-displacement matrices at the element
            % center (nxi = neta = 0)
            [Bm,Bb,~,~,~]=obj.shape2(0,0,c);

            % Evaluate nonlinear strain-displacement matrices at element center
            [~,Btheta,Theta,~,~]=obj.shape2_n0(0,0,c,eye(3),xLocal);
            
            % Compute the stresses
            inl = true;
            if inl  % include nonlinear terms
              eleStress = obj.elementProps.Dmembrane*(Bm*xLocal(obj.mi)+...
                  0.5*Theta*Btheta*xLocal(obj.bi)-dz*Bb*xLocal(ir));
            else  % linear terms only
              eleStress = obj.elementProps.Dmembrane*(Bm*dr(obj.mi)-dz*Bb*dr(ir));
            end
            
        end
        function [eleStrain] = ComputeStrain(obj,x)
            % Compute the strains of the S4S element in the local
            % coordinate system and then transform back to global.
            
            % Assume outer surface
            z = obj.section.t/2;
            
            % initialize strain vector
            eleStrain = zeros(1,9);
            
            % Transform to local coordinates
            xLocal = obj.transform*x;
            ir = [4:5,10:11,16:17,22:23];
            
            % Grab local coordinates
            c = obj.localCoords;
            
            % Evaluate linear strain-displacement matrices at the element
            % center (nxi = neta = 0)
            [Bm,Bb,~,~,~]=obj.shape2(0,0,c);

            % Evaluate nonlinear strain-displacement matrices at element center
            [~,Btheta,Theta,~,~]=obj.shape2_n0(0,0,c,eye(3),xLocal);
            
            % Compute the strains
            elm = Bm*xLocal(obj.mi);
            eleStrain(1:3) = elm';

            enlm = .5*Theta*Btheta*xLocal(obj.bi);
            eleStrain(4:6) = enlm';

            elb = Bb*xLocal(ir);
            eleStrain(7:9) = -z*elb';
            
            % Tranform back
%             eleStrain = obj.
        end
        function [eleStrain] = ComputeStrainFromStress(obj,eleStress)
            % This function computes the strains of a fe model 
            % based upon strains using hookes law for isotropic materials
            
            % Grab current modulus term 
            if isempty(obj.temperature)
                E = obj.material.getProp('E');
                rho = obj.material.getProp('rho');
                nu = obj.material.getProp('nu');
            else
                E = obj.material.getProp('E',obj.temperature);
                E = obj.material.getProp('rho',obj.temperature);
                E = obj.material.getProp('nu',obj.temperature);
            end
            
            nu2 = 2*(1+nu);
            cMat = 1/E*[1 -nu -nu 0 0 0;...
                        -nu 1 -nu 0 0 0;...
                        -nu -nu 1 0 0 0;...
                        0    0  0 nu2 0 0;
                        0    0  0 0   nu2 0;...
                        0    0  0 0   0  nu2];
              
            eleStrain = cMat*eleStress;
                    
        end
        function [eleStress] = ComputeStressFromStrain(obj,eleStrain)
            % This function computes the stresses of a fe model 
            % based upon strains using hookes law for isotropic materials
            
            % Grab current modulus term 
            if isempty(obj.temperature)
                E = obj.material.getProp('E');
                rho = obj.material.getProp('rho');
                nu = obj.material.getProp('nu');
            else
                E = obj.material.getProp('E',obj.temperature);
                rho = obj.material.getProp('rho',obj.temperature);
                nu = obj.material.getProp('nu',obj.temperature);
            end
            
            nu1 = 1-nu; nu2 = (1-2*nu)/2;
            cMat = E/((1+nu)*(1-2*nu))*[nu1 nu nu 0 0 0;...
                                        nu nu1 nu 0 0 0;...
                                        nu nu nu1 0 0 0;...
                                        0    0  0 nu2 0 0;
                                        0    0  0 0   nu2 0;...
                                        0    0  0 0   0  nu2];
            strainVec = [eleStrain([1:3]);2*eleStrain([4,5,6])];
            eleStress = cMat*strainVec;
                    
        end
        
        % ----- Thermal Stress/Strain/Nodal Loads Function
        function [Sele,Eele,Pele] = ComputeThermalLoads(obj,varargin)
            % This functions computes the thermal effects of applied
            % temperature to a S4 element.
            
            % Check if nodal temperatues are set directly
            
            % Check provided information
            if nargin > 3
                nodalTemp = obj.material.getProp('tRef')*ones(obj.nNode,1);
                nodalDef = varargin{2};
            elseif nargin > 2 % nodal temps and deformations supplied
                nodalTemp = varargin{1};
                nodalDef = varargin{2};
            elseif nargin > 1 % only nodal temps provided
                nodalTemp = varargin{1};
                nodalDef = zeros(obj.nDof,1);
            else % use nodal temps set in nodeObj (for 
%                 nodalTemp = 
            end
            
            % Transform to local coordinates
            indx = [1:3 7:9 13:15 19:21];
            xLocal = obj.GetNodalCoords()+reshape(nodalDef(indx),3,4);
            norg = sum(xLocal,2)/4;
            u = [xLocal(1,:)'-norg(1) xLocal(2,:)'-norg(2) xLocal(3,:)'-norg(3)];
            xLocal = u*obj.Te(1:2,:)';

            % Grab reference temperature, thermal expansion and section
            refTemp = obj.material.getProp('tRef');
            
            % Grab current termal expansion term 
            if isempty(obj.temperature)
                alpha = obj.material.getProp('alpha');
            else
                alpha = obj.material.getProp('alpha',obj.temperature);
            end
            
            D = obj.elementProps.Dmembrane(1:2,1:2);
            
            % compute the average element temperature
            avgTemp = mean(nodalTemp);
           
            % Difference from provided reference temp
            thermalGrad = avgTemp-refTemp;
            
            % compute thermal stresses
            Ee = [alpha*thermalGrad; alpha*thermalGrad];
            Se = D*Ee;
            
            % compute induced nodal loads using 2x2 Gauss quadrature
            Pel = zeros(24,1);
            e0 = ones(2,1);
            
            % Use Gauss Quadrature to integrate thermal loads
            for ii = 1:length(obj.QUADpointsBM)
                xi = obj.QUADpointsBM(ii);
                for jj = 1:length(obj.QUADpointsBM)
                    eta = obj.QUADpointsBM(jj);
                    
                    % Compute shape functions
                    [B,Jd,N]=obj.shape20(xi,eta,xLocal);
                    
                    % Add to nodal temperatures
                    Ti = N*nodalTemp-refTemp;
%                     Pel(obj.mi) = Pel(obj.mi)+B(1:2,:)'*Se*Jd*1; %obj.section.t;
                    % Add to nodal temperatues
                    Pel(obj.mi) = Pel(obj.mi)+B(1:2,:)'*D*alpha*Ti*e0*Jd*1; %obj.section.t;
                end
            end
            
            % Initialize and set variables
            Sele = zeros(3,1); Eele = Sele;
            Sele(1:2) = -Se; Eele(1:2) = -Ee;

            %  transform Pel to global coordinates
            Pele = obj.transform*Pel;
            
        end

        function [fEle] = ComputePressureLoads(obj,pVal,varargin)
            
            % Check provided information
            if nargin > 2
                nodalDef = varargin{2};
            else
                nodalDef = zeros(obj.nDof,1);
            end
            
            % Transform to local coordinates
            indx = [1:3 7:9 13:15 19:21];
            xLocal = obj.GetNodalCoords()+reshape(nodalDef(indx),3,4);
            norg = sum(xLocal,2)/4;
            u = [xLocal(1,:)'-norg(1) xLocal(2,:)'-norg(2) xLocal(3,:)'-norg(3)];
            xLocal = u*obj.Te(1:2,:)';
            
            pVec = obj.elementProps.area*pVal*obj.normal;
            
            fEle = zeros(obj.nDof,1);
            
            % Ad hoc procedure
            % Create pressure loads for translational dof
            fEle(1:3,:) = 0.25*pVec;
            fEle(7:9,:) = 0.25*pVec;
            fEle(13:15,:) = 0.25*pVec;
            fEle(19:21,:) = 0.25*pVec;
        
            % Use Gauss Quadrature to integrate presssure loads
%             for ii = 1:length(obj.QUADpointsBM)
%                 xi = obj.QUADpointsBM(ii);
%                 for jj = 1:length(obj.QUADpointsBM)
%                     eta = obj.QUADpointsBM(jj);
%                     
%                     % Compute shape functions
%                     [~,~,N]=obj.shape20(xi,eta,xLocal);
%                     
%                     % Add to the force
%                     fEle = fEle + (N'*N)*pVec;
%                 end
%             end
        end
        
        % ---- Shape Functions --- %        
        function [obj] = BuildShapeNL(obj)
            % Precompute and store shape function quantities for evaluation
            % of nonlinear forces at each integration point.
            
            X = obj.localCoords(:, 1);
            Y = obj.localCoords(:, 2);
            
            
            for i = 1:length(obj.QUADpointsNL)
                xi = obj.QUADpointsNL(i);
                wi = obj.QUADweightsNL(i);
                for j = 1:length(obj.QUADpointsNL)
                    eta = obj.QUADpointsNL(j);
                    wj = obj.QUADweightsNL(j);
                    
                    %                     [detJ, G, Nm, C, Bm] = obj.evalNL(xi, eta, X, Y, xLocal(obj.mi), xLocal(obj.bi), obj.elementProps.Dmembrane);
                    
                    % Write shape function derivatives
                    N1xi = -0.25*(1 - eta); N2xi = 0.25*(1 - eta); N3xi = 0.25*(1 + eta); N4xi = -0.25*(1 + eta);
                    N1eta = -0.25*(1 - xi); N2eta = -0.25*(1 + xi); N3eta = 0.25*(1 + xi); N4eta = 0.25*(1 - xi);
                    
                    J =  [N1xi,  N2xi,  N3xi,  N4xi;
                        N1eta, N2eta, N3eta, N4eta]*[X(:), Y(:)];
                    
                    detJ = J(1, 1)*J(2, 2) - J(2, 1)*J(1, 2);
                    
                    % Define "gamma" products
                    Gamma11 = J(2, 2)/detJ;
                    Gamma12 = -J(1, 2)/detJ;
                    Gamma21 = -J(2, 1)/detJ;
                    Gamma22 = J(1, 1)/detJ;
                    
                    % Write shape function derivatives wrt x
                    N1x = Gamma11*N1xi + Gamma12*N1eta; N1y = Gamma21*N1xi + Gamma22*N1eta;
                    N2x = Gamma11*N2xi + Gamma12*N2eta; N2y = Gamma21*N2xi + Gamma22*N2eta;
                    N3x = Gamma11*N3xi + Gamma12*N3eta; N3y = Gamma21*N3xi + Gamma22*N3eta;
                    N4x = Gamma11*N4xi + Gamma12*N4eta; N4y = Gamma21*N4xi + Gamma22*N4eta;
                    
                    % Write each submatrix
                    Bm1 = [N1x, 0;  0, N1y;  N1y, N1x];
                    Bm2 = [N2x, 0;  0, N2y;  N2y, N2x];
                    Bm3 = [N3x, 0;  0, N3y;  N3y, N3x];
                    Bm4 = [N4x, 0;  0, N4y;  N4y, N4x];
                    Bm = [Bm1, Bm2, Bm3, Bm4];
                    
                    G = [N1x, N2x, N3x, N4x;
                        N1y, N2y, N3y, N4y];
                    
                    obj.nlShape(i, j).Ghat = 0.5*wi*wj*G'*detJ;
                    obj.nlShape(i, j).G = G;
                    obj.nlShape(i, j).DBm = obj.elementProps.Dmembrane*Bm;
                    
                    
                end
            end
            
            
            
            % Write shape functions
            N1 = 0.25*(1 - xi)*(1 - eta); N2 = 0.25*(1 + xi)*(1 - eta);
            N3 = 0.25*(1 + xi)*(1 + eta); N4 = 0.25*(1 - xi)*(1 + eta);
            
            % Write shape function derivatives
            N1xi = -0.25*(1 - eta); N2xi = 0.25*(1 - eta); N3xi = 0.25*(1 + eta); N4xi = -0.25*(1 + eta);
            N1eta = -0.25*(1 - xi); N2eta = -0.25*(1 + xi); N3eta = 0.25*(1 + xi); N4eta = 0.25*(1 - xi);
            
            J =  [N1xi,  N2xi,  N3xi,  N4xi;
                N1eta, N2eta, N3eta, N4eta]*[X(:), Y(:)];
            
            detJ = J(1, 1)*J(2, 2) - J(2, 1)*J(1, 2);
            
            % Define "gamma" products
            Gamma11 = J(2, 2)/detJ;
            Gamma12 = -J(1, 2)/detJ;
            Gamma21 = -J(2, 1)/detJ;
            Gamma22 = J(1, 1)/detJ;
            
            % Write shape function derivatives wrt x
            N1x = Gamma11*N1xi + Gamma12*N1eta; N1y = Gamma21*N1xi + Gamma22*N1eta;
            N2x = Gamma11*N2xi + Gamma12*N2eta; N2y = Gamma21*N2xi + Gamma22*N2eta;
            N3x = Gamma11*N3xi + Gamma12*N3eta; N3y = Gamma21*N3xi + Gamma22*N3eta;
            N4x = Gamma11*N4xi + Gamma12*N4eta; N4y = Gamma21*N4xi + Gamma22*N4eta;
            
            % Write each B submatrix
            
            % Membrane
            Bm1 = [N1x, 0;  0, N1y;  N1y, N1x];
            Bm2 = [N2x, 0;  0, N2y;  N2y, N2x];
            Bm3 = [N3x, 0;  0, N3y;  N3y, N3x];
            Bm4 = [N4x, 0;  0, N4y;  N4y, N4x];
            
            % Bending
            Bb1 = [0, N1x, 0;  0, 0, N1y;  0, N1y, N1x];
            Bb2 = [0, N2x, 0;  0, 0, N2y;  0, N2y, N2x];
            Bb3 = [0, N3x, 0;  0, 0, N3y;  0, N3y, N3x];
            Bb4 = [0, N4x, 0;  0, 0, N4y;  0, N4y, N4x];
            
            % Return full B matrix
            Bm = [Bm1, Bm2, Bm3, Bm4];
            Bb = [Bb1, Bb2, Bb3, Bb4];
            
            % Shape functions
            Nm = [N1,  0, N2,  0, N3,  0, N4, 0;
                0, N1,  0, N2,  0, N3,  0, N4];
            
            Nb = [N1,  0,  0, N2,  0,  0, N3,  0,  0, N4,  0,  0;
                0, N1,  0,  0, N2,  0,  0, N3,  0,  0, N4,  0;
                0,  0, N1,  0,  0, N2,  0,  0, N3,  0,  0, N4];
            
        end
           
        function [detJ, Bm, Bb, Nm, Nb] = EvalMembraneBending(obj,xi, eta, X, Y)
            % Evaluate B membrane and bending matrices, along with shape functions themselves,
            % at the coordinate 'xi' and 'eta'
            % Inputs :
            %
            % Outputs :
            %
            %
            
            % Write shape functions
            N1 = 0.25*(1 - xi)*(1 - eta); N2 = 0.25*(1 + xi)*(1 - eta);
            N3 = 0.25*(1 + xi)*(1 + eta); N4 = 0.25*(1 - xi)*(1 + eta);
            
            % Write shape function derivatives
            N1xi = -0.25*(1 - eta); N2xi = 0.25*(1 - eta); N3xi = 0.25*(1 + eta); N4xi = -0.25*(1 + eta);
            N1eta = -0.25*(1 - xi); N2eta = -0.25*(1 + xi); N3eta = 0.25*(1 + xi); N4eta = 0.25*(1 - xi);
            
            J =  [N1xi,  N2xi,  N3xi,  N4xi;
                N1eta, N2eta, N3eta, N4eta]*[X(:), Y(:)];
            
            detJ = J(1, 1)*J(2, 2) - J(2, 1)*J(1, 2);
            
            % Define "gamma" products
            Gamma11 = J(2, 2)/detJ;
            Gamma12 = -J(1, 2)/detJ;
            Gamma21 = -J(2, 1)/detJ;
            Gamma22 = J(1, 1)/detJ;
            
            % Write shape function derivatives wrt x
            N1x = Gamma11*N1xi + Gamma12*N1eta; N1y = Gamma21*N1xi + Gamma22*N1eta;
            N2x = Gamma11*N2xi + Gamma12*N2eta; N2y = Gamma21*N2xi + Gamma22*N2eta;
            N3x = Gamma11*N3xi + Gamma12*N3eta; N3y = Gamma21*N3xi + Gamma22*N3eta;
            N4x = Gamma11*N4xi + Gamma12*N4eta; N4y = Gamma21*N4xi + Gamma22*N4eta;
            
            % Write each B submatrix
            
            % Membrane
            Bm1 = [N1x, 0;  0, N1y;  N1y, N1x];
            Bm2 = [N2x, 0;  0, N2y;  N2y, N2x];
            Bm3 = [N3x, 0;  0, N3y;  N3y, N3x];
            Bm4 = [N4x, 0;  0, N4y;  N4y, N4x];
            
            % Bending
            Bb1 = [0, 0, -N1x;  0, N1y, 0;  0, N1x, -N1y];
            Bb2 = [0, 0, -N2x;  0, N2y, 0;  0, N2x, -N2y];
            Bb3 = [0, 0, -N3x;  0, N3y, 0;  0, N3x, -N3y];
            Bb4 = [0, 0, -N4x;  0, N4y, 0;  0, N4x, -N4y];
            
            % Return full B matrix
            Bm = [Bm1, Bm2, Bm3, Bm4];
            Bb = [Bb1, Bb2, Bb3, Bb4];
            
            % Shape functions
            Nm = [N1,  0, N2,  0, N3,  0, N4, 0;
                0, N1,  0, N2,  0, N3,  0, N4];
            
            Nb = [N1,  0,  0, N2,  0,  0, N3,  0,  0, N4,  0,  0;
                0, N1,  0,  0, N2,  0,  0, N3,  0,  0, N4,  0;
                0,  0, N1,  0,  0, N2,  0,  0, N3,  0,  0, N4];
        end
        
        function [detJ, Bs] = EvalShear(obj,xi, eta, X, Y)
            % Evaluate B shear matrix at the coordinate 'xi' and 'eta'
            
            % Write shape functions
            N1 = 0.25*(1 - xi)*(1 - eta); N2 = 0.25*(1 + xi)*(1 - eta);
            N3 = 0.25*(1 + xi)*(1 + eta); N4 = 0.25*(1 - xi)*(1 + eta);
            
            % Write shape function derivatives
            N1xi = -0.25*(1 - eta); N2xi = 0.25*(1 - eta); N3xi = 0.25*(1 + eta); N4xi = -0.25*(1 + eta);
            N1eta = -0.25*(1 - xi); N2eta = -0.25*(1 + xi); N3eta = 0.25*(1 + xi); N4eta = 0.25*(1 - xi);
            
            J =  [N1xi,  N2xi,  N3xi,  N4xi;
                N1eta, N2eta, N3eta, N4eta]*[X(:), Y(:)];
            
            detJ = J(1, 1)*J(2, 2) - J(2, 1)*J(1, 2);
            
            % Define "gamma" products
            Gamma11 = J(2, 2)/detJ;
            Gamma12 = -J(1, 2)/detJ;
            Gamma21 = -J(2, 1)/detJ;
            Gamma22 = J(1, 1)/detJ;
            
            % Write shape function derivatives wrt x
            N1x = Gamma11*N1xi + Gamma12*N1eta; N1y = Gamma21*N1xi + Gamma22*N1eta;
            N2x = Gamma11*N2xi + Gamma12*N2eta; N2y = Gamma21*N2xi + Gamma22*N2eta;
            N3x = Gamma11*N3xi + Gamma12*N3eta; N3y = Gamma21*N3xi + Gamma22*N3eta;
            N4x = Gamma11*N4xi + Gamma12*N4eta; N4y = Gamma21*N4xi + Gamma22*N4eta;
            
            % Write each B submatrix
            Bs1 = [N1x, 0, N1; N1y, -N1, 0];
            Bs2 = [N2x, 0, N2; N2y, -N2, 0];
            Bs3 = [N3x, 0, N3; N3y, -N3, 0];
            Bs4 = [N4x, 0, N4; N4y, -N4, 0];
            
            % Return full B matrix
            Bs = [Bs1, Bs2, Bs3, Bs4];
        end
        
        % ---- Build Functions --- %
        function [obj] = BuildTransform(obj)
            % This function builds the transformation matrix of the beam
            % element to rotate from local CS to global CS.
            %
            % Inputs :
            %           obj
            % Outputs :
            %           obj
            
            % Extract Offsets (removed for now)
            off1 = [0 0 0];
            off2 = [0 0 0];
            off3 = [0 0 0];
            off4 = [0 0 0];
            
            % Compute Unit Vectors (e1 & e2)
            c = obj.GetNodalCoords().' + [off1; off2; off3; off4];
            e1 = (c(2, :) - c(1, :))/norm(c(2, :) - c(1, :));
            e2 = (c(4, :) - c(1, :))/norm(c(4, :) - c(1, :));
            e3 = cross(e1,e2);
            
            % Compute Unit Vector (e13 & e42)
            e13 = (c(3, :) - c(1, :))/norm(c(3, :) - c(1, :));
            e24 = (c(2, :) - c(4, :))/norm(c(2, :) - c(4, :));
            
            % Generate Element Local Coordinate System Unit Vectors
            % ** cvd - need to check this **
            obj.Te = zeros(3);
            obj.Te(1,:) = e13 + e24; % Add together ?? why ??
            obj.Te(3,:) = Element.cross2(obj.Te(1,:),e13);
            obj.Te(2,:) = Element.cross2(obj.Te(3,:),obj.Te(1,:));
            
            % Normalize Matrix
            obj.Te(1,:) = obj.Te(1,:)/norm(obj.Te(1,:));
            obj.Te(2,:) = obj.Te(2,:)/norm(obj.Te(2,:));
            obj.Te(3,:) = obj.Te(3,:)/norm(obj.Te(3,:));
            
            % Assemble Transformation Matrix
            T = zeros(24);
            T(1:3,1:3) = obj.Te';
            T(4:6,4:6) = obj.Te';
            T(7:9,7:9) = obj.Te';
            T(10:12,10:12) = obj.Te';
            T(13:15,13:15) = obj.Te';
            T(16:18,16:18) = obj.Te';
            T(19:21,19:21) = obj.Te';
            T(22:24,22:24) = obj.Te';
            
            % Store Transformation
            obj.transform = T;
            
            % Build Local Coordinates
            norg = sum(c)/4;
            u = [c(:,1)'-norg(1);c(:,2)'-norg(2);c(:,3)'-norg(3)]';
            obj.localCoords = u*obj.Te(1:2,:)';
            obj.normal = e3;
        end
        
        function [obj] = BuildSection(obj)
            % This function computes the derived sectional properties of a
            % beam from a given cross-section shape and parameters.
            %
            % Inputs  :
            %           obj
            % Outputs :
            %
            %
            
            % Grab current terms at temperature 
            if isempty(obj.temperature)
                E = obj.material.getProp('E');
                nu = obj.material.getProp('nu');
                rho = obj.material.getProp('rho');
            else
                E = obj.material.getProp('E',obj.temperature);
                nu = obj.material.getProp('nu',obj.temperature);
                rho = obj.material.getProp('nu',obj.temperature);
            end
            
            % Compute shear modulus
            G =  E/(2*(1 + nu));
            obj.material.setProp('G', G);
            
            % Form constituitive matrices
            obj.elementProps.Dmembrane = E*obj.section.t/(1 - nu^2)*[1, nu, 0;
                nu,  1, 0;
                0,  0, 0.5*(1 - nu)];
            obj.elementProps.Dbending = E*obj.section.t^3/(12*(1 - nu^2))*[1, nu, 0;
                nu,  1, 0;
                0,  0, 0.5*(1 - nu)];
            obj.elementProps.Dshear = 5/6*G*obj.section.t*[1, 0; 0, 1];
            
            % Compute x and y lengths
            dx = .5*(obj.localCoords(2,1)-obj.localCoords(1,1)+obj.localCoords(3,1)-obj.localCoords(4,1));
            dy = .5*(obj.localCoords(4,2)-obj.localCoords(1,2)+obj.localCoords(3,2)-obj.localCoords(2,2));
            
            % Store Properties
            obj.elementProps.area = dx*dy;
            obj.volume = dx*dy*obj.section.t;
            obj.mass = rho*dx*dy*obj.section.t;
            obj.elementProps.alpha = 0.01;
            
            % Adding this temporarily
            obj.section.dx = dx; obj.section.dy = dy;
            obj.section.area = dx*dy;
            obj.section.vol = dx*dy*obj.section.t;
            obj.section.alpha = 0.01;
            
        end
        
        
        % ---- Utility Functions --- %
        function coords = GetNodalCoords(obj)
            
            c1 = obj.nodes{1}.GetXYZ(); % 1st Nodal Coordinate
            c2 = obj.nodes{2}.GetXYZ(); % 2nd Nodal Coordinate
            c3 = obj.nodes{3}.GetXYZ(); % 3rd Nodal Coordinate
            c4 = obj.nodes{4}.GetXYZ(); % 4th Nodal Coordinate
            coords = [c1, c2, c3, c4]; % All nodal cooridinates
        end
        
        function mass = GetMass(obj)
            mass = obj.mass;
%             mass = obj.section.vol*obj.material.getProp('rho');
        end
        function volume = GetVolume(obj)
            volume = obj.volume;
%             mass = obj.section.vol*obj.material.getProp('rho');
        end
        % 
    end
    
    methods(Static)
        
        function Kstab = stabilization(K, x, y, factor)
            
            % First, symmetrize the matrix
            K = 0.5*(K + K');
            
            % Obtain eigenvalues; find zero-strain modes
            [phi, lambda] = eig(K); lambda = diag(lambda);
            lambdaMax = max(lambda);
            
            % Use a threshold to determine what counts as zero-strain
            threshold = 1E-3;
            phiz = phi(:, lambda < threshold);
            
            % Construct analytical rigid body modes
            psi = zeros(size(K, 1), 6);
            % Translation is easy
            psi(1:6:end, 1) = 1; psi(2:6:end, 2) = 1; psi(3:6:end, 3) = 1;
            
            % Rotation a bit tougher
            for i = 1:3 % For each rotation
                rotation = zeros(3, 1); rotation(i) = 1;
                for j = 1:length(x) % For each node
                    rx = x(j); ry = y(j); r = [rx; ry; 0];
                    dx = cross(rotation, r);
                    inds = (j - 1)*6 + [1:6];
                    psi(inds, 3 + i) = [dx; rotation];
                end
            end
            
            % Use analytical RB matrix to construct a projector
            Psi = psi*((psi'*psi)\psi'); % Projector
            thetaBar = phiz - Psi*phiz;  % Remove RB modes from zero-strain modes
            
            % Obtain 3 unique zero-strain modes
            [Q, ~] = qr(thetaBar, 0);
            theta = Q(:, 1:3);
            lambdaStar = factor*lambdaMax;
            Kstar = pinv(theta')*lambdaStar*eye(3)*pinv(theta);
            Kstar = 0.5*(Kstar + Kstar');
            
            Kstab = K + Kstar;
            
        end
        
        % Linear Shape Functions
        function [Bm,Bb,Bs,Jd,N]=shape2(nxi,neta,nxy)
            %  [Bm,Bb,Bs,Jd,N]=shape2(nxi,neta,nxy)
            %  R Gordon   7/8/94...12/21/94
            %		        7/11/95 bending added
            %			3/21/96 mem, bend & ts coded separately
            %
            %  Computes the shape functions, Jacobian matrix and the strain-
            %  displacement "B" matrices for an isoparametric quadrilateral
            %  plate finite element at point (nxi,neta).  Separate B matrices
            %  are calculated for membrane, bending and transverse shear
            %  contributions.  Called by the function quad4.m.
            %
            %  Inputs:
            %	(nxi,neta) -	isoparametric coordinates of point
            %	nxy 	   -	matrix of x,y,z coordinates of corner nodes
            %
            %  Outputs: (evaluated at the point (nxi,neta)
            %	Bm -	element strain-displacement matrix - membrane
            %	Bb -	element strain-displacement matrix - bending
            %	Bs -	element strain-displacement matrix - transverse shear
            %	Jd -	determinant of the Jacobian matrix
            %	N  -	shape function matrix
            %
            
            xii = [-1 1 1 -1];
            etai = [-1 -1 1 1];
            
            %  compute shape functions, N (1x4), and derivatives, dN (2x4)
            d1 = (1+xii*nxi);
            d2 = (1+etai*neta);
            N = d1.*d2/4;
            dN = zeros(2,4);
            dN(1,:) = d2.*xii/4;
            dN(2,:) = d1.*etai/4;
            
            %  compute Jacobian, J, its inverse, Jin, and determinant, Jd
            J = dN*nxy;
            Jin = inv(J);
            Jd = det(J);
            
            %  form strain-displacement matrices for membrane, Bm(3x8),
            %  bending, Bb(3x8), and transverse shear, Bs(2x12)
            M1m = [1 0 0 0;0 0 0 1;0 1 1 0];
            M1b = [0 0 -1 0;0 1 0 0;1 0 0 -1];
            M1s = [1 0 0 1;0 1 -1 0];
            
            M2mb = zeros(4);
            M2mb(1:2,1:2) = Jin;
            M2mb(3:4,3:4) = Jin;
            
            M2s = M2mb;
            M2s(3:4,3:4) = eye(2);
            
            M3mb = zeros(4,8);
            indm = [1 3 5 7];
            M3mb(1:2,indm) = dN;
            M3mb(3:4,indm+1) = dN;
            
            M3s = zeros(2,12);
            inds = [1 4 7 10];
            M3s(1:2,inds) = dN;
            M3s(3,inds+1) = N;
            M3s(4,inds+2) = N;
            
            Bm = M1m*M2mb*M3mb;
            Bb = M1b*M2mb*M3mb;
            Bs = M1s*M2s*M3s;
        end
        function [B,Jd,N]=shape20(nxi,neta,nxy)
            %  [B,Jd,N]=shape2(nxi,neta,nxy)
            %  R Gordon   7/8/94...12/21/94
            %
            %  Computes the shape functions, Jacobian matrix and the strain-
            %  displacement "B" matrix for an isoparametric quadrilateral
            %  plane-stress finite element at point (nxi,neta).  Called by the
            %  functions psquad.m and psquads.m.
            %
            %  Inputs:
            %	(nxi,neta) -	isoparametric coordinates of point
            %	nxy -		matrix of x,y,z coordinates of corner nodes
            %
            %  Outputs: (evaluated at the point (nxi,neta)
            %	B -		element strain-displacement matrix
            %	Jin -		inverse of Jacobian matrix
            %	Jd -		determinant of the Jacobian matrix
            %
            
            ind = [1 3 5 7];
            xii = [-1 1 1 -1];
            etai = [-1 -1 1 1];
            
            %  compute shape functions, N and derivatives, Nxi and Neta
            d1 = (1+xii*nxi);
            d2 = (1+etai*neta);
            N = d1.*d2/4;
            dN = zeros(2,4);
            dN(1,:) = d2.*xii/4;
            dN(2,:) = d1.*etai/4;
            
            %  compute Jacobian, J, its inverse, Jin, and determinant, Jd
            J = dN*nxy;
            Jin = inv(J);
            Jd = det(J);
            
            %  form strain-displacement matrix, B
            M1 = [1 0 0 0;0 0 0 1;0 1 1 0];
            
            M2 = zeros(4);
            M2(1:2,1:2) = Jin;
            M2(3:4,3:4) = Jin;
            
            M3 = zeros(4,8);
            M3(1:2,ind) = dN;
            M3(3:4,ind+1) = dN;
            
            B = M1*M2*M3;
        end
        
        % Nonlinar Shap Functions
        function [Bm,Btheta,Theta,Nm,Jd]=shape2_n0(nxi,neta,nxy,D,def)
            %  [Bm,Btheta,Theta,Nm,Jd]=shape2_n0(nxi,neta,nxy,D,def)
            %  R Gordon   5/8/01
            %
            %  Computes the shape functions, Jacobian matrix and the strain-
            %  displacement "B" matrices for large displacement in-plane
            %  stretching (nonlinear) of an isoparametric quadrilateral plate
            %  finite element at point (nxi,neta).  A C0 membrane formulation
            %  is used.  Called by the function QUAD4_N0.M.
            %
            %  Inputs:
            %    (nxi,neta) - isoparametric coordinates of point
            %    nxy 	- matrix of x,y,z coordinates of corner nodes
            %    D          - 3x3 constitutive matrix
            %    def        - vector of deformed shape nodal displacements (24x1)
            %
            %  Outputs: (evaluated at the point (nxi,neta)
            %    Bm      - element strain-displ matrix - membrane
            %    Btheta  - element strain-displ matrix - bending (nonlinear)
            %    Theta   - element slope matrix (function of deformed shape)
            %    Nm      - element membrane force matrix (4x4)
            %    Jd      - determinant of the Jacobian matrix
            %
            
            xii = [-1 1 1 -1];
            etai = [-1 -1 1 1];
            
            im = [1:2 7:8 13:14 19:20];
            iw = [3 9 15 21];
            
            %  compute shape functions, N (1x4), and derivatives, dN (2x4)
            d1 = (1+xii*nxi);
            d2 = (1+etai*neta);
            N = d1.*d2/4;
            dN = zeros(2,4);
            dN(1,:) = d2.*xii/4;
            dN(2,:) = d1.*etai/4;
            
            %  compute Jacobian, J, its inverse, Jin, and determinant, Jd
            J = dN*nxy;
            Jin = inv(J);
            Jd = det(J);
            
            %  strain-displacement matrix for membrane, Bm(2x4)
            M1m = [1 0 0 0;0 0 0 1;0 1 1 0];
            M2mb = zeros(4);
            M2mb(1:2,1:2) = Jin;
            M2mb(3:4,3:4) = Jin;
            M3mb = zeros(4,8);
            indm = [1 3 5 7];
            M3mb(1:2,indm) = dN;
            M3mb(3:4,indm+1) = dN;
            Bm = M1m*M2mb*M3mb;
            
            %strain-displacement matrix for nonlinear out-of-plane, Btheta(3x8)
            Btheta = Jin*dN;
            
            % element slope matrix, Theta (3x2)
            Gamat = zeros(3,4);
            Gamat(1,1:2) = Jin(1,:);
            Gamat(2,3:4) = Jin(2,:);
            Gamat(3,1:2) = Jin(2,:);
            Gamat(3,3:4) = Jin(1,:);
            
            if nargin == 5
                wb = def(iw);
                dNw = dN*wb;
                Theta = Gamat*[dNw zeros(2,1);zeros(2,1) dNw];
                
                % element membrane force matrix, Nm
                wm = def(im);
                Nm0 = D*Bm*wm;
                Nm = [Nm0(1) Nm0(3);Nm0(3) Nm0(2)];
            else
                Theta = [];
                Nm = [];
            end
            
        end
        function [Bm,Btheta,Theta,Nm,Jd]=shape2_n1(nxi,neta,nxy,D,def)
            %  [Bm,Btheta,Theta,Nm,Jd]=shape2_n1(nxi,neta,nxy,D,def)
            %  R Gordon   6/24/05
            %
            %  Computes the shape functions, Jacobian matrix and the strain-
            %  displacement "B" matrices for large displacement in-plane
            %  stretching (nonlinear) of an isoparametric quadrilateral plate
            %  finite element at point (nxi,neta).  A C1 continuous membrane 
            %  formulation is used.  Called by the function QUAD4_N1.M.
            %
            %  Inputs:
            %    (nxi,neta) - isoparametric coordinates of point
            %    nxy 	- matrix of x,y,z coordinates of corner nodes
            %    D          - 3x3 constitutive matrix
            %    def        - vector of deformed shape nodal displacements (24x1)
            %
            %  Outputs: (evaluated at the point (nxi,neta)
            %    Bm      - element strain-displ matrix - membrane
            %    Btheta  - element strain-displ matrix - bending (nonlinear)
            %    Theta   - element slope matrix (function of deformed shape)
            %    Nm      - element membrane force matrix (4x4)
            %    Jd      - determinant of the Jacobian matrix
            %
            
            xii = [-1 1 1 -1];
            etai = [-1 -1 1 1];
            
            im = [1:2 7:8 13:14 19:20];
            iw = [3 9 15 21];
            
            %  compute shape functions, N (1x4), and derivatives, dN (2x4)
            d1 = (1+xii*nxi);
            d2 = (1+etai*neta);
            N = d1.*d2/4;
            dN = zeros(2,4);
            dN(1,:) = d2.*xii/4;
            dN(2,:) = d1.*etai/4;
            
            %  compute Jacobian, J, its inverse, Jin, and determinant, Jd
            J = dN*nxy;
            Jin = inv(J);
            Jd = det(J);
            
            %  strain-displacement matrix for membrane, Bm(2x4)
            M1m = [1 0 0 0;0 0 0 1;0 1 1 0];
            M2mb = zeros(4);
            M2mb(1:2,1:2) = Jin;
            M2mb(3:4,3:4) = Jin;
            M3mb = zeros(4,8);
            indm = [1 3 5 7];
            M3mb(1:2,indm) = dN;
            M3mb(3:4,indm+1) = dN;
            Bm = M1m*M2mb*M3mb;
            
            %strain-displacement matrix for nonlinear out-of-plane, Btheta(3x8)
            Btheta = Jin*dN;
            
            % element slope matrix, Theta (3x2)
            Gamat = zeros(3,4);
            Gamat(1,1:2) = Jin(1,:);
            Gamat(2,3:4) = Jin(2,:);
            Gamat(3,1:2) = Jin(2,:);
            Gamat(3,3:4) = Jin(1,:);
            
            if nargin == 5
                wb = def(iw);
                dNw = dN*wb;
                Theta = Gamat*[dNw zeros(2,1);zeros(2,1) dNw];
                
                % element membrane force matrix, Nm
                wm = def(im);
                Nm0 = D*Bm*wm;
                Nm = [Nm0(1) Nm0(3);Nm0(3) Nm0(2)];
            else
                Theta = [];
                Nm = [];
            end
            
        end
    end
    
end

