classdef S4D4 < Element
    % S4 shell element with drilling DOF from "On drilling degrees of
    % freedom." Called a QM4D4 in that work because they didn't use a
    % shell.
    % 
    % All nonlinear functions are removed until ready for use. They are
    % still contained in S4.m.
    
    properties (GetAccess = 'private', SetAccess = 'private')
        
        % Derived Section Properties
        sectionProps
        % A, Iyy, Izz, J, Ky, Kz
        elementProps
        % L, X, S
        % Y[y1, y2, y3, y4] , PHIY
        % Z[z1, z2, z3, z4] , PHIZ
        Te
    end
    
    % --- Constant Properties Cannot Be Changed 
    properties (Constant, GetAccess = 'private')
        irot = [6, 12, 18, 24];  % Rotation indices 
        mi = [1, 2, 7, 8, 13, 14, 19, 20]; % Membrane indices
        bsi = [3, 4, 5, 9, 10, 11, 15, 16, 17, 21, 22, 23]; % Bending/shear indices
        bi = [3, 9, 15, 21];
        QUADpointsBM = [-1/sqrt(3), 1/sqrt(3)]; % Quadrature Points; bending/membrane
        QUADweightsBM = [1, 1]; % Quadrature Weights; bending/membrane
        QUADpointsS = 0; % Quadrature Points; shear
        QUADweightsS = 2; % Quadrature Weights; shear
    end 
    
    methods
        % ----- Initialization ----- %
        function obj = S4D4(nodeCoord, material, section)
            
            obj = obj@Element(nodeCoord, material, section);
            
        end
        
        % ----- Abstract Methods that Must Be Used -----
        function [obj] = build(obj)
            % This function is called to generate the beam section
            % properties and transformation matrix for a beam element object
            
            % Build Tranfromation Matrix
            obj.buildTransform();
            
            % Build Beam Section Properties
            obj.buildSection();
            
            % Build Beam Shape (Element Properties)
%             obj.buildShape();
           
            
        end
        
        % ------ Element Generation Functions ------ 
        function [Mout] = buildMass(obj)
            % This function builds the element mass matrix of a C1
            % Continous Beam Element in the element coordinate system.
            % Inputs :
            %
            % Outputs :
            %
            %
            
            
            % Extract Nodes (can be reomved in future for debugging)
%             c1 = obj.nodes.coord(1,:)+off1; % 1st Nodal Coordinate
%             c2 = obj.nodes.coord(2,:)+off2; % 2nd Nodal Coordinate
%             c3 = obj.nodes.coord(3,:)+off1; % 3rd Nodal Coordinate
%             c4 = obj.nodes.coord(4,:)+off2; % 4th Nodal Coordinate
%             c = [c1 c2 c3 c4]; % All nodal cooridinates 
            
            % Initialize Matrices
            Mel = zeros(24);
            
            % Extract Projected X and Y (inplane) Nodal Coordinates
            X = obj.localCoords(:, 1);
            Y = obj.localCoords(:, 2);
            
            % Full integration (2 x 2 Quadrature) for membrane and bending terms
            % -- Membrane, Bending and Shear Indicies are S4 Constant Properties 
            % -- Quadrature Points and Weights are S4 Constant Properties
            for ii = 1:length(obj.QUADpointsBM)
                % Assign Points and Weights 
                xi = obj.QUADpointsBM(ii);
                wi = obj.QUADweightsBM(ii);
                for jj = 1:length(obj.QUADpointsBM)
                    
                    % Assign Points and Weights 
                    eta = obj.QUADpointsBM(jj);
                    wj = obj.QUADweightsBM(ii);
                    
                    % Evaluate Shape Functions (hopefully can be removed)
                    [detJ, Bm, Bb, Nm, Nb] = obj.evalMBN(xi, eta, X, Y);
                  
                    % Generate mass
                    Mel(obj.mi, obj.mi) = Mel(obj.mi, obj.mi) + obj.material.getProp('rho')*obj.section.t*wi*wj*(Nm'*Nm)*detJ;
                    Mel(obj.bsi, obj.bsi) = Mel(obj.bsi, obj.bsi) + obj.material.getProp('rho')*obj.section.t*wi*wj*(Nb'*Nb)*detJ;
                    
                end
            end
            
            % Tranform to Global System 
            Mel = obj.transform*Mel*obj.transform';
            
            % Change Ouput to Vector
            Mout = Mel(:);
            
        end
        
        function [Kout] = buildStiff(obj)
            
            % Initialize Matrices
            Kel = zeros(24);
            
            % Extract Projected X and Y (inplane) Nodal Coordinates
            X = obj.localCoords(:, 1);
            Y = obj.localCoords(:, 2);
            
            % Full integration (2 x 2 Quadrature) for membrane and bending terms
            % -- Membrane, Bending and Shear Indicies are Constant Properties
            % -- Quadrature Points and Weights are Constant Properties
            for ii = 1:length(obj.QUADpointsBM)
                % Assign Points and Weights
                xi = obj.QUADpointsBM(ii);
                wi = obj.QUADweightsBM(ii);
                for jj = 1:length(obj.QUADpointsBM)
                    
                    % Assign Points and Weights
                    eta = obj.QUADpointsBM(jj);
                    wj = obj.QUADweightsBM(ii);
                    
                    % Evaluate Bending Shape Functions and generate 
                    % stiffness (bending/shear indices)
                    [detJ, ~, Bb, ~, ~] = obj.evalMBN(xi, eta, X, Y);
                    Kel(obj.bsi, obj.bsi) = Kel(obj.bsi, obj.bsi) + wi*wj*Bb'*obj.elementProps.Dbending*Bb*detJ;
                    
                    % Generate Membrane/Drilling terms
                    % This portion is given by 15.3.1 of "On Drilling Degrees of
                    % Freedom," Eqs. 15.48 thru 15.94.
                    
                    
                    
                    
                end
            end
            
            % Evaluate shear terms
            for i = 1:length(obj.QUADpointsS)
                xi = obj.QUADpointsS(i);
                wi = obj.QUADweightsS(i);
                for j = 1:length(obj.QUADpointsS)
                    eta = obj.QUADpointsS(j);
                    wj = obj.QUADweightsS(j);
                    
                    [detJ, Bs] = obj.evalS(xi, eta, X, Y);
                    Kel(obj.bsi, obj.bsi) = Kel(obj.bsi, obj.bsi) + wi*wj*Bs'*obj.elementProps.Dshear*Bs*detJ;
                end
            end
            
            % Tranform to Global System
            Kel = obj.transform*Kel*obj.transform';
            
            % Change Ouput to Vector
            Kout = Kel(:);
            
        end
    
        % ----- Stress/Strain Generation Functions ---
        
        % ---- Additional Functions --- %

        function [obj] = buildTransform(obj)
            % This function builds the transformation matrix of the beam
            % element to rotate from local CS to global CS.
            %
            % Inputs :
            %           obj
            % Outputs :
            %           obj
            
            % Extract Offsets (removed for now)
            off1 = [0 0 0];
            off2 = [0 0 0];
            off3 = [0 0 0];
            off4 = [0 0 0];
            
            % Extract Nodes
            c1 = obj.nodes(1).coord+off1; % 1st Nodal Coordinate
            c2 = obj.nodes(2).coord+off2; % 2nd Nodal Coordinate
            c3 = obj.nodes(3).coord+off3; % 3rd Nodal Coordinate
            c4 = obj.nodes(4).coord+off4; % 4th Nodal Coordinate
            c = [c1; c2; c3; c4]; % All nodal cooridinates 

            % Compute Unit Vectors (e1 & e2)
            e1 = (c2 - c1)/norm(c2-c1);
            e2 = (c4 - c1)/norm(c4-c1);
            e3 = cross(e1,e2);
            
            % Compute Unit Vector (e13 & e42)
            e13 = (c3-c1)/norm(c3-c1);
            e24 = (c2-c4)/norm(c2-c4);
            
            % Generate Element Local Coordinate System Unit Vectors 
            % ** cvd - need to check this **
            obj.Te = zeros(3);
            obj.Te(1,:) = e13 + e24; % Add together ?? why ??
            obj.Te(3,:) = Element.cross2(obj.Te(1,:),e13);
            obj.Te(2,:) = Element.cross2(obj.Te(3,:),obj.Te(1,:));
            
            % Normalize Matrix
            obj.Te(1,:) = obj.Te(1,:)/norm(obj.Te(1,:));
            obj.Te(2,:) = obj.Te(2,:)/norm(obj.Te(2,:));
            obj.Te(3,:) = obj.Te(3,:)/norm(obj.Te(3,:));
            
            % Assemble Transformation Matrix
            T = zeros(24);
            T(1:3,1:3) = obj.Te';
            T(4:6,4:6) = obj.Te';
            T(7:9,7:9) = obj.Te';
            T(10:12,10:12) = obj.Te';
            T(13:15,13:15) = obj.Te';
            T(16:18,16:18) = obj.Te';
            T(19:21,19:21) = obj.Te';
            T(22:24,22:24) = obj.Te';
            
            % Store Transformation 
            obj.transform = T;
%             keyboard
            
            % Build Local Coordinates
            % Compute node point coords in element coordinate system
            % ** cvd not confident about this **
            norg = sum(c)/4;
            u = [c(:,1)'-norg(1);c(:,2)'-norg(2);c(:,3)'-norg(3)]';
            obj.localCoords = u*obj.Te(1:2,:)'; 
                        
        end
        
        function [obj] = buildSection(obj)
            % This function computes the derived sectional properties of a
            % beam from a given cross-section shape and parameters.
            %
            % Inputs  :
            %           obj
            % Outputs :
            %
            %
            
            % Assign Material if not specified 
            E = obj.material.getProp('E');
            nu = obj.material.getProp('nu');
            G =  E/(2*(1 + nu));
            
            obj.material.setProp('G', G);
            
            % Form constituitive matrices
            obj.elementProps.Dmembrane = E*obj.section.t/(1 - nu^2)*[1, nu, 0;
                                         nu,  1, 0;
                                         0,  0, 0.5*(1 - nu)];
            obj.elementProps.Dbending = E*obj.section.t^3/(12*(1 - nu^2))*[1, nu, 0;
                                        nu,  1, 0;
                                        0,  0, 0.5*(1 - nu)];
            obj.elementProps.Dshear = 5/6*G*obj.section.t*[1, 0; 0, 1];
          
            % Compute x and y lengths 
            dx = .5*(obj.localCoords(2,1)-obj.localCoords(1,1)+obj.localCoords(3,1)-obj.localCoords(4,1));
            dy = .5*(obj.localCoords(4,2)-obj.localCoords(1,2)+obj.localCoords(3,2)-obj.localCoords(2,2));
            
            % Store Properties 
            obj.elementProps.vol = dx*dy*obj.section.t;
            obj.elementProps.alpha = 0.01;
            
        end
        
        function [obj] = buildShape(obj)
            % Evaluate B membrane and bending matrices, along with shape functions themselves,
            % at the coordinate 'xi' and 'eta'
            % Inputs :
            %           obj 
            %               
            % Outputs :
            %           obj.shapeFun
            %
            %
            
            % Write shape functions
            N1 = 0.25*(1 - xi)*(1 - eta); N2 = 0.25*(1 + xi)*(1 - eta);
            N3 = 0.25*(1 + xi)*(1 + eta); N4 = 0.25*(1 - xi)*(1 + eta);
            
            % Write shape function derivatives
            N1xi = -0.25*(1 - eta); N2xi = 0.25*(1 - eta); N3xi = 0.25*(1 + eta); N4xi = -0.25*(1 + eta);
            N1eta = -0.25*(1 - xi); N2eta = -0.25*(1 + xi); N3eta = 0.25*(1 + xi); N4eta = 0.25*(1 - xi);
            
            J =  [N1xi,  N2xi,  N3xi,  N4xi;
                  N1eta, N2eta, N3eta, N4eta]*[X(:), Y(:)];
            
            detJ = J(1, 1)*J(2, 2) - J(2, 1)*J(1, 2);
            
            % Define "gamma" products
            Gamma11 = J(2, 2)/detJ;
            Gamma12 = -J(1, 2)/detJ;
            Gamma21 = -J(2, 1)/detJ;
            Gamma22 = J(1, 1)/detJ;
            
            % Write shape function derivatives wrt x
            N1x = Gamma11*N1xi + Gamma12*N1eta; N1y = Gamma21*N1xi + Gamma22*N1eta;
            N2x = Gamma11*N2xi + Gamma12*N2eta; N2y = Gamma21*N2xi + Gamma22*N2eta;
            N3x = Gamma11*N3xi + Gamma12*N3eta; N3y = Gamma21*N3xi + Gamma22*N3eta;
            N4x = Gamma11*N4xi + Gamma12*N4eta; N4y = Gamma21*N4xi + Gamma22*N4eta;
            
            % Write each B submatrix
            
            % Membrane
            Bm1 = [N1x, 0;  0, N1y;  N1y, N1x];
            Bm2 = [N2x, 0;  0, N2y;  N2y, N2x];
            Bm3 = [N3x, 0;  0, N3y;  N3y, N3x];
            Bm4 = [N4x, 0;  0, N4y;  N4y, N4x];
            
            % Bending
            Bb1 = [0, N1x, 0;  0, 0, N1y;  0, N1y, N1x];
            Bb2 = [0, N2x, 0;  0, 0, N2y;  0, N2y, N2x];
            Bb3 = [0, N3x, 0;  0, 0, N3y;  0, N3y, N3x];
            Bb4 = [0, N4x, 0;  0, 0, N4y;  0, N4y, N4x];
            
            % Return full B matrix
            Bm = [Bm1, Bm2, Bm3, Bm4];
            Bb = [Bb1, Bb2, Bb3, Bb4];
            
            % Shape functions
            Nm = [N1,  0, N2,  0, N3,  0, N4, 0;
                0, N1,  0, N2,  0, N3,  0, N4];
            
            Nb = [N1,  0,  0, N2,  0,  0, N3,  0,  0, N4,  0,  0;
                0, N1,  0,  0, N2,  0,  0, N3,  0,  0, N4,  0;
                0,  0, N1,  0,  0, N2,  0,  0, N3,  0,  0, N4];
            
        end
        
        % ----- Old Linear Functions ----- % 
     
        function [detJ, Bm, Bb, Nm, Nb] = evalMBN(obj,xi, eta, X, Y)
            % Evaluate B membrane and bending matrices, along with shape functions themselves,
            % at the coordinate 'xi' and 'eta'
            % Inputs : 

            
            % Write shape functions
            N1 = 0.25*(1 - xi)*(1 - eta); N2 = 0.25*(1 + xi)*(1 - eta);
            N3 = 0.25*(1 + xi)*(1 + eta); N4 = 0.25*(1 - xi)*(1 + eta);
            
            % Write shape function derivatives
            N1xi = -0.25*(1 - eta); N2xi = 0.25*(1 - eta); N3xi = 0.25*(1 + eta); N4xi = -0.25*(1 + eta);
            N1eta = -0.25*(1 - xi); N2eta = -0.25*(1 + xi); N3eta = 0.25*(1 + xi); N4eta = 0.25*(1 - xi);
            
            J =  [N1xi,  N2xi,  N3xi,  N4xi;
                  N1eta, N2eta, N3eta, N4eta]*[X(:), Y(:)];
            
            detJ = J(1, 1)*J(2, 2) - J(2, 1)*J(1, 2);
            
            % Define "gamma" products
            Gamma11 = J(2, 2)/detJ;
            Gamma12 = -J(1, 2)/detJ;
            Gamma21 = -J(2, 1)/detJ;
            Gamma22 = J(1, 1)/detJ;
            
            % Write shape function derivatives wrt x
            N1x = Gamma11*N1xi + Gamma12*N1eta; N1y = Gamma21*N1xi + Gamma22*N1eta;
            N2x = Gamma11*N2xi + Gamma12*N2eta; N2y = Gamma21*N2xi + Gamma22*N2eta;
            N3x = Gamma11*N3xi + Gamma12*N3eta; N3y = Gamma21*N3xi + Gamma22*N3eta;
            N4x = Gamma11*N4xi + Gamma12*N4eta; N4y = Gamma21*N4xi + Gamma22*N4eta;
            
            % Write each B submatrix
            
            % Membrane
            Bm1 = [N1x, 0;  0, N1y;  N1y, N1x];
            Bm2 = [N2x, 0;  0, N2y;  N2y, N2x];
            Bm3 = [N3x, 0;  0, N3y;  N3y, N3x];
            Bm4 = [N4x, 0;  0, N4y;  N4y, N4x];
            
            % Bending
            Bb1 = [0, N1x, 0;  0, 0, N1y;  0, N1y, N1x];
            Bb2 = [0, N2x, 0;  0, 0, N2y;  0, N2y, N2x];
            Bb3 = [0, N3x, 0;  0, 0, N3y;  0, N3y, N3x];
            Bb4 = [0, N4x, 0;  0, 0, N4y;  0, N4y, N4x];
            
            % Return full B matrix
            Bm = [Bm1, Bm2, Bm3, Bm4];
            Bb = [Bb1, Bb2, Bb3, Bb4];
            
            % Shape functions
            Nm = [N1,  0, N2,  0, N3,  0, N4, 0;
                  0, N1,  0, N2,  0, N3,  0, N4];
            
            Nb = [N1,  0,  0, N2,  0,  0, N3,  0,  0, N4,  0,  0;
                  0, N1,  0,  0, N2,  0,  0, N3,  0,  0, N4,  0;
                  0,  0, N1,  0,  0, N2,  0,  0, N3,  0,  0, N4];
        end
        
        function [detJ, Bs] = evalS(obj,xi, eta, X, Y)
            % Evaluate B shear matrix at the coordinate 'xi' and 'eta'
            
            % Write shape functions
            N1 = 0.25*(1 - xi)*(1 - eta); N2 = 0.25*(1 + xi)*(1 - eta);
            N3 = 0.25*(1 + xi)*(1 + eta); N4 = 0.25*(1 - xi)*(1 + eta);
            
            % Write shape function derivatives
            N1xi = -0.25*(1 - eta); N2xi = 0.25*(1 - eta); N3xi = 0.25*(1 + eta); N4xi = -0.25*(1 + eta);
            N1eta = -0.25*(1 - xi); N2eta = -0.25*(1 + xi); N3eta = 0.25*(1 + xi); N4eta = 0.25*(1 - xi);
            
            J =  [N1xi,  N2xi,  N3xi,  N4xi;
                N1eta, N2eta, N3eta, N4eta]*[X(:), Y(:)];
            
            detJ = J(1, 1)*J(2, 2) - J(2, 1)*J(1, 2);
            
            % Define "gamma" products
            Gamma11 = J(2, 2)/detJ;
            Gamma12 = -J(1, 2)/detJ;
            Gamma21 = -J(2, 1)/detJ;
            Gamma22 = J(1, 1)/detJ;
            
            % Write shape function derivatives wrt x
            N1x = Gamma11*N1xi + Gamma12*N1eta; N1y = Gamma21*N1xi + Gamma22*N1eta;
            N2x = Gamma11*N2xi + Gamma12*N2eta; N2y = Gamma21*N2xi + Gamma22*N2eta;
            N3x = Gamma11*N3xi + Gamma12*N3eta; N3y = Gamma21*N3xi + Gamma22*N3eta;
            N4x = Gamma11*N4xi + Gamma12*N4eta; N4y = Gamma21*N4xi + Gamma22*N4eta;
            
            % Write each B submatrix
            Bs1 = [-N1x, N1, 0;  -N1y, 0, N1];
            Bs2 = [-N2x, N2, 0;  -N2y, 0, N2];
            Bs3 = [-N3x, N3, 0;  -N3y, 0, N3];
            Bs4 = [-N4x, N4, 0;  -N4y, 0, N4];
            
            % Return full B matrix
            Bs = [Bs1, Bs2, Bs3, Bs4];
        end
        
             
    end
    
end

