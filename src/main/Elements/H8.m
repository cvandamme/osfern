classdef H8 < Element
    % Hexagonal 8-nodes shell element with selectively reduced integration
    % for the shear stiffness terms. This element is from Cook (add ref)
    
    properties (GetAccess = 'private', SetAccess = 'private')
        
        % Derived Section Properties
        sectionProps
        elementProps
        Te
        nlShape = struct('G', [], 'D', []);
    end
    
    % --- Constant Properties Cannot Be Changed
    properties (Constant)
        
        nNode = 8;     % Number of nodes in this element
        activeDof = 3;  % Number of active DOF per node for this element
        
        % Faces
        faces = [1 4 5 8; 2 3 6 7; 1 2 5 6; 4 3 8 7; 1:4; 5:8];
        fNormals = [-1 1; -1 1; -1 1]; % [eta;nu;mu]
        
        % Sides
        sides = [1 2;2 3;3 4;4 1;1 5;2 6;3 7;4 8;5 6;6 7;7 8;8 5];
        
        % Linear Quad Points and Weights
        quadPointsLin = [-1/sqrt(3), 1/sqrt(3)]; 
        quadWeightsLin = [1, 1];
        
        % NonLinear Quad Points and Weights
        quadPointsNonLin = [-sqrt(0.6), 0, sqrt(0.6)];
        quadWeightsNonLin = [5/9, 8/9, 5/9];
        
        
    end
    
    methods
        % ----- Initialization ----- %
        function obj = H8(id)
            
%             obj = obj@3D(id);
            obj = obj@Element(id);
            
        end
        
        % ----- Abstract Method Implementations -----
        function [obj] = Build(obj, node, material)
            % This function is called to generate the solid properties.

            
            % Check correct number of nodes are provided
            if length(node) ~=8
                error('Number of nodes provided is incorrect')
            else
                obj.nodes = node;
            end
            
            % Store material
            obj.material = material;
            
            % Build Tranfromation Matrix
            obj.BuildTransform();
            
            % Build Section Properties
            obj.BuildSection();
            
            % Build shapes
%             obj.BuildShapeNL();
            
            
        end
        
        % ------ Element Generation Functions ------
        function [Mout] = BuildMass(obj)
            % This function builds the element mass matrix of a 8-nodes
            % hexagonal element. 
            % Inputs :
            %
            % Outputs :
            %
            %
           
            
            % Initialize Matrices
            Mel = zeros(24);
            
            % Extract Projected X and Y (inplane) Nodal Coordinates
            X = obj.localCoords(:, 1);
            Y = obj.localCoords(:, 2);
            Z = obj.localCoords(:, 3);
            xyz = [X Y Z];
            
            % Full integration 
            for ii = 1:length(obj.quadPointsLin)
                for jj = 1:length(obj.quadPointsLin)
                    for kk = 1:length(obj.quadPointsLin)
                        % Evaluate Shape Functions
                        [~,Jdd,Nd] = obj.Shape3(obj.quadWeightsLin(ii),...
                                                obj.quadWeightsLin(jj),...
                                                obj.quadWeightsLin(kk),...
                                                xyz);
                        % Add to element
                        Mel = Mel+obj.material.GetProp('rho')*Nd'*Nd*Jdd;
                    end
                end
            end
            
            % Tranform to Global System
            Mel = obj.transform*Mel*obj.transform';
            
            % Change Ouput to Vector
            Mout = Mel(:);
            
        end
        
        function [Kout] = BuildStiff(obj, x)
            
            % Initialize Matrices
            Kfull = zeros(30);
            
            % Extract Projected X and Y (inplane) Nodal Coordinates
            X = obj.localCoords(:, 1);
            Y = obj.localCoords(:, 2);
            Z = obj.localCoords(:, 3);
            xyz = [X Y Z];
            
            % Full integration 
            for ii = 1:length(obj.quadPointsLin)
                for jj = 1:length(obj.quadPointsLin)
                    for kk = 1:length(obj.quadPointsLin)
                        % Evaluate Shape Functions
                        [Bd,Jdd,~] = obj.Shape3(obj.quadWeightsLin(ii),...
                                                obj.quadWeightsLin(jj),...
                                                obj.quadWeightsLin(kk),...
                                                obj.localCoords);
                        % Add to element
                        Kfull = Kfull+Bd(1:3,:)'*obj.sectionProps.D*Bd(1:3,:)*Jdd;
                    end
                end
            end
            
            %  qauss quadrature (reduced) for shear strain terms
            %  in Kel (Bs'*Ds*Bs)
            for i = 1:length(obj.quadPointsLin)
              [Bs,Jds,~]=obj.Shape3(0,0,obj.quadWeightsLin(ii),...
                  obj.localCoords);
              Kfull = Kfull+4*Bs(4,:)'*obj.material.getProp('G')*Bs(4,:)*Jds;
              [Bs,Jds,~]=obj.Shape3(obj.quadWeightsLin(ii),0,0,...
                  obj.localCoords);
              Kfull = Kfull+4*Bs(5,:)'*obj.material.getProp('G')*Bs(5,:)*Jds;
              [Bs,Jds,~]=obj.Shape3(0,obj.quadWeightsLin(ii),0,...
                  obj.localCoords);
              Kfull = Kfull+4*Bs(6,:)'*obj.material.getProp('G')*Bs(6,:)*Jds;
            end
            
            % Condense down to 24 DOF using static condensation
            full = false;
            if full
                Kel = Kfull;
            else
                Kel = Kfull(1:24,1:24)-Kfull(1:24,25:30)/(Kfull(25:30,25:30))*Kfull(25:30,1:24);
            end
            
            % Tranform to Global System
            Kel = obj.transform*Kel*obj.transform';
            
            % Change Ouput to Vector
            Kout = Kel(:);
            
            % If a displacement is supplied, also return the nonlinear tangent
            % stiffness matrix
            if ~isempty(x)
                [~, Kt] = obj.BuildNLStiff(x);
                Kout = Kout + Kt;
            end
            
        end
        
        function [Kn, Kt] = BuildNLStiff(obj, x)
            
            
            xLocal = obj.transform'*x;
            xm = xLocal(obj.mi);
            xb = xLocal(obj.bi);
            
            K1nm = zeros(4);
            K1bm = zeros(4, 8);
            K2b = zeros(4);
            n = length(obj.QUADweightsNL);
            
            for i = 1:n
                for j = 1:n
                    
                    %                     [detJ, G, Nm, C, Bm] = obj.evalNL(xi, eta, X, Y, xLocal(obj.mi), xLocal(obj.bi), obj.elementProps.Dmembrane);
                    %                     [Nm, C] = obj.evalNL(i, j, xm, xb);
                    
                    nm = obj.nlShape(i, j).DBm*xm;
                    Nm = [nm(1), nm(3); nm(3), nm(2)];
                    
                    dw = obj.nlShape(i, j).G*xb;
                    C = [dw(1), 0;  0, dw(2);  dw(2), dw(1)];
                    
                    K1nm = K1nm + obj.nlShape(i, j).Ghat*Nm*obj.nlShape(i, j).G;
                    K1bm = K1bm + obj.nlShape(i, j).Ghat*C.'*obj.nlShape(i, j).DBm;
                    K2b = K2b + obj.nlShape(i, j).Ghat*C.'*obj.elementProps.Dmembrane*C*obj.nlShape(i, j).G;
                    
                    
                end
            end
            
            Kn = [K1nm + K2b,  K1bm; K1bm.', zeros(8)];
            Kt = [2*K1nm + 3*K2b, 2*K1bm; 2*K1bm.', zeros(8)];
            reorder = [obj.bi, obj.mi];
            
            Kne = zeros(24);
            Kte = zeros(24);
            
            Kne(reorder, reorder) = Kn;
            Kte(reorder, reorder) = Kt;
            
            Kne = obj.transform*Kne*obj.transform.';
            Kte = obj.transform*Kte*obj.transform.';
            
            Kn = Kne(:);
            Kt = Kte(:);
            
        end
        
        % ----- Stress/Strain Generation Functions ---
        function [eleStress,eleStrain] = ComputeStessAndStrain(obj,x)
        end
        
        % ----- Thermal Stress/Strain/Nodal Loads Function
        function [eleStress,eleStrain,nodeLoads] = ComputeThermalLoads(obj,varargin)
            % This functions computes the thermal effects of applied
            % temperature to a S4 element.
            
            % Check if nodal temperatues are set directly
            
            % Check provided information
            if nargin > 2 % nodal temps and deformations supplied
                nodalTemp = varargin{1};
                nodalDef = varargin{2};
            elseif nargin > 1 % only nodal temps provided
                nodalTemp = varargin{1};
            else % use nodal temps set in nodeObj (for 
                
            end
                
            
            % compute the average element temperature
            avgTemp = 0;
            for ii = 1:4
                trow = obj.nodes(ii).GetTemperature();
                avgTemp = avgTemp+obj.node(ii);
            end
            
            % Average over the elements
            avgTemp = avgTemp/4;
            
            % Difference from provided reference temp
            thermalGrad = avgTemp-T0;
            
            % compute thermal stresses
            Ee = [obj.material.alpha*thermalGrad; obj.material.alpha*thermalGrad];
            Se = obj.sectionProps.D*Ee;
            
            % form a vector of nodal temperatures
            Tni = Tn(nodes);
            
            % compute induced nodal loads using 2x2 Gauss quadrature
            im = [1:2 7:8 13:14 19:20];
            rad3 = 1/sqrt(3);
            loc2 = [-rad3,rad3];
            Pel = zeros(24,1);
            e0 = ones(2,1);
            
            % Use Gauss Quadrature to integrate thermal loads
            for ii = 1:length(obj.quadPtsThermal)
                xi = obj.QUADpointsS(ii);
                wxi = obj.QUADweightsS(ii);
                for jj = 1:length(obj.quadPtsThermal)
                    eta = obj.QUADpointsS(ji);
                    weta = obj.QUADweightsS(ji);
                    
                    % Compute shape functions
                    [B,Jd,N]=shape20(nxi,neta,nxy);
                    
                    % Add to nodal temperatures
                    Ti = N*Tni-T0;
                    
                    % Add to nodal temperatues
                    Pel(im) = Pel(im)+B(1:2,:)'*D*obj.material.alpha*Ti*e0*Jd*t;
                end
            end
        end

        % ---- Additional Functions --- %
        function [Nm, C] = evalNL(obj, i, j, wm, wb)
            % Evaluate nonlinear matrix contribution at integration point.
            nm = obj.elementProps.Dmembrane*obj.nlShape(i, j).Bm*wm;
            Nm = [nm(1), nm(3); nm(3), nm(2)];
            
            dw = obj.nlShape(i, j).G*wb;
            C = [dw(1), 0;  0, dw(2);  dw(2), dw(1)];
            
        end
        
        function [obj] = BuildShapeNL(obj)
            % Precompute and store shape function quantities for evaluation
            % of nonlinear forces at each integration point.
            
            X = obj.localCoords(:, 1);
            Y = obj.localCoords(:, 2);
            
            
            for i = 1:length(obj.QUADpointsNL)
                xi = obj.QUADpointsNL(i);
                wi = obj.QUADweightsNL(i);
                for j = 1:length(obj.QUADpointsNL)
                    eta = obj.QUADpointsNL(j);
                    wj = obj.QUADweightsNL(j);
                    
                    %                     [detJ, G, Nm, C, Bm] = obj.evalNL(xi, eta, X, Y, xLocal(obj.mi), xLocal(obj.bi), obj.elementProps.Dmembrane);
                    
                    % Write shape function derivatives
                    N1xi = -0.25*(1 - eta); N2xi = 0.25*(1 - eta); N3xi = 0.25*(1 + eta); N4xi = -0.25*(1 + eta);
                    N1eta = -0.25*(1 - xi); N2eta = -0.25*(1 + xi); N3eta = 0.25*(1 + xi); N4eta = 0.25*(1 - xi);
                    
                    J =  [N1xi,  N2xi,  N3xi,  N4xi;
                        N1eta, N2eta, N3eta, N4eta]*[X(:), Y(:)];
                    
                    detJ = J(1, 1)*J(2, 2) - J(2, 1)*J(1, 2);
                    
                    % Define "gamma" products
                    Gamma11 = J(2, 2)/detJ;
                    Gamma12 = -J(1, 2)/detJ;
                    Gamma21 = -J(2, 1)/detJ;
                    Gamma22 = J(1, 1)/detJ;
                    
                    % Write shape function derivatives wrt x
                    N1x = Gamma11*N1xi + Gamma12*N1eta; N1y = Gamma21*N1xi + Gamma22*N1eta;
                    N2x = Gamma11*N2xi + Gamma12*N2eta; N2y = Gamma21*N2xi + Gamma22*N2eta;
                    N3x = Gamma11*N3xi + Gamma12*N3eta; N3y = Gamma21*N3xi + Gamma22*N3eta;
                    N4x = Gamma11*N4xi + Gamma12*N4eta; N4y = Gamma21*N4xi + Gamma22*N4eta;
                    
                    % Write each submatrix
                    Bm1 = [N1x, 0;  0, N1y;  N1y, N1x];
                    Bm2 = [N2x, 0;  0, N2y;  N2y, N2x];
                    Bm3 = [N3x, 0;  0, N3y;  N3y, N3x];
                    Bm4 = [N4x, 0;  0, N4y;  N4y, N4x];
                    Bm = [Bm1, Bm2, Bm3, Bm4];
                    
                    G = [N1x, N2x, N3x, N4x;
                        N1y, N2y, N3y, N4y];
                    
                    obj.nlShape(i, j).Ghat = 0.5*wi*wj*G'*detJ;
                    obj.nlShape(i, j).G = G;
                    obj.nlShape(i, j).DBm = obj.elementProps.Dmembrane*Bm;
                    
                    
                end
            end
            
            
            
            % Write shape functions
            N1 = 0.25*(1 - xi)*(1 - eta); N2 = 0.25*(1 + xi)*(1 - eta);
            N3 = 0.25*(1 + xi)*(1 + eta); N4 = 0.25*(1 - xi)*(1 + eta);
            
            % Write shape function derivatives
            N1xi = -0.25*(1 - eta); N2xi = 0.25*(1 - eta); N3xi = 0.25*(1 + eta); N4xi = -0.25*(1 + eta);
            N1eta = -0.25*(1 - xi); N2eta = -0.25*(1 + xi); N3eta = 0.25*(1 + xi); N4eta = 0.25*(1 - xi);
            
            J =  [N1xi,  N2xi,  N3xi,  N4xi;
                N1eta, N2eta, N3eta, N4eta]*[X(:), Y(:)];
            
            detJ = J(1, 1)*J(2, 2) - J(2, 1)*J(1, 2);
            
            % Define "gamma" products
            Gamma11 = J(2, 2)/detJ;
            Gamma12 = -J(1, 2)/detJ;
            Gamma21 = -J(2, 1)/detJ;
            Gamma22 = J(1, 1)/detJ;
            
            % Write shape function derivatives wrt x
            N1x = Gamma11*N1xi + Gamma12*N1eta; N1y = Gamma21*N1xi + Gamma22*N1eta;
            N2x = Gamma11*N2xi + Gamma12*N2eta; N2y = Gamma21*N2xi + Gamma22*N2eta;
            N3x = Gamma11*N3xi + Gamma12*N3eta; N3y = Gamma21*N3xi + Gamma22*N3eta;
            N4x = Gamma11*N4xi + Gamma12*N4eta; N4y = Gamma21*N4xi + Gamma22*N4eta;
            
            % Write each B submatrix
            
            % Membrane
            Bm1 = [N1x, 0;  0, N1y;  N1y, N1x];
            Bm2 = [N2x, 0;  0, N2y;  N2y, N2x];
            Bm3 = [N3x, 0;  0, N3y;  N3y, N3x];
            Bm4 = [N4x, 0;  0, N4y;  N4y, N4x];
            
            % Bending
            Bb1 = [0, N1x, 0;  0, 0, N1y;  0, N1y, N1x];
            Bb2 = [0, N2x, 0;  0, 0, N2y;  0, N2y, N2x];
            Bb3 = [0, N3x, 0;  0, 0, N3y;  0, N3y, N3x];
            Bb4 = [0, N4x, 0;  0, 0, N4y;  0, N4y, N4x];
            
            % Return full B matrix
            Bm = [Bm1, Bm2, Bm3, Bm4];
            Bb = [Bb1, Bb2, Bb3, Bb4];
            
            % Shape functions
            Nm = [N1,  0, N2,  0, N3,  0, N4, 0;
                0, N1,  0, N2,  0, N3,  0, N4];
            
            Nb = [N1,  0,  0, N2,  0,  0, N3,  0,  0, N4,  0,  0;
                0, N1,  0,  0, N2,  0,  0, N3,  0,  0, N4,  0;
                0,  0, N1,  0,  0, N2,  0,  0, N3,  0,  0, N4];
            
        end
        
        function coords = GetNodalCoords(obj)
            
            c1 = obj.nodes{1}.GetXYZ(); % 1st Nodal Coordinate
            c2 = obj.nodes{2}.GetXYZ(); % 2nd Nodal Coordinate
            c3 = obj.nodes{3}.GetXYZ(); % 3rd Nodal Coordinate
            c4 = obj.nodes{4}.GetXYZ(); % 4th Nodal Coordinate
            c5 = obj.nodes{5}.GetXYZ(); % 5th Nodal Coordinate
            c6 = obj.nodes{6}.GetXYZ(); % 6th Nodal Coordinate
            c7 = obj.nodes{7}.GetXYZ(); % 7th Nodal Coordinate
            c8 = obj.nodes{8}.GetXYZ(); % 8th Nodal Coordinate
            % All nodal cooridinates
            coords = [c1, c2, c3, c4, c5, c6, c7, c8]; 
        end
        
        function [obj] = BuildTransform(obj)
            % This function builds the transformation matrix of the
            % hexagonal element to transform from local to global.
            % Additionally the function
            %
            % Inputs :
            %           obj = H8 Object
            % Outputs :
            %           obj = H8 Object
            
            % Grab nodes
            c = obj.GetNodalCoords().';
            
            % Compute each faces normal vector
            Nc = zeros(6,3);
            for ii = 1:6
              Nc(ii,:) = sum(c(obj.faces(ii,:),:))/4;
            end
            
            % Element rotation matrix
            obj.Te = [Nc(2,:)-Nc(1,:);Nc(4,:)-Nc(3,:);Nc(6,:)-Nc(5,:)];
            le = sqrt(obj.Te(:,1).^2+obj.Te(:,2).^2+obj.Te(:,3).^2);
            obj.Te = [obj.Te(1,:)/le(1);obj.Te(2,:)/le(2);obj.Te(3,:)/le(3)];

            %  compute orthogonal unit vectors for element coord system
            if abs(dot(cross2(obj.Te(1,:),obj.Te(2,:)),obj.Te(3,:))) == 1
              Tp = obj.Te;
            else
              Tp = obj.hexelco(obj.Te);
            end

            %  compute node point coords in element system
            norg = sum(c)/8;
            u = [c(:,1)'-norg(1);c(:,2)'-norg(2);c(:,3)'-norg(3)]';
            obj.localCoords = u*Tp';
            
    
            %  test for element aspect ratio
            ledge = sqrt((obj.localCoords (obj.sides(:,2),1)-obj.localCoords (obj.sides(:,1),1)).^2 ...
                +(obj.localCoords (obj.sides(:,2),2)-obj.localCoords(obj.sides(:,1),2)).^2 ...
                +(obj.localCoords (obj.sides(:,2),3)-obj.localCoords(obj.sides(:,1),3)).^2);
            lrat = max(ledge)/min(ledge);
            if lrat > 50
                warning(['HEX8 aspect ratio = ',num2str(lrat)]);
            end
 

            e1 = (c(2, :) - c(1, :))/norm(c(2, :) - c(1, :));
            e2 = (c(4, :) - c(1, :))/norm(c(4, :) - c(1, :));
            e3 = cross(e1,e2);
            
            % Compute Unit Vector (e13 & e42)
            e13 = (c(3, :) - c(1, :))/norm(c(3, :) - c(1, :));
            e24 = (c(2, :) - c(4, :))/norm(c(2, :) - c(4, :));
            
            % Generate Element Local Coordinate System Unit Vectors
            % ** cvd - need to check this **
            obj.Te = zeros(3);
            obj.Te(1,:) = e13 + e24; % Add together ?? why ??
            obj.Te(3,:) = Element.cross2(obj.Te(1,:),e13);
            obj.Te(2,:) = Element.cross2(obj.Te(3,:),obj.Te(1,:));
            
            % Normalize Matrix
            obj.Te(1,:) = obj.Te(1,:)/norm(obj.Te(1,:));
            obj.Te(2,:) = obj.Te(2,:)/norm(obj.Te(2,:));
            obj.Te(3,:) = obj.Te(3,:)/norm(obj.Te(3,:));
            
            % Assemble Transformation Matrix
            T = zeros(24);
            T(1:3,1:3) = obj.Te';
            T(4:6,4:6) = obj.Te';
            T(7:9,7:9) = obj.Te';
            T(10:12,10:12) = obj.Te';
            T(13:15,13:15) = obj.Te';
            T(16:18,16:18) = obj.Te';
            T(19:21,19:21) = obj.Te';
            T(22:24,22:24) = obj.Te';
            
            % Store Transformation
            obj.transform = T;
            
            
        end
        
        function [obj] = BuildSection(obj)
            % This function computes the constitutive matrix of a hexagonal
            % element.
            %
            % Inputs  :
            %           obj
            % Outputs :
            %           obj
            
            % Assign Material if not specified
            E = obj.material.getProp('E');
            nu = obj.material.getProp('nu');
            G =  E/(2*(1 + nu));
            obj.material.setProp('G', G);
            
            % Form constituitive matrices
            d2 = 2*nu/(1-2*nu);
            dd = d2*ones(3);
            dd(1,1) = 2*(1-nu)/(1-2*nu);
            dd(2,2) = dd(1,1);
            dd(3,3) = dd(1,1);
            obj.sectionProps.D = dd*G;

            % Compute x, y and z normal face lengths
            dx = .5*(obj.localCoords(2,1)-obj.localCoords(1,1)+obj.localCoords(3,1)-obj.localCoords(4,1));
            dy = .5*(obj.localCoords(4,2)-obj.localCoords(1,2)+obj.localCoords(3,2)-obj.localCoords(2,2));
            dz = .5*(obj.localCoords(4,2)-obj.localCoords(1,2)+obj.localCoords(3,2)-obj.localCoords(2,2));
            
            % Store Properties
            obj.elementProps.vol = dx*dy*dz;
            obj.elementProps.mass = obj.elementProps.vol*obj.material.getProp('rho');
            
        end
        


    end
    
    methods(Static)
        
        function Kstab = stabilization(K, x, y, factor)
            
            % First, symmetrize the matrix
            K = 0.5*(K + K');
            
            % Obtain eigenvalues; find zero-strain modes
            [phi, lambda] = eig(K); lambda = diag(lambda);
            lambdaMax = max(lambda);
            
            % Use a threshold to determine what counts as zero-strain
            threshold = 1E-3;
            phiz = phi(:, lambda < threshold);
            
            % Construct analytical rigid body modes
            psi = zeros(size(K, 1), 6);
            % Translation is easy
            psi(1:6:end, 1) = 1; psi(2:6:end, 2) = 1; psi(3:6:end, 3) = 1;
            
            % Rotation a bit tougher
            for i = 1:3 % For each rotation
                rotation = zeros(3, 1); rotation(i) = 1;
                for j = 1:length(x) % For each node
                    rx = x(j); ry = y(j); r = [rx; ry; 0];
                    dx = cross(rotation, r);
                    inds = (j - 1)*6 + [1:6];
                    psi(inds, 3 + i) = [dx; rotation];
                end
            end
            
            % Use analytical RB matrix to construct a projector
            Psi = psi*((psi'*psi)\psi'); % Projector
            thetaBar = phiz - Psi*phiz;  % Remove RB modes from zero-strain modes
            
            % Obtain 3 unique zero-strain modes
            [Q, ~] = qr(thetaBar, 0);
            theta = Q(:, 1:3);
            lambdaStar = factor*lambdaMax;
            Kstar = pinv(theta')*lambdaStar*eye(3)*pinv(theta);
            Kstar = 0.5*(Kstar + Kstar');
            
            Kstab = K + Kstar;
            
        end
        
        function [B,Jd,N] = Shape3(nxi,neta,nzeta,nxyz)
            %  [B,Jd,N]=shape3(nxi,neta,nzeta,nxyz)
            %  R Gordon   7/8/94
            %
            %  Computes the shapes functions, Jacobian matrix and the strain-
            %  displacement "B" matrix for an 8-noded solid isoparametric
            %  finite element.  Six incompatible modes (bubble functions) are
            %  added to improve performance.  Called by the function hex8.m.
            %
            %  Inputs:
            %	(nxi,neta,nzeta) -	isoparametric coordinates of point
            %	nxyz -			node coordinate matrix
            %
            %				x1  y1  z1
            %				x2  y2  z2
            %				.   .   .
            %				x8  y8  z8
            %
            %  Outputs: (evaluated at the point (nxi,neta,nzeta))
            %	B  -		element strain-displacement matrix (6x30)
            %	Jd -		determinant of the Jacobian matrix (scalar)
            %	N  -		shape function matrix (3x24)
            %
            
            %  compute shape functions, N and derivatives, Nxi, Neta, and Nzeta
            ind = [1 4 7 10 13 16 19 22];
            xii = [-1 1 1 -1 -1 1 1 -1];
            etai = [-1 -1 1 1 -1 -1 1 1];
            zetai = [-1 -1 -1 -1 1 1 1 1];
            d1 = (1+xii*nxi);
            d2 = (1+etai*neta);
            d3 = (1+zetai*nzeta);
            Nn = d1.*d2.*d3/8;
            N = zeros(3,24);
            N(1,ind) = Nn;
            N(2,ind+1) = Nn;
            N(3,ind+2) = Nn;
            
            dN = zeros(3,8);
            dN(1,:) = d2.*d3.*xii/8;
            dN(2,:) = d1.*d3.*etai/8;
            dN(3,:) = d1.*d2.*zetai/8;
            
            %  compute Jacobian, J, its inverse, Jin, and determinant, Jd
            J = dN*nxyz;
            Jin = inv(J);
            Jd = det(J);
            
            %  form strain-displacement matrix, B
            B = zeros(6,30);
            M1 = zeros(6,9);
            M1(1,1) = 1; M1(2,5) = 1; M1(3,9) = 1;
            M1(4,2) = 1; M1(4,4) = 1;
            M1(5,6) = 1; M1(5,8) = 1;
            M1(6,3) = 1; M1(6,7) = 1;
            
            M2 = zeros(9);
            M2(1:3,1:3) = Jin;
            M2(4:6,4:6) = Jin;
            M2(7:9,7:9) = Jin;
            
            M3 = zeros(9,24);
            M3(1:3,ind) = dN;
            M3(4:6,ind+1) = dN;
            M3(7:9,ind+2) = dN;
            B(:,1:24) = M1*M2*M3;
            
            % add bubble function states
            B(1,25) = nxi/Jd;
            B(1,28) = nxi*neta/Jd;
            B(1,30) = nxi*nzeta/Jd;
            B(2,26) = neta/Jd;
            B(2,28) = nxi*neta/Jd;
            B(2,29) = neta*nzeta/Jd;
            B(3,27) = nzeta/Jd;
            B(3,29) = neta*nzeta/Jd;
            B(3,30) = nxi*nzeta/Jd;
            
        end
        function Mp = hexelco(M)
            %  function Mp = hexelco(M);
            %  R Gordon   2/25/97
            %              3/2/05 speed improvement
            %
            %  Computes system of unit vectors, Mp, of orthogonal element
            %  coordinate system for HEX8 element given an initial,
            %  non-orthogonal system, M.
            
            % compute central unit vector, C, of original system
            R = M(1,:);   S = M(2,:);   T = M(3,:);
            RS = S-R;     RT = T-R;
            C = cross2(RS,RT);
            C = C/norm(C);
            
            D = dot(R,C)*C;
            DR = R-D;     DS = S-D;     DT = T-D;
            DRmag = norm(DR);
            thetaS = acos(dot(DR,DS)/(DRmag*norm(DS)));
            thetaT = acos(dot(DS,DT)/(norm(DS)*norm(DT)));
            
            % compute delR
            delS0 = 2*pi/3-thetaS;
            delT0 = 4*pi/3-thetaS-thetaT;
            delvec = [0,delS0,delT0];
            delR = -min(delvec)-.5*(max(delvec)-min(delvec));
            
            % compute unit vectors of local coordinate system for RST plane
            DR = DR/DRmag;
            L = [DR;cross2(C,DR);C];
            
            % compute Rp, Sp and Tp in local system
            Mp = sqrt(1/3)*ones(3);
            const = sqrt(.5+.5*(tan(pi/6))^2);
            Mp(1,1:2) = const*[cos(delR),sin(delR)];
            Mp(2,1:2) = const*[cos(delR+2*pi/3),sin(delR+2*pi/3)];
            Mp(3,1:2) = const*[cos(delR+4*pi/3),sin(delR+4*pi/3)];
            
            % transform Rp/Sp/Tp system into global coordinates
            Mp = Mp*L;
        end
        
    end
    
end

