classdef S4BDI < Element
    % Four node shell element with drilling DOF. Fully integrated for all
    % stiffness terms. Based on work by Blathe, Dvorkin, and Ibrahimbegovic
    % (BDI).
    %
    % The idea to use these particular procedures 
    % in tandeom comes from "An improved quadrilateral flat element
    % with drilling degrees of freedom for shell structural analysis" by
    % Nguyen-Van et al. [1] (This element is not actually the subject of 
    % the paper, rather, it follows the development of a "standard" shell 
    % element in section 2.)
    %
    % However, different works were consulted to actually implement the
    % element, due to some omissions in [1]. For the plate bending part, 
    % "A Four Node Plate Bending Element Based on Mindlin/Reissner Plate
    % Theory and a Mixed Interpolation" by Bathe and Dvorkin, 1985 [2] was
    % used.
    %
    % For the membrane part with drilling DOF, "A Robust Quadrilateral
    % Membrane Finite Element with Drilling DOF" by Ibrahimbegovic et al,
    % 1990 was used. [3]. This was sumplemented by "membrane element with
    % normal rotations" by Ed Wilson [4], available online.
    %
    % TODO List:
    %
    %
    % - At some point something on this element got broken. Problem seems
    % to be in the stiffness matrix. Maybe check back on master branch
    % first before merging?
    % - The main problem with this element right now is that the drilling
    % DOF is very computationall intensive to derive. It could almost
    % certainly be sped up with some thought. 
    
    properties (GetAccess = 'private', SetAccess = 'private')
        
        % Derived Section Properties
        sectionProps
        % A, Iyy, Izz, J, Ky, Kz
        elementProps
        % L, X, S
        % Y[y1, y2, y3, y4] , PHIY
        % Z[z1, z2, z3, z4] , PHIZ
        Te
        
        nlShape = struct('G', [], 'DBm', [], 'Ghat', []);
    end
    
    % --- Constant Properties Cannot Be Changed
    properties (Constant)
        
        gamma = 1; % Penalty parameter in Eq. (15) of reference.
        nNode = 4;     % Number of nodes in this element
        activeDof = 6;  % Number of active DOF per node for this element
        
        ri = [6, 12, 18, 24];  % Drilling DOF indices
        mi = [1, 2, 7, 8, 13, 14, 19, 20]; % Membrane indices
        bsi = [3, 4, 5, 9, 10, 11, 15, 16, 17, 21, 22, 23]; % Bending/shear indices
        bi = [3, 9, 15, 21]; % Bending indices
        mri = [1, 2, 6, 7, 8, 12, 13, 14, 18, 19, 20, 24] ; % Membrane/rotation indices; to be assigned
        
        QUADpointsBM = [-1/sqrt(3), 1/sqrt(3)]; % Quadrature Points; bending/membrane
        QUADweightsBM = [1, 1]; % Quadrature Weights; bending/membrane
        %         QUADpointsBM = [-sqrt(0.6), 0, sqrt(0.6)];
        %         QUADweightsBM = [5/9, 8/9, 5/9];
        %         QUADpointsS = [-1/sqrt(3), 1/sqrt(3)]; % Quadrature Points; bending/membrane
        %         QUADweightsS = [1, 1]; % Quadrature Weights; bending/membrane
        
        % Used to avoid spurious rigid body modes in a flat plate mesh.
        %         blendFactor = 0;
        QUADpointsS = 0; % Quadrature Points; shear
        QUADweightsS = 2; % Quadrature Weights; shear
        
        QUADpointsNL = [-sqrt(0.6), 0, sqrt(0.6)];
        QUADweightsNL = [5/9, 8/9, 5/9];
        
        
    end
    
    methods
        % ----- Initialization ----- %
        function obj = S4BDI(id)
            
            obj = obj@Element(id);
            
        end
        
        % ----- Abstract Methods that Must Be Used -----
        function [obj] = Build(obj, nodeCoord, material, section)
            % This function is called to generate the beam section
            % properties and transformation matrix for a beam element object
            
            obj.nodes = nodeCoord;
            obj.material = material;
            obj.section = section;
            
            % Build Tranfromation Matrix
            obj.BuildTransform();
            
            % Build Section Properties
            obj.BuildSection();
            
            % Build shapes
            obj.BuildShapeNL();
            
            
        end
        
        % ------ Element Generation Functions ------
        function [Mout] = BuildMass(obj)
            % This function builds the element mass matrix of a C1
            % Continous Beam Element in the element coordinate system.
            % Inputs :
            %
            % Outputs :
            %
            %
            
            
            % Extract Nodes (can be reomved in future for debugging)
            %             c1 = obj.nodes.coord(1,:)+off1; % 1st Nodal Coordinate
            %             c2 = obj.nodes.coord(2,:)+off2; % 2nd Nodal Coordinate
            %             c3 = obj.nodes.coord(3,:)+off1; % 3rd Nodal Coordinate
            %             c4 = obj.nodes.coord(4,:)+off2; % 4th Nodal Coordinate
            %             c = [c1 c2 c3 c4]; % All nodal cooridinates
            
            % Initialize Matrices
            Mel = zeros(24);
            
            % Extract Projected X and Y (inplane) Nodal Coordinates
            X = obj.localCoords(:, 1);
            Y = obj.localCoords(:, 2);
            
            % Full integration (2 x 2 Quadrature) for membrane and bending terms
            % -- Membrane, Bending and Shear Indicies are S4 Constant Properties
            % -- Quadrature Points and Weights are S4 Constant Properties
            for ii = 1:length(obj.QUADpointsBM)
                % Assign Points and Weights
                xi = obj.QUADpointsBM(ii);
                wi = obj.QUADweightsBM(ii);
                for jj = 1:length(obj.QUADpointsBM)
                    
                    % Assign Points and Weights
                    eta = obj.QUADpointsBM(jj);
                    wj = obj.QUADweightsBM(ii);
                    
                    % Evaluate Shape Functions (hopefully can be removed)
                    [detJm, ~, Nm, ~] = obj.evalMembrane(xi, eta, X, Y);
                    [detJb, ~, Nb] = obj.evalBending(xi, eta, X, Y);
                    
                    % Generate mass
                    Mel(obj.mri, obj.mri) = Mel(obj.mri, obj.mri) + obj.material.getProp('rho')*obj.section.t*wi*wj*(Nm'*Nm)*detJm;
                    % Suspect fix to mass matrix removes mass from
                    % rotational terms
                    Nb = Nb(:, 1:3:end); % Put on bending nodes only??
%                     keyboard
                    Mel(obj.bi, obj.bi) = Mel(obj.bi, obj.bi) + obj.material.getProp('rho')*obj.section.t*wi*wj*(Nb'*Nb)*detJb;
                    
                end
            end
            
            % Tranform to Global System
            Mel = obj.transform*Mel*obj.transform';
            
            % Change Ouput to Vector
            Mout = Mel(:);
            
        end
        
        function [Kout] = BuildStiff(obj, x)
            
            % Initialize Matrices
            Kel = zeros(24);
            
            % Extract Projected X and Y (inplane) Nodal Coordinates
            X4 = obj.localCoords(:, 1);
            Y4 = obj.localCoords(:, 2);
            
            % Generate X and Y by creating fake midside nodes
            X8(4) = 0.5*(X4(4) + X4(1)); Y8(4) = 0.5*(Y4(4) + Y4(1));
            X8(3) = 0.5*(X4(3) + X4(4)); Y8(3) = 0.5*(Y4(3) + Y4(4));
            X8(2) = 0.5*(X4(2) + X4(3)); Y8(2) = 0.5*(Y4(2) + Y4(3));
            X8(1) = 0.5*(X4(1) + X4(2)); Y8(1) = 0.5*(Y4(1) + Y4(2));
            
            X = [X4; X8(:)]; Y = [Y4; Y8(:)];
            
            % Full integration (2 x 2 Quadrature) for standard membrane and bending terms
            % -- Membrane, Bending and Shear Indicies are Constant Properties
            % -- Quadrature Points and Weights are Constant Properties
            for ii = 1:length(obj.QUADpointsBM)
                % Assign Points and Weights
                xi = obj.QUADpointsBM(ii);
                wi = obj.QUADweightsBM(ii);
                for jj = 1:length(obj.QUADpointsBM)
                    
                    % Assign Points and Weights
                    eta = obj.QUADpointsBM(jj);
                    wj = obj.QUADweightsBM(ii);
                    
                    % Evaluate Shape Functions (hopefully can be removed)
                    [detJm, Bm, ~, ~] = obj.evalMembrane(xi, eta, X, Y);
                    [detJb, Bb, ~] = obj.evalBending(xi, eta, X4, Y4);
                    [detJs, Bs] = obj.evalS(xi, eta, X4, Y4);
%                     [detJs, Bs] = obj.evalS(xi, eta, X4, Y4);
                    % Generate Stiffness
                    % Use local area estimate to make patch test stiffness
                    % adjustment (Eq. 9.8 of [4])
                    riLocal = [3, 6, 9, 12];
                    Bm(:, riLocal) = Bm(:, riLocal) - Bm(:, riLocal)/(wi*wj*detJm);
                    Kel(obj.bsi, obj.bsi) = Kel(obj.bsi, obj.bsi) + wi*wj*Bs'*obj.elementProps.Dshear*Bs*detJs;
                    Kel(obj.bsi, obj.bsi) = Kel(obj.bsi, obj.bsi) + wi*wj*Bb'*obj.elementProps.Dbending*Bb*detJb;
                    Kel(obj.mri, obj.mri) = Kel(obj.mri, obj.mri) + wi*wj*Bm'*obj.elementProps.Dmembrane*Bm*detJm;
                    %                     Kel(obj.bsi, obj.bsi) = Kel(obj.bsi, obj.bsi) + (1 - obj.blendFactor)*wi*wj*Bs'*obj.elementProps.Dshear*Bs*detJb;
                    
                    
                end
            end
            
            % Reduced integration for the shear terms and membrane benalty
            for i = 1:length(obj.QUADpointsS)
                xi = obj.QUADpointsS(i);
                wi = obj.QUADweightsS(i);
                for j = 1:length(obj.QUADpointsS)
                    eta = obj.QUADpointsS(j);
                    wj = obj.QUADweightsS(j);
%                     [detJs, Bs] = obj.evalSBad(xi, eta, X4, Y4);
                    [detJm, ~, ~, b0] = obj.evalMembrane(xi, eta, X, Y);
                                        
%                     Kel(obj.bsi, obj.bsi) = Kel(obj.bsi, obj.bsi) + wi*wj*Bs'*obj.elementProps.Dshear*Bs*detJs;
                    
                    Kel(obj.mri, obj.mri) = Kel(obj.mri, obj.mri) + obj.gamma*obj.material.getProp('G')*wi*wj*(b0.'*b0)*detJm;
                    %                     keyboard
                end
            end
            
            % Ensure that the element is symmetric
            Kel = 0.5*(Kel.' + Kel);
            
            % Tranform to Global System
            Kel = obj.transform*Kel*obj.transform';
            
            % Change Ouput to Vector
            Kout = Kel(:);
            %             K = Kel(obj.mri, obj.mri);
            % If a displacement is supplied, also return the nonlinear tangent
            % stiffness matrix
            if ~isempty(x)
                [~, Kt] = obj.buildNLStiff(x);
                Kout = Kout + Kt;
            end
            
        end
        
        
        function [Kn, Kt] = BuildNLStiff(obj, x)
            
            
            xLocal = obj.transform'*x;
            xm = xLocal(obj.mi);
            xb = xLocal(obj.bi);
            
            K1nm = zeros(4);
            K1bm = zeros(4, 8);
            K2b = zeros(4);
            n = length(obj.QUADweightsNL);
            
            for i = 1:n
                for j = 1:n
                    
                    %                     [detJ, G, Nm, C, Bm] = obj.evalNL(xi, eta, X, Y, xLocal(obj.mi), xLocal(obj.bi), obj.elementProps.Dmembrane);
                    %                     [Nm, C] = obj.evalNL(i, j, xm, xb);
                    
                    nm = obj.nlShape(i, j).DBm*xm;
                    Nm = [nm(1), nm(3); nm(3), nm(2)];
                    
                    dw = obj.nlShape(i, j).G*xb;
                    C = [dw(1), 0;  0, dw(2);  dw(2), dw(1)];
                    
                    K1nm = K1nm + obj.nlShape(i, j).Ghat*Nm*obj.nlShape(i, j).G;
                    K1bm = K1bm + obj.nlShape(i, j).Ghat*C.'*obj.nlShape(i, j).DBm;
                    K2b = K2b + obj.nlShape(i, j).Ghat*C.'*obj.elementProps.Dmembrane*C*obj.nlShape(i, j).G;
                    
                    
                end
            end
            
            Kn = [K1nm + K2b,  K1bm; K1bm.', zeros(8)];
            Kt = [2*K1nm + 3*K2b, 2*K1bm; 2*K1bm.', zeros(8)];
            reorder = [obj.bi, obj.mi];
            
            Kne = zeros(24);
            Kte = zeros(24);
            
            Kne(reorder, reorder) = Kn;
            Kte(reorder, reorder) = Kt;
            
            Kne = obj.transform*Kne*obj.transform.';
            Kte = obj.transform*Kte*obj.transform.';
            
            Kn = Kne(:);
            Kt = Kte(:);
            
        end
        
        function [K1, K2] = BuildNLmodal(obj, phi1, phi2)
            
            xLocal = obj.transform'*phi1;
            xm1 = xLocal(obj.mi);
            xb1 = xLocal(obj.bi);
            
            xLocal = obj.transform'*phi2;
            xb2 = xLocal(obj.bi);
            
            K1nm = zeros(4);
            K1bm = zeros(4, 8);
            K2b = zeros(4);
            n = length(obj.QUADweightsNL);
            
            for i = 1:n
                for j = 1:n
                    
                    nm = obj.nlShape(i, j).DBm*xm1;
                    Nm1 = [nm(1), nm(3); nm(3), nm(2)];
                    dw = obj.nlShape(i, j).G*xb1;
                    C1 = [dw(1), 0;  0, dw(2);  dw(2), dw(1)];
                    
                    dw = obj.nlShape(i, j).G*xb2;
                    C2 = [dw(1), 0;  0, dw(2);  dw(2), dw(1)];
                    
                    K1nm = K1nm + obj.nlShape(i, j).Ghat*Nm1*obj.nlShape(i, j).G;
                    K1bm = K1bm + obj.nlShape(i, j).Ghat*C1.'*obj.nlShape(i, j).DBm;
                    K2b = K2b + obj.nlShape(i, j).Ghat*C1.'*obj.elementProps.Dmembrane*C2*obj.nlShape(i, j).G;
                    
                end
            end
            
            K1e = [K1nm,  K1bm; K1bm.', zeros(8)];
            K2e = [K2b, zeros(4, 8); zeros(8, 4), zeros(8)];
            
            reorder = [obj.bi, obj.mi];
            
            K1 = zeros(24);
            K2 = zeros(24);
            
            K1(reorder, reorder) = K1e;
            K2(reorder, reorder) = K2e;
            
            %  transform Kne and Kte to global coordinates
            K1 = obj.transform*K1*obj.transform';
            K2 = obj.transform*K2*obj.transform';
            
            
        end
        
        % ----- Stress/Strain Generation Functions ---
        
        % ---- Additional Functions --- %
        function [Nm, C] = evalNL(obj, i, j, wm, wb)
            % Evaluate nonlinear matrix contribution at integration point.
            nm = obj.elementProps.Dmembrane*obj.nlShape(i, j).Bm*wm;
            Nm = [nm(1), nm(3); nm(3), nm(2)];
            
            dw = obj.nlShape(i, j).G*wb;
            C = [dw(1), 0;  0, dw(2);  dw(2), dw(1)];
            
        end
        
        function [obj] = BuildShapeNL(obj)
            % Precompute and store shape function quantities for evaluation
            % of nonlinear forces at each integration point.
            
            X = obj.localCoords(:, 1);
            Y = obj.localCoords(:, 2);
            
            
            for i = 1:length(obj.QUADpointsNL)
                xi = obj.QUADpointsNL(i);
                wi = obj.QUADweightsNL(i);
                for j = 1:length(obj.QUADpointsNL)
                    eta = obj.QUADpointsNL(j);
                    wj = obj.QUADweightsNL(j);
                    
                    % Write shape function derivatives
                    N1xi = -0.25*(1 - eta); N2xi = 0.25*(1 - eta); N3xi = 0.25*(1 + eta); N4xi = -0.25*(1 + eta);
                    N1eta = -0.25*(1 - xi); N2eta = -0.25*(1 + xi); N3eta = 0.25*(1 + xi); N4eta = 0.25*(1 - xi);
                    
                    J =  [N1xi,  N2xi,  N3xi,  N4xi;
                        N1eta, N2eta, N3eta, N4eta]*[X(:), Y(:)];
                    
                    detJ = J(1, 1)*J(2, 2) - J(2, 1)*J(1, 2);
                    
                    % Define "gamma" products
                    Gamma11 = J(2, 2)/detJ;
                    Gamma12 = -J(1, 2)/detJ;
                    Gamma21 = -J(2, 1)/detJ;
                    Gamma22 = J(1, 1)/detJ;
                    
                    % Write shape function derivatives wrt x
                    N1x = Gamma11*N1xi + Gamma12*N1eta; N1y = Gamma21*N1xi + Gamma22*N1eta;
                    N2x = Gamma11*N2xi + Gamma12*N2eta; N2y = Gamma21*N2xi + Gamma22*N2eta;
                    N3x = Gamma11*N3xi + Gamma12*N3eta; N3y = Gamma21*N3xi + Gamma22*N3eta;
                    N4x = Gamma11*N4xi + Gamma12*N4eta; N4y = Gamma21*N4xi + Gamma22*N4eta;
                    
                    % Write each submatrix
                    Bm1 = [N1x, 0;  0, N1y;  N1y, N1x];
                    Bm2 = [N2x, 0;  0, N2y;  N2y, N2x];
                    Bm3 = [N3x, 0;  0, N3y;  N3y, N3x];
                    Bm4 = [N4x, 0;  0, N4y;  N4y, N4x];
                    Bm = [Bm1, Bm2, Bm3, Bm4];
                    
                    G = [N1x, N2x, N3x, N4x;
                        N1y, N2y, N3y, N4y];
                    
                    obj.nlShape(i, j).Ghat = 0.5*wi*wj*G'*detJ;
                    obj.nlShape(i, j).G = G;
                    obj.nlShape(i, j).DBm = obj.elementProps.Dmembrane*Bm;
                    
                    
                end
            end

            % Write shape functions
            N1 = 0.25*(1 - xi)*(1 - eta); N2 = 0.25*(1 + xi)*(1 - eta);
            N3 = 0.25*(1 + xi)*(1 + eta); N4 = 0.25*(1 - xi)*(1 + eta);
            
            % Write shape function derivatives
            N1xi = -0.25*(1 - eta); N2xi = 0.25*(1 - eta); N3xi = 0.25*(1 + eta); N4xi = -0.25*(1 + eta);
            N1eta = -0.25*(1 - xi); N2eta = -0.25*(1 + xi); N3eta = 0.25*(1 + xi); N4eta = 0.25*(1 - xi);
            
            J =  [N1xi,  N2xi,  N3xi,  N4xi;
                N1eta, N2eta, N3eta, N4eta]*[X(:), Y(:)];
            
            detJ = J(1, 1)*J(2, 2) - J(2, 1)*J(1, 2);
            
            % Define "gamma" products
            Gamma11 = J(2, 2)/detJ;
            Gamma12 = -J(1, 2)/detJ;
            Gamma21 = -J(2, 1)/detJ;
            Gamma22 = J(1, 1)/detJ;
            
            % Write shape function derivatives wrt x
            N1x = Gamma11*N1xi + Gamma12*N1eta; N1y = Gamma21*N1xi + Gamma22*N1eta;
            N2x = Gamma11*N2xi + Gamma12*N2eta; N2y = Gamma21*N2xi + Gamma22*N2eta;
            N3x = Gamma11*N3xi + Gamma12*N3eta; N3y = Gamma21*N3xi + Gamma22*N3eta;
            N4x = Gamma11*N4xi + Gamma12*N4eta; N4y = Gamma21*N4xi + Gamma22*N4eta;
            
            % Write each B submatrix
            
            % Membrane
            Bm1 = [N1x, 0;  0, N1y;  N1y, N1x];
            Bm2 = [N2x, 0;  0, N2y;  N2y, N2x];
            Bm3 = [N3x, 0;  0, N3y;  N3y, N3x];
            Bm4 = [N4x, 0;  0, N4y;  N4y, N4x];
            
            % Bending
            Bb1 = [0, N1x, 0;  0, 0, N1y;  0, N1y, N1x];
            Bb2 = [0, N2x, 0;  0, 0, N2y;  0, N2y, N2x];
            Bb3 = [0, N3x, 0;  0, 0, N3y;  0, N3y, N3x];
            Bb4 = [0, N4x, 0;  0, 0, N4y;  0, N4y, N4x];
            
            % Return full B matrix
            Bm = [Bm1, Bm2, Bm3, Bm4];
            Bb = [Bb1, Bb2, Bb3, Bb4];
            
            % Shape functions
            Nm = [N1,  0, N2,  0, N3,  0, N4, 0;
                0, N1,  0, N2,  0, N3,  0, N4];
            
            Nb = [N1,  0,  0, N2,  0,  0, N3,  0,  0, N4,  0,  0;
                0, N1,  0,  0, N2,  0,  0, N3,  0,  0, N4,  0;
                0,  0, N1,  0,  0, N2,  0,  0, N3,  0,  0, N4];
            
        end
        
        function [detJ, G, Nm, C, Bm] = evalNLOld(obj, xi, eta, X, Y, wm, wb, Dmembrane)
            % Evaluate B membrane and bending matrices, along with shape functions themselves,
            % at the coordinate 'xi' and 'eta'
            
            % Write shape functions
            % N1 = 0.25*(1 - xi)*(1 - eta); N2 = 0.25*(1 + xi)*(1 - eta);
            % N3 = 0.25*(1 + xi)*(1 + eta); N4 = 0.25*(1 - xi)*(1 + eta);
            
            % Write shape function derivatives
            N1xi = -0.25*(1 - eta); N2xi = 0.25*(1 - eta); N3xi = 0.25*(1 + eta); N4xi = -0.25*(1 + eta);
            N1eta = -0.25*(1 - xi); N2eta = -0.25*(1 + xi); N3eta = 0.25*(1 + xi); N4eta = 0.25*(1 - xi);
            
            J =  [N1xi,  N2xi,  N3xi,  N4xi;
                N1eta, N2eta, N3eta, N4eta]*[X(:), Y(:)];
            
            detJ = J(1, 1)*J(2, 2) - J(2, 1)*J(1, 2);
            
            % Define "gamma" products
            Gamma11 = J(2, 2)/detJ;
            Gamma12 = -J(1, 2)/detJ;
            Gamma21 = -J(2, 1)/detJ;
            Gamma22 = J(1, 1)/detJ;
            
            % Write shape function derivatives wrt x
            N1x = Gamma11*N1xi + Gamma12*N1eta; N1y = Gamma21*N1xi + Gamma22*N1eta;
            N2x = Gamma11*N2xi + Gamma12*N2eta; N2y = Gamma21*N2xi + Gamma22*N2eta;
            N3x = Gamma11*N3xi + Gamma12*N3eta; N3y = Gamma21*N3xi + Gamma22*N3eta;
            N4x = Gamma11*N4xi + Gamma12*N4eta; N4y = Gamma21*N4xi + Gamma22*N4eta;
            
            % Write each submatrix
            Bm1 = [N1x, 0;  0, N1y;  N1y, N1x];
            Bm2 = [N2x, 0;  0, N2y;  N2y, N2x];
            Bm3 = [N3x, 0;  0, N3y;  N3y, N3x];
            Bm4 = [N4x, 0;  0, N4y;  N4y, N4x];
            Bm = [Bm1, Bm2, Bm3, Bm4];
            
            nm = Dmembrane*Bm*wm;
            Nm = [nm(1), nm(3); nm(3), nm(2)];
            
            G = [N1x, N2x, N3x, N4x;
                N1y, N2y, N3y, N4y];
            
            dw = G*wb;
            C = [dw(1), 0;  0, dw(2);  dw(2), dw(1)];
            
        end
        
        function [obj] = BuildTransform(obj)
            % This function builds the transformation matrix of the beam
            % element to rotate from local CS to global CS.
            %
            % Inputs :
            %           obj
            % Outputs :
            %           obj
            
            % Extract Offsets (removed for now)
            off1 = [0 0 0];
            off2 = [0 0 0];
            off3 = [0 0 0];
            off4 = [0 0 0];
            
            % Extract Nodes
            c1 = obj.nodes{1}.GetXYZ()'+off1; % 1st Nodal Coordinate
            c2 = obj.nodes{2}.GetXYZ()'+off2; % 2nd Nodal Coordinate
            c3 = obj.nodes{3}.GetXYZ()'+off3; % 3rd Nodal Coordinate
            c4 = obj.nodes{4}.GetXYZ()'+off4; % 4th Nodal Coordinate
            c = [c1; c2; c3; c4]; % All nodal cooridinates
            
            % Compute Unit Vectors (e1 & e2)
            e1 = (c2 - c1)/norm(c2-c1);
            e2 = (c4 - c1)/norm(c4-c1);
            e3 = cross(e1,e2);
            
            % Compute Unit Vector (e13 & e42)
            e13 = (c3-c1)/norm(c3-c1);
            e24 = (c2-c4)/norm(c2-c4);
            
            % Generate Element Local Coordinate System Unit Vectors
            % ** cvd - need to check this **
            obj.Te = zeros(3);
            obj.Te(1,:) = e13 + e24; % Add together ?? why ??
            obj.Te(3,:) = Element.cross2(obj.Te(1,:),e13);
            obj.Te(2,:) = Element.cross2(obj.Te(3,:),obj.Te(1,:));
            
            % Normalize Matrix
            obj.Te(1,:) = obj.Te(1,:)/norm(obj.Te(1,:));
            obj.Te(2,:) = obj.Te(2,:)/norm(obj.Te(2,:));
            obj.Te(3,:) = obj.Te(3,:)/norm(obj.Te(3,:));
            
            % Assemble Transformation Matrix
            T = zeros(24);
            T(1:3,1:3) = obj.Te';
            T(4:6,4:6) = obj.Te';
            T(7:9,7:9) = obj.Te';
            T(10:12,10:12) = obj.Te';
            T(13:15,13:15) = obj.Te';
            T(16:18,16:18) = obj.Te';
            T(19:21,19:21) = obj.Te';
            T(22:24,22:24) = obj.Te';
            
            % Store Transformation
            obj.transform = T;
            %             keyboard
            
            % Build Local Coordinates
            % Compute node point coords in element coordinate system
            % ** cvd not confident about this **
            norg = sum(c)/4;
            u = [c(:,1)'-norg(1);c(:,2)'-norg(2);c(:,3)'-norg(3)]';
            obj.localCoords = u*obj.Te(1:2,:)';
            
        end
        
        function [obj] = BuildSection(obj)
            % This function computes the derived sectional properties of a
            % beam from a given cross-section shape and parameters.
            %
            % Inputs  :
            %           obj
            % Outputs :
            %
            %
            
            % Assign Material if not specified
            E = obj.material.getProp('E');
            nu = obj.material.getProp('nu');
            G =  E/(2*(1 + nu));
            
            obj.material.setProp('G', G);
            
            % Form constituitive matrices
            obj.elementProps.Dmembrane = E*obj.section.t/(1 - nu^2)*[1, nu, 0;
                nu,  1, 0;
                0,  0, 0.5*(1 - nu)];
            obj.elementProps.Dbending = E*obj.section.t^3/(12*(1 - nu^2))*[1, nu, 0;
                nu,  1, 0;
                0,  0, 0.5*(1 - nu)];
            obj.elementProps.Dshear = 5/6*G*obj.section.t*[1, 0; 0, 1];
            
            % Compute x and y lengths
            dx = .5*(obj.localCoords(2,1)-obj.localCoords(1,1)+obj.localCoords(3,1)-obj.localCoords(4,1));
            dy = .5*(obj.localCoords(4,2)-obj.localCoords(1,2)+obj.localCoords(3,2)-obj.localCoords(2,2));
            
            % Compute normal
            side1 = obj.nodes{1}.GetXYZ()' - obj.nodes{2}.GetXYZ()';
            side2 = obj.nodes{3}.GetXYZ()' - obj.nodes{2}.GetXYZ()';
            normal = cross(side2, side1);
            obj.elementProps.normal = normal/norm(normal);
            
            % Store Properties
            obj.elementProps.vol = dx*dy*obj.section.t;
            obj.elementProps.alpha = 0.01;
            
        end
        
        function [detJ, Bm, Nm, b0] = evalMembrane(obj, xi, eta, X, Y)
            % Evaluate B membrane matrices, along with the shape functions
            % themselves, at the coordinate 'xi' and 'eta'
            
            % Note that X and Y must have 8 coordinates each, rather than
            % 4, due to the fictituous midside nodes.
            
            % Write shape functions
            N5 = 0.5*(1 - xi^2)*(1 - eta); N6 = 0.5*(1 + xi)*(1 - eta^2);
            N7 = 0.5*(1 - xi^2)*(1 + eta); N8 = 0.5*(1 - xi)*(1 - eta^2);
            
            N1 = 0.25*(1 - xi)*(1 - eta);
            N2 = 0.25*(1 + xi)*(1 - eta);
            N3 = 0.25*(1 + xi)*(1 + eta);
            N4 = 0.25*(1 - xi)*(1 + eta);
            
            N = [N1; N2; N3; N4; N5; N6; N7; N8];
            
            % Write shape function derivatives
            N5xi = -xi*(1 - eta); N6xi = 0.5*(1 - eta^2); N7xi = -xi*(1 + eta); N8xi = -0.5*(1 - eta^2);
            N5eta = -0.5*(1 - xi^2); N6eta = -eta*(1 + xi); N7eta = 0.5*(1 - xi^2); N8eta = -eta*(1 - xi);
            
            N1xi = -0.25*(1 - eta); N2xi = 0.25*(1 - eta); N3xi = 0.25*(1 + eta); N4xi = -0.25*(1 + eta);
            N1eta = -0.25*(1 - xi); N2eta = -0.25*(1 + xi); N3eta = 0.25*(1 + xi); N4eta = 0.25*(1 - xi);
            
            J =  [N1xi,  N2xi,  N3xi,  N4xi;
                N1eta, N2eta, N3eta, N4eta]*[X(1:4), Y(1:4)];
            
            detJ = J(1, 1)*J(2, 2) - J(2, 1)*J(1, 2);

            % Define "gamma" products
            Gamma11 = J(2, 2)/detJ;
            Gamma12 = -J(1, 2)/detJ;
            Gamma21 = -J(2, 1)/detJ;
            Gamma22 = J(1, 1)/detJ;
            
            % Write shape function derivatives wrt x
            % A little confusing: The Jacobian does not use all 8 shape
            % functions (the "hierarchical" functions are omitted). But
            % their derivatives are still calculated below.
            N1x = Gamma11*N1xi + Gamma12*N1eta; N1y = Gamma21*N1xi + Gamma22*N1eta;
            N2x = Gamma11*N2xi + Gamma12*N2eta; N2y = Gamma21*N2xi + Gamma22*N2eta;
            N3x = Gamma11*N3xi + Gamma12*N3eta; N3y = Gamma21*N3xi + Gamma22*N3eta;
            N4x = Gamma11*N4xi + Gamma12*N4eta; N4y = Gamma21*N4xi + Gamma22*N4eta;
            N5x = Gamma11*N5xi + Gamma12*N5eta; N5y = Gamma21*N5xi + Gamma22*N5eta;
            N6x = Gamma11*N6xi + Gamma12*N6eta; N6y = Gamma21*N6xi + Gamma22*N6eta;
            N7x = Gamma11*N7xi + Gamma12*N7eta; N7y = Gamma21*N7xi + Gamma22*N7eta;
            N8x = Gamma11*N8xi + Gamma12*N8eta; N8y = Gamma21*N8xi + Gamma22*N8eta;
            
            Nix = [N1x; N2x; N3x; N4x; N5x; N6x; N7x; N8x];
            Niy = [N1y; N2y; N3y; N4y; N5y; N6y; N7y; N8y];
            
            % Write Allman's incompatible shape functions and their
            % derivatives. Start with the index definitions.
            I = 1:4; M = I + 4; L = M - 1 + 4*floor(1./I);
            K = mod(M, 4) + 1; J = L - 4;
            
            % Allmans' incompatible functions
            %             Nx = zeros(4, 1); Ny = zeros(4, 1);
            Nxx = zeros(4, 1); Nxy = zeros(4, 1);
            Nyx = zeros(4, 1); Nyy = zeros(4, 1);
            
            % Also construct the third b0 entry here
            b03 = zeros(1, 4);
            Nu = zeros(1, 4);
            Nv = zeros(1, 4);
            
            for i = 1:4
                j = J(i); k = K(i); l = L(i); m = M(i);
                
                % Orient correctly so that the normal points outward from
                % each edge. 
                dx = X(i) - X(j); dy = Y(i) - Y(j);
                nj = cross([dx, dy, 0], [0, 0, 1]);
                nxij = nj(1);
                nyij = nj(2);
                
                dx = X(k) - X(i); dy = Y(k) - Y(i);
                nk = cross([dx, dy, 0], [0, 0, 1]);
                nxik = nk(1);
                nyik = nk(2);
                
                % Shape functions
                Nu(i) = 0.125*(nxij*N(l) - nxik*N(m));
                Nv(i) = 0.125*(nyij*N(l) - nyik*N(m));
                
                % Derivatives
                Nxx(i) = 0.125*(nxij*Nix(l) - nxik*Nix(m));
                Nxy(i) = 0.125*(nxij*Niy(l) - nxik*Niy(m));
                Nyx(i) = 0.125*(nyij*Nix(l) - nyik*Nix(m));
                Nyy(i) = 0.125*(nyij*Niy(l) - nyik*Niy(m));
                
                % Penalty matrix entry
                b03(i) = 1/16*(-nxij*Niy(l) + nxik*Niy(m) + nyij*Nix(l) - nyik*Nix(m)) - N(i);
            end
            
            
            % Write each B submatrix
            Bm1 = [N1x, 0, Nxx(1);  0, N1y, Nyy(1);  N1y, N1x, Nxy(1) + Nyx(1)];
            Bm2 = [N2x, 0, Nxx(2);  0, N2y, Nyy(2);  N2y, N2x, Nxy(2) + Nyx(2)];
            Bm3 = [N3x, 0, Nxx(3);  0, N3y, Nyy(3);  N3y, N3x, Nxy(3) + Nyx(3)];
            Bm4 = [N4x, 0, Nxx(4);  0, N4y, Nyy(4);  N4y, N4x, Nxy(4) + Nyx(4)];
            
            % Return full B matrix
            Bm = [Bm1, Bm2, Bm3, Bm4];
            
            % Construct b0 penalty vector
            b0 = [0.5*[-Niy(1), -Niy(2), -Niy(3), -Niy(4);
                Nix(1),  Nix(2)   Nix(3),  Nix(4)];
                b03];
            b0 = b0(:)';
            
            % Shape functions require some additional thought.
            Nm = [N1,  0,   0, N2,  0,   0, N3,  0,   0, N4, 0,  0;
                   0,  N1,  0,  0, N2,   0,  0, N3,   0, 0, N4,  0;
                   0,   0, N1,  0,  0, N2,   0,  0, N3,  0,  0, N4];
             
        end
        
        function [detJ, Bb, Nb] = evalBending(obj, xi, eta, X, Y)
            % Evaluate B membrane and bending matrices, along with shape functions themselves,
            % at the coordinate 'xi' and 'eta'
            % Inputs :
            %
            % Outputs :
            %
            %
            
            % Write shape functions
            N1 = 0.25*(1 - xi)*(1 - eta); N2 = 0.25*(1 + xi)*(1 - eta);
            N3 = 0.25*(1 + xi)*(1 + eta); N4 = 0.25*(1 - xi)*(1 + eta);
            
            % Write shape function derivatives
            N1xi = -0.25*(1 - eta); N2xi = 0.25*(1 - eta); N3xi = 0.25*(1 + eta); N4xi = -0.25*(1 + eta);
            N1eta = -0.25*(1 - xi); N2eta = -0.25*(1 + xi); N3eta = 0.25*(1 + xi); N4eta = 0.25*(1 - xi);
%             % Bathe shape functions
%             N1 = 0.25*(1 + xi)*(1 + eta); N2 = 0.25*(1 - xi)*(1 + eta);
%             N3 = 0.25*(1 - xi)*(1 - eta); N4 = 0.25*(1 + xi)*(1 - eta);
%             
%             % Write shape function derivatives
%             N1xi = 0.25*(1 + eta); N2xi = -0.25*(1 + eta); N3xi = -0.25*(1 - eta); N4xi = 0.25*(1 - eta);
%             N1eta = 0.25*(1 + xi); N2eta = 0.25*(1 - xi); N3eta = -0.25*(1 - xi); N4eta = -0.25*(1 + xi);

            J =  [N1xi,  N2xi,  N3xi,  N4xi;
                N1eta, N2eta, N3eta, N4eta]*[X(:), Y(:)];
            
            detJ = J(1, 1)*J(2, 2) - J(2, 1)*J(1, 2);
            
            % Define "gamma" products
            Gamma11 = J(2, 2)/detJ;
            Gamma12 = -J(1, 2)/detJ;
            Gamma21 = -J(2, 1)/detJ;
            Gamma22 = J(1, 1)/detJ;
            
            % Write shape function derivatives wrt x
            N1x = Gamma11*N1xi + Gamma12*N1eta; N1y = Gamma21*N1xi + Gamma22*N1eta;
            N2x = Gamma11*N2xi + Gamma12*N2eta; N2y = Gamma21*N2xi + Gamma22*N2eta;
            N3x = Gamma11*N3xi + Gamma12*N3eta; N3y = Gamma21*N3xi + Gamma22*N3eta;
            N4x = Gamma11*N4xi + Gamma12*N4eta; N4y = Gamma21*N4xi + Gamma22*N4eta;
            
            % Write each B submatrix
            % Bending
%             Bb1 = [0, N1x, 0;  0, 0, N1y;  0, N1y, N1x];
%             Bb2 = [0, N2x, 0;  0, 0, N2y;  0, N2y, N2x];
%             Bb3 = [0, N3x, 0;  0, 0, N3y;  0, N3y, N3x];
%             Bb4 = [0, N4x, 0;  0, 0, N4y;  0, N4y, N4x];
            Bb1 = [0, 0, -N1x;  0, N1y, 0;  0, N1x, -N1y];
            Bb2 = [0, 0, -N2x;  0, N2y, 0;  0, N2x, -N2y];
            Bb3 = [0, 0, -N3x;  0, N3y, 0;  0, N3x, -N3y];
            Bb4 = [0, 0, -N4x;  0, N4y, 0;  0, N4x, -N4y];
            
            % Return full B matrix
            Bb = [Bb1, Bb2, Bb3, Bb4];
            
            % Shape functions
            Nb = [N1,  0,  0, N2,  0,  0, N3,  0,  0, N4,  0,  0;
                0, N1,  0,  0, N2,  0,  0, N3,  0,  0, N4,  0;
                0,  0, N1,  0,  0, N2,  0,  0, N3,  0,  0, N4];
        end
        
      
        
        
        function [detJ, Bs] = evalS(obj, xi, eta, X, Y)
            % Evaluate B shear matrix at the coordinate 'xi' and 'eta'
            
            % Write shape functions
%             N1 = 0.25*(1 - xi)*(1 - eta); N2 = 0.25*(1 + xi)*(1 - eta);
%             N3 = 0.25*(1 + xi)*(1 + eta); N4 = 0.25*(1 - xi)*(1 + eta);
            
            % Write shape function derivatives
            N1xi = -0.25*(1 - eta); N2xi = 0.25*(1 - eta); N3xi = 0.25*(1 + eta); N4xi = -0.25*(1 + eta);
            N1eta = -0.25*(1 - xi); N2eta = -0.25*(1 + xi); N3eta = 0.25*(1 + xi); N4eta = 0.25*(1 - xi);
%             Nixi = [N1xi, N2xi, N3xi, N4xi];
%             Nieta = [N1eta, N2eta, N3eta, N4eta];
            
            J =  [N1xi,  N2xi,  N3xi,  N4xi;
                N1eta, N2eta, N3eta, N4eta]*[X(:), Y(:)];
            
            detJ = J(1, 1)*J(2, 2) - J(2, 1)*J(1, 2);
            
            % Define inverse Jacobian
%             Jinv = [J(2, 2), -J(1, 2);
%                    -J(2, 1), J(1, 1)]*(1/detJ);
%             
            % Write supporting terms; evaluate the Jacobian at each
            % midpoint (?) (Trying to match eqs. 21-22 in [1]).
            x14 = X(1) - X(4); x12 = X(1) - X(2); x23 = X(2) - X(3); x43 = X(4) - X(3);
            y12 = Y(1) - Y(2); y14 = Y(1) - Y(4); y23 = Y(2) - Y(3); y43 = Y(4) - Y(3);
            
            Ax = [1, -1, -1, 1]*X; Bx = [1, -1, 1, -1]*X; Cx = [1, 1, -1, -1]*X;
            Ay = [1, -1, -1, 1]*Y; By = [1, -1, 1, -1]*Y; Cy = [1, 1, -1, -1]*Y;
            
            coeffRz = sqrt((Cx + xi*Bx)^2 + (Cy + xi*By)^2);
            coeffSz  = sqrt((Ax + eta*Bx)^2 + (Ay + eta*By)^2);
            
            sp1 = coeffRz*(eta + 1); sm1 = coeffRz*(1 - eta);
            rp1 = coeffSz*(xi + 1); rm1 = coeffSz*(1 - xi);
            
            Bs = 1/(32*detJ)*[2*sp1, -y12*sp1, x12*sp1, -2*sp1, -y12*sp1, x12*sp1, ...
                             -2*sm1, -y43*sm1, x43*sm1,  2*sm1, -y43*sm1, x43*sm1; ...
                              2*rp1, -y14*rp1, x14*rp1,  2*rm1, -y23*rm1, x23*rm1, ...
                             -2*rm1, -y23*rm1, x23*rm1, -2*rp1, -y14*rp1, x14*rp1];
            
%             
        end
        
        
        
    end
  
    
end

