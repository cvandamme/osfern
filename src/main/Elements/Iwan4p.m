classdef Iwan4p < Element
    % S4 shell element with selectively reduced integration for the shear
    % stiffness terms. 
    
    properties (GetAccess = 'private', SetAccess = 'private')
        
        % Derived Section Properties
        sectionProps
        % A, Iyy, Izz, J, Ky, Kz
        elementProps
        % L, X, S
        % Y[y1, y2, y3, y4] , PHIY
        % Z[z1, z2, z3, z4] , PHIZ
        Te
        normal
        nlShape = struct('G', [], 'DBm', [], 'Ghat', []);
    end
    
    % --- Constant Properties Cannot Be Changed
    properties (Constant)
        
        nNode = 4;     % Number of nodes in this element
        activeDof = 6;  % Number of active DOF per node for this element
        nDof = 24;
        irot = [6, 12, 18, 24];  % Rotation indices
        mi = [1, 2, 7, 8, 13, 14, 19, 20]; % Membrane indices
        bsi = [3, 4, 5, 9, 10, 11, 15, 16, 17, 21, 22, 23]; % Bending/shear indices
        bi = [3, 9, 15, 21];
        QUADpointsBM = [-1/sqrt(3), 1/sqrt(3)]; % Quadrature Points; bending/membrane
        QUADweightsBM = [1, 1]; % Quadrature Weights; bending/membrane
        %         QUADpointsS = [-1/sqrt(3), 1/sqrt(3)]; % Quadrature Points; bending/membrane
        %         QUADweightsS = [1, 1]; % Quadrature Weights; bending/membrane
        blendFactor = 1; % Blending ratio of 1x1 shear quadrature to 2x2 shear quadrature
        % Used to avoid spurious rigid body modes in a flat plate mesh.
        %         blendFactor = 0;
        QUADpointsS = 0; % Quadrature Points; shear
        QUADweightsS = 2; % Quadrature Weights; shear
        
        QUADpointsNL = [-sqrt(0.6), 0, sqrt(0.6)];
        QUADweightsNL = [5/9, 8/9, 5/9];
        
        
    end
    
    methods
        % ----- Initialization ----- %
        function obj = Iwan4p(id)
            
            obj = obj@Element(id);
            
        end 
        
        function obj = Build(beta,chi,Kt,w)
            
        end
        
        
    end
    
end