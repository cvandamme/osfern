classdef Spring2 < Element & handle
    % This class represents a 3D spring element in which there are discrete
    % stiffness terms for each dof (3 disp. 3 rot)
    
    properties
        sLength % spring original length 
        kLin  % Linear stiffness [kx,ky,kx,krx,kry,kz]
        kNl   % Nonlinear stiffness curve
        cLin  % Linear damping 
        cNl   % Nonlinear damping
        massVal % discete mass added at nodal points
    end
    
    properties (Constant)
        nDof = 12;
        nNode = 2;
        activeDof = 6;
    end
    
    methods
        % Constructor function
        function [obj] = Spring2()
            obj = @Element;
        end
        
        % Build the element
        function [obj] = build(obj,nodeObjs,kLinIn,cLinIn)
            
            % Save nodes
            if size(nodeObj) ~= 2
                error('Must Provide Two Nodes')
            end
            obj.nodes = nodeObj;
            
            % Check inputs 
            if length(kLinIn) == 6
                obj.kLin = kLinIn;
            else
                error('Currently must supply entire stiffness')
            end
            
            % Check inputs
            if length(cLinIn) == 6 || isempty(cLinIn)
                obj.cLin = cLinIn;
            else
                error('Currently must supply entire damping')
            end
           
        end
        
        % State Functions 
        function [kOut] = buildStiff(obj,x)
            
            % For stiffness simply added to the node
            kOut = zeros(6);
            kOut(1:6,1:6) = diag(obj.kLin);
            kOut(1,7) = -obj.kLin(1);
            kOut(7,1) = kOut(1,7);
            kOut(2,8) = -obj.kLin(2);
            kOut(8,2) = kOut(2,8);
            kOut(3,9) = -obj.kLin(3);
            kOut(9,3) = kOut(3,9);
            kOut(4,10) = -obj.kLin(4);
            kOut(10,4) = kOut(4,10);
            kOut(5,11) = -obj.kLin(5);
            kOut(11,5) = kOut(5,11);
            kOut(6,12) = -obj.kLin(6);
            kOut(12,6) = kOut(12,6);
            
            kOut = kOut(:);
        end
        
        function [mOut] = buildMass(obj)
            if isempty(obj.massVal)
                mOut = zeros(12);
            elseif length(obj.massVal) == 6
                mOut = blkdiag(obj.massVal,obj.massVal);
            elseif length(obj.massVal) == 1
                mMat = [repmat(obj.massVal,3,1) zeros(3,1)];
                mOut = blkdiag(mMat,mMat);
            end
        end
        
        % 
        function [kNlOut] = buildNlStiffness(obj,x)
            
            % Initialize
            kNlOut = zeros(size(obj.kLin));
            % Check if nonlinear stiffness was provided
            if ~isempty(obj.kNl)
                for ii = 1:length(obj.kNl)
                    % Interpoloate value
                    
                end
            end
                
        end
        function [cNlOut] = buildNlDamping(obj,v)
            
        end
        
        % Set Functions
        function [obj] = SetLinearStiffness(obj,kLinIn,varargin)
        end
        function [obj] = SetNonlinearStiffness(obj,kNlFun,varargin)
        end
        function [obj] = SetLinearDamping(obj,cLinIn,varargin)
        end
        function [obj] = SetNonlinearDamping(obj,cNlFun,varargin)
        end
        
        % Get Functions
        function [kLinOut] = GetLinearStiffness(obj,varargin)
        end
        function [kNlOut] = GetNonlinearStiffness(obj,varargin)
        end
        function [cLinOut] = GetLinearDamping(obj,varargin)
        end
        function [cNlOut] = GetNonlinearDamping(obj,varargin)
        end
        
    end
    

end

