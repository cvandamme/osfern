classdef Buckling < DirectStep
    
    properties
        
        lambda
        phi
        
    end
    
    methods
        
        function obj = Buckling(name, osFern, initialDisplacement)
            
            obj = obj@DirectStep(name, osFern);
            
            if nargin == 3
                obj.initialDisplacement = initialDisplacement;
            end
            
        end
        
        function obj = Solve(obj, nModes)
            
            % Extract linear stiffness matrix, with fixed DOF already
            % removed.
            K = obj.osFern.GetStiffnessMatrix();
            
            % Assemble force
            fExt = obj.AssembleForce(0);
            
            % Initialize displacement
            x0 = zeros(obj.osFern.nDof,1);
            
            % Solve 
            x0(obj.osFern.freeDof) = K\fExt;
            
            % Compute Stresses
            [S] = obj.osFern.ComputeStress(x0);
            
            % Compute Stress Stiffness Matrix
            [Kss] = obj.osFern.GetStressStiffness(S);
            
            % Eigenvalue problem
            [psi, lam] = eigs(K,-Kss, nModes, 'SM');
            
            % Stiffness Normalize Modes
            for i = 1:nModes
                psi(:, i) = psi(:, i)/sqrt(psi(:, i)'*K*psi(:, i));
            end
            
            % Save
            [obj.lambda, I] = sort(diag(lam));
            obj.phi = zeros(obj.osFern.nDof, nModes);
            obj.phi(obj.osFern.freeDof, :) = psi(:, I);
            
        end
        
    end
    
end