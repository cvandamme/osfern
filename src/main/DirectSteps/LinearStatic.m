

classdef LinearStatic < DirectStep
    
    properties
        
        response;
        
    end
 
    methods
        
        function obj = LinearStatic(name, osFern, initialDisplacement)
            
            obj = obj@DirectStep(name, osFern);
            obj.finalDisplacement = zeros(obj.osFern.nDof, 1);
            
            if nargin == 3
                obj.initialDisplacement = initialDisplacement;
            end
            
        end
        
        function obj = Solve(obj)
            
            % Obtain stiffness matrix and force vector
            K = obj.osFern.GetStiffnessMatrix();
            F = obj.AssembleForce([]);
            
            % Add nonlinear tangent stiffness if needed
            if ~isempty(obj.initialDisplacement)
                [~, Kt] = obj.osFern.GetTangentStiffness(obj.initialDisplacement);
                K = K + Kt;
            end
            
            % Solve
            obj.finalDisplacement(obj.osFern.freeDof) = K\F;
            
            % Obtain response DOF
            responseDof = [];
            for i = 1:length(obj.monitor)
                responseDof = unique([responseDof, obj.monitor(i).dof]);
            end
            
            obj.response = obj.finalDisplacement(responseDof);
            
        end
        
        function U = SolveMultiple(obj,forces)
            
            % Identify number of load cases
            nLoadCases = size(forces,2);
            freeDof = obj.osFern.freeDof; 
            
            % Check size of forces provided
            if size(forces,1) == sum(obj.osFern.freeDof)
                disp('Forces provided are consistnent with free dof')
                U = zeros(size(forces));
                loadCases = forces(obj.osFern.freeDof,:);
            elseif size(forces,1) == obj.osFern.nDof
               disp('Forces provided are consistent with total dof')
               disp('Reducing down to free dof only during solution')
               disp('Will expand back to full dof when done')
               
               loadCases = forces(obj.osFern.freeDof,:);
               U = zeros(obj.osFern.nDof,nLoadCases);
            else
               error('Forces provided are inconsistnent with system size')
            end
            
            % initialzie solution cell array
            uPar = cell(nLoadCases,1);

            % Obtain stiffness matrix and force vector
            K = obj.osFern.GetStiffnessMatrix();
            F = obj.AssembleForce([]);
            
            % Add nonlinear tangent stiffness if needed
            if ~isempty(obj.initialDisplacement)
                [~, Kt] = obj.osFern.GetTangentStiffness(obj.initialDisplacement);
                K = K + Kt;
            end
            
            % Factorize matrix for quicker solution
            [kL,kU,kP] = lu(K);
           
            % Solve - either serial or paralle
            if obj.parallelOpts.logic
               parfor (jj = 1:nLoadCases,obj.parallelOpts.numWorkers)
                   % Output solution number
                    disp(['Solving Load Case : ',num2str(jj)]);
                    
                    % Grab the specifc solutoin
                    F = loadCases(:,jj);

                    % Solve the system of linear equations
                    uPar{jj} = kU\(kL\F);
                                        
               end
            else
                for jj = 1:nLoadCases
                    % Output solution number
                    disp(['Solving Load Case : ',num2str(jj)]);

                    % Solve the system of linear equations
                    uPar{jj} = kU\(kL\F);
                end
            end
            
            % Store in output array
            for jj = 1:nLoadCases; U(freeDof,jj) = uPar{jj}; end
           
            
        end
        % Disable "addEnforced" method
        function addEnforced(obj, varargin)
            error('Cannot add enforced displacement to linear static step.');
        end
        
    end
    
end