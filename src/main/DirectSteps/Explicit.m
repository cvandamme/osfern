classdef Explicit < Dynamic & handle
    % This class contains explict integration procedure for finite element
    % analysis. % It currently only works for linear and geometrically
    % nonlinear systems. 
    
    properties
        
        % Time step structure containing max frequency, timestep and
        % courant number
        timeStep = struct('wMax',0,'dtMax',0,'C',0.998,'setMax',false);
    end
    
    methods
        % Constructor
        function obj = Explicit(name, osFern, initialDisplacement, initialVelocity)
            
            obj = obj@Dynamic(name, osFern);
            obj.finalDisplacement = zeros(obj.osFern.nDof, 1);
            obj.finalVelocity = zeros(obj.osFern.nDof, 1);
            
            if nargin > 2
                obj.initialDisplacement = initialDisplacement;
            end
            
            if nargin > 3
                obj.initialVelocity = initialVelocity;
            end            
            
            % Check the timestep of the model
            obj = obj.EstimateTimeStep();
            
        end
        
        % Time Step Calculator
        function obj = EstimateTimeStep(obj)
            
            % Initialize vairables
            wMax = 0; dtMax = 1e6;
            % Loop through each element and find smallest characteristic
            % length
            for ii = 1:length(obj.osFern.element)
                tTemp = obj.osFern.element{ii}.GetTimeStep();
                if tTemp < dtMax
                    dtMax = tTemp;
                end
            end
            
            % Store in structure
            obj.timeStep.wMax = 2/dtMax;
            obj.timeStep.dtMax = dtMax;
        end
        
        % Set Courrant Number Desired
        function obj = SetCourrantNumber(obj,C)
            
            if C >= 1
                error('Courrant # should not be greater than 1')
            end
            obj.timeStep.C = C;
        end
        
        % Set Max Time Step Number Desired
        function obj = SetMaxTimeStep(obj,dtMax)
            
            if dtMax >= 2/obj.wMax
                error(['Provided a max time step larger than ',...
                       'the stable limit'])
            end
            obj.timeStep.C = C;
        end
        
        % Integrator
        function obj = Solve(obj,T)
            % This explicit solver is based on backward euler
            % more documentation to come
            
            
            % Get timestep from structure
            if obj.timeStep.setMax
               dt0 = obj.timeStep.dtMax;
            else
               dt0 = obj.timeStep.C*(2/obj.timeStep.wMax); 
            end
            
            % Estimate even stepsize
            nSteps = ceil(T/dt0); % creates a slightly smaller timestep
            dt = T/nSteps; idt = 1/dt;
            dt2 = dt^2; idt2 = 1/dt^2;
            t = 0:dt:T; obj.t = t;
            
            % Generate Diagonal Mass Matrix and invert
            M = obj.osFern.GetDiagonalMassMatrix();
            iM = diag(diag(M).^-1); iM(isnan(iM))=0;iM(isinf(iM))=0;
            
            % Generate individual element linear stiffness and indices 
            kEle = cell(length(obj.osFern.element),1); 
            dofEle = cell(length(obj.osFern.element),1);
            for jj = 1:length(obj.osFern.element)
                kEle{jj} = reshape(obj.osFern.element{jj}.BuildStiff([]),...
                    obj.osFern.element{jj}.nDof,obj.osFern.element{jj}.nDof);
                dofEle{jj} =obj.osFern.element{jj}.dofVec;
            end
            
            % Shorthand for initial velocity and displacement
            if isempty(obj.initialDisplacement)
                x0 = zeros(obj.osFern.nDof, 1); 
            else
                x0 = obj.initialDisplacement;
            end
            
            if isempty(obj.initialVelocity)
                v0 = zeros(obj.osFern.nDof, 1); 
            else
                v0 = obj.initialVelocity;
            end
            a0 = zeros(obj.osFern.nDof, 1); 
            
            % Obtain response DOF
            responseDof = [];
            for i = 1:length(obj.monitor)
                responseDof = unique([responseDof, obj.monitor(i).dof]);
            end
            
            % Get Free Dof Structure
            freeDof = obj.osFern.freeDof;
            nDof = length(freeDof); nFreeDof = sum(freeDof);
            cDof = logical(bitxor(freeDof,ones(size(freeDof))));
            
            % Assemble force
            fExt = obj.AssembleForce(0);
            
            % compute the linear static response at the first load step
            obj.solution.displacement = zeros(obj.osFern.nDof, length(t));
            obj.solution.velocity = zeros(obj.osFern.nDof, length(t));
            obj.solution.acceleration = zeros(obj.osFern.nDof, length(t));
            
            % Initialize displacement 
            obj.solution.displacement(:, 1) = x0;
            
            % Solve for initial acceleration and half velocities
            a0 = iM*fExt; vCn2 = -0.5*dt*a0;
            
            % Need to 
            obj.solution.velocity(:, 1) = v0;

            % Setup plotting if desired
            if obj.plotOpts.logic
               obj = obj.InitializePlot(x0);
            end
            
            % Loop through the timesteps (fixed size)
            for ii = 1:length(t)
                
                % This reinitilization may be costly 
                fInt = zeros(nDof,1); dInt = zeros(nDof,1);
                
                % Enforce/Constrain certain deformations
%                 x0(cDof) = 0;
                
                % Assemble force
                fExt = obj.AssembleForce(t(ii));
                
                % Loop through the elements to evaluate internal forces
                for jj = 1:length(obj.osFern.element)
                    
                    % Assemble internal force
                    fInt(dofEle{jj}) = fInt(dofEle{jj}) + ...   % previous
                                kEle{jj}*x0(dofEle{jj}) + ...   % linear 
                                obj.osFern.element{jj}.GetFintNl(x0(dofEle{jj}));    % nonlinear 
                            
                    % Assemble damping force (not implemented currently)
                    % will most likely be element proportional damping
%                     dInt(nid) = dInt(nid) + ...
%                                 GetDampForce(obj,ii,vCn2(nid));        
                end
                
                % Integrate (only free dof)
                xC = dt2*iM*(fExt- fInt(freeDof)-dInt(freeDof)) + ...
                     x0(freeDof) + dt*vCn2;
                
                % Half time variables
                vCn2 = idt*(xC-x0(freeDof)); vCp2 = idt*(xC-x0(freeDof)); % velocity
                aCn2 = idt*(xC-x0(freeDof)); aCp2 = idt*(xC-x0(freeDof)); % acceleration
                
                % Store data
                obj.solution.velocity(freeDof,ii) = vCn2;
                obj.solution.displacement(freeDof,ii) = xC;
                obj.solution.acceleration(freeDof,ii) = aCn2;
                
                % Update displacement
                x0 = zeros(nDof,1); x0(freeDof) = xC;
                
                % Output message
                if obj.printOpts.logic && mod(ii,obj.printOpts.frameRate) == 0
                    fprintf('Increments  %i of %i at time : %d \n', ii, nSteps, t(ii));
                end
                
                % Plot if desired
                if obj.plotOpts.logic && mod(ii,obj.plotOpts.frameRate) == 0
                   obj = UpdatePlot(obj,obj.solution.acceleration(:,ii),ii);
                end

            end
        end
    end
    
end

