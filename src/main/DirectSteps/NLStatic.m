

classdef NLStatic < DirectStep
    
    properties
        
        response;
        
    end
 
    methods
        
        % Constructor
        function obj = NLStatic(name, osFern, initialDisplacement)
            
            obj = obj@DirectStep(name, osFern);
            obj.finalDisplacement = zeros(obj.osFern.nDof, 1);
            
            if nargin == 3
                obj.initialDisplacement = initialDisplacement;
            end
            
        end

        % Solve the nonlinear static problem
        function obj = Solve(obj, nStep)
            % Solve the nonlinear static problem specified in the
            % "nlStatic" object over nStep increments.  
            %
            % nlStatic.Solve(nStep)
            %
            % The force to apply is defined within the nlStatic object, as
            % well as the model.  For example, see:
            %   nlStatic.force.Force
            %
            % Key outputs of interest include:
            %   nlStatic.finalDisplacement
            %       - Deformation at the last load increment
            %   nlStatic.solution.solution
            %       - Deformation of FE model at each load increment
            %                                     
            %
            % CVD: This solver is based on the new assembler...
            
            % Obtain stiffness matrix and force vector
            K = obj.osFern.GetStiffnessMatrix();
            F = obj.AssembleForce([]);
            
            % Solve
            steps = linspace(0, 1, nStep);
            
            % compute the linear static response at the first load step
            xold = zeros(obj.osFern.nDof, 1);
            xold(obj.osFern.freeDof) = K\F*steps(1);
            
            % Vector of translational DOF
            % loop to compute solution at each load step
            icount = 0;
            obj.solution.solution = zeros(obj.osFern.nDof, length(steps));
            
            for ii = 1:length(steps)
                derr = 1;
                ferr = 1;
                disp([' load step',num2str(ii)]);
                
                
                % iterate the nonlinear solution
                iloop = 1;
                dtol = 1E-3;
                ftol = 1E-3;
                
                while (derr > dtol) || (ferr > ftol)
                    
                    % compute the nonlinear stiffness matrices at the current state
                    [Kn,Kt] = obj.osFern.GetTangentStiffness(xold);
                    
                    % form and factor the tangent stiffness matrix
                    Ktan = K + Kt;
                    pp = symamd(Ktan);
                    
                    % compute the effective load
                    Fnew = (K + Kn)*xold(obj.osFern.freeDof);
                    
                    % compute the residual force
                    Fres = steps(ii)*F(pp) - Fnew(pp);
                    
                    % compute the incremental nonlinear static response
                    del_x = Ktan(pp,pp)\Fres;
                    del_x(pp) = del_x;
                    
                    if iloop == 1
                        del_x0 = del_x;
                    end
                    
                    % update the displacement vector
                    x_nli = xold(obj.osFern.freeDof) + del_x;

                    % compute the displacement error for all DOF
                    derr = norm(del_x)/norm(del_x0);
                    
                    % compute the force error
                    ferr = norm(Fres)/norm(F);
                    
                    % print the error values
                    if printopt
                        fprintf('   displ error = %8.3e, force error = %8.3e\n',[derr ferr]);
                    end
                    
                    xold(obj.osFern.freeDof) = x_nli;
                    iloop = iloop+1;
                    icount = icount+1;
                end
                
                % save the converged displacement vector
                obj.solution.solution(obj.osFern.freeDof, ii) = x_nli;
            end

            obj.finalDisplacement(obj.osFern.freeDof) = x_nli;
            
            % Obtain response DOF
            responseDof = [];
            for i = 1:length(obj.monitor)
                responseDof = unique([responseDof, obj.monitor(i).dof]);
            end
            
            obj.response = obj.finalDisplacement(responseDof);
            
        end

        % Solve a series of nonlinear static problems
        function [U] = SolveMultiple(obj, nStep, forces)
            % This solver is used to solve many different static load
            % cases. It is seperate from the original in order to avoid
            % reassembling the linear stiffness matrix. Additionally these
            % can all be solved in parallel if parallelOpts is used.
            % 
            % This function is also unique in that force should be directly
            % supplied for simplicity. They must be consistent with either
            % total dof or free dof. 
            % 
            % Inputs :
            %       obj = Nonlinear Static Object
            %       nStep = number of steps to use
            %       forces = matrix of force vectors
            % Ouputs :
            %       U = displacement fields
           
            % Obtain stiffness matrix and force vector
            K = obj.osFern.GetStiffnessMatrix();
            
            % Identify number of load cases
            nLoadCases = size(forces,2);
            
            % Check size of forces provided
            if size(forces,1) == sum(obj.osFern.freeDof)
                disp('Forces provided are consistnent with free dof')
                U = zeros(size(forces));
                loadCases = forces(obj.osFern.freeDof,:);
            elseif size(forces,1) == obj.osFern.nDof
               disp('Forces provided are consistent with total dof')
               disp('Reducing down to free dof only during solution')
               disp('Will expand back to full dof when done')
               
               loadCases = forces(obj.osFern.freeDof,:);
               U = zeros(obj.osFern.nDof,nLoadCases);
            else
               error('Forces provided are inconsistnent with system size')
            end
            
            if obj.parallelOpts.logic
                nDof = obj.osFern.nDof; freeDof = obj.osFern.freeDof; 
                uPar = cell(nLoadCases,1);
                % Share the object across all workers
                sharedObj = parallel.pool.Constant(obj.osFern());
%                 x_nli = zeros(nDof,1);
                % Loop through all the forces provided
                parfor (jj = 1:nLoadCases,obj.parallelOpts.numWorkers)

                    % Output solution number
                    disp(['Solving Load Case : ',num2str(jj)]);

                    % Grab the specifc solutoin
                    F = loadCases(:,jj);

                    % Start solution
                    steps = linspace(0, 1, nStep);

                    % compute the linear static response at the first load step
                    xold = zeros(nDof, 1); x_nli = zeros(nDof, 1);
                    xold(freeDof) = K\F*steps(1);

                    % Vector of translational DOF
                    % loop to compute solution at each load step
                    icount = 0;
                    printopt = false;
                    for ii = 1:length(steps)
                        derr = 1; ferr = 1;
                        if printopt
                            disp([' load step',num2str(ii)]);
                        end

                        % iterate the nonlinear solution
                        iloop = 1; dtol = 1E-3;  ftol = 1E-3;

                        while (derr > dtol) || (ferr > ftol)

                            % compute the nonlinear stiffness matrices at the current state
                            [Kn,Kt] = sharedObj.Value.GetTangentStiffness(xold);
                            
                            % form and factor the tangent stiffness matrix
                            Ktan = K + Kt;
                            pp = symamd(Ktan);

                            % compute the effective load
                            Fnew = (K + Kn)*xold(freeDof);

                            % compute the residual force
                            Fres = steps(ii)*F(pp) - Fnew(pp);

                            % compute the incremental nonlinear static response
                            del_x = Ktan(pp,pp)\Fres;
                            del_x(pp) = del_x;

                            if iloop == 1
                                del_x0 = del_x;
                            end

                            % update the displacement vector
                            x_nli = xold(freeDof) + del_x;

                            % compute the displacement error for all DOF
                            derr = norm(del_x)/norm(del_x0);

                            % compute the force error
                            ferr = norm(Fres)/norm(F);

                            % print the error values
                            if printopt
                                fprintf(' displ error = %8.3e, force error = %8.3e\n',[derr ferr]);
                            end

                            xold(freeDof) = x_nli;
                            iloop = iloop+1;
                            icount = icount+1;
                        end
                    end
                    uPar{jj} = x_nli;
%                     U(freeDof,jj) = x_nli;
                end
                % Parallel loop doesn't like the matrix storage so do this
                for jj = 1:nLoadCases; U(freeDof,jj) = uPar{jj}; end
            else
                % Loop through all the forces provided
                for jj = 1:nLoadCases

                    % Output solution number
                    disp(['Solving Load Case : ',num2str(jj)]);

                    % Grab the specifc solutoin
                    F = loadCases(:,jj);

                    % Start solution
                    steps = linspace(0, 1, nStep);

                    % compute the linear static response at the first load step
                    xold = zeros(obj.osFern.nDof, 1);
                    xold(obj.osFern.freeDof) = K\F*steps(1);

                    % Vector of translational DOF
                    % loop to compute solution at each load step
                    icount = 0;
                    obj.solution.solution = zeros(obj.osFern.nDof, length(steps));
                    printopt = false;
                    for ii = 1:length(steps)
                        derr = 1;
                        ferr = 1;
                        if printopt
                            disp([' load step',num2str(ii)]);
                        end

                        % iterate the nonlinear solution
                        iloop = 1; dtol = 1E-3;  ftol = 1E-3;

                        while (derr > dtol) || (ferr > ftol)

                            % compute the nonlinear stiffness matrices at the current state
                            [Kn,Kt] = obj.osFern.GetTangentStiffness(xold);

                            % form and factor the tangent stiffness matrix
                            Ktan = K + Kt;
                            pp = symamd(Ktan);

                            % compute the effective load
                            Fnew = (K + Kn)*xold(obj.osFern.freeDof);

                            % compute the residual force
                            Fres = steps(ii)*F(pp) - Fnew(pp);

                            % compute the incremental nonlinear static response
                            del_x = Ktan(pp,pp)\Fres;
                            del_x(pp) = del_x;

                            if iloop == 1
                                del_x0 = del_x;
                            end

                            % update the displacement vector
                            x_nli = xold(obj.osFern.freeDof) + del_x;

                            % compute the displacement error for all DOF
                            derr = norm(del_x)/norm(del_x0);

                            % compute the force error
                            ferr = norm(Fres)/norm(F);

                            % print the error values
                            if printopt
                                fprintf('   displ error = %8.3e, force error = %8.3e\n',[derr ferr]);
                            end

                            xold(obj.osFern.freeDof) = x_nli;
                            iloop = iloop+1;
                            icount = icount+1;
                        end

                        % save the converged displacement vector
                        obj.solution.solution(obj.osFern.freeDof, ii) = x_nli;
                    end
                    U(obj.osFern.freeDof,jj) = x_nli;
                end
            end
            
        end

        % Special Static Cases
        function [Kt,Kn,Fint] = RunEnforcedDisp(obj,x,opt)
            % This is a special static function in which a displacement
            % field is applied to the model and the forces required to hold
            % it are calculated. Additionally the tangent stiffness matrix
            % can be outputed.
            % 

            
            % compute the nonlinear stiffness matrices at the current state
            [Kn,Kt] = obj.osFern.GetTangentStiffness(x);
                    
            % compute the effective load
            Fint = Kn*x(obj.osFern.freeDof);
            
        end
        
        function [fInt,Kt] = RunMultipleEnforcedDisp(obj,x)
            
            % Identify number of load cases
            nLoadCases = size(x,2);
            
            % Check size of displacements provided
            if size(x,1) == sum(obj.osFern.freeDof)
                disp('Forces provided are consistnent with free dof')
                fInt = zeros(size(x));
                U = x; %(obj.osFern.freeDof,:);
            elseif size(x,1) == obj.osFern.nDof
               disp('Forces provided are consistent with total dof')
               disp('Reducing down to free dof only during solution')
               disp('Will expand back to full dof when done')
               
               U = x; %(obj.osFern.freeDof,:);
               fInt= zeros(obj.osFern.nDof,nLoadCases);
            else
               error('Forces provided are inconsistnent with system size')
            end

            
            % Build linear stiffnes
            K = obj.osFern.GetStiffnessMatrix();
            Kt = cell(nLoadCases,1); Kn = cell(nLoadCases,1);
            nDof = obj.osFern.nDof; freeDof = obj.osFern.freeDof; 
            
            % Check parallel logic
            if obj.parallelOpts.logic
                                
                % Share the object across all workers
                sharedObj = parallel.pool.Constant(obj.osFern());
                
                % Loop through all the forces provided
                parfor (jj = 1:nLoadCases,obj.parallelOpts.numWorkers)
                    % Compute the nonlinear stiffness matrices at the current state
                    [Kn{jj},Kt{jj}] = sharedObj.Value.GetTangentStiffness(U(:,jj));
                    
                    % Compute internal force
                    fInttemp = (K+Kt{jj})*U(:,jj);
                    fInt(freeDof,jj) = fInttemp;
                end
                
            else
                for jj = 1:nLoadCases
                    % Compute the nonlinear stiffness matrices at the current state
                    [Kn{jj},Kt{jj}] = obj.osFern.GetTangentStiffness(U(:,jj));
                    
                    % Compute internal force
                    fInttemp = (K+Kt{jj})*U(obj.osFern.freeDof,jj);
                    fInt(freeDof,jj) = fInttemp;
                end
            end 
        end
        
        % Compute potential energy of the solution
        function [E] = PotentialEnergy(obj,x)
            
            % Check size of displacements provided
            if size(x,1) == sum(obj.osFern.freeDof)
                disp('Forces provided are consistnent with free dof')
                U = x(obj.osFern.freeDof,:);
            elseif size(x,1) == obj.osFern.nDof
               disp('Forces provided are consistent with total dof')
               disp('Reducing down to free dof only during solution')
               disp('Will expand back to full dof when done')
               U = x(obj.osFern.freeDof,:);
            else
               error('Forces provided are inconsistnent with system size')
            end
            K = obj.osFern.GetStiffnessMatrix();
            [Kn, ~] = obj.osFern.GetTangentStiffness(x);
            
            E = 0.5*U'*(K+Kn)*U;
        end
        
        function [fR] = RecoverReactions(obj,x)
            % Recover the reaction forces after a static solution has been
            % done. 
        end
        
        % Disable "addEnforced" method
        function addEnforced(obj, varargin)
            error('Cannot add enforced displacement to linear static step.');
        end
        
    end
    
end