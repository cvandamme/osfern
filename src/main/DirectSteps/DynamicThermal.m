classdef DynamicThermal < Dynamic 
    
    properties
        thermal;
    end
    
    properties(Constant)
                
    end
 
    methods
        
        % Constructor
        function obj = DynamicThermal(name, osFern, initialDisplacement, initialVelocity)
            
            obj = obj@Dynamic(name, osFern);
            obj.finalDisplacement = zeros(obj.osFern.nDof, 1);
            obj.finalVelocity = zeros(obj.osFern.nDof, 1);
            
            if nargin > 2
                obj.initialDisplacement = initialDisplacement;
            end
            
            if nargin > 3
                obj.initialVelocity = initialVelocity;
            end            
            
        end
        
        % Basic Solver/Integrator
        function obj = Solve(obj, T, dt)
            % This function integrates the equations of motion of a
            % geometrically nonlinear finite element model with external
            % loading and an applied thermal field. It is capable of time
            % varying thermal loads and time varying external loads. It is
            % currently a fixed step size integrator. 
            % 
            % Inputs :
            %          obj = DynamicThermal Object
            %          T = time duration 
            %          dt = time step
            
            
             
            % Shorthand for initial velocity and displacement
            if isempty(obj.initialDisplacement)
                x0 = zeros(obj.osFern.nDof, 1); 
            else
                x0 = obj.initialDisplacement;
            end
            
            if isempty(obj.initialVelocity)
                v0 = zeros(obj.osFern.nDof, 1); 
            else
                v0 = obj.initialVelocity;
            end
            
            
            % Obtain response DOF
            responseDof = [];
            for i = 1:length(obj.monitor)
                responseDof = unique([responseDof, obj.monitor(i).dof]);
            end
            
            % Setup plotting if desired
            if obj.plotOpts.logic
                obj = obj.InitializePlot(x0);
            end 
            
            % Obtain time vector
            obj.t = 0:dt:T; 
            if obj.t(end) < T
                obj.t = [obj.t, obj.t(end) + dt];
            end
            dt2 = dt^2;
            nstep = length(obj.t);            
            
            % compute the linear static response at the first load step
            obj.solution.displacement = zeros(obj.osFern.nDof, nstep);
            obj.solution.velocity = zeros(obj.osFern.nDof, nstep);
            obj.solution.acceleration = zeros(obj.osFern.nDof, nstep);
            
            obj.solution.displacement(:, 1) = x0;
            obj.solution.velocity(:, 1) = v0;
            
            % Form stiffness, amss and damping matrices
            Km = obj.GetStiffnessMatrix();
            Mm = obj.GetMassMatrix();
            Cm = obj.GetRayleighDamping(Km, Mm);
            
            % Thermal loads
            [fThermal,Se,Ee] = obj.AssembleThermalField(0, x0);
            
            % Stress stiffness matrix
            Kss = obj.GetStressStiffness(Se,x0);

            % compute constant terms in Keff
            Keff0 = 4*Mm/dt2 + 2*Cm/dt;
            fold = obj.AssembleForce(0) + sparse(fThermal(obj.osFern.freeDof));
            
            % initialize temporary vectors of modal displacements, dold,
            % velocities, vold, and accelerations, aold
            dold = x0(obj.osFern.freeDof);  dfull = x0;
            vold = v0(obj.osFern.freeDof);
            aold = 0*x0(obj.osFern.freeDof);
            
            if obj.nl
                Kno = obj.GetNLTangentStiffness(dfull);
                Knold = Km + Kss + Kno; % Not sure if tangent or actual force
            else
                Knold = Km + Kss;
            end
            pp = symamd(Knold); 
            
            % compute initial modal accelerations
            aold = Mm\(fold - Cm*vold - Knold*dold);
            dnew = dold;
            
            % Initialize some variables
            eigIndex = 1;
            
            % perform the integration loop for (npts - 1) time points
            for ii = 2:nstep
                
                tC = obj.t(ii);
                
                err = 1;  dtemp = dold;
                dfullt = zeros(obj.osFern.nDof,1);
                dfullt(obj.osFern.freeDof) = dtemp;

                % Assemble new thermal force vector
                [fThermal,Se,~] = obj.AssembleThermalField(obj.t(ii), dfullt);
            
                % Stress stiffness matrix
                Kss = obj.GetStressStiffness(Se,dfullt);                
                
                % Assemble new force vector
                Fnew = obj.AssembleForce(obj.t(ii));                
                
                % compute the effective load
                Reff = fThermal(obj.osFern.freeDof) + Fnew + Mm*4*(dold + dt*vold + (dt2/4)*aold)/dt2 ...
                    + Cm*(2*dold/dt + vold);
                
                % loop to converge on dnew
                niter = 0;
                while err > obj.tol
                    niter = niter + 1;

                    % test for non-convergence
                    if niter > obj.integrator.maxIters
                        disp(['Fatal Error - Reached Maximum Number of Iterations : ', num2str(obj.integrator.maxIters),' without convergence'])
                        disp([' at time step ',num2str(ii)])
                        return
                    end
                    
                    % build the nonlinear modal stiffness terms
                    if mod(niter+1,obj.integrator.quasiRate) == 0 || niter == 1
                        dfull(obj.osFern.freeDof) = dnew;
                        if obj.nl
                            Knnew = Km + obj.GetNLStiffness(dfull);
                            Knnew = Km + obj.GetNLTangentStiffness(dfull);
                            Keff = Keff0 + Kss + Knnew;
                        else
                            Keff = Keff0 + Kss + Km;
                        end
                    end
                    
                    % compute new displacements
%                     dNew = obj.SolveLinearSystem(Keff,Reff);
                    
                    switch obj.solverOpts.method
                        case {'direct','Direct','DIRECT'} 
                            
%                             dnew_p = Keff(pp,pp)\Reff(pp);
%                             dnew = dnew_p(pp);
                            dnew = Keff\Reff;
                        case {'cg'} 
                            pp = amd(Keff);
                            [L,U] = ilu(Keff(pp,pp));
                            [dnew,flag,relres,iter,resvec] = gmres(Keff(pp,pp),Reff(pp),...
                                                             10,1e-8,1e3,L,U);
                            dnew = dnew(pp);
                        otherwise
                    end
                    
                    % compute the error between dnew and dold
                    err = max(abs((dnew - dtemp)))/norm(dnew);
%                     disp(['Error = ',num2str(err)]);
                    % Store temp value
                    dtemp = dnew;
                    
                end
                
                % Output message
                if obj.printOpts.logic && mod(ii,obj.printOpts.frameRate) == 0 
                    fprintf('Increment %i of %i complete in %i iterations\n', ii, nstep, niter);
                end
                
                % compute new values for velocity and acceleration
                vnew = -vold+2*(dnew-dold)/dt;
                anew = 4*(dnew-dold)/dt2-4*vold/dt-aold;
                
                % Store old values
                dold = dnew; vold = vnew; aold = anew;                
               
                % Save converged solutions
                obj.solution.displacement(obj.osFern.freeDof, ii) = dnew;
                obj.solution.velocity(obj.osFern.freeDof, ii) = vnew;
                obj.solution.acceleration(obj.osFern.freeDof, ii) = dnew;

                % Plot if desired
                if obj.plotOpts.logic && mod(ii,obj.plotOpts.frameRate) == 0
                    switch obj.plotOpts.field
                       case 'accleration' 
                           obj = UpdatePlot(obj,anew,ii);
                       case 'velocity'
                           obj = UpdatePlot(obj,vnew,ii);
                       case 'displacement'
                           obj = UpdatePlot(obj,dfull,ii);
                    end
                end
                
                % Perform intermittent eigenvalue extraction
                if obj.eigenComp.logic && tC > obj.eigenComp.tPoints(eigIndex)
                    
                    % Interpolate displacement to desired time
                    xInterp = interp1([obj.t(ii-1),obj.t(ii)],...
                            [obj.solution.displacement(:, ii-1),...
                             obj.solution.displacement(:, ii)]',...
                             obj.eigenComp.tPoints(eigIndex));
                         
                    % Currently this is about the closest time point not
                    % the exact time, this is to avoid recomputing
                    % nonlinear stiffness and stress stiffnes matrix
                    % Eigenvalue problem
                    [phi_temp, lam] = eigs(Keff,Mm,obj.eigenComp.numEigs,'SM');

                    % Mass Normalize Modes
                    for i = 1:obj.eigenComp.numEigs
                        phi_temp(:, i) = phi_temp(:, i)...
                            /sqrt(phi_temp(:, i)'*Mm*phi_temp(:, i));
                    end

                    % Save modal propertie within object
                    [lam, I] = sort(diag(lam)); fn = sqrt(lam)/2/pi;
                    phi = zeros(obj.osFern.nDof, obj.eigenComp.numEigs);
                    phi(obj.osFern.freeDof, :) = phi_temp(:, I);
                    
                    % Store values
                    obj.eigenComp.results(eigIndex).phi = phi;
                    obj.eigenComp.results(eigIndex).fn = fn;
                    
                    % add to the search index
                    eigIndex = eigIndex + 1;
                    
                    % Turn off after finishing all time points
                    if eigIndex > length(obj.eigenComp.tPoints)
                       obj.eigenComp.logic = false; 
                    end
                    
                end
                
            end
            
            % Interploate final solution so its at desired time
            [xF] = interp1([obj.t(end-1),obj.t(end)],...
                           [obj.solution.displacement(:, end-1),...
                           obj.solution.displacement(:, end)]',...
                           T);
            [vF] = interp1([obj.t(end-1),obj.t(end)],...
                           [obj.solution.velocity(:, end-1),...
                           obj.solution.velocity(:, end)]',...
                           T);
            [aF] = interp1([obj.t(end-1),obj.t(end)],...
                           [obj.solution.acceleration(:, end-1),...
                           obj.solution.acceleration(:, end)]',...
                           T);
            obj.solution.displacement(:, end) = xF;
            obj.solution.velocity(:, end) = vF;
            obj.solution.acceleration(:, end) = aF;
            obj.t(end) = T;
            
            % Store final values           
            obj.finalDisplacement = obj.solution.displacement(:, end);
            obj.finalVelocity = obj.solution.velocity(:, end);
            obj.response = obj.solution.displacement(responseDof, :);
            
        end
        
        % Sub-Solvers
        function obj = Solve_HHT(obj, T, alpha)
            % This function integrates the equations of motion of a
            % geometrically nonlinear finite element model with external
            % loading and an applied thermal field. It is capable of time
            % varying thermal loads and time varying external loads.=
            % 
            % Inputs :
            %          obj = DynamicThermal Object
            %          T = time duration 
            %          alpha = alpha parameter from HHT-alpha method
            
            
            % Comput other HHT parameters
            try
                if alpha < 0 || alpha > 1/3
                    error('Alpha is invalid')
                else
                    beta = 0.25*(1+alpha)^2;
                    gamma = 0.5 + alpha;
                end
            catch
                alpha = 0; beta = 0.25; gamma = 0.75;
            end

            % Shorthand for initial velocity and displacement
            if isempty(obj.initialDisplacement)
                x0 = zeros(obj.osFern.nDof, 1); 
            else
                x0 = obj.initialDisplacement;
            end
            
            if isempty(obj.initialVelocity)
                v0 = zeros(obj.osFern.nDof, 1); 
            else
                v0 = obj.initialVelocity;
            end
            
            % Obtain response DOF
            responseDof = [];
            for i = 1:length(obj.monitor)
                responseDof = unique([responseDof, obj.monitor(i).dof]);
            end
            
            % Setup plotting if desired
            if obj.plotOpts.logic
                obj = obj.InitializePlot(x0);
            end 
            
            % Initialize a maximum size time vector
            tFinal = T + obj.integrator.stepMax;
            obj.t = zeros(round(tFinal/obj.integrator.stepMin),1); 
            
            % Check if history options are present
            if isempty(obj.historyOpts.tPoints)
                obj.historyOpts.tPoints = linspace(0,tFinal);
            end
            
            % Initial step size
            dt = obj.integrator.stepStart; dt2 = dt^2; ti = 0;  dtOld = dt;
            
            % compute the linear static response at the first load step
%             obj.solution.displacement = zeros(obj.osFern.nDof, nstep);
%             obj.solution.velocity = zeros(obj.osFern.nDof, nstep);
%             obj.solution.acceleration = zeros(obj.osFern.nDof, nstep);
            
            obj.solution.displacement(:, 1) = x0;
            obj.solution.velocity(:, 1) = v0;
            
            % Form linear stiffness, mass and damping matrices
            Km = obj.GetStiffnessMatrix();
            Mm = obj.GetMassMatrix(); Minv = inv(M);
            Cm = obj.GetRayleighDamping(Km, Mm);
            
            % Nonlinear internal force
%             dR = rNew - rOld;
            
            % Thermal loads
            [fThermal,Se,~] = obj.AssembleThermalField(0, x0);
            
            % Stress stiffness matrix
            Kss = obj.GetStressStiffness(Se,x0);

            % Compute linear coefficients
            vMat = (1/(beta*dt))*Mm + (1-alpha)*(gamma/beta)*Cm;
            aMat = (0.5*beta)*Mm - (1-alpha)*dt*(1-gamma/(2*beta))*Cm;
            dxMat = alpha*(gamma/(beta*dt)*Cm + Km);
            vOldMat = alpha*(gamm/beta)*Cm;
            aOldMat = alpha*dt*(1-gamma/(2*beta))*Cm;
            
            
            % compute constant terms in Keff
            Keff0 = 4*Mm/dt2 + 2*Cm/dt;
            fold = obj.AssembleForce(0) + fThermal(obj.osFern.freeDof);
            
            % initialize temporary vectors of modal displacements, dold,
            % velocities, vold, and accelerations, aold
            dold = x0(obj.osFern.freeDof);  dfull = x0;
            vold = v0(obj.osFern.freeDof);
            aold = 0*x0(obj.osFern.freeDof);
            
            % Check if geometric nonlinearity is considered
            if obj.nl
                Kto = obj.GetNLTangentStiffness(dfull);
                Ktold = Km + Kss + Kto; % Not sure if tangent or actual force
            else
                Ktold = Km + Kss;
            end
            
            % compute initial modal accelerations
            aold = Mm\(fold - Cm*vold - Ktold*dold);
            dnew = dold;
            
            % Start step count
            stpNum = 2; niter = obj.integrator.optIters +1; hIndex = 2;
            
            % perform the integration loop for (npts - 1) time points
            while ti < T
                
                dtOld = dt;
                % step control
                if niter < obj.integrator.optIters - obj.integrator.iterWin
                    dt = dt*obj.integrator.stepScale;
                elseif niter > obj.integrator.optIters + obj.integrator.iterWin
                    dt = dt*(1/obj.integrator.stepScale);
                end
                
                % Step size limits
                if dt > obj.integrator.stepMax
                    dt = obj.integrator.stepMax;
                end
                if dt < obj.integrator.stepMin
                    dt = obj.integrator.stepMin;
                    warning('Minimum Step Size Reached');
                end
                dt2 = dt^2;
                
                % Record previous step and add increment
                ti = ti + dt; obj.t(stpNum) = ti;
                
                % Initialzie error
                err = 1; dtemp = dold;
                
                % Current displacement
                dfullt = zeros(obj.osFern.nDof,1);
                dfullt(obj.osFern.freeDof) = dtemp;

                % Assemble new thermal force vector
                [fThermal,Se,~] = obj.AssembleThermalField(ti, dfullt);
            
                % Stress stiffness matrix
                Kss = obj.GetStressStiffness(Se,dfullt);                
                
                % Assemble new force vector
                Fnew = obj.AssembleForce(ti);                
                
                % compute the effective load
                Reff = fThermal(obj.osFern.freeDof) + Fnew + Mm*4*(dold + dt*vold + (dt2/4)*aold)/dt2 ...
                    + Cm*(2*dold/dt + vold);
                
                % loop to converge on dnew
                niter = 0; intFailed = false;
                while err > obj.tol
                    niter = niter + 1;

                    % test for non-convergence
                    if niter > obj.integrator.maxIters
                        disp(['Reached Maximum Number of Iterations : ',...
                             num2str(obj.integrator.maxIters),...
                             ' without convergence at time ', num2str(ti),...
                             'Reducing stepsize'])
                        intFailed = true;
                        break;
                    end
                    
                    % build the nonlinear modal stiffness terms
                    if mod(niter+1,obj.integrator.quasiRate) == 0 || niter == 1
                        dfull(obj.osFern.freeDof) = dnew;
                        if obj.nl
                            Knnew = Km + obj.GetNLStiffness(dfull);
                            Keff = Keff0 + Kss + Knnew;
                        else
                            Keff = Keff0 + Kss + Km;
                        end
                    end
                    
                    % compute new displacements
                    switch obj.solverOpts.method
                        case {'direct','Direct','DIRECT'} 
                            dnew = Keff\Reff;
                        case {'cg'} 
                            
                        otherwise
                    end
                    
                    
                    % compute the error between dnew and dold
                    err = max(abs((dnew - dtemp)))/norm(dnew);
%                     err2 = Minv*()
                    % Store temp value
                    dtemp = dnew;
                    
                end
                
                % if integration passed store values
                if ~intFailed
                    
                    % Output message
                    if obj.printOpts.logic && mod(stpNum,obj.printOpts.frameRate) == 0 
                        fprintf('Time %d of Total Time %d complete in %i iterations\n', ti, T, niter);
                        fprintf('Current Step Size : %d \n', dt);
                    end

                    % compute new values for velocity and acceleration
                    vnew = -vold+2*(dnew-dold)/dt;
                    anew = 4*(dnew-dold)/dt2-4*vold/dt-aold;

                    % Save converged solutions
%                     if obj.historyOpts.tPoints(hIndex) >= obj.t(stpNum-1) && ...
%                        obj.historyOpts.tPoints(hIndex) <= obj.t(stpNum)
% 
%                         % Interpolate displacement to desired time
%                         dInterp = interp1([obj.t(stpNum-1),obj.t(stpNum)],...
%                                 [dold,dnew]',obj.historyOpts.tPoints(hIndex));
% 
%                         vInterp = interp1([obj.t(stpNum-1),obj.t(stpNum)],...
%                                 [vold,vnew]',obj.historyOpts.tPoints(hIndex));
% 
%                         aInterp = interp1([obj.t(stpNum-1),obj.t(stpNum)],...
%                                 [aold,anew]',obj.historyOpts.tPoints(hIndex));
% 
%                         obj.solution.displacement(obj.osFern.freeDof, hIndex) = dInterp;
%                         obj.solution.velocity(obj.osFern.freeDof, hIndex) = vInterp;
%                         obj.solution.acceleration(obj.osFern.freeDof, hIndex) = aInterp;
%                         hIndex = hIndex + 1;
%                     end

                    obj.solution.displacement(obj.osFern.freeDof, stpNum) = dnew;
                    obj.solution.velocity(obj.osFern.freeDof, stpNum) = vnew;
                    obj.solution.acceleration(obj.osFern.freeDof, stpNum) = anew;
                    
                    % Store old values
                    dold = dnew; vold = vnew; aold = anew;   

                    % Plot if desired
                    if obj.plotOpts.logic && mod(stpNum,obj.plotOpts.frameRate) == 0
                        switch obj.plotOpts.field
                           case 'accleration' 
                               obj = UpdatePlot(obj,obj.solution.acceleration(:,stpNum),stpNum);
                           case 'velocity'
                               obj = UpdatePlot(obj,obj.solution.velocity(:,stpNum),stpNum);
                           case 'displacement'
                               obj = UpdatePlot(obj,dfull,stpNum);
                        end
                    end

                    % Add to step count 
                    stpNum = stpNum + 1;
                else
                    fprintf(['Integration failed at time %d of Total Time',...
                             '%d complete in %i iterations\n', ti, T,...
                             'reducing step size']);                    
                end
            end
            
            % Remove unused data
            obj.t = obj.t(1:stpNum-1);
            
            % Interploate final solution so its at desired time
            [xF] = interp1([obj.t(end-1),obj.t(end)],...
                           [obj.solution.displacement(:, hIndex-2),...
                           obj.solution.displacement(:, hIndex-1)]',...
                           T);
            [vF] = interp1([obj.t(end-1),obj.t(end)],...
                           [obj.solution.velocity(:, hIndex-2),...
                           obj.solution.velocity(:, hIndex-1)]',...
                           T);
            [aF] = interp1([obj.t(end-1),obj.t(end)],...
                           [obj.solution.acceleration(:, hIndex-2),...
                           obj.solution.acceleration(:, hIndex-1)]',...
                           T);
                       
            % Reduce the number of points
            obj.solution.displacement = obj.solution.displacement(:,1:stpNum-1);
            obj.solution.velocity = obj.solution.velocity(:,1:stpNum-1);
            obj.solution.acceleration = obj.solution.acceleration(:,1:stpNum-1);
            obj.t = obj.t(1:stpNum-1);
            
            % Store final values
            obj.solution.displacement(:, end) = xF;
            obj.solution.velocity(:,end) = vF;
            obj.solution.acceleration(:,end) = aF;
            obj.t(end) = T;
            
            % Store final values           
            obj.finalDisplacement = obj.solution.displacement(:, end);
            obj.finalVelocity = obj.solution.velocity(:, end);
            obj.response = obj.solution.displacement(responseDof, :);
            
        end
        
        function K = GetStiffnessMatrix(obj)
            % Obtain the base state stiffness matrix
            K = obj.osFern.GetStiffnessMatrix();             
        end
        
        function M = GetMassMatrix(obj)
            % Obtain the mass matrix
            M = obj.osFern.GetMassMatrix(); 
        end
        
        function C = GetRayleighDamping(obj, K, M)
            % Obtain the Rayleigh damping matrix. Must supply K and M
            % matrices to compute the damping matrix.
            C = obj.damping.alpha*M + obj.damping.beta*K;
        end
        
        function Kt = GetNLTangentStiffness(obj, x)
            % Obtain the nonlinear contribution to the tangent stiffness
            % matrix, i.e. Ktangent = Kbase + Kt(x)
            if obj.parallelOpts.logic
                [~, Kt] = obj.osFern.GetTangentStiffnessPar(x);
            else
                [~, Kt] = obj.osFern.GetTangentStiffness(x);
            end
        end
        
        function Kn = GetNLStiffness(obj, x)
            % Obtain the nonlinear contribution to the true stiffness
            % matrix, i.e. fStiffness = (Kbase + Kn(x))*x
            if obj.parallelOpts.logic
                [Kn, ~] = obj.osFern.GetTangentStiffnessPar(x);
            else
                [Kn, ~] = obj.osFern.GetTangentStiffness(x);
            end
        end
        
        % Thermal specific fucntions
        function Kss = GetStressStiffness(obj,S0,xi)
            Kss = obj.osFern.GetStressStiffness(S0,xi);
        end
  
        function obj = SetThermalField(obj,thermalField)
            obj.thermalField = thermalField;
        end
        function obj = SetIntegrator(obj,quasiRate,stepControl)
            obj.integrator.quasiRate = quasiRate;
        end
        
        function obj = SetDamping(obj,alpha,beta)
            % Currently only proportional damping is used
            obj.damping.alpha = alpha;
            obj.damping.beta = beta;
        end
        
        % Disable "addEnforced" method
        function addEnforced(obj, varargin)
            error('Not implemented.');
        end
                
    end
    
end