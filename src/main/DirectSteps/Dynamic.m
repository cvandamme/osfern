

classdef Dynamic < DirectStep
    
    properties
        
        response;
        t;
        integrator = struct('quasiRate',1,'tol',1e-6,...
                            'adaptiveStepSize',false,'optIters',6,'iterWin',2,...
                            'maxIters',50,'stepMin',1e-5,'stepStart',1e-4,...
                            'stepMax',0.001,'stepScale',2,'beta',0.6,'alpha',0.2);
        eigenComp = struct('logic',false,'tPoints',0,'numEigs',0,...
                           'results',[])
        nl = true;                
        damping = struct('alpha',0.0,'beta',0.0);
    end
    
    properties(Constant)
        
        tol = 1E-6;
        
    end
 
    methods
        
        % Constructor
        function obj = Dynamic(name, osFern, initialDisplacement, initialVelocity)
            
            obj = obj@DirectStep(name, osFern);
            obj.finalDisplacement = zeros(obj.osFern.nDof, 1);
            obj.finalVelocity = zeros(obj.osFern.nDof, 1);
            
            if nargin > 2
                obj.initialDisplacement = initialDisplacement;
            end
            
            if nargin > 3
                obj.initialVelocity = initialVelocity;
            end            
            
        end
        
        % Basic Solver/Integrator
        function obj = Solve(obj, T, dt)
            % This is still almost directly copied from the old Bobtran
            % "nlstat" function. 
            % A big thing to note is that I'm writing everything using a
            % fully unconstrained solution vector. This is for ease of
            % indexing, especially with the NL assembly. It has to come at
            % some sort of performance cost, but until we can quantify
            % exactly what that penalty is, I don't want to bother messing
            % with it. (Right now, the vast majority of the solution time
            % is spent in element assembly.)
            
%             % Obtain vector of free DOF
%             freeDof = 1:length(obj.osFern.dof);
%             constrDof = unique([obj.osFern.constraint.dof; obj.constraint.dof]);
%             freeDof(constrDof) = [];
%             
            % Shorthand for initial velocity and displacement
            if isempty(obj.initialDisplacement)
                x0 = zeros(obj.osFern.nDof, 1); 
            else
                x0 = obj.initialDisplacement;
            end
            
            if isempty(obj.initialVelocity)
                v0 = zeros(obj.osFern.nDof, 1); 
            else
                v0 = obj.initialVelocity;
            end
            
            
            % Obtain response DOF
            responseDof = [];
            for i = 1:length(obj.monitor)
                responseDof = unique([responseDof, obj.monitor(i).dof]);
            end
            
            % Setup plotting if desired
            if obj.plotOpts.logic
                obj = obj.InitializePlot(x0);
            end 
            
            % Obtain time vector
            obj.t = 0:dt:T; 
            if obj.t(end) < T
                obj.t = [obj.t, obj.t(end) + dt];
            end
            dt2 = dt^2;
            nstep = length(obj.t);            
            
            % compute the linear static response at the first load step
            obj.solution.displacement = zeros(obj.osFern.nDof, nstep);
            obj.solution.velocity = zeros(obj.osFern.nDof, nstep);
            obj.solution.acceleration = zeros(obj.osFern.nDof, nstep);
            
            obj.solution.displacement(:, 1) = x0;
            obj.solution.velocity(:, 1) = v0;
            
            % Form transformed stiffness and damping matrices
            Km = obj.GetStiffnessMatrix();
            Mm = obj.GetMassMatrix();
            Cm = obj.GetRayleighDamping(Km, Mm);
            
            % compute constant terms in Keff
            Keff0 = 4*Mm/dt2 + 2*Cm/dt;
            fold = obj.AssembleForce(0);
            
            % initialize temporary vectors of modal displacements, dold,
            % velocities, vold, and accelerations, aold
            dold = x0(obj.osFern.freeDof);
            dfull = x0;
            vold = v0(obj.osFern.freeDof);
            aold = 0*x0(obj.osFern.freeDof);
            
            if obj.nl
                Kto = obj.GetNLTangentStiffness(dfull);
                Ktold = Km + Kto; % Not sure if tangent or actual force
            else
                Ktold = Km;
            end
            
            % compute initial modal accelerations
            aold = Mm\(fold - Cm*vold - Ktold*dold);
            dnew = dold;
            
            % Check if parallel options are desired
            if obj.parallelOpts.logic
                delete(gcp('nocreate')); % delete current pool if present
                pPool = parpool('local',obj.osFern.parallelOpts.numWorkers);
                % This doesn't work for me (JDS) but it's probably a 
                % version thing, not concerned with it. 
                parallel.pool.Constant(obj)
            end
            
            % Initialize some variables
            eigIndex = 1;
            
            % perform the integration loop for (npts - 1) time points
            for ii = 2:nstep
                
                % Store current time
                tC = obj.t(ii);
                
                % Initialize error and old displacement vector
                err = 1; dtemp = dold;
                
                % Assemble new force vector
                Fnew = obj.AssembleForce(obj.t(ii));                
                
                % compute the effective load
                Reff = Fnew + Mm*4*(dold + dt*vold + (dt2/4)*aold)/dt2 ...
                    + Cm*(2*dold/dt + vold);
                
                % loop to converge on dnew
                niter = 0;
                while err > obj.tol
                    niter = niter + 1;

                    % test for non-convergence
                    if niter > obj.integrator.maxIters
                        disp(['Fatal Error - Reached Maximum Number of Iterations : ', num2str(obj.integrator.maxIters),' without convergence'])
                        disp([' at time step ',num2str(ii)])
                        return
                    end
                    
                    % build the nonlinear modal stiffness terms
                    if mod(niter+1,obj.integrator.quasiRate) == 0 || niter == 1
                        dfull(obj.osFern.freeDof) = dnew;
                        if obj.nl
                            Knnew = Km + obj.GetNLStiffness(dfull);
                            Keff = Keff0 + Knnew;
                        else
                            Keff = Keff0 +Km;
                        end
                    end
                    
                    % compute new displacements
                    switch obj.solverOpts.method
                        case {'direct','Direct','DIRECT'} 
                            dnew = Keff\Reff;
                        case {'cg'} 
                            
                        otherwise
                    end
                    
                    
                    % compute the error between dnew and dold
                    err = max(abs((dnew - dtemp)))/norm(dnew);
                    
                    % Store temp value
                    dtemp = dnew;
                    
                end
                
                % Output message
                if obj.printOpts.logic && mod(ii,obj.printOpts.frameRate) == 0 
                    fprintf('Increment %i of %i complete in %i iterations\n', ii, nstep, niter);
                end
                
                % compute new values for velocity and acceleration
                vnew = -vold+2*(dnew-dold)/dt;
                anew = 4*(dnew-dold)/dt2-4*vold/dt-aold;
                
                % Store old values
                dold = dnew;
                vold = vnew;
                aold = anew;                
               
                % Save converged solutions
                obj.solution.displacement(obj.osFern.freeDof, ii) = dnew;
                obj.solution.velocity(obj.osFern.freeDof, ii) = vnew;
                obj.solution.acceleration(obj.osFern.freeDof, ii) = anew;
                                
                % Plot if desired
                if obj.plotOpts.logic && mod(ii,obj.plotOpts.frameRate) == 0
                    switch obj.plotOpts.field
                       case 'accleration' 
                           obj = UpdatePlot(obj,obj.solution.acceleration(:,ii),ii);
                       case 'velocity'
                           obj = UpdatePlot(obj,obj.solution.velocity(:,ii),ii);
                       case 'displacement'
                           obj = UpdatePlot(obj,obj.solution.displacement(:,ii),ii);
                    end
                end

                % Perform intermittent eigenvalue extraction
                if obj.eigenComp.logic && tC > obj.eigenComp.tPoints(eigIndex)
                    
                    % Interpolate displacement to desired time
                    xInterp = interp1([obj.t(ii-1),obj.t(ii)],...
                            [obj.solution.displacement(:, ii-1),...
                             obj.solution.displacement(:, ii)]',...
                             obj.eigenComp.tPoints(eigIndex));
                    
                    % Perform eigenvalue analysis at current state
                    modal = Frequency('IntEig',obj.osFern,xInterp);
                    modal.Solve(obj.eigenComp.numEigs);
                    
                    % Store values
                    obj.eigeComp.results(ii).phi = modal.phi;
                    obj.eigeComp.results(ii).fn = modal.fn;
                    
                    % add to the search index
                    eigIndex = eigIndex + 1;
                    
                    % Turn off after finishing all time points
                    if eigIndex > length(obj.eigenComp.tPoints)
                       obj.eigenComp.logic = false; 
                    end
                    
                end
                
            end
            
            % Interploate final solution so its at desired time
            [xF] = interp1([obj.t(end-1),obj.t(end)],...
                           [obj.solution.displacement(:, end-1),...
                           obj.solution.displacement(:, end)]',...
                           T);
            [vF] = interp1([obj.t(end-1),obj.t(end)],...
                           [obj.solution.velocity(:, end-1),...
                           obj.solution.velocity(:, end)]',...
                           T);
            [aF] = interp1([obj.t(end-1),obj.t(end)],...
                           [obj.solution.acceleration(:, end-1),...
                           obj.solution.acceleration(:, end)]',...
                           T);
            obj.solution.displacement(:, end) = xF;
            obj.solution.velocity(:, end) = vF;
            obj.solution.acceleration(:, end) = aF;
            obj.t(end) = T;
            
            % Store final values           
            obj.finalDisplacement = obj.solution.displacement(:, end);
            obj.finalVelocity = obj.solution.velocity(:, end);
            obj.response = obj.solution.displacement(responseDof, :);
            
        end
        
        % Sub-Solvers
        function obj = FixedStep(obj,T,nSteps)
        end
        
        function obj = NewMarkBeta(obj, T, dt)
            % This function uses implicit integration of a nonlinear finite
            % element model. 
            % Inputs :
            %          obj - dynamic object
            %          T - time to solve for

            
            % Shorthand for initial velocity and displacement
            if isempty(obj.initialDisplacement)
                x0 = zeros(obj.osFern.nDof, 1); 
            else
                x0 = obj.initialDisplacement;
            end
            
            if isempty(obj.initialVelocity)
                v0 = zeros(obj.osFern.nDof, 1); 
            else
                v0 = obj.initialVelocity;
            end
            
            
            % Obtain response DOF
            responseDof = [];
            for i = 1:length(obj.monitor)
                responseDof = unique([responseDof, obj.monitor(i).dof]);
            end
            
            % Setup plotting if desired
            if obj.plotOpts.logic
                obj = obj.InitializePlot(x0);
            end 
            
            % Obtain time vector
            obj.t = 0:dt:T; 
            if obj.t(end) < T
                obj.t = [obj.t, obj.t(end) + dt];
            end
            dt2 = dt^2;
            nstep = length(obj.t);            
            
            % compute the linear static response at the first load step
            obj.solution.displacement = zeros(obj.osFern.nDof, nstep);
            obj.solution.velocity = zeros(obj.osFern.nDof, nstep);
            obj.solution.acceleration = zeros(obj.osFern.nDof, nstep);
            
            obj.solution.displacement(:, 1) = x0;
            obj.solution.velocity(:, 1) = v0;
            
            % Form transformed stiffness and damping matrices
            Km = obj.GetStiffnessMatrix();
            Mm = obj.GetMassMatrix();
            Cm = obj.GetRayleighDamping(Km, Mm);
            
            % compute constant terms in Keff
            Keff0 = 4*Mm/dt2 + 2*Cm/dt;
            fold = obj.AssembleForce(0);
            
            % initialize temporary vectors of modal displacements, dold,
            % velocities, vold, and accelerations, aold
            dold = x0(obj.osFern.freeDof);
            dfull = x0;
            vold = v0(obj.osFern.freeDof);
            aold = 0*x0(obj.osFern.freeDof);
            
            Kto = obj.GetNLTangentStiffness(dfull);
            Ktold = Km + Kto; % Not sure if tangent or actual force
            
            % compute initial modal accelerations
            aold = Mm\(fold - Cm*vold - Ktold*dold);
            dnew = dold;
            
            % Check if parallel options are desired
            if obj.osFern.parallelOpts.parallel
                delete(gcp('nocreate')); % delete current pool if present
                pPool = parpool('local',obj.osFern.parallelOpts.numWorkers);
                % This doesn't work for me (JDS) but it's probably a 
                % version thing, not concerned with it. 
                parallel.pool.Constant(obj)
            end
            
            % perform the integration loop for (npts - 1) time points
            for ii = 2:nstep
                err = 1;
                dtemp = dold;
                
                % Assemble new force vector
                Fnew = obj.AssembleForce(obj.t(ii));                
                
                % compute the effective load
                Reff = Fnew + Mm*4*(dold + dt*vold + (dt2/4)*aold)/dt2 ...
                    + Cm*(2*dold/dt + vold);
                
                % loop to converge on dnew
                niter = 0;
                while err > obj.tol
                    niter = niter + 1;

                    % test for non-convergence
                    if niter > obj.integrator.maxIters
                        disp(['Fatal Error - Reached Maximum Number of Iterations : ', num2str(obj.integrator.maxIters),' without convergence'])
                        disp([' at time step ',num2str(ii)])
                        return
                    end
                    
                    % build the nonlinear modal stiffness terms
                    if mod(niter+1,obj.integrator.quasiRate) == 0 || niter == 1
                        dfull(obj.osFern.freeDof) = dnew;
                        Knnew = Km + obj.GetNLStiffness(dfull);
                        Keff = Keff0 + Knnew;
                    end
                    
                    % compute new displacements
                    dnew = Keff\Reff;
                    
                    % compute the error between dnew and dold
%                     err = max(abs((dnew - dtemp)./dnew));
                    err = max(abs((dnew - dtemp)))/norm(dnew);
                    
                    dtemp = dnew;
                    
                end
                
                % Output message
                if obj.printOpts.logic && mod(ii,obj.printOpts.frameRate) == 0 
                    fprintf('Increment %i of %i complete in %i iterations\n', ii, nstep, niter);
                end
                
                % compute new values for velocity and acceleration
                vnew = -vold+2*(dnew-dold)/dt;
                anew = 4*(dnew-dold)/dt2-4*vold/dt-aold;
                
                % Store old values
                dold = dnew;
                vold = vnew;
                aold = anew;                
               
                % Save converged solutions
                obj.solution.displacement(obj.osFern.freeDof, ii) = dnew;
                obj.solution.velocity(obj.osFern.freeDof, ii) = vnew;
                obj.solution.acceleration(obj.osFern.freeDof, ii) = anew;
                
                % Calculate new stepsize
                
                
                % Plot if desired
                if obj.plotOpts.logic && mod(ii,obj.plotOpts.frameRate) == 0
                    switch obj.plotOpts.field
                       case 'accleration' 
                           obj = UpdatePlot(obj,obj.solution.acceleration(:,ii),ii);
                       case 'velocity'
                           obj = UpdatePlot(obj,obj.solution.velocity(:,ii),ii);
                       case 'displacement'
                           obj = UpdatePlot(obj,obj.solution.displacement(:,ii),ii);
                    end
                end

            end
            
            % Interploate final solution so its at desired time
            [xF] = interp1([obj.t(end-1),obj.t(end)],...
                           [obj.solution.displacement(:, end-1),...
                           obj.solution.displacement(:, end)]',...
                           T);
            [vF] = interp1([obj.t(end-1),obj.t(end)],...
                           [obj.solution.velocity(:, end-1),...
                           obj.solution.velocity(:, end)]',...
                           T);
            [aF] = interp1([obj.t(end-1),obj.t(end)],...
                           [obj.solution.acceleration(:, end-1),...
                           obj.solution.acceleration(:, end)]',...
                           T);
            obj.solution.displacement(:, end) = xF;
            obj.solution.velocity(:, end) = vF;
            obj.solution.acceleration(:, end) = aF;
            obj.t(end) = T;
            
            % Store final values           
            obj.finalDisplacement = obj.solution.displacement(:, end);
            obj.finalVelocity = obj.solution.velocity(:, end);
            obj.response = obj.solution.displacement(responseDof, :);
            
        end
        
        function [t,zp0] = SolveInitialDisp(obj, T, nSteps, zp0)
            % This is still almost directly copied from the old Bobtran
            % "nlstat" function. 
            % A big thing to note is that I'm writing everything using a
            % fully unconstrained solution vector. This is for ease of
            % indexing, especially with the NL assembly. It has to come at
            % some sort of performance cost, but until we can quantify
            % exactly what that penalty is, I don't want to bother messing
            % with it. (Right now, the vast majority of the solution time
            % is spent in element assembly.)
            
            % Shorthand for initial velocity and displacement
            x0 = zp0(1:obj.osFern.nDof);
            v0 = zp0(obj.osFern.nDof+1:end);
               
            % Obtain response DOF
            responseDof = [];
            for i = 1:length(obj.monitor)
                responseDof = unique([responseDof, obj.monitor(i).dof]);
            end
            
            % Setup plotting if desired
            if obj.plotOpts.logic
                obj = obj.InitializePlot(x0);
            end 
            
            % Obtain time vector
            obj.t = linspace(0,T,nSteps);
            dt = T/nSteps; dt2 = dt^2;
            nstep = length(obj.t);            
            
            % compute the linear static response at the first load step
            obj.solution.displacement = zeros(obj.osFern.nDof, nstep);
            obj.solution.velocity = zeros(obj.osFern.nDof, nstep);
            obj.solution.acceleration = zeros(obj.osFern.nDof, nstep);
            
            obj.solution.displacement(:, 1) = x0;
            obj.solution.velocity(:, 1) = v0;
            
            % Form transformed stiffness and damping matrices
            Km = obj.GetStiffnessMatrix();
            Mm = obj.GetMassMatrix();
            Cm = obj.GetRayleighDamping(Km, Mm);
            
            % compute constant terms in Keff
            Keff0 = 4*Mm/dt2 + 2*Cm/dt;
            fold = obj.AssembleForce(0);
            
            % initialize temporary vectors of modal displacements, dold,
            % velocities, vold, and accelerations, aold
            dold = x0(obj.osFern.freeDof);
            dfull = x0;
            vold = v0(obj.osFern.freeDof);
            aold = 0*x0(obj.osFern.freeDof);
            
            Kto = obj.GetNLTangentStiffness(dfull);
%             Kto = obj.osFern.GetTangentStiffnessPar(dfull);
            Ktold = Km + Kto; % Not sure if tangent or actual force
            
            % compute initial modal accelerations
            aold = Mm\(fold - Cm*vold - Ktold*dold);
            dnew = dold;
            
            % Check if parallel options are desired
            
            if obj.parallelOpts.logic
%                 delete(gcp('nocreate')); % delete current pool if present
%                 parpool('local',obj.osFern.parallelOpts.numWorkers);
                eV = obj.osFern.element;
                sharedObj = parallel.pool.Constant(eV);
            end
            
            % perform the integration loop for (npts - 1) time points
            tStart = tic;
            for ii = 2:nstep
                err = 1;
                dtemp = dold;
                
                % Assemble new force vector
               Fnew = obj.AssembleForce(obj.t(ii));                
                
                % compute the effective load
                Reff = Fnew + Mm*4*(dold + dt*vold + (dt2/4)*aold)/dt2 ...
                    + Cm*(2*dold/dt + vold);
                
                % loop to converge on dnew
                niter = 0;
                while err > obj.tol
                    niter = niter + 1;

                    % test for non-convergence
                    if niter > obj.integrator.maxIters
                        disp(['Fatal Error - Reached Maximum Number of Iterations : ', num2str(obj.integrator.maxIters),' without convergence'])
                        disp([' at time step ',num2str(ii)])
                        return
                    end
                    
                    % build the nonlinear modal stiffness terms
                    if mod(niter+1,obj.integrator.quasiRate) == 0 || niter == 1
                        dfull(obj.osFern.freeDof) = dnew;
                        dParallel = parallel.pool.Constant(dfull);
                        if obj.osFern.parallelOpts.parallel % Currently not working
                            % Allocate memory
                            Knv = cell(length(eV),1); Ktv = cell(length(eV),1);
                            parfor (kk = 1:length(eV), obj.parallelOpts.numWorkers)
                                a = length([sharedObj.Value{kk}.nodes])
                                tempNodeVec = ([sharedObj.Value{kk}.nodes{:}]);
                                xEle = dParallel([tempNodeVec.globalDof]);
                                [KnEle,KtEle] = sharedObj.Value{kk}.buildNLStiff(xEle);
                                Knv{kk} = [sharedObj.Value{kk}.rid, sharedObj.Value{kk}.cid, KnEle];
                                Ktv{kk} = [sharedObj.Value{kk}.rid, sharedObj.Value{kk}.cid, KtEle];
                            end
                            
                            % Expand to matrix
                            Knm = cell2mat(Knv);
                            Ktm = cell2mat(Ktv);

                            % Build sparse matrices
                            Kn = sparse(Knm(:, 1), Knm(:, 2), Knm(:, 3), obj.nDof, obj.nDof);
                            Kt = sparse(Ktm(:, 1), Ktm(:, 2), Ktm(:, 3), obj.nDof, obj.nDof);

                            Kn = Kn(obj.freeDof, obj.freeDof);
                            Kt = Kt(obj.freeDof, obj.freeDof);
                            Ktnew = Km + Kt;
                        else
                            Knnew = Km + obj.GetNLStiffness(dfull);
                        end
                        Keff = Keff0 + Knnew;
                    end
                    
                    % compute new displacements
                    dnew = Keff\Reff;
                    
                    % compute the error between dnew and dold
%                     err = max(abs((dnew - dtemp)./dnew));
                    err = max(abs((dnew - dtemp)))/norm(dnew);
                    
                    dtemp = dnew;
                    
                end
                
                % Output message
                if obj.printOpts.logic
                    if mod(ii,obj.printOpts.frameRate) == 0
                        fprintf('Increment %i of %i complete in %i iterations\n', ii, nstep, niter);
                    end
                end
                
                % compute new values for velocity and acceleration
                vnew = -vold+2*(dnew-dold)/dt;
                anew = 4*(dnew-dold)/dt2-4*vold/dt-aold;
                
                % Store old values
                dold = dnew;
                vold = vnew;
                aold = anew;                
               
                % Save converged solutions
                obj.solution.displacement(obj.osFern.freeDof, ii) = dnew;
                obj.solution.velocity(obj.osFern.freeDof, ii) = vnew;
                obj.solution.acceleration(obj.osFern.freeDof, ii) = anew;
                
                % Calculate new stepsize
                
                
                % Plot if desired
                if obj.plotOpts.logic && mod(ii,obj.plotOpts.frameRate) == 0
                    switch obj.plotOpts.field
                       case 'accleration' 
                           obj = UpdatePlot(obj,obj.solution.acceleration(:,ii),ii);
                       case 'velocity'
                           obj = UpdatePlot(obj,obj.solution.velocity(:,ii),ii);
                       case 'displacement'
                           obj = UpdatePlot(obj,obj.solution.displacement(:,ii),ii);
                    end
                    % Force update
                    drawnow
                end
                
            end
            tEnd = toc(tStart);
            
            obj.finalDisplacement = obj.solution.displacement(:, end);
            obj.finalVelocity = obj.solution.velocity(:, end);
            obj.response = obj.solution.displacement(responseDof, :);
            
            % Ouptut
            t = obj.t;
            zp0 = [obj.solution.displacement' obj.solution.velocity'];
            
            % If closing figure
            if obj.plotOpts.close
                close(fgn)
            end
        end
        
        % State matrix extraction procedures
        function K = GetStiffnessMatrix(obj)
            % Obtain the base state stiffness matrix
            K = obj.osFern.GetStiffnessMatrix(); 
        end
        
        function M = GetMassMatrix(obj)
            % Obtain the mass matrix
            M = obj.osFern.GetMassMatrix(); 
        end
        
        function C = GetRayleighDamping(obj, K, M)
            % Obtain the Rayleigh damping matrix. Must supply K and M
            % matrices to compute the damping matrix.
            C = obj.damping.alpha*M + obj.damping.beta*K;
        end
        
        function Kt = GetNLTangentStiffness(obj, x)
            % Obtain the nonlinear contribution to the tangent stiffness
            % matrix, i.e. Ktangent = Kbase + Kt(x)
            [~, Kt] = obj.osFern.GetTangentStiffness(x);
        end
        
        function Kn = GetNLStiffness(obj, x)
            % Obtain the nonlinear contribution to the true stiffness
            % matrix, i.e. fStiffness = (Kbase + Kn(x))*x
            [Kn, ~] = obj.osFern.GetTangentStiffness(x);
        end
        
        % Set methods
        function obj = SetIntegrator(obj,quasiRate,stepControl)
            obj.integrator.quasiRate = quasiRate;
        end
        
        function obj = SetDamping(obj,alpha,beta)
            % Currently only proportional damping is used
            obj.damping.alpha = alpha;
            obj.damping.beta = beta;
        end
        
        function obj = SetEigenComp(obj,logic,tPoints,numEigs)
            obj.eigenComp.logic = logic;
            obj.eigenComp.tPoints = tPoints;
            obj.eigenComp.numEigs = numEigs;
        end
        
        % Disable "addEnforced" method
        function addEnforced(obj, varargin)
            error('Not implemented.');
        end
                
    end
    
end