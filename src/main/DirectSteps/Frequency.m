

classdef Frequency < DirectStep
    
    properties
        Kss
        fn
        phi
        
    end
    
    methods
        
        function obj = Frequency(name, osFern, initialDisplacement,varargin)
            % Eigenvalue solver for OsFern Objects
            %
            % obj = Frequency(name, osFern, initialDisplacement, varargin)
            %
            % 'name' = User defined name for this solution step
            % 'OsFern' = OsFern object defining FEM
            % [optional] 'initialDisplacement' = Deformation of FEM about
            %       which to compute the mode. If this is supplied, the FEM
            %       is deformed into this displacement state, the tangent
            %       stiffness matrix is calculated and then the eigenvalue
            %       problem is solved.
            %
            %
            obj = obj@DirectStep(name, osFern);
            
            if nargin == 3
                obj.initialDisplacement = initialDisplacement;
            end
            
            if nargin > 3
                obj.Kss = varargin{1};
            end
        end
        
        function obj = Solve(obj, nModes, noEigsFlag)
            
            % Extract mass and stiffness matrices, with fixed DOF already
            % removed.
            M = obj.osFern.GetMassMatrix();
            K = obj.osFern.GetStiffnessMatrix();
            
            % Add nonlinear tangent stiffness if needed
            if ~isempty(obj.initialDisplacement)
                [~, Kt] = obj.osFern.GetTangentStiffness(obj.initialDisplacement);
                K = K + Kt;
            end
            
            if ~isempty(obj.Kss) 
                K = K + obj.Kss;
            end
            
            if nargin < 3
                [psi, lambda] = eigs(K, M, nModes, 'SM');
            elseif noEigsFlag == true
                [psi, lambda] = eig(full(K), full(M));
                psi = psi(:, 1:nModes); lambda = lambda(1:nModes, 1:nModes);
            end
            
            % Mass Normalize Modes
            for i = 1:nModes
                psi(:, i) = psi(:, i)/sqrt(psi(:, i)'*M*psi(:, i));
            end
            

            % Sort results
            [lambda, I] = sort(diag(lambda));
            obj.phi = zeros(obj.osFern.nDof, nModes);
            obj.phi(obj.osFern.freeDof, :) = psi(:, I);
            obj.fn = sqrt(lambda)/(2*pi);
            fprintf('Found %i modes, stored in the object, i.e. obj.fn, etc...\n',length(lambda));
            
        end
        
        % Disable "addForce" and "addEnforced" methods
        function addForce(obj, varargin)
            error('Cannot add forces to frequency step.');
        end
        
        function addEnforced(obj, varargin)
            error('Cannot add enforced displacement to frequency step.');
        end
        
    end
    
end