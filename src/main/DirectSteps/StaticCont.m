classdef StaticCont < handle
    % This class implements a static continuation routine to compute
    % branches of. This is based upon riks method. Additional enhancements
    % include branch switchign and identification of bifurcation points.
    %   Detailed explanation goes here
    
    properties
        name    % name of object
        osFern      % matlFem Object
        nlStatic% Nonlinear static object
        nDof    % Number of DOF of the model
        fDof    % Number of free DOF of the model
        fDofVec % Free dof vector
        fExt    % External force vector [fDoF,1]
        K       % Linear Stiffness K = K(fDofVec,fDofVec)
        % Control Parameters
        contOpts  = struct('stpInt',1e-4,'stpMin',1e-20,'stpMax',1e2,...
                           'optIter',10,'tol',1e-4,'numSols',200,...,
                           'numModes',20,'gui',false,'maxIter',20,...
                           'gamma',1e-4,'ei',[],'phi0',[],'nSols',2000,...
                           'lamBound',[-1e6,1e6]);
        % Values at each converged solution point
        solpt   % Current Solution Number
        x       % Displacement Amplitudes
        lam     % Load Scaling Factors
        mu      % Penalty values
        energy  % Energy values
        eigVecs % Eigenvectors of Tangent Stiffness [nTerms x nDof x Nsols]
        eigVals % Eigenvalues  of Tangent Stiffness [nDof x Nsols]
        residual       % Equilibrium Equation Residual     [nDof x Nsols]
        p       % Predictor                         [nDof+1 x Nsols]
        numIters % number of iterations required to converge [nSol x 1]
        stepSize % step size at each solution [nSol x 1]
        bifurCalc = struct('logic',true,'rate',10,'tol',1e-3)
        plotOpts = struct('logic',true,'dof2plot',[]);
        % Plotting Parameters
        gui      % if gui == 'on, use gui / if gui == 'off' no gui 
    end
    
    methods
        % Class Constructor
        function [obj] = StaticCont(name,osFernObj)
            
            % Store data
            obj.name = name;
            obj.osFern = osFernObj;
            obj.nDof = obj.osFern.nDof;
            
            % Obtain vector of free DOF
            obj.fDofVec = find(obj.osFern.freeDof);
            obj.fDof = length(obj.fDofVec);
            
            % Assemble stiffness matrix 
            obj.K = obj.osFern.GetStiffnessMatrix();
        end
        
        % Starts Continuation Routine
        function [obj] = Start(obj,fExt,varargin)
            % This starts the continuation routine
            
            % Store Data
            if length(fExt) ~= obj.nDof
                error('Size of force vector is incorrect size')
            else
                obj.fExt = fExt(obj.fDofVec);
            end
            
            % Save Controls
%             if nargin > 2
%                 obj = obj.CheckInputs(varargin{1});
%             else
%                  obj = obj.CheckInputs([]);
%             end
                obj.contOpts.lambda0 = 1e-4;
                obj.contOpts.x0 = obj.K\(obj.contOpts.lambda0*obj.fExt);
            
            
            % Initialize Continuation Variables
            obj.solpt = 1;
            obj.x = zeros(obj.fDof,obj.contOpts.numSols);
            obj.lam = zeros(1,obj.contOpts.numSols);
            obj.p = zeros(obj.fDof+1,obj.contOpts.numSols);
            obj.residual = zeros(obj.fDof,obj.contOpts.numSols);
            obj.energy = zeros(1,obj.contOpts.numSols);
            obj.eigVals = zeros(obj.contOpts.numModes,obj.contOpts.numSols);
            obj.eigVecs = cell(obj.contOpts.numSols,1);
            obj.lam(1) = 1e-6;
            
            % Output Some Information
            disp(['There are ',num2str(size(obj.p,1)),' Continuation Variables'])
            
            if obj.contOpts.gui
                % Start GUI
                StaticContGui('initialize',obj);
            else
                
                obj.InitializePlot();
                
                % Run Simulation
                [obj] = obj.Continue();
                
                % Reduce Down if Not All the Solutions were filled
                obj.x = obj.x(:,1:obj.solpt-1);
                obj.p = obj.p(:,1:obj.solpt-1);
                obj.lam = obj.lam(:,1:obj.solpt-1);
%                 obj.residual = obj.G(1,1:obj.solpt-1);
                obj.energy = obj.energy(1,1:obj.solpt-1);
                obj.eigVals = obj.eigVals(:,1:obj.solpt-1);
                obj.eigVecs = obj.eigVecs{1:obj.solpt-1};
                
                % Identify Data to Save
                SCrun = obj; %#ok<NASGU>
                
                % Save Class Data
%                 save(obj.Name,'SCrun')
                
            end
        end
        
        % Continues along branch, stores info and plots info
        function [obj] = Continue(obj)
            % This is a arc-length based control continuation routine
            % to compute the static-equilibrium path of a structure. It
            % will still compute and plot eigenvalues along the path (for
            % stability identification).
            
            
            if obj.solpt==1
                obj.stepSize(1) = obj.contOpts.stpInt;
                
                % Start Solution Count
                obj.contOpts.stp(1) = obj.contOpts.stpInt;
                xC = obj.contOpts.x0; lamC = obj.contOpts.lambda0;
                obj.p(:,obj.solpt) = [zeros(obj.fDof,1);...
                                      obj.contOpts.lambda0]; 
                obj.p(:,obj.solpt) = obj.p(:,obj.solpt)/norm(obj.p(:,obj.solpt));
                
                % Evaluate Initial guess (check it is actually a solution)
                gPr = obj.CalcFint(xC,lamC);
                
                % Check if Residual Passed
                if norm(gPr) < obj.contOpts.tol
                    disp('Initial Condition Guess is Correct')
                    obj.contOpts.numks(obj.solpt) = 0;
                    
                    % Save Converged Values
                    obj.x(:,obj.solpt) = obj.contOpts.x0;
                    obj.lam(obj.solpt) = obj.contOpts.lambda0;
                    
                    % Asseble Kt
                    xIn = zeros(obj.nDof,1);
                    xIn(obj.fDofVec) = xC;
                    [Kn,Kt] = obj.osFern.GetTangentStiffness(xIn);
                    obj.energy(obj.solpt) = obj.CalcEnergy(xC,lamC);
                    
                    % Compute eigenvalues and eigenvectors
                    try 
                        [phi,lambda] = eigs(Kn,obj.contOpts.numModes,'SM');
                        obj.eigVals(:,obj.solpt) = diag(lambda);
                        obj.eigVecs{obj.solpt} = phi; % Need to normalize to stiffness
                    catch
                        obj.eigVals(:,obj.solpt) = 0;
                        obj.eigVecs{obj.solpt} = 0; % Need to normalize to stiffness
                    end
                else
                    disp('Initial Conditions Guess is Incorrect -> Start Correction')
                    [obj] = obj.Correct(gPr);
                end
                
                % Store Some Stuff
                obj.contOpts.endcont = 1;
                obj.solpt = obj.solpt + 1;
                obj.contOpts.stp(obj.solpt) = obj.contOpts.stpInt;
                
                % 
            end
            
            % Continue until certain conditions are broken
            while obj.contOpts.endcont ~= 0
                
                
                % Call Predictor Function
                [obj,xPr,lamPr] = obj.Predict();
                
                % Check for Bifucation Point at Previous Solution
%                 if obj.bifurCalc.logic && mod(obj.bifurCalc.rate,obj.solpt)
%                     [SPlogic,xBPpr] = obj.DetectBifurcation();
%                 else
%                     SPlogic = 0;
%                 end
%                 
%                 if SPlogic == 1
%                     [obj] = obj.PlotBranches(xpr,lamPr,xBPpr,lamBPpr);
%                     [obj,xpr,lampr] = obj.ChoosePath(xpr,lampr,xBPpr,lamBPpr);
%                 end
%                 
                
                % Compute static residual
                gPr = obj.CalcFint(xPr,lamPr);
                
                disp(['Residual = ',num2str(norm(gPr))])
                disp(['lambda = ',num2str(norm(lamPr))])
                
                % Check Residual Equation
                if norm(gPr)/norm(xPr) < obj.contOpts.tol
                    disp('Initial Prediction Meets Tolerance')
                    % Save Converged Values
                    obj.x(:,obj.solpt) = xPr;
                    obj.lam(obj.solpt) = lamPr;
                    obj.contOpts.numks(obj.solpt) = 0;
                    obj.numIters(obj.solpt) = 0;
                else
                    disp('Initial Prediction Does Not Meets Tolerance --> Start Correction')
                    [obj] = obj.Correct(gPr,xPr,lamPr);
                end
                
                % Asseble Kt
                xIn = zeros(obj.nDof,1);
                xIn(obj.fDofVec) = obj.x(:,obj.solpt);
                [Kn,Kt] = obj.osFern.GetTangentStiffness(xIn);
                
                % Calculate Other Quantities
                obj.energy(obj.solpt) = obj.CalcEnergy(obj.x(:,obj.solpt),obj.lam(obj.solpt));
                
                % Calculate eigenvectors of nonlinear stiffness
                try
                    [phi,lambda] = eigs(Kn,obj.contOpts.numModes,'SM');
                    obj.eigVals(:,obj.solpt) = diag(lambda);
                    obj.eigVecs{obj.solpt} = phi;
                catch
                    obj.eigVals(:,obj.solpt) = 0;
                    obj.eigVecs{obj.solpt} = 0;
                end
                
                % Plot Solution
                if obj.contOpts.gui
                    WiscSCGUI('plotSOL')
                end
                
                % Define Breaking Condtions
                if obj.stepSize(obj.solpt) < obj.contOpts.stpMin
                    disp('Minimum Step Size Reached --> Ending Continuation')
                    obj.contOpts.endcont = 0;
                elseif obj.numIters(obj.solpt) > obj.contOpts.maxIter
                    disp('Maximum Number of Iteration Attempted (Non-Convergent) --> Ending Continuation')
                    obj.contOpts.endcont = 0;
                elseif obj.lam(obj.solpt) < obj.contOpts.lamBound(1) || obj.lam(obj.solpt) > obj.contOpts.lamBound(2)
                    disp('Load Boundary Exceeded --> Ending Continuation')
                    obj.contOpts.endcont = 0;
%                 elseif any(abs(obj.x(:,obj.solpt)) > obj.contOpts.dispBound)
%                     disp('Displacement Boundary Exceeded --> Ending Continuation')
%                     obj.contOpts.endcont = 0;
                elseif obj.solpt > obj.contOpts.nSols
                    disp('Maximum Number of Solutions Computedd --> Ending Continuation')
                    obj.contOpts.endcont = 0;
                end
                
%                 figure(100);
%                 plot(obj.lam(1:obj.solpt),obj.eigVals(1:2,1:obj.solpt),'-o');
                
                % Plot the current results 
                obj.UpdatePlot();

                % Add to solution count
                obj.solpt = obj.solpt + 1;
                

            end
            
        end
        
        % Predicts next solutions
        function [obj,xPr,lamPr] = Predict(obj,varargin)
            % This function performs the prediction portion of the
            % predictor corrector algorithm
            % Uses -- 
            
            % Number of DOF in Problem 
            N = obj.fDof;
            
            if nargin < 2
                % Generate Tangent Stiffness if not supplied
                xIn = zeros(obj.nDof,1);
                xIn(obj.fDofVec) = obj.x(:,obj.solpt-1);
                [~, Kt] = obj.osFern.GetTangentStiffness(xIn);
                dHdx = obj.K + Kt;
            else
                % Use provided
                dHdx = varargin{1};
            end
            
            % Generate Prediction Matrix
            dHdlam = obj.fExt;
            AAp = [-dHdx, dHdlam;...
                   obj.p(:,obj.solpt-1)'];
            
            % ----  Generate and Normalize Prediction Vector ----
            BBp = [zeros(obj.fDof,1); 1];
            pTemp = AAp\BBp;
            obj.p(:,obj.solpt) = pTemp/norm(pTemp); % Normalzied
            
            % Check if sign changes (limit point)
            if sign(obj.p(end,obj.solpt)) ~= sign(obj.p(end,obj.solpt-1))
                disp('The sign on the prediction vector changed')
                pause(3)
            end

            % Calculate Step Size for Curent Solution Point
            if obj.solpt > 2
               obj = obj.StepSizeCalc();
               disp(['Current Stepsize: ',num2str(obj.stepSize(obj.solpt)),...
                     ', Num. Iter Prev. ',num2str(obj.numIters(obj.solpt-1))]);
            else
                obj.stepSize(obj.solpt) = obj.contOpts.stpInt;
                disp(['Initial Stepsize: ',num2str(obj.contOpts.stp(obj.solpt))]);
            end
            
            % Evaluate Predictions
            xPr = obj.x(:,obj.solpt-1) + obj.stepSize(obj.solpt)*obj.p(1:N,obj.solpt);
            lamPr = obj.lam(obj.solpt-1) + obj.stepSize(obj.solpt)*obj.p(end,obj.solpt);
            
        end
        
        % Correct current solution
        function [obj,varargout] = Correct(obj,Gpr,xPr,lamPr)
            % This function performs the correction portion of the
            % predictor corrector algorithm
            % Only initiatied if the intial guess is > allowed tolerance
            
            % Bring in previous solution values current solution values
            if obj.solpt == 1
                x0 = obj.contOpts.x0; xC = x0;
                lam0 = obj.contOpts.lambda0; lamC = lam0;
                G0=Gpr; GC=G0;
            else
                x0 = obj.x(:,obj.solpt-1); xC = xPr;
                lam0 = obj.lam(obj.solpt-1); lamC = lamPr;
                G0=Gpr; GC=G0;
            end
     
            % --- Start Correction Loop --- % 
            k=0; % Initialize # of iterations
            while norm(GC)/norm(xC) > obj.contOpts.tol
                
                % Extract Force Vector
                dHdlam = obj.fExt;
                
                % Expand to full Fe
                xIn = zeros(obj.nDof,1);
                xIn(obj.fDofVec) = xC;
            
                % Generate Tangent Stiffness
                [~,Kt] = obj.osFern.GetTangentStiffness(xIn);
                dHdx = obj.K + Kt;
                
                % Generate Update Matrix
                AA2 = [-dHdx, dHdlam; ...
                       obj.p(:,obj.solpt)']; 
                BB2 = [GC; 0];
                
                % Basic Linear System Update 
                correct = -AA2\BB2;
                
                % ------- Final Step (update values)----------
                xC = xC + correct(1:end-1);
                lamC = lamC + correct(end);
                
                % --- Check Residual --- %
                GC = obj.CalcFint(xC,lamC);
                
                k=k+1; % Update Iteration Count
%                 disp(['Iteration : ',num2str(k),' Residual = ',num2str(norm(GC)/norm(xC))])
                
                % ------- IF CONVERGENCE NOT MET ----------- %
                if  k > obj.contOpts.optIter || norm(GC)/norm(xC) > 10*norm(G0)/norm(x0)

                    % Determine what the problem is 
                    if k >= obj.contOpts.optIter
                        disp('No Optimal Convergence --- Reducing Step Size');
                    elseif norm(GC)/norm(xC) > norm(G0)/norm(x0)
                        disp('Residue is increasing --- Reducing Step Size');
                    end
                    
                    % Reduce stepsize of predictor if convergence isn't met
                    obj.stepSize(obj.solpt) = obj.stepSize(obj.solpt)/2; %obj.contOpts.stepSizeReduction;
                    if abs(obj.stepSize(obj.solpt)) < obj.contOpts.stpMin % stepsize is below the minimum
                        obj.stepSize(obj.solpt) = obj.contOpts.stpMin;
%                         obj.endcont = 1;
                        disp('Minimum step size reached, algorithm terminating');
                    else
                        disp(['New Stepsize: ',num2str(obj.stepSize(obj.solpt))]);
                    end
                    
                    % --- Evaluate the New Prediction --- %  
                    xPr = x0 + obj.stepSize(obj.solpt)*obj.p(1:obj.fDof,obj.solpt);
                    lamPr = lam0 + obj.stepSize(obj.solpt)*obj.p(end,obj.solpt);
                                   
                    % Check Residual based on type
                    Gpr = obj.CalcFint(xPr,lamPr);
                    
                    % Update Values
                    k = 0; xC = xPr; lamC = lamPr; GC = Gpr;
                end
            end
            
            % Save Converged Values
            obj.x(:,obj.solpt) = xC;
            obj.lam(obj.solpt) = lamC;
            obj.numIters(obj.solpt) = k;
            
            if nargout > 1
                varargout{1} =dHdx;
            end
        end
        
        % Calculates step size
        function [obj] = StepSizeCalc(obj)
            Csign = sign(obj.stepSize(obj.solpt-1)*(obj.p(:,obj.solpt).'*obj.p(:,obj.solpt-1)));
            rat = obj.contOpts.optIter/(obj.numIters(obj.solpt-1)+1);
            if rat > 1.5
                rat = 1.5;
            end
            tempSTP = Csign*rat*abs(obj.stepSize(obj.solpt-1));
            if abs(tempSTP) > obj.contOpts.stpMax
                tempSTP = sign(tempSTP)*obj.contOpts.stpMax;
            end
            if abs(tempSTP) < obj.contOpts.stpMin
                tempSTP = sign(tempSTP)*obj.contOpts.stpMin;
            end
            obj.stepSize(obj.solpt)=tempSTP;
        end
        
        % Calculates internal residual
        function [fInt] = CalcFint(obj,xC,lamC)
            
            % Expand to full Fe
            xIn = zeros(obj.nDof,1);
            xIn(obj.fDofVec) = xC;
            
            % Extract Tangent Stiffness
            [Kn,Kt] = obj.osFern.GetTangentStiffness(xIn);
            
            % Internal force 
            fInt = - (obj.K+Kn)*xC + obj.fExt*lamC;
        end
        
        % Calculates energy
        function [energy] = CalcEnergy(obj,xC,lamC,varargin)
            
            % Expand to full Fe
            xIn = zeros(obj.nDof,1);
            xIn(obj.fDofVec) = xC;
            
            if nargin > 3
                Kt = varargin{1};
            else
                Kt = obj.osFern.GetTangentStiffness(xIn);
            end
            
            energy = 0.5*xC'*(obj.K+Kt)*xC + lamC*xC'*obj.fExt;
        end
        
        % Check for bifurcations
        function [SPlogic,xpr] = DetectBifurcation(obj)
            % This is a simplified function to evaluate if a a bifucation
            % point is identified.
            % Inputs
            %
            % Outputs :
            %          SPlogic = 1 if Stability Point is Found
            %                    0 if no bifurcaation Point foun
            %          xout = predicted displacement state
            %          lamout = predicted scalign factor
            
            
            tau = 100;
            if any(find(abs(obj.eigVals(:,obj.solpt-1))<obj.bifurCalc.tol))
                obj.eigVals(:,obj.solpt-1)
                disp('Bifurcation Point Detected');  pause(1);
                SPlogic = 1;
                index = find(abs(obj.eigVals(:,obj.solpt-1))<=obj.bifurCalc.tol);
                if length(index) > 1
                    disp('Mulitple Bifucation Points Detected'); pause(1)
                end
                xpr = obj.x(:,obj.solpt-1);
                EVS = obj.eigVecs{obj.solpt-1};
                for jj = 1:length(index)
                    xpr = xpr + (norm(xpr)/(tau*norm(EVS(:,jj)))*EVS(:,jj));
                end
            else
                SPlogic = 0;
                xpr = [];
            end
            
            
        end

        % Check Inputs
        function [obj] = CheckInputs(obj,controls)
            % This function checks the controls provided by the user, and
            % those that are not and sets them within the class.
            
            
            obj.solpt              = [];    % Solution Point
            obj.contOpts.stpint   = 1e-1;  % Initial step size
            obj.contOpts.stpmin   = 1e-10; % Min step size
            obj.contOpts.stpMax   = 10;    % Max step size
            obj.contOpts.lamBounds  = [-1000 1000]; % frequency boundary
            obj.contOpts.tol      = 1e-2;  % Convergence tolerance
            obj.contOpts.optiter  = 10;     % Optimal # of iterations.
            obj.contOpts.maxiter  = 50;    % Maximum # of iterations.
            obj.contOpts.nSols    = 5000;   % Max number of solutions
            obj.contOpts.endcont  = 1;     % Run Continuation Condition
            
            if ~isfield(controls,'lambda0')
                obj.contOpts.lambda0 = 1e-4;
            end
            if ~isfield(controls,'x0')
                obj.contOpts.x0 = obj.K\(obj.contOpts.lambda0*obj.fExt);
            end
                
        end
        
        % Initialize Plot
        function obj = InitializePlot(obj)
            
                
            % Change title name
            title = 'Static Riks Continuation';
            
            % Initialize Figure
            fgn = figure('Visible','on','Renderer','zbuffer','NumberTitle','off', ...
                'Name',[title,' Plot for FERN']);
            set(fgn,'uni','nor','position',[0.05 0.1 0.9 0.8],'Color',[1 1 1],'toolbar','figure');
            
            % First subplot is deformation at that point
            sp_axes{1} = subplot(2,1,1);
            
            % Expand to full fem dof
            fieldVec  = zeros(obj.osFern.nDof,1);
            fieldVec(obj.osFern.freeDof) = obj.x(:,1);
            
            
            % Use osFern plot tool 
            cla(sp_axes{1}); 
            hold(sp_axes{1},'on') 
            obj.osFern.Plot(fieldVec,'norm',sp_axes{1});
            hold(sp_axes{1},'off')
            
            % Second subplot is specific dof vs time
            sp_axes{2} = subplot(2,1,2);
            
            % Get number of dof to track
            nDof2Plot = length(obj.plotOpts.dof2plot);
            for ii = 1:nDof2Plot
                lString{ii} = [' DOF ', num2str(obj.plotOpts.dof2plot(ii))]; %#ok<AGROW>
            end
            x2Plot = obj.x(obj.plotOpts.dof2plot(:),1);
            sp_data{2} = plot(obj.lam(1),x2Plot,'LineStyle','-','LineWidth',2,'Marker','o','MarkerSize',18);
            legend(lString);
            set(sp_axes{2},'Units','normalized','Position',[0.05 0.1 0.9 0.4],'Box','on');
            set(sp_axes{2},'XScale','linear','YScale','linear','FontSize',11,'XGrid','on','YGrid','on');
            set(get(sp_axes{2},'XLabel'),'string','DOF Amplitude(-)','FontSize',16);
            set(get(sp_axes{2},'YLabel'),'string','\lambda Amplitude(-)','FontSize',16);
            
            % Save into object
            obj.gui.figHandle = fgn;
            obj.gui.axesHandle{1} = sp_axes{1};
            obj.gui.axesHandle{2} = sp_axes{2};
            obj.gui.dataHandle{1} = sp_data{1};
            obj.gui.dataHandle{2} = sp_data{2};
            
        end
                
        % Update Plot 
        function obj = UpdatePlot(obj)
            
           
            % Expand to full fem dof
            fieldVec  = zeros(obj.osFern.nDof,1);
            fieldVec(obj.osFern.freeDof) = obj.x(:,obj.solpt);
            
            % Use osFern plot tool 
            cla(obj.gui.axesHandle{1}); 
            hold(obj.gui.axesHandle{1},'on') 
            obj.osFern.Plot(fieldVec,'norm',obj.gui.axesHandle{1});
%             set(obj.gui.axesHandle{1},'view',[0 -1 0]);
%             view([0 -1 0])
            hold(obj.gui.axesHandle{1},'off')
                       
            % Plot specific dof vs time
            x2Plot = obj.x(obj.plotOpts.dof2plot(:),1:obj.solpt);
            for k = 1:size(x2Plot,1)
                set(obj.gui.dataHandle{2}(k),...
                    'XData',x2Plot(k,:),...
                    'YData',obj.lam(1:obj.solpt)); % hold on
            end
            set(get(obj.gui.axesHandle{2},'XLabel'),'string','DOF Amplitude(-)','FontSize',16);
            set(get(obj.gui.axesHandle{2},'YLabel'),'string','\lambda','FontSize',16);
            
            % Force update
            drawnow
        end
        
    end
    
end

