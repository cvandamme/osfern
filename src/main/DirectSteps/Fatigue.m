classdef Fatigue < DirectStep
    % This class contains some functionality to estimate the fatigue life
    % of structures. It is a fairly basic approach in which the finite
    % element model is simulated over a 
    %   Detailed explanation goes here
    
    properties
    end
    
    methods
        function obj = Fatigue(name,saveDir,mfObj,dynamicObj)
            
            if ~license('test', 'signalprocessing_toolbox')
                error('Must have Signal Processing Toolbox')
            end
            if version('-release') ~= '2018s'
                error('Must have 2018a atleast due to rainflow calculaator')
            end
        end
    end
    
end

