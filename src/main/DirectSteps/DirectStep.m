classdef DirectStep < handle
    % Superclass for direct steps. Direct steps operate on the full order mass
    % and stiffness matrices of the osFern object.
    
    % Using private properties because this will be more like what an
    % eventual C++ implementation will look like.
    properties(GetAccess = 'public', SetAccess = 'public')
        
        % Meta properties (most for convenience).
        name;
        
        % osFern object
        osFern;
        
        % Step options
        analysisOptions = struct;
        outputOptions = struct;
        
        % Constraints built into the model. Loads  and additional constraints
        % can be applied during each analysis step.
        nodesForceActive; % List of node indices with active fores engaged. 
                  % Keeps us from having to loop through all nodes
                  % to build the force vector.
        % at solve time.
        
        % Response monitor storage
        monitor = []; %struct('dof', [],'node',[]);
        
        % Plotting options
        plotOpts = struct('logic', false,'frameRate',5,'scaleOpt','norm',...
                          'close',false,'field','displacement')
                      
        % Progress display options              
        printOpts = struct('logic', true,'frameRate',5)
        
        % Solver options (basic direct or iterative)
        solverOpts = struct('method', 'direct', 'tol',1e-10,'iters',1e4);
        
        % Parallelization options
        parallelOpts = struct('logic', false,'numWorkers',2)
        
        % Solution history (storage) options for dynamic analysis
        historyOpts = struct('dof',[],'tPoints',[]);
        
        % Non-nodal forces, i.e. element loads. Converted to nodal loads in preprocessing.
        load = struct('type', [], 'direction', [], 'magnitude', [], 'elset', []);
        force = []; % Cell array of force objects (i.e. concentrated forcce, etc)
        field = []; % Cell array of field objects (i.e. thermal, pressure, etc)
        constraint = struct('array', [], 'dof', []);  % List of 0-valued boundary conditions
        enforced = struct('array', [], 'dof', [], 'value', []);    % List of enforced boundary conditions
        temperature = struct('type', [], 'id', [], 'value', []); % List of enforced temperatures at nodes or elements
        pressureField = []; % Cell array of pressure field objects
        thermalField = [];  % Cell array of thermal field objects
        
        % Field and amplitude definitions
        amplitude = [];

        % Initial and final model states
        % The initial and final displacement states of the model are always saved. If nonzero, final velocity is also saved. Temperatures are
        % available only as constraints (until thermal physics are implemented, at least), but if a step included temperature
        % constraints then those are available as initial and final states also. These need to have an easy access point from the assembler
        % because system assembly will be dependent on current system state.
        initialDisplacement,finalDisplacement;
        initialVelocity,finalVelocity;
        initialTemperature,finalTemperature;
        
        % Stored solution steps. Stored in terms of a mutually exclusive time or frequency parameter.
        % (It's not actually mutually exclusie, because this is a
        % structure. Should probably write this as its own separate class
        % in order to enforce that behavior, and maybe add some convenience
        % functions too.)
        solution = struct('time', [], 'frequency', [], 'solutionDof', [], 'solution', []);
        
        % This is the storage for plotting
        stepGui = struct('figHandle',[],'axesHandle',[],'dataHandle',[]);
    end
    
    methods
        % Initialization of object
        function obj = DirectStep(name, osFern)
            
            obj.name = name;
            obj.osFern = osFern;
            
        end
        
        % Solve linear system
        function x = SolveLinearSystem(obj,A,b,varargin)
            % This is just a wrapper to solve a linear system of equations
            % based upon desired method, i.e. direct or iterative
            
            if nargin > 3
                x0 = varargin{1};
            else
                x0 = [];
            end
            
            if strcmp(obj.solverOpts.method,'direct')
                x = A\b;
            elseif strcmp(obj.solverOpts.method,'pcg')
                pVals = diag(A); nP = length(pVals);
                pMat = sparse([1:nP]',[1:nP]',pVals,nP,nP);
                [x,flag1,relres1,iter1,resvec1] = pcg(A,b,...
                            obj.solverOpts.tol,obj.solverOpts.iters,...
                            pMat,[],x0);
                if flag1 ~= 1
                    error('Iterative solver did not converge')
                end
            end
        end
        % Constraint functions
        function obj = AddConstraint(obj, varargin)
            %    obj = AddConstraint(nodeId, dof, 
            %...)
            % Add zero-BC constraints to step. These just pass through to
            % the osFern object.
            
            obj.osFern.AddConstraint(varargin);
            
            
        end
        
        % Monitor functions
        function obj = AddMonitor(obj, nodes, dof)
            % Add a response monitor to the model. This will be output at
            % either the final displacement level (static) or at every time
            % increment (dynamic).
            
            % TODO: Handle case where single Node object is supplied, or if
            % elements are supplied. (Need a different method?)
            n = length(nodes);
            
            if iscell(nodes) % Cell array of nodes supplied
                for i = 1:n
                    globalDof = nodes{i}.GetGlobalDof();
                    if isempty(obj.monitor)
                        obj.monitor(1).dof = globalDof(dof);
                        obj.monitor(1).node = nodes{i}.id;
                    else
                        obj.monitor(end + 1).dof = globalDof(dof);
                        obj.monitor(end + 1).node = nodes{i}.id;
                    end
                end
            else % Vector of node id's
                for i = 1:n
                    if isempty(obj.monitor)
                        obj.monitor(1).dof = obj.osFern.GetDofIndex(nodes(i), dof(i));
                        obj.monitor(1).node = nodes(i);
                    else
                        index = length(obj.monitor);
                        obj.monitor(index + 1).dof = obj.osFern.GetDofIndex(nodes(i), dof(i));
                        obj.monitor(index + 1).node = nodes(i);
                    end
                end
            end
%             obj.monitor(n + 1).node = obj.osFern.GetDofIndex(nodes, dof);
            
        end
        
        % Force functions
        function obj = AddForce(obj,forceObj)
           if isempty(obj.load)
               obj.force = forceObj;
           else
               obj.force{end+1} = forceObj;
           end
               
        end
        function F = AssembleForce(obj,ti,varargin)
            
            % Initialize Sparse Array
            F = sparse(obj.osFern.nDof, 1);
            
            % If forces are present assemble them
            if ~isempty(obj.force)
                for ii = 1:length(obj.force)
                    % Forces are assembled as sparse arrays
                    F = F + obj.force{ii}.AssembleForce(ti);
                end
            end
            
            % If pressure fields are present assemble them
            if ~isempty(obj.pressureField)
                for ii = 1:length(obj.pressureField)
                    if nargin > 2
                        % Forces are assembled as sparse arrays
                        F = F + sparse(obj.pressureField{ii}.AssembleForce(ti,...
                                              varargin{1}));
                    else
                        % Forces are assembled as sparse arrays
                        F = F + sparse(obj.pressureField{ii}.AssembleForce(ti));
                    end
                end
            end
                        
            % Reduce down to free dof only
            F = F(obj.osFern.freeDof);
        end
        
        % Add Thermal Field
        function obj = AddThermalField(obj,fieldObj)
            if isempty(obj.thermalField)
               obj.thermalField{1} = fieldObj;
           else
               obj.thermalField{end+1} = fieldObj;
           end
        end
        function [F,S,E] = AssembleThermalField(obj,ti,varargin)
            
            
            % Initialize Sparse Array of Forces
            F = sparse(obj.osFern.nDof, 1);
            S = sparse(obj.osFern.nElement, 3);
            E = sparse(obj.osFern.nElement, 3);
            
            % If forces are present assemble them
            if ~isempty(obj.thermalField)
                for ii = 1:length(obj.thermalField)
                    if nargin > 2
                        % Forces are assembled as sparse arrays
                        [Fe,Se,Ee] = obj.thermalField{ii}.AssembleForce(ti,...
                                              varargin{1});
                    else
                        % Forces are assembled as sparse arrays
                        [Fe,Se,Ee] = obj.thermalField{ii}.AssembleForce(ti);
                    end
                    
                    % Cumulate the fields
                    F = F + Fe; S = S + Se; E = E + Ee;
                end
            end
            
            % Reduce down to free dof only
%             F = F(obj.osFern.freeDof);
            
        end
  
        % Add Pressure Field
        function obj = AddPressureField(obj,fieldObj)
            if isempty(obj.pressureField)
               obj.pressureField{1} = fieldObj;
           else
               obj.pressureField{end+1} = fieldObj;
           end
        end
        function F = AssemblePressureField(obj,ti,varargin)
            
            
            % Initialize Sparse Array
            F = sparse(obj.osFern.nDof, 1);
            
            % If forces are present assemble them
            if ~isempty(obj.field)
                for ii = 1:length(obj.field)
                    if nargin > 2
                        % Forces are assembled as sparse arrays
                        F = F + sparse(obj.field{ii}.AssembleForce(ti,...
                                              varargin{1}));
                    else
                        % Forces are assembled as sparse arrays
                        F = F + sparse(obj.field{ii}.AssembleField(ti));
                    end
                end
            end
            
            % Reduce down to free dof only
            F = F(obj.osFern.freeDof);
            
        end
 
        % Amplitude functions
        function obj = AddAmplitude(obj, ampStr, amplitude)
            %    Add2Nset(obj, nset, node)
            %
            % Add ampliude objects to specfiied amplitude name,
            % creating it if it doesn't exist.
            
            if obj.amplitude.isKey(ampStr)
                warning('Overwritting a curren amplitude');
                obj.amplitude(ampStr) = amplitude;
            else
                obj.amplitude(ampStr) = amplitude;
            end
        end
                
        function obj = AddEnforced(obj, varargin)
            % Add enforced displacements to model
            % Arguments given as (nDof, forceNode(s), forceDof, forceValue, ...)
            error('Not implemented');
            for i = 1:n/3
                nodes = varargin{3*(i - 1) + 1};
                nodeInds = obj.osFern.getNodeIndex(nodes);
                dof = varargin{3*(i - 1) + 2};
                val = varargin{3*i};
                
                dofSet = bsxfun(@plus, (nodeInds - 1)*obj.osFern.dofPerNode, dof)';
                dofSet = dofSet(:);
                
                f(dofSet) = val;
            end
        end
        
        function obj = GetForceVector(obj, time)
            % Assemble force for the desired time. Currently, this treates
            % point forces only. Eventually we will want to generalize to
            % handle distributed loads and body loads.
            
            % Create temporary force vector in memory
            Ftemp = zeros(obj.osFern.nDof, 1);
            
            % Loop through each force definition
            for i = 1:length(obj.force)
                amp = 1; % Default amplitude
                if (nargin == 2) && (~isempty(obj.force(i).amplitude))
                    amp = obj.force(i).amplitude(time); % This needs work
                end
                %                 keyboard
                Ftemp(obj.force(i).dof) = Ftemp(obj.force(i).dof) + amp*obj.force(i).value;
                
            end
            
            % Convert to sparse for use
            obj.F = sparse(Ftemp);
        end
        
        function disp(obj)
            
            fprintf('%s Object\n', class(obj));
            
            fprintf('\tName: %s\n', obj.name);
            
            fprintf('Associated osFern\n');
            fprintf('To see all available data/properties type: fieldnames(%s),\n',obj.name);
            disp(obj.osFern);
            
        end
        
        % Plotting functions
        function obj = InitializePlot(obj,fieldVec)
            
                
            % Check if initial vector field is applied
            if isempty(fieldVec)
                fieldVec = zeros(obj.osFern.nDof, 1);
            else
                
            end
            
            % Change title name
            if isa(obj,'Dynamic')
                title = 'Implicit Dynamic';
            elseif isa(obj,'Explicit')
                title = 'Explicit Integrator';
            elseif isa(obj,'NLStatic') || isa(obj,'Static')
                title = 'Static Step';
            end
            
            % Initialize Figure
            fgn = figure('Visible','on','Renderer','zbuffer','NumberTitle','off', ...
                'Name',[title,' Plot for FERN']);
            set(fgn,'uni','nor','position',[0.05 0.1 0.9 0.8],'Color',[1 1 1],'toolbar','figure');
            
            % First subplot is deformation at that point
            sp_axes{1} = subplot(2,1,1);
            
            % Use osFern plot tool 
            cla(sp_axes{1}); 
            hold(sp_axes{1},'on') 
            obj.osFern.Plot(fieldVec,'norm',sp_axes{1});
            hold(sp_axes{1},'off')
            
%             % Make empty arrays of vertices and elements
%             vertices = zeros(length(obj.osFern.node), 3);
%             eles = zeros(length(obj.osFern.element), 4);
%             
%             % Grab Nodes
%             for i = 1:length(obj.osFern.node)
%                 vertices(i, :) = obj.osFern.node{i}.GetXYZ()';
%             end
%             
%             % Grab elements
%             for j = 1:length(obj.osFern.element)
%                 tempEleVec = ([obj.osFern.element{j}.nodes{1:end}]);
%                 eles(j, :)  = [tempEleVec.id];
%             end
%             
%             % Plot FEM
%             sp_data{1} = patch('Vertices', vertices,'Faces', eles);
%             
%             % Formatting
%             set(sp_data{1}, 'MarkerEdgeColor',[0 0 0], ...
%                 'Marker','None', ...
%                 'FaceColor',[0.6 0.6 1], ...
%                 'FaceAlpha', 0.5, ...
%                 'EdgeColor',[0 0 0.3]);
%             view([0,1,0])
%             axis equal
%             xlabel('\bfX'); ylabel('\bfY'); zlabel('\bfZ');
%             
%             % Set color data for elements
%             nd = 6;
%             cdata = dot([fieldVec(1:nd:end), fieldVec(2:nd:end), fieldVec(3:nd:end)],...
%                 [fieldVec(1:nd:end), fieldVec(2:nd:end), fieldVec(3:nd:end)], 2);
%             set(sp_data{1}, 'FaceVertexCData', cdata, 'FaceColor', 'Interp');
%             set(sp_axes{1},'Units','normalized','Position',[0.05 0.55 0.90 0.4],'XGrid','on','YGrid','on');
%             colorbar('peer',sp_axes{1})
            
            % Second subplot is specific dof vs time
            sp_axes{2} = subplot(2,1,2);
            
            % Get number of dof to track
            nDof2Plot = length(obj.monitor);
            for ii = 1:nDof2Plot
                lString{ii} = ['Node ',num2str(obj.monitor(ii).node),...
                    ' DOF ', num2str(obj.monitor(ii).dof)]; %#ok<AGROW>
                %                    dof2plot(ii) = obj.monitor.dof(ii);
            end
            try
                x2Plot = fieldVec([obj.monitor(:).dof]);
            catch
                x2Plot = 0; lString = 'null'
            end
            sp_data{2} = plot(0,x2Plot,'LineStyle','-','LineWidth',2,'Marker','.','MarkerSize',18);
            set(sp_axes{2},'Units','normalized','Position',[0.05 0.1 0.9 0.4],'Box','on');
            set(sp_axes{2},'XScale','linear','YScale','linear','FontSize',11,'XGrid','on','YGrid','on');
            set(get(sp_axes{2},'XLabel'),'string','Time (s)','FontSize',16);
            set(get(sp_axes{2},'YLabel'),'string','DOF Amplitude(-)','FontSize',16);
            legend(lString);
            
            % Save into object
            obj.stepGui.figHandle = fgn;
            obj.stepGui.axesHandle{1} = sp_axes{1};
            obj.stepGui.axesHandle{2} = sp_axes{2};
            obj.stepGui.dataHandle{1} = sp_data{1};
            obj.stepGui.dataHandle{2} = sp_data{2};
            
        end
        
        function obj = UpdatePlot(obj,fieldVec,stepNum)
            
            
            if length(fieldVec) == obj.osFern.nDof
                dTemp = fieldVec;
            elseif length(fieldVec) == length(obj.osFern.freeDof)
                dTemp = zeros(obj.osFern.nDof,1);
                dTemp(obj.osFern.freeDof) = fieldVec;
            else
                error('Field Vector is incorrect size')
            end
            
            % Use osFern plot tool 
            cla(obj.stepGui.axesHandle{1}); 
            hold(obj.stepGui.axesHandle{1},'on') 
            obj.osFern.Plot(fieldVec,'norm',obj.stepGui.axesHandle{1});
            set(obj.stepGui.axesHandle{1},'View',[-45 45]);
            hold(obj.stepGui.axesHandle{1},'off')
            
%             % plot deformation
%             vertices = zeros(length(obj.osFern.node),3);
%             for i = 1:length(obj.osFern.node)
%                 gdof = obj.osFern.node{i}.globalDof;
%                 vertices(i, :) = obj.osFern.node{i}.GetXYZ()' + dTemp(gdof(1:3))';
%             end
%             
%             % Plot
%             set(obj.stepGui.dataHandle{1},'Vertices', vertices);
%             
%             % Set color data for elements
%             nd = 6;
%             switch obj.plotOpts.scaleOpt
%                 case {'X', 'x'}
%                     cdata = dTemp(1:nd:end);
%                 case {'Y', 'y'}
%                     cdata = dTemp(2:nd:end);
%                 case {'Z', 'z'}
%                     cdata = dTemp(3:nd:end);
%                 case {'norm', 'mag'}
%                     cdata = sqrt(dot([dTemp(1:nd:end), dTemp(2:nd:end), dTemp(3:nd:end)],...
%                         [dTemp(1:nd:end), dTemp(2:nd:end), dTemp(3:nd:end)], 2));
%                 otherwise
%                     error('case not supported')
%             end
%             set(obj.stepGui.dataHandle{1}, 'FaceVertexCData', cdata, 'FaceColor', 'Interp');
%             
            % Plot specific dof vs time
            try
                x2Plot = obj.solution.displacement([obj.monitor(:).dof],1:stepNum);
            catch
                x2Plot = 0;
            end
                
            for k = 1:size(x2Plot,1)
                set(obj.stepGui.dataHandle{2}(k), 'XData',obj.t(1:stepNum), 'YData',x2Plot(k,:)); % hold on
            end
            
            % Force update
            drawnow
        end
        
        % Seconday options for plotting, printing and parallel computing
        function obj = SetPlotOpts(obj,plotLogic,frameRate)
            obj.plotOpts.logic = plotLogic;
            obj.plotOpts.frameRate = frameRate;
        end
        function obj = SetPrintOpts(obj,printLogic,frameRate)
            obj.printOpts.logic = printLogic;
            obj.printOpts.frameRate = frameRate;
        end
        function obj = SetParallelOpts(obj,parallelLogic,numWorkers)
            obj.parallelOpts.logic = parallelLogic;
            obj.parallelOpts.numWorkers = numWorkers;
            
            % Set within main osfern as well
            obj.osFern.parallelOpts.logic = parallelLogic;
            obj.osFern.parallelOpts.numWorkers = numWorkers;
        end
    end
    
    % These must be implemented by all subclasses of DirectStep
    methods(Abstract)
        
        obj = Solve(obj)
        
    end
    
end
