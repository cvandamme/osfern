classdef Thermal < DirectStep
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        thermalForce
        response
        Kss % Stress stiffness matrix
        Kf  % Final total stiffness : Kf = K + Kt + Kss
        phi, fn % modal properties
        psi, lambda % buckling properties
        beta = 1;%0.6; % relaxation factor
            % MSA changed this to 1 (no relaxation).  Is this needed for
            % some problems? This seems like a sub optimal way to try to
            % improve convergence.
        tempField;  % temperature field
        tempDependence = false;
        stabilize = struct('logic',false,'factor',0.001,...
                           'density',1);
    end
    
    methods
        function obj = Thermal(name, osFern, varargin)
            % Thermal(name, osFern)
            % Thermal(name, osFern, x0, beta)
            %   name = name of thermal object
            %   osFern = OSFern FEM object
            %   x0 = Initial Displacement of the FEM (assumed zero if not
            %   given)
            %   beta = relaxation factor to use when solving the thermal
            %   object.  (set to 1 if not given - no relaxation)  Try
            %   setting this to 0.6 or so if the model fails to converge.
            %   
            
            obj = obj@DirectStep(name, osFern);
            obj.initialDisplacement = zeros(obj.osFern.nDof, 1);            
            
            if nargin > 2
                obj.initialDisplacement = varargin{1};
                if nargin > 3
                    obj.beta = varargin{2};
                    if ~isnumeric(obj.beta)
                        error('The relaxation factor should be a number.');
                    end
                    if obj.beta < 0 | obj.beta > 1
                        warning(['You set the relaxation factor beta = ',...
                            num2str(obj.beta),'. Are you sure?']);
                    end
                end
            end          
        end
        function obj = Solve(obj,nSteps,varargin)
            % Solve for the static equilibrium of a thermally activated
            % structure. Uses a netwon-rhapson iteration procedure.
            % 
            
            
            % Setup step vector
            steps = linspace(0, 1, nSteps); dt = 1/nSteps;
            
            % Initialize displacement vector
            xold = zeros(obj.osFern.nDof, 1);
            fOld = zeros(obj.osFern.nDof, 1);
            freeDof = obj.osFern.freeDof;
            
            % Extract initial displacement
            if nargin > 2
                x0 = varargin{1};
            else
                x0 = obj.initialDisplacement;
            end
            
            % Grab linear stiffness
            K = obj.osFern.GetStiffnessMatrix();
            
            % If stabilization is requested (helpful for buckling problems)
            if obj.stabilize.logic
                M = obj.osFern.GetMassMatrix();
                M = obj.stabilize.density*M/normest(M);
            end
            
            % Thermal loads
            [Fn,Se,Ee] = obj.AssembleThermalField(0, x0);
                % This is found within "DirectStep", but just uses
                % "ThermalField.AssembleForce" to do this.

            % compute the nonlinear stiffness matrices
            [Kn,Kt] = obj.osFern.GetTangentStiffness(x0);
            
            % Assemble externally applied force
            Fext = sparse(length(freeDof),1);
            Fext(freeDof) = obj.AssembleForce(0);
            
            % Combine thermal and applied force
            F = Fext*steps(1) + Fn;
            
            % Compute initial displacement and update
            xold(freeDof) = (K+Kn)\F(freeDof); 
            
            % Vector of translational DOF
            % loop to compute solution at each load step
            icount = 0;
            obj.solution.solution = zeros(obj.osFern.nDof, length(steps));
            Fnold = zeros(size(Fext));
            
            v = zeros(size(xold(freeDof)));
            
            % Increment through the temperatue linearly
            for  ii = 1:nSteps
                
                % Initialize error metrics
                derr = 1; ferr = 1;
                
                % Output information
                if obj.printOpts.logic
                    try
                        disp([' load step ' ,num2str(ii),...
                            ', Temperature ',num2str(steps(ii)*obj.thermalField{1}.val)]);
                    catch
                        disp([' load step ' ,num2str(ii)]);
                    end
                end
                
                % Compute internal thermal load at the nodes steps(1);
                [Fn,Se,~] = obj.AssembleThermalField(0, xold);
                
                % Combine thermal and applied force
                F = steps(ii)*Fext + Fn*steps(ii);
                
                % Change in force
                delF = F-Fnold;
                
                % Stress stiffness matrix
                [Ks] = obj.osFern.GetStressStiffness(Se*steps(ii),xold);
                
                % iterate the nonlinear solution
                iloop = 1; dtol = 1E-6;   ftol = 1E-6;
                
                % Continue updating until convergence
                while  (ferr > ftol) || (derr > dtol)
                    
                    % compute the nonlinear stiffness matrices
                    [Kn,Kt] = obj.osFern.GetTangentStiffness(xold);
                    
                    % form and factor the tangent stiffness matrix
                    Ktot = K + Kn + Ks;
                    Ktan = K + Kt + Ks;
                
                    % compute the effective load
                    Fint = Ktot*xold(obj.osFern.freeDof);
                    
                    % compute the residual force
                    
                    
                    % Stabilize solution if desired
                    if obj.stabilize.logic
                        Fres = F(freeDof) - Fint - obj.stabilize.factor*M*v;
                    else
                        Fres = F(freeDof) - Fint;
                    end
                    
                    % compute the incremental nonlinear static response
                    del_x = Ktan\Fres;
%                     xnl = Ktot\F(freeDof);
                    
                    % If first iteration
                    if iloop == 1;  del_x0 = del_x; end
                    
                    % update the displacement vector - v1
                    x_nli = xold(obj.osFern.freeDof) + obj.beta*del_x;
                    
                    % update the displacement vector - v2
%                     x_nli = obj.beta*xnl+(1-obj.beta)*xold(freeDof);
                    
                    % compute the displacement error for all DOF
                    if norm(del_x0) == 0
                        derr = norm(del_x);
                    else
                        derr = norm(del_x)/norm(del_x0);
%                         derr = norm(xnl-xold(freeDof))/norm(xold(freeDof));
                    end
                    
                    % compute the force error
                    if norm(F) == 0
                        ferr = norm(Fres);
                    else
                        ferr = norm(Fres)/norm(F);
                    end
                    
                    % print the error values
                    if obj.printOpts.logic
                        fprintf(' displ error = %8.3e, force error = %8.3e\n',[derr ferr]);
                    end
                    
                    % Store the current displacement
                    xold(obj.osFern.freeDof) = x_nli;

                    % Update counts
                    iloop = iloop+1; icount = icount+1;
                    
                    % Compute artificial velocity
                    v = del_x/dt;
%                     if obj.plotOpts.logic
%                        figure; 
%                        obj.osFern.Plot(xold);
%                        title(['Step Number : ',num2str(ii)])
%                        colorbar; view([0 0 -1]); drawnow; 
%                     end
                end
                
                fOld = Fn;

                % save the converged displacement vector
                obj.solution.solution(obj.osFern.freeDof, ii) = x_nli;
                
                if obj.plotOpts.logic
                   figure; 
                   obj.osFern.Plot(obj.solution.solution(:, ii));
                   title(['Step Number : ',num2str(ii)])
                   colorbar; view([0 0 -1]); drawnow; 
                end
            end
            
            % Store final displacements
            obj.finalDisplacement = zeros(size(obj.osFern.freeDof));
            obj.finalDisplacement(obj.osFern.freeDof) = x_nli;
            
            % Obtain response DOF
            responseDof = [];
            for i = 1:length(obj.monitor)
                responseDof = unique([responseDof, obj.monitor(i).dof]);
            end
            
            % Store final information
            obj.response = obj.finalDisplacement(responseDof);
            
            % This information is stored for later analyses if desired
            obj.Kss = Ks;  obj.Kf = Ktan; obj.thermalForce = Fn;
        end
        function [U,Ueq,varargout] = Solve_Heat_n_Load(obj,nSteps,forces,varargin)
            % Solve for the static equilibrium of a thermally activated
            % structure. Uses a netwon-rhapson iteration procedure.
            % This solution varies from the previous in that it solves
            % for static solutions about the previously identified thermal
            % equilibrium. This is used for reduced order model generation
            % i.e. IC.
            % Inputs :
            %       obj : thermal object
            %       nSteps : number of steps to use in NR scheme
            %       forces : matrix of vectors applied to the model
            %       
            
            % Check if additional argument is provided
            if nargin > 3
               solved = varargin{1}; 
            else
               solved  = false; 
            end
            
            % Identify number of load cases
            nLoadCases = size(forces,2); freeDof = obj.osFern.freeDof;
            nDof = obj.osFern.nDof; fDof = sum(freeDof);

            % Check size of forces provided
            if size(forces,1) == sum(obj.osFern.freeDof)
                disp('Forces provided are consistnent with free dof')
                U = zeros(size(forces));
                loadCases = forces; %(obj.osFern.freeDof,:);
            elseif size(forces,1) == obj.osFern.nDof
               disp('Forces provided are consistent with total dof')
               disp('Reducing down to free dof only during solution')
               disp('Will expand back to full dof when done')
%                loadCases = forces(obj.osFern.freeDof,:);
               U = zeros(obj.osFern.nDof,nLoadCases);
            else
               error('Forces provided are inconsistnent with system size')
            end
            
            % Solve thermal equilibrium
            disp('Solving for thermal equilibrium')
            if ~solved
                obj.Solve(nSteps);
            end
            
            % Grab linear stiffness
            K = obj.osFern.GetStiffnessMatrix();
            
            % Store shorthand thermal equilibrium values
            Ueq = obj.finalDisplacement;
            
            % Extract print logic 
            pLogic = obj.printOpts.logic;
            
            disp('Solving for static solutions about thermal equilibrium')
            % Now Perform multiple load cases about thermal equilibrium
            if obj.parallelOpts.logic
                % Initiliaze cell of solutions
                uPar = cell(nLoadCases,1);
                
                % Share the object across all workers
                sharedObj = parallel.pool.Constant(obj.osFern());

                % Loop through all the forces provided
                parfor (jj = 1:nLoadCases,obj.parallelOpts.numWorkers)
%                 for jj = 1:nLoadCases
                    % Output solution number
                    if pLogic
                        disp(['Solving Load Case : ',num2str(jj)]);
                    end
                    
                    % Grab the specifc solutoin
                    Fapp = forces(:,jj);

                    % Start solution
                    steps = linspace(0, 1, nSteps);

                    % compute the linear static response at the first load step
                    xold = zeros(nDof, 1); x_nli = zeros(nDof, 1);
                    xold(freeDof) = Ueq(freeDof); %#ok<PFBNS>

                    % loop to compute solution at each load step
                    icount = 0;   %printopt = false;
                    for ii = 1:length(steps)
                        derr = 1; ferr = 1;
%                         if pLogic
%                             disp([' load step',num2str(ii)]);
%                         end

                        % Compute internal thermal load at the nodes steps(1);
                        [Fn,Se,~] = obj.AssembleThermalField(0,xold);

                        % Combine thermal and applied force
                        F = steps(ii)*Fapp + Fn;

                        % Stress stiffness matrix
                        [Ks] = obj.osFern.GetStressStiffness(Se,xold);
                        
                        % iterate the nonlinear solution
                        iloop = 1; dtol = 1E-4;  ftol = 1E-4;

                        while (derr > dtol) || (ferr > ftol)

                            % compute the nonlinear stiffness matrices at the current state
                            [Kn,Kt] = sharedObj.Value.GetTangentStiffness(xold);
                            
                            % form and factor the tangent stiffness matrix
                            Ktan = K + Ks + Kt; Ktot = K + Ks + Kn;

                            % compute the effective load
                            Fnew = Ktot*xold(freeDof);

                            % compute the residual force
                            Fres = F(freeDof) - Fnew;

                            % compute the incremental nonlinear static response
                            del_x = Ktan\Fres;

                            if iloop == 1; del_x0 = del_x; end

                            % update the displacement vector
                            x_nli = xold(freeDof) + obj.beta*del_x;

                            % compute the displacement error for all DOF
                            derr = norm(del_x)/norm(del_x0);

                            % compute the force error
                            ferr = norm(Fres)/norm(F);

                            % print the error values
%                             if pLogic
%                                 fprintf(' displ error = %8.3e, force error = %8.3e\n',[derr ferr]);
%                             end
                            
                            % Store and update increment
                            xold(freeDof) = x_nli;
                            iloop = iloop+1; icount = icount+1;
                        end
                    end
                    uPar{jj} = x_nli;
                end
                % Parallel loop doesn't like the matrix storage so do this
                for jj = 1:nLoadCases; U(freeDof,jj) = uPar{jj}; end
            else
                % Loop through all the forces provided
                for jj = 1:nLoadCases

                    % Output solution number
                    if obj.printOpts.logic
                        disp(['Solving Load Case : ',num2str(jj)]);
                    end
                    
                    % Grab the specifc solutoin
                    Fapp = forces(:,jj);

                    % Start solution
                    steps = linspace(0, 1, nSteps);

                    % compute the linear static response at the first load step
                    xold = zeros(obj.osFern.nDof, 1);
                    xold(obj.osFern.freeDof) = Ueq(obj.osFern.freeDof); % Ktot_ThEq\Fapp(freeDof)*steps(1);

                    % Vector of translational DOF
                    % loop to compute solution at each load step
                    icount = 0;
                    obj.solution.solution = zeros(obj.osFern.nDof, length(steps));
%                     printopt = false;
                    for ii = 1:length(steps)
                        derr = 1;
                        ferr = 1;
                        if obj.printOpts.logic
                            disp([' load step',num2str(ii)]);
                        end

                        % Compute internal thermal load at the nodes steps(1);
                        [Fn,Se,~] = obj.AssembleThermalField(0, xold);

                        % Combine thermal and applied force
                        F = steps(ii)*Fapp + Fn; %*steps(ii);
                        delF = F;

                        % Stress stiffness matrix
                        [Ks] = obj.osFern.GetStressStiffness(steps(ii)*Se);
                
                        % iterate the nonlinear solution
                        iloop = 1; dtol = 1E-3;  ftol = 1E-3;

                        while (derr > dtol) || (ferr > ftol)

                            % compute the nonlinear stiffness matrices at the current state
                            [Kn,Kt] = obj.osFern.GetTangentStiffness(xold);

                            % form and factor the tangent stiffness matrix
                            Ktan = K + Ks + Kt;
                            Ktot = K + Ks + Kn;
                            
                            % compute the effective load
                            Fint = Ktot*xold(obj.osFern.freeDof);
                            
                            % compute the residual force
                            Fres = delF(freeDof) - Fint;
                            
                            % compute the incremental nonlinear static response
                            del_x = Ktan\Fres;
                            
                            % If first iteration
                            if iloop == 1;  del_x0 = del_x; end
                            
                            % update the displacement vector - with relaxation
                            x_nli = xold(obj.osFern.freeDof) + obj.beta*del_x;

                            % compute the displacement error for all DOF
                            derr = norm(del_x)/norm(del_x0);

                            % compute the force error
                            ferr = norm(Fres)/norm(F);

                            % print the error values
                            if obj.printOpts.logic
                                fprintf('   displ error = %8.3e, force error = %8.3e\n',[derr ferr]);
                            end

                            xold(obj.osFern.freeDof) = x_nli;
                            iloop = iloop+1;
                            icount = icount+1;
                        end

                        % save the converged displacement vector
                        obj.solution.solution(obj.osFern.freeDof, ii) = x_nli;
                    end
                    U(obj.osFern.freeDof,jj) = x_nli;
                end
            end
        end
        function obj = Buckling(obj,nModes,varargin)
            % This function is used to estimate the buckling load of a
            % thermal field. This is only a linear buckling prediction and
            % will be non-conservative. 
                        
            % Grab linear stiffness
            K = obj.osFern.GetStiffnessMatrix();
            
            % Extract initial displacement
            x0 = obj.initialDisplacement;
            
            % Compute internal thermal load at the nodes steps(1);            
            [Fn,Se,~] = obj.AssembleThermalField(0,x0);
             
            % Assemble stress stiffned matrix
            [Ks] = obj.osFern.GetStressStiffness(Se);
                
            % Eigenvalue problem
            [psi, lam] = eigs(K,-Ks, nModes, 'SM');
            
            % Stiffness Normalize Modes
            for i = 1:nModes
                psi(:, i) = psi(:, i)/sqrt(psi(:, i)'*K*psi(:, i));
            end
            
            % Save
            [obj.lambda, I] = sort(diag(lam));
            obj.phi = zeros(obj.osFern.nDof, nModes);
            obj.phi(obj.osFern.freeDof, :) = psi(:, I);
        end
        function obj = Frequency(obj,nModes,varargin)
            % This function computes the modal properites of the finite
            % element model about a thermally deformed state. It is
            % recommended that this procedure be done following a thermal
            % solve via thermal.Solve(), in this case both the tangent
            % stiffness and thermal stiffness about the equilibrium will be
            % used. 
            

            % Check if thermal.Solve() has been run 
            if isempty(obj.finalDisplacement)
                warning(['It is recommended to perform this after a ',...
                    'thermal.Solve() because it will contain both the ', ...
                    'tangent stiffness and stress stiffness about the ',...
                    'equilibrium state'])
                
                % Extract initial displacement
                if nargin > 2
                    x0 = varargin{1};
                else
                    x0 = obj.initialDisplacement;
                end
                
                % Extract stress stiffness if provided 
                if nargin > 3
                    Kss = varargin{2};
                else
                    % Thermal loads
                    [Fn,Se,~] = obj.AssembleThermalField(0, x0);
                    
                    % Compute internal thermal load at the nodes steps(1);
                    [Se,~,~] = obj.osFern.ComputeThermalLoads(x0,fThermal);
                    
                    % Assemble stress stiffned matrix
                    [Ks] = obj.osFern.GetStressStiffness(Se);
                end

                % Grab linear stiffness
                K = obj.osFern.GetStiffnessMatrix();
                
                % Grab linear stiffness
                [~,Kt] = obj.osFern.GetTangentStiffness(x0);
                
                % Assemble kEff
                Keff = K + Kt + Ks;
            else
                Keff = obj.Kf;
            end

            % Build mass matrix
            M = obj.osFern.GetMassMatrix();

            % Eigenvalue problem
            [phi_temp, lam] = eigs(Keff,M,nModes,'SM');
            
            % Mass Normalize Modes
            for i = 1:nModes
                phi_temp(:, i) = phi_temp(:, i)/sqrt(phi_temp(:, i)'*M*phi_temp(:, i));
            end
            
            % Save modal propertie within object
            [lam, I] = sort(diag(lam)); obj.fn = sqrt(lam)/2/pi;
            obj.phi = zeros(obj.osFern.nDof, nModes);
            obj.phi(obj.osFern.freeDof, :) = phi_temp(:, I);
        end
        function Kss = GetStressStiffness(obj)
            % Compute stress stiffness matrix for a given thermal load.
            % Note that this is not gauranteed to be about an equilibrium
            % point. 
            
            % Output warning 
            warning(['It is recommended to extract stress stiffness matrix ',...
                    'from thermal.Kss after a thermal.Solve() because ', ...
                    ' it is about the deformed state'])
                
            % Grab thermal vector
            fThermal = obj.thermalForce;
            
            % Extract initial displacement
            x0 = obj.initialDisplacement;
            
            % Compute internal thermal load at the nodes steps(1);            
            [Se,~,~] = obj.osFern.ComputeThermalLoads(x0,fThermal);
             
            % Assemble stress stiffned matrix
            [Kss] = obj.osFern.GetStressStiffness(Se);
        end
    end
    
end

