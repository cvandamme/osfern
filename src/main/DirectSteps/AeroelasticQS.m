

classdef AeroelasticQS < Dynamic
    % Dynamic aeroelastic solver for linear, quasi-static theories. The
    % structural stiffness and damping matrices must be externally
    % generated and supplied to this step. Additionally, any static
    % aerodynamic loading must also be given as a standard force vector.
    
    properties
        
        Kaero;
        Caero;
        
    end
 
    methods
        
        % Constructor
        function obj = AeroelasticQS(name, osFern, initialDisplacement, initialVelocity, Kaero, Caero)
            
            obj = obj@Dynamic(name, osFern, initialDisplacement, initialVelocity);
            obj.finalDisplacement = zeros(obj.osFern.nDof, 1);
            obj.finalVelocity = zeros(obj.osFern.nDof, 1);
            
            if nargin > 2
                obj.initialDisplacement = initialDisplacement;
            end
            
            if nargin > 3
                obj.initialVelocity = initialVelocity;
            end            
            
            if nargin > 4
                obj.Kaero = Kaero;
            else
                obj.Kaero = sparse([], [], [], osFern.nDof, osFern.nDof, 0);
            end
            
            if nargin > 5
                obj.Caero = Caero;
            else
                obj.Caero = sparse([], [], [], osFern.nDof, osFern.nDof, 0);
            end
            
        end
          
        function K = GetStiffnessMatrix(obj)
            % Obtain the base state stiffness matrix
            K = obj.osFern.GetStiffnessMatrix() + ... 
                obj.Kaero(obj.osFern.freeDof, obj.osFern.freeDof); 
        end
        
        function C = GetRayleighDamping(obj, K, M)
            % Obtain the Rayleigh damping matrix. Must supply K and M
            % matrices to compute the damping matrix.
            C = obj.damping.alpha*M + obj.damping.beta*K + ...
                obj.Caero(obj.osFern.freeDof, obj.osFern.freeDof); 
        end
        

                
    end
    
end
