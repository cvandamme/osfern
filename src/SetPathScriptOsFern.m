cd main
    path(genpath(cd),path);
    cd ..
cd responses
    path(genpath(cd),path);
    cd ..
cd models
    path(genpath(cd),path); close all;
    cd ..
disp('  ------------------------------------------------------------------');
disp('  Setting up Path for OsFern.  This file must be in your current directory to work');
disp('   ');
disp('  Open Source Finite Element Reseach code for Nonlinearity (OSFERN)');
disp('  Version 2018.01');
fprintf(['  Copyright: Christopher Van Damme - cvandamme@wisc.edu, \n '...
         '            Joe Schoneman - joe.schoneman@ata-e.com \n']);
disp('  ------------------------------------------------------------------');

