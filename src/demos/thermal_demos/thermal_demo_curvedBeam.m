%% This script creates a finite element model of a flat-beam using 
%  beam elements. The script performs the following analyses :
%  1) Linear Modal
%  2) Thermal
%  3) Linaer Modal about Thermally Deformed State
clear; clc; close all;
clear classes;
%% Build Model

curvedBeam = OsFern('curvedBeam');

% Plate Dimensions and mesh properties
xLength = 18; xEles = 80; xNodes = xEles + 1;

% Cross section properties
thk = 0.09; width = 1; beamSection.w = width; beamSection.t = thk;

% Radius of curvature
r = 81.25; zPosMax = r - sqrt(4*r^2 - xLength^2)/2;

% Material Properties (aluminum)
E = 10.6*10^6; rho = 2.588*10^-4; nu = 0.32;
alpha = 1.24E-005; refTemp = 0;
aluminum = Material('aluminum');
aluminum = aluminum.setProp('E',E);
aluminum = aluminum.setProp('rho',rho);
aluminum = aluminum.setProp('nu',nu);
aluminum = aluminum.setProp('alpha',alpha);
aluminum = aluminum.setProp('tRef',refTemp);

% Setup constraints
endConstraints = true;  
sideConstraints = true; % enforces inplane motion only

% Creat basic mesh
nodeId = 1; eleId = 1;
for ii = 1:xNodes
        
    % Coordinates
    xPos = xLength*((ii-1)/xEles);
    yPos = 0;
    zPos = sqrt(r^2 - (xPos-xLength/2)^2) - r + zPosMax;
    
    % Create Node Object
    tempNode = Node(nodeId,xPos,yPos,zPos);

    % Build node
    curvedBeam = curvedBeam.AddNodeObject(tempNode);

    % Clamp end nodes
    if ii == 1 || ii == xNodes
        tempNode = tempNode.SetFixed();
    else
        % Constrain in plane motion
        if sideConstraints
            tempNode = tempNode.SetFixed([2,4,6]);
        end
    end

    % Identify center node
    if ii == xEles/2 + 1
        curvedBeam.Add2Nset('CenterNode', tempNode);
        tempNode.SetXYZ(xPos + 1e-4,yPos,zPos);
    end

    % Create element
    if ii > 1
        b31Ele = B2(eleId); eleId = eleId + 1;
        eleNodeIds = [(ii - 1) (ii)]; csVec = [0 1 0];
        b31Ele = b31Ele.Build(curvedBeam.GetNodes(eleNodeIds),...
                 aluminum, beamSection,csVec);
        curvedBeam.AddElementObject(b31Ele); 
    end

    nodeId = nodeId + 1;
        
end

curvedBeam = curvedBeam.Update();
figure; curvedBeam.Plot(); 

%% Linear modal analysis
nmodes = 40;
modal = Frequency('modal', curvedBeam);
modal.Solve(nmodes);



%% Thermal step
xScale = 0;
xDisp = xScale*modal.phi(:,1)/norm(modal.phi(:,1));

name = 'fullDynamic';
thermal = Thermal(name, curvedBeam, xDisp);

% Apply uniform temperature distribution
temperature = 50;

% Create thermal field object
thermalField = ThermalField('Uniform',curvedBeam,temperature);
thermalField.AddFieldVector(ones(length(curvedBeam.node)));

% Apply thermal field
thermal.AddThermalField(thermalField);

% Solve thermal problem
thermal.Solve(6);

% Plot final displacement field
figure; curvedBeam.Plot(thermal.finalDisplacement,'Z'); colorbar;
view([0 -1 0]);

% Compute frequncies about thermal state
thermal.Frequency(20);



figure; hold on
plot(modal.fn,'-o'); plot(thermal.fn,'-o'); 
legend('Nominal','Heated'); xlabel('Mode #'); ylabel('Frequency (Hz)');


figure
subplot(3,2,1)
title(['Unheated - Mode 1 : ',num2str(round(modal.fn(1))),' Hz'])
curvedBeam.Plot(modal.phi(:,1)/norm(modal.phi(:,1)),'norm')
view([0 -1 0]); grid('on');
subplot(3,2,2)
title(['Heated - Mode 1 : ',num2str(round(thermal.fn(1))),' Hz'])
curvedBeam.Plot(thermal.phi(:,1)/norm(thermal.phi(:,1)),'norm')
view([0 -1 0]); grid('on');
subplot(3,2,3)
title(['Unheated - Mode 2 : ',num2str(round(modal.fn(2))),' Hz'])
curvedBeam.Plot(modal.phi(:,2)/norm(modal.phi(:,2)),'norm')
view([0 -1 0]); grid('on');
subplot(3,2,4)
title(['Heated - Mode 2 : ',num2str(round(thermal.fn(2))),' Hz'])
curvedBeam.Plot(thermal.phi(:,2)/norm(thermal.phi(:,2)),'norm')
view([0 -1 0]); grid('on');
subplot(3,2,5)
title(['Unheated - Mode 3 : ',num2str(round(modal.fn(3))),' Hz'])
curvedBeam.Plot(modal.phi(:,3)/norm(modal.phi(:,3)),'norm')
view([0 -1 0]); grid('on');
subplot(3,2,6)
title(['Heated - Mode 3 : ',num2str(round(thermal.fn(3))),' Hz'])
curvedBeam.Plot(thermal.phi(:,3)/norm(thermal.phi(:,3)),'norm')
view([0 -1 0]); grid('on');
