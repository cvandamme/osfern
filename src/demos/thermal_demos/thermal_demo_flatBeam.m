%% This script creates a finite element model of a flat-beam using 
%  beam elements. The script performs the following analyses :
%  1) Linear Modal
%  2) Thermal
%  3) Linaer Modal about Thermally Deformed State
clear; clc; close all;
clear classes;
%% Build Model

flatBeam = OsFern('flatBeam');

% Plate Dimensions and mesh properties
xLength = 9; xEles = 40; xNodes = xEles + 1;

% Cross section properties
thk = 0.031; width = 0.5;
beamSection.w = width; beamSection.t = thk;

% Material Properties (steel)
E = 2.97e7; rho = 0.000736; nu = 0.280712;
alpha = 6.3e-06;refTemp = 0;
steel = Material('steel');
steel = steel.setProp('E',E);
steel = steel.setProp('rho',rho);
steel = steel.setProp('nu',nu);
steel = steel.setProp('alpha',alpha);
steel = steel.setProp('tRef',refTemp);

% Setup constraints
endConstraints = true;  
sideConstraints = true; % enforces inplane motion only

% Creat basic mesh
nodeId = 1; eleId = 1;
for ii = 1:xNodes
        
    % Coordinates
    xPos = xLength*((ii-1)/xEles);
    yPos = 0; zPos = 0;
    
    % Create Node Object
    tempNode = Node(nodeId,xPos,yPos,zPos);

    % Build node
    flatBeam = flatBeam.AddNodeObject(tempNode);

    % Clamp end nodes
    if ii == 1 || ii == xNodes
        tempNode = tempNode.SetFixed();
    else
        % Constrain in plane motion
        if sideConstraints
            tempNode = tempNode.SetFixed([2,4,6]);
        end
    end

    % Identify center node
    if ii == xEles/2 + 1
        flatBeam.Add2Nset('CenterNode', tempNode);
    end

    % Create element
    if ii > 1
        b31Ele = B2(eleId); eleId = eleId + 1;
        eleNodeIds = [(ii - 1) (ii)]; csVec = [0 1 0];
        b31Ele = b31Ele.Build(flatBeam.GetNodes(eleNodeIds),...
                 steel, beamSection,csVec);
        flatBeam.AddElementObject(b31Ele); 
    end

    nodeId = nodeId + 1;
        
end

flatBeam = flatBeam.Update();
figure; flatBeam.Plot(); 

%% Linear modal analysis
nmodes = 40;
modal = Frequency('modal', flatBeam);
modal.Solve(nmodes);

%% Thermal step
xScale = 0.0;
xDisp = xScale*modal.phi(:,1)/norm(modal.phi(:,1));

name = 'fullDynamic';
thermal = Thermal(name, flatBeam, xDisp);

% Create thermal field object
temperature = 2;
thermalField = ThermalField('Uniform',flatBeam,temperature);
thermalField.AddFieldVector(ones(length(flatBeam.node)));

% Apply thermal field
thermal.AddThermalField(thermalField);

% Grab center node and apply force
forceNode = flatBeam.nset('CenterNode'); 
force = Force('CenterNode',flatBeam);
force.AddForce(forceNode,3,0);
thermal.AddForce(force);
thermal.AddMonitor(forceNode{1}.id, 3);  

% Solve thermal problem
thermal.Solve(6);

% Solve thermal buckling problems
thermal.Buckling(10);
bLoads = temperature.*thermal.lambda;
I = (width*thk^3)/12; A = width*thk;
bTheory_p = (pi^2)*(E*I)/((0.5*xLength)^2);
bTheory_dT = 4*(pi^2)*E*I/(E*A*alpha*xLength^2);

% Plot final displacement field
figure;flatBeam.Plot(thermal.finalDisplacement,'Z'); colorbar;
view([0 -1 0]);

figure
subplot(2,2,1)
title(['Buckling : Mode 1 : \lambda = ',num2str(round(thermal.lambda(1),2)),' '])
flatBeam.Plot(thermal.phi(:,1)/norm(thermal.phi(:,1)),'norm')
view([0 -1 0]); grid('on');
subplot(2,2,2)
title(['Buckling : Mode 2 : \lambda = ',num2str(round(thermal.lambda(2),2)),' '])
flatBeam.Plot(thermal.phi(:,2)/norm(thermal.phi(:,2)),'norm')
view([0 -1 0]); grid('on');
subplot(2,2,3)
title(['Buckling : Mode 3 : \lambda = ',num2str(round(thermal.lambda(3),2)),' '])
flatBeam.Plot(thermal.phi(:,3)/norm(thermal.phi(:,3)),'norm')
view([0 -1 0]); grid('on');
subplot(2,2,4)
title(['Buckling : Mode 4 : \lambda = ',num2str(round(thermal.lambda(4),2)),' '])
flatBeam.Plot(thermal.phi(:,4)/norm(thermal.phi(:,4)),'norm')
view([0 -1 0]); grid('on');

% Modal solution about equilibrium state - recommended and quicker
thermal.Frequency(20);

%% Compare with abaqus

if temperature == 2
    abqFn_Nom = [79.02;217.92;427.46];
    abqFn_Heated = [65.38;200.43;408.78];

    figure; hold on
    subplot(2,2,1); 
    bar([abqFn_Nom, modal.fn(1:3)]); 
    legend('Abaqus','OsFern');
    xlabel('Mode #'); ylabel('Frequency (Hz)');
    title('Base State Modes')

    subplot(2,2,2); 
    bar([abqFn_Heated, thermal.fn(1:3)]); 
    legend('Abaqus','OsFern');
    xlabel('Mode #'); ylabel('Frequency (Hz)');
    title('Heated Modes')

    subplot(2,2,3); 
    bar(100*(abqFn_Nom-modal.fn(1:3))./abqFn_Nom); 
    xlabel('Mode #'); ylabel('% Error');
    title('Error in Base State Modes')

    subplot(2,2,4); 
    bar(100*(abqFn_Heated-thermal.fn(1:3))./abqFn_Heated); 
    xlabel('Mode #'); ylabel('% Error');
    title('Error in Heated Modes')
end

% Plot mode comaprison - eventually overlay results to show axial
% contribution
figure
subplot(3,2,1); hold on
title(['Unheated - Mode 1 : ',num2str(round(modal.fn(1))),' Hz'])
flatBeam.Plot(modal.phi(:,1)/norm(modal.phi(:,1)),'norm')
view([0 -1 0]); grid('on');
subplot(3,2,2)
title(['Heated - Mode 1 : ',num2str(round(thermal.fn(1))),' Hz'])
flatBeam.Plot(thermal.phi(:,1)/norm(thermal.phi(:,1)),'norm')
view([0 -1 0]); grid('on');
subplot(3,2,3)
title(['Unheated - Mode 2 : ',num2str(round(modal.fn(2))),' Hz'])
flatBeam.Plot(modal.phi(:,2)/norm(modal.phi(:,2)),'norm')
view([0 -1 0]); grid('on');
subplot(3,2,4)
title(['Heated - Mode 2 : ',num2str(round(thermal.fn(2))),' Hz'])
flatBeam.Plot(thermal.phi(:,2)/norm(thermal.phi(:,2)),'norm')
view([0 -1 0]); grid('on');
subplot(3,2,5)
title(['Unheated - Mode 3 : ',num2str(round(modal.fn(3))),' Hz'])
flatBeam.Plot(modal.phi(:,3)/norm(modal.phi(:,3)),'norm')
view([0 -1 0]); grid('on');
subplot(3,2,6)
title(['Heated - Mode 3 : ',num2str(round(thermal.fn(3))),' Hz'])
flatBeam.Plot(thermal.phi(:,3)/norm(thermal.phi(:,3)),'norm')
view([0 -1 0]); grid('on');
