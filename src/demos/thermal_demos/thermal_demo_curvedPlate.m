%% This scripts generates a basic finite element model of a curved
%  plate. 
%  Analysis then performs
%   1) linear modal analysis
%   2) Nonlinear Static with thermal loads
%   4) Modal about nonlinear state 
clear; clc; close all
clear classes
addpath(genpath('../classes'));


%% Build Model

CP = OsFern('beam');

% Plate Dimensions and mesh properties (inches)
xLength = 15.75; xEles = 64; xNodes = xEles + 1;
yLength = 9.75; yEles = 40; yNodes = yEles + 1;
r = 100; zPosMax = r - sqrt(4*r^2 - xLength^2)/2;

% Element/shell thickeness (inches)
thk = 0.048; shellSection.t = thk;

% Material Properties
E = 2.85e7; rho = 0.000748; nu = 0.3; 
alpha = 10.5e-06; refTemp = 0; 
steel = Material('steel');steel = steel.setProp('E',E);
steel = steel.setProp('rho',rho);steel = steel.setProp('nu',nu);
steel = steel.setProp('alpha',alpha);
steel = steel.setProp('tRef',refTemp);

% Setup constraints
endConstraints = true;
sideConstraints = false;

% Creat basic mesh
nodeId = 1; elementId = 1;
endNodes = []; sideNodes = [];
for ii = 1:yNodes
    for jj = 1:xNodes
        
        % Coordinates
        xPos = xLength*((jj-1)/xEles);
        yPos = yLength*((ii-1)/yEles);
        zPos = sqrt(r^2 - (xPos-xLength/2)^2) - r + zPosMax;
        
        % Create Node Object
        tempNode = Node(nodeId,xPos,yPos,zPos);
        
        % Build node
        CP = CP.AddNodeObject(tempNode);
        
        % Clamp end nodes
        if jj == 1 || jj == xNodes
            tempNode = tempNode.SetFixed();
            CP.Add2Nset('BoundaryNodes', tempNode);
        end
        
        if ii == 1 || ii == yNodes
            tempNode = tempNode.SetFixed();
            CP.Add2Nset('BoundaryNodes', tempNode);
        end
        
        % Create Center Node Object
        if ii == yEles/2 + 1 && jj == xEles/2 + 1
            CP.Add2Nset('CenterNode', tempNode);
        end
        
        % Create element
        if ii > 1 && jj > 1
            s4Ele = S4S(elementId); elementId = elementId + 1;
            nodeIds = [(ii - 2)*xNodes + (jj - 1);
                       (ii - 2)*xNodes + (jj);
                       (ii - 1)*xNodes + (jj);
                       (ii - 1)*xNodes + (jj - 1)];
            s4Ele = s4Ele.Build(CP.GetNodes(nodeIds), steel, shellSection);
            CP.AddElementObject(s4Ele);
        end
        
        nodeId = nodeId + 1;
        
    end
end

CP.Update();
CP.Plot();

cNode = CP.nset('CenterNode');
cNodeDof = cNode{1}.GetGlobalDof();
cNodeVertDof = cNodeDof(3);

%% Linear modal analysis
nmodes = 10;
modal = Frequency('modal', CP);
modal.Solve(nmodes);

% Plot the first four modes
figure(3); clf(3);
scale = 0.01;
for k=1:8
    subplot(4,2,k)
    title(['Mode ',num2str(k),' : ',num2str(round(modal.fn(k))),' Hz'])
    CP.Plot(scale*modal.phi(:,k),'norm')
    view([0 0 -1]); grid('on');
    ph=get(gca,'Children'); set(ph,'LineStyle','none');
end

% Also try CP.uimplot(modal.phi,modal.fn) for an interactive plot.

%% Perform thermal analysis
xScale = 0.0;
xDisp = xScale*modal.phi(:,1)/norm(modal.phi(:,1));

name = 'fullDynamic';
thermal = Thermal(name, CP, xDisp);

% Create thermal field object
temperature = 50;
thermalField = ThermalField('UniformField',CP,temperature);
%     % CASE 1 - Uniform Temperature Field - (to match Abaqus)
    tDist=ones(length(CP.node),1); 

    % CASE 2 - Heat the right side of the panel only with a uniform temp.
    % (to demonstrate this capability)
%     tDist=zeros(size(nodes,1),1); 
%         tDist(nodes(:,2)>15.75/2)=1; % apply temperature field to one side only.
thermalField.AddFieldVector(tDist);

% Add thermal field to thermal object
thermal.AddThermalField(thermalField);

% Grab center node and apply force
forceNode = CP.nset('CenterNode'); 
force = Force('CenterNode',CP);
force.AddForce(forceNode,3,0e-8);

% Create force object
thermal.AddForce(force);
thermal.AddMonitor(forceNode{1}.id, 3);              

% Full serial
tic; thermal.Solve(1); tSimSerial = toc;
    % The number (1) above sets the number os load increments to use in the
    % thermal solution.  It converges with one, but if snap through or
    % anything is encountered then use more load increments.

% Compute the stresses
sigma = CP.ComputeStress(thermal.finalDisplacement);

% Compute the strains
epsilon = CP.ComputeStrain(thermal.finalDisplacement);

% Plot final displacement field
figure;CP.Plot(thermal.finalDisplacement,'norm'); colorbar;

% Modal solution about equilibrium state - recommended and quicker
thermal.Frequency(20);

figure; hold on
plot(modal.fn,'-o'); plot(thermal.fn,'-s'); 
legend('Nominal','Heated'); xlabel('Mode #'); ylabel('Frequency (Hz)');

figure
subplot(2,2,1)
title(['Unheated : Mode 1 : ',num2str(round(modal.fn(1))),' Hz'])
CP.Plot(modal.phi(:,1)/norm(modal.phi(:,1)),'Z')
view([0 0 -1]); grid('on');
subplot(2,2,2)
title(['Heated : Mode 1 : ',num2str(round(thermal.fn(1))),' Hz'])
CP.Plot(thermal.phi(:,1)/norm(thermal.phi(:,1)),'Z')
view([0 0 -1]); grid('on');
subplot(2,2,3)
title(['Unheated : Mode 2 : ',num2str(round(modal.fn(2))),' Hz'])
CP.Plot(modal.phi(:,2)/norm(modal.phi(:,2)),'Z')
view([0 0 -1]); grid('on');
subplot(2,2,4)
title(['Heated : Mode 2 : ',num2str(round(thermal.fn(2))),' Hz'])
CP.Plot(thermal.phi(:,2)/norm(thermal.phi(:,2)),'Z')
view([0 0 -1]); grid('on');


% Compare with abaqus
if temperature == 20
    abqResults = [298.25;307.19;354.81];
    figure;
    subplot(1,2,1);
    bar([thermal.fn(1:3),abqResults]);
    xlabel('Model #'); ylabel('Frequency (Hz');
    subplot(1,2,2);
    bar(100*(abqResults-thermal.fn(1:3))./abqResults);
    xlabel('Model #'); ylabel('# Difference');
    title('Comparison of Heated Modes with Abaqus')
elseif temperature == 50
    abqResults = [357.99;371.09;371.57];
    figure;
    subplot(1,2,1);
    title('Heated Modes')
    bar([thermal.fn(1:3),abqResults]);
    xlabel('Model #'); ylabel('Frequency (Hz');
    legend('OsFern','Abaqus')
    subplot(1,2,2);
    bar(100*(abqResults-thermal.fn(1:3))./abqResults);
    xlabel('Model #'); ylabel('% Difference');
    title('Comparison of Heated Modes with Abaqus')
elseif temperature == 100
    abqResults = [301.84;327.09;436];
    figure;
    subplot(1,2,1);
    title('Heated Modes')
    bar([thermal.fn(1:3),abqResults]);
    xlabel('Model #'); ylabel('Frequency (Hz');
    legend('OsFern','Abaqus')
    subplot(1,2,2);
    bar(100*(abqResults-thermal.fn(1:3))./abqResults);
    xlabel('Model #'); ylabel('% Difference');
    title('Comparison of Heated Modes with Abaqus')    
end