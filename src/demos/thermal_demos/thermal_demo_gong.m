%% This scripts generates a finite element model of a gong using 4 node 
% shell elements. 
%  Analysis then performs
%   1) linear modal analysis
%   2) Nonlinear Static with thermal loads
%   4) Modal about nonlinear state 
clear; clc; close all;

% Setup Model 
SAVEdir = cd;

%% Build Model

% Read in Curved Beam Raw Data
% load('..\models\GongMeshData.mat');
load('..\..\models\GongMeshData_v2.mat');

% Initialize MatFem Object
gong = OsFern('Gong');

% Element/shell thickeness
thk = 0.067;

% Material Properties
set = 1;

% Set 1 
E = 1.45038e+07; rho = 0.0005869; nu = 0.3;
alpha = 1.14E-005; refTemp = 20; 
thermalCond = 14.374517;

% Set 2 
E = 10964900; rho = 0.000749; nu = 0.3; 
alpha = 1.14E-005; refTemp = 20; 

% Create material class
brass = Material('brass');
brass = brass.setProp('E',E);
brass = brass.setProp('rho',rho);
brass = brass.setProp('nu',nu);
brass = brass.setProp('alpha',alpha);
brass = brass.setProp('tRef',refTemp);
brass = brass.setProp('thermalCond',thermalCond);


shellSection.t = 0.07;

% Setup constraints
endConstraints = [];

% Decidie if doing force/damped analysis or just NNM
forced = false;

% Creat basic mesh
nodeId = 1; elementId = 1;
endNodes = []; sideNodes = [];
for ii = 1:length(Gong.nodes)
        
    % Node Id 
    nodeId = Gong.nodes(ii,1);
    
    % Coordinates
    xPos = Gong.nodes(ii,2);
    yPos = Gong.nodes(ii,3);
    zPos = Gong.nodes(ii,4);
    
    % Create Node Object
    tempNode = Node(nodeId,xPos,yPos,zPos);

    % Build node object
    gong = gong.AddNodeObject(tempNode);

    % Clamp end nodes
    if any(endConstraints==ii)
        tempNode = tempNode.SetFixed();
        endNodes = [endNodes; tempNode];
    end
 
end

for ii = 1:length(Gong.elements)
    
    % create 4-node shell element
    s4Ele = S4S(elementId); elementId = elementId + 1;
    nodeId = Gong.elements(ii,2:5);
    eleObjNodes = [gong.node(nodeId(1)),...
                gong.node(nodeId(2)),...
                gong.node(nodeId(3)),...
                gong.node(nodeId(4))];
    s4Ele = s4Ele.Build(eleObjNodes, brass, shellSection);
    gong.AddElementObject(s4Ele);
    
end

% Build Constraints and DOF
gong = gong.Update();
gong.Plot();

% Grab center node
cNodeId = gong.findNODEfromXYZ([0 0 0]);
cNodeDof = gong.node{cNodeId}.GetGlobalDof();
cNodeVertDof = cNodeDof(2);

%% Initialize and solve modal equation
nmodes = 50;
modal = Frequency('modal', gong);
modal.Solve(nmodes);
modal.phi = real(modal.phi);
modal.fn = real(modal.fn);

% Plot the response as individual tabs
f = figure; scale = 1/100;
tabgp = uitabgroup(f,'Position',[.05 .05 .9 .9]);
for ii = 1:length(modal.fn)
    modalTab{ii} = uitab(tabgp,'Title',['Mode ', num2str(ii),' : ',num2str(round(modal.fn(ii))),' Hz']);
    axes('Parent',modalTab{ii});
    title(['Mode ', num2str(ii),' : ',num2str(round(modal.fn(ii))),' Hz']);
    gong.Plot(scale*modal.phi(:,ii),'norm');  view([0 1 -0]); colorbar
end


%% Thermal analysis
xScale = 0.0;
xDisp = xScale*modal.phi(:,1)/norm(modal.phi(:,1));

name = 'fullDynamic';
thermal = Thermal(name, gong, xDisp);

% Create thermal field object
temperature = 100;
thermalField = ThermalField('Uniform',gong,temperature);
thermalField.AddFieldVector(ones(length(gong.node)));

% Apply thermal field
thermal.AddThermalField(thermalField);

% Full serial
tic
thermal.Solve(6);
tSimSerial = toc;

% 
figure; gong.Plot(thermal.finalDisplacement,'Y')

% Comptue modes 