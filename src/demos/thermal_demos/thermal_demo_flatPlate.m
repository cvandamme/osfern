%% This scripts generates a basic finite element model of a flat
%  plate. 
%  Analysis then performs
%   1) linear modal analysis
%   2) Nonlinear Static with thermal loads
%   4) Modal about nonlinear state 
clear; clc; close all
clear classes;
%% Build Model

flatPlate = OsFern('flatPlate');

% Plate Dimensions and mesh properties
xLength = 0.254; xEles = 52; xNodes = xEles + 1;
yLength = 0.1271; yEles = 26; yNodes = yEles + 1;

% Element/shell thickeness
thk = 0.000635;

% Material Properties
E = 205e9; rho = 7850; nu = 0.280712;
alpha = 1.22E-005; refTemp = 0; 
steel = Material('steel');
steel = steel.setProp('E',E);
steel = steel.setProp('rho',rho);
steel = steel.setProp('nu',nu);
steel = steel.setProp('alpha',alpha);
steel = steel.setProp('tRef',refTemp);

shellSection.t = thk;

% Setup constraints
endConstraints = true;
sideConstraints = true;

% Creat basic mesh
nodeId = 1; elementId = 1;
for ii = 1:yNodes
    for jj = 1:xNodes
        
        % Coordinates
        xPos = xLength*((jj-1)/xEles);
        yPos = yLength*((ii-1)/yEles);
        zPos = 0;
        
        % Create Node Object
        tempNode = Node(nodeId,xPos,yPos,zPos);
        
        % Build node
        flatPlate = flatPlate.AddNodeObject(tempNode);
        
        % Clamp all side nodes
        if jj == 1 || jj == xNodes || ii == 1 || ii == yNodes
            tempNode = tempNode.SetFixed();
            flatPlate.Add2Nset('BoundaryNodes', tempNode);
        else
            flatPlate.Add2Nset('InternalNodes', tempNode);
        end
        
        if jj == 26 && ii == 13
            flatPlate.Add2Nset('CenterNode', tempNode);
        end
            
        % Create element
        if ii > 1 && jj > 1
            s4Ele = S4S(elementId); elementId = elementId + 1;
            nodeIds = [(ii - 2)*xNodes + (jj - 1);
                       (ii - 2)*xNodes + (jj);
                       (ii - 1)*xNodes + (jj);
                       (ii - 1)*xNodes + (jj - 1)];
            s4Ele = s4Ele.Build(flatPlate.GetNodes(nodeIds), steel, shellSection);
            flatPlate.AddElementObject(s4Ele);
        end
        nodeId = nodeId + 1;
    end
end

% Update the model with correct DOF indexing for nodes and elements
flatPlate.Update();
flatPlate.Plot();

%% Linear modal analysis
nmodes = 10;
modal = Frequency('modal', flatPlate);
modal.Solve(nmodes);

%% Thermal step
xScale = 0.00;
xDisp = xScale*modal.phi(:,1)/norm(modal.phi(:,1));

% Create thermal object
name = 'fullDynamic';
thermal = Thermal(name, flatPlate, xDisp);

% Grab side node ids 
sideNodes = flatPlate.nset('BoundaryNodes');
sideNodes = [sideNodes{:}]; sideNodesId = [sideNodes.id];
internalNodes = flatPlate.nset('InternalNodes');
internalNodes = [internalNodes{:}]; internalNodeId = [internalNodes.id];

% Create thermal field for inner nodes
innerFieldTemperature = 18;
innerThermalField = ThermalField('Uniform',flatPlate,innerFieldTemperature);
innerThermalField.AddNodes(sideNodesId);

% Create thermal field for outer nodes
outerFieldTemperature = 1;
outerThermalField = ThermalField('Uniform',flatPlate,outerFieldTemperature);
outerThermalField.AddNodes(sideNodesId);

% Add thermal fields to step
thermal.AddThermalField(innerThermalField);
thermal.AddThermalField(outerThermalField);

% Grab center node and apply force if desired
forceNode = flatPlate.nset('CenterNode'); 
force = Force('CenterNode',flatPlate);
force.AddForce(forceNode,3,10);
thermal.AddForce(force);

% Add Monitor
thermal.AddMonitor(forceNode{1}.id, 3);              

% Solve thermal problem
thermal.Solve(10);

% Compute the stresses
sigma = flatPlate.ComputeStress(thermal.finalDisplacement);

% Compute the strains
epsilon = flatPlate.ComputeStrain(thermal.finalDisplacement);

% Plot final displacement field
figure;flatPlate.Plot(thermal.finalDisplacement,'Z'); colorbar;

% Modal solution about equilibrium state - recommended and quicker
thermal.Frequency(20);

% Plot linear frequency differences
figure; hold on
bar([modal.fn(1:5), thermal.fn(1:5)]); 
legend('Nominal','Heated - Equilibrium');
xlabel('Mode #'); ylabel('Frequency (Hz)');


figure
subplot(2,2,1)
title(['Unheated - Mode 1 : ',num2str(round(modal.fn(1))),' Hz'])
flatPlate.Plot(modal.phi(:,1)/norm(modal.phi(:,1)),'norm')
view([0 0 -1]); grid('on');
subplot(2,2,2)
title(['Heated - Mode 1 : ',num2str(round(thermal.fn(1))),' Hz'])
flatPlate.Plot(thermal.phi(:,1)/norm(thermal.phi(:,1)),'norm')
view([0 0 -1]); grid('on');
subplot(2,2,3)
title(['Unheated - Mode 3 : ',num2str(round(modal.fn(2))),' Hz'])
flatPlate.Plot(modal.phi(:,2)/norm(modal.phi(:,2)),'norm')
view([0 0 -1]); grid('on');
subplot(2,2,4)
title(['Heated - Mode 2 : ',num2str(round(thermal.fn(2))),' Hz'])
flatPlate.Plot(thermal.phi(:,2)/norm(thermal.phi(:,2)),'norm')
view([0 0 -1]); grid('on');
