%% This script creates a finite element model of a curved-beam using 
%  beam elements. The script performs the following analyses :
%  1) Linear Modal
%  2) implicit time integration with thermal effects and random loads
clear; clc; close all;
clear classes;
%% Build Model

curvedBeam = OsFern('curvedBeam');

% Plate Dimensions and mesh properties
xLength = 18; xEles = 80; xNodes = xEles + 1;

% Cross section properties
thk = 0.09; width = 1; beamSection.w = width; beamSection.t = thk;

% Radius of curvature
r = 81.25; zPosMax = r - sqrt(4*r^2 - xLength^2)/2;

% Material Properties (aluminum)
E = 10.6*10^6; rho = 2.588*10^-4; nu = 0.32;
alpha = 1.24E-005; refTemp = 0;
aluminum = Material('aluminum');
aluminum = aluminum.setProp('E',E);
aluminum = aluminum.setProp('rho',rho);
aluminum = aluminum.setProp('nu',nu);
aluminum = aluminum.setProp('alpha',alpha);
aluminum = aluminum.setProp('tRef',refTemp);

% Setup constraints
endConstraints = true;  
sideConstraints = true; % enforces inplane motion only

% Creat basic mesh
nodeId = 1; eleId = 1;
for ii = 1:xNodes
        
    % Coordinates
    xPos = xLength*((ii-1)/xEles);
    yPos = 0;
    zPos = sqrt(r^2 - (xPos-xLength/2)^2) - r + zPosMax;
    
    % Create Node Object
    tempNode = Node(nodeId,xPos,yPos,zPos);

    % Build node
    curvedBeam = curvedBeam.AddNodeObject(tempNode);

    % Clamp end nodes
    if ii == 1 || ii == xNodes
        tempNode = tempNode.SetFixed();
    else
        % Constrain in plane motion
        if sideConstraints
            tempNode = tempNode.SetFixed([2,4,6]);
        end
    end

    % Identify center node
    if ii == xEles/2 + 1
        curvedBeam.Add2Nset('CenterNode', tempNode);
        tempNode.SetXYZ(xPos + 1e-4,yPos,zPos);
    end

    % Create element
    if ii > 1
        b31Ele = B2(eleId); eleId = eleId + 1;
        eleNodeIds = [(ii - 1) (ii)]; csVec = [0 1 0];
        b31Ele = b31Ele.Build(curvedBeam.GetNodes(eleNodeIds),...
                 aluminum, beamSection,csVec);
        curvedBeam.AddElementObject(b31Ele); 
    end

    nodeId = nodeId + 1;
        
end

curvedBeam = curvedBeam.Update();
figure; curvedBeam.Plot(); 

%% Linear modal analysis
nmodes = 40;
modal = Frequency('modal', curvedBeam);
modal.Solve(nmodes);



%% Thermal step
xScale = 0;
xDisp = xScale*modal.phi(:,1)/norm(modal.phi(:,1));

name = 'fullDynamic';
thermal = Thermal(name, curvedBeam, xDisp);

% Create thermal field object
temperature = 50;
thermalField = ThermalField('UniformField',curvedBeam,temperature);
thermalField.AddFieldVector(ones(length(curvedBeam.node),1));

% Add thermal field to thermal object
thermal.AddThermalField(thermalField);

% Solve thermal problem
thermal.Solve(6);

% Solve eigenvalue problem about thermal equilibrium
thermal.Frequency(20);

% Plot final displacement field
figure; curvedBeam.Plot(thermal.finalDisplacement,'norm'); colorbar;
view([0 -1 0]);

% Plot difference
figure; hold on
plot(modal.fn,'-o'); plot(thermal.fn,'-o'); 
legend('Nominal','Heated'); xlabel('Mode #'); ylabel('Frequency (Hz)');

figure
subplot(3,2,1)
title(['Unheated : Mode 1 : ',num2str(round(modal.fn(1))),' Hz'])
curvedBeam.Plot(modal.phi(:,1)/norm(modal.phi(:,1)),'norm')
view([0 -1 0]); grid('on');
subplot(3,2,2)
title(['Heated : Mode 1 : ',num2str(round(thermal.fn(1))),' Hz'])
curvedBeam.Plot(thermal.phi(:,1)/norm(thermal.phi(:,1)),'norm')
view([0 -1 0]); grid('on');
subplot(3,2,3)
title(['Unheated : Mode 2 : ',num2str(round(modal.fn(2))),' Hz'])
curvedBeam.Plot(modal.phi(:,2)/norm(modal.phi(:,2)),'norm')
view([0 -1 0]); grid('on');
subplot(3,2,4)
title(['Heated : Mode 2 : ',num2str(round(thermal.fn(2))),' Hz'])
curvedBeam.Plot(thermal.phi(:,2)/norm(thermal.phi(:,2)),'norm')
view([0 -1 0]); grid('on');
subplot(3,2,5)
title(['Unheated : Mode 3 : ',num2str(round(modal.fn(3))),' Hz'])
curvedBeam.Plot(modal.phi(:,3)/norm(modal.phi(:,3)),'norm')
view([0 -1 0]); grid('on');
subplot(3,2,6)
title(['Heated : Mode 3 : ',num2str(round(thermal.fn(3))),' Hz'])
curvedBeam.Plot(thermal.phi(:,3)/norm(thermal.phi(:,3)),'norm')
view([0 -1 0]); grid('on');

%% Integrate Model
name = 'FullDynamic';


% Set duration and number of steps 
T = 5; dt = 1e-5; nSteps = floor(T/dt); fs = 1/dt;
t = linspace(0,T,nSteps); dt = T/length(t);

% Get the center node
cNode = curvedBeam.nset('CenterNode');
cNodeDof = cNode{1}.GetGlobalDof();
cNodeVertDof = cNodeDof(3);

% PSD properties
minFreq = 0; maxFreq = 500;
No = 1;
spl = 200;     % sound pressure level (dB)
pref = 2.9e-9; % reference pressure
Prms = pref*10.^(spl/20);
spl = 20*log10(Prms/pref); %(db)
Pms = Prms^2; 
density = Prms/(maxFreq-minFreq); % Computed density 
psd = @(freq) density*(freq < maxFreq).*(freq > minFreq) + 1E-6;
[amp, time, inputVar] = Amplitude.GenRandAmp(psd, fs, T, No);

% Create amplitude object
randAmp = Amplitude('Sine',time,amp);

% Create thermal field object
thermalField = ThermalField('Uniform',curvedBeam,temperature);
thermalField.AddFieldVector(ones(length(curvedBeam.node)));

% Generate pressure field
pVal = 1e0; % psi
pressure = Pressure('UniformPressure',curvedBeam,pVal);
pressure.AddElements(curvedBeam.element);
pressure.AddAmplitude(randAmp);

% Initialize dynamic object
dynamic = DynamicThermal(name, curvedBeam, thermal.finalDisplacement);

% Add rayleigh damping
dynamic.SetDamping(34,4e-6); % (mass proportional, stiffness proportional)

% Add dnodes and dof to monitor
dynamic.AddMonitor(cNode{1}.id, 3);

% Add plotting options
dynamic.SetPlotOpts(true,5);

% Apply thermal field
dynamic.AddThermalField(thermalField);

% Apply thermal field
dynamic.AddPressureField(pressure);

% Integrate model
tic
dynamic.Solve(T, dt);
tSimSerial = toc;

% Plot the response
figure;
plot(dynamic.t,dynamic.response/0.09,'LineWidth',6);
line([0 dynamic.t(end)],...
    [thermal.finalDisplacement(dynamic.monitor(1).dof),...
    thermal.finalDisplacement(dynamic.monitor(1).dof)]/0.09,...
    'LineStyle',':','Color','r','LineWidth',6);
xlabel('Time (s)'); ylabel('Response (in)')
legend('Dynamic Response','Static Solution')
title('Center Node Vertical Displacement');

figure;
plot(-1*dynamic.response(1:end-3)/0.09,rampAmp,'LineWidth',6);
ylabel('Pressure (psi)'); xlabel('Response (in)')
legend('Dynamic Response','Static Solution')
title('Center Node Vertical Displacement');


% PSD and PDF
[xPsd, fx] = pwelch(dynamic.response, fs);  % Obtain PSD of response
[xPdf, px] = ksdensity(dynamic.response'); % obtain PDF

% Plot the response
figure; plot(dynamic.t,dynamic.response); hold on
xlabel('Time (s)'); ylabel('Response (in)')

% Plot Response Spectrum
figure
semilogy(fx*fs/2/pi, xPsd);
xlabel('Frequency (Hz)')
ylabel('PSD (in^2/Hz)')
xlim([0, 2*maxFreq]);

% Plot Probabiliy Density Function Spectrum
figure
patch(px, xPdf/max(xPdf),'g')
xlabel('Displacement State (m)')
ylabel('PDF Value (-)')

