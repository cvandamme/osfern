%% This scripts generates a basic finite element model of a curved plate,
%  with an adjustable size and number of elements per direction, and
%  generates various ROMs.
%
%  Analyses Performed
%  1) Linear Modal
%  2) Nonlinear Static - Full FE
%  3) ROM Genearation via Shi and Mei, Modal Derivatives and Implicit
%     Condensations
%  4) Nonlinear Static - ROM

clear; clc; close all;
warning(['These scripts are used to demonstrate the ROM building capabilities ',...
        'in OsFern. ROMs generated are not gauranteed to be accurate'])
%% Build Model
curvedPlate = OsFern('curvedPlate');

% Plate Dimensions and mesh properties
xLength = 15.75; xEles = 64/2; xNodes = xEles + 1;
yLength = 9.75; yEles = 40/2; yNodes = yEles + 1;

% Radius of Curvature
r = 100; zPosMax = sqrt(r^2 - (xLength/2)^2) - r;

% Element/shell thickeness
thk = 0.048; shellSection.t = thk;

% Material Properties
E = 2.85e7; rho = 7.48e-4; nu = 0.3; alpha = 10.5e-6;
al = Material('aluminum');
al = al.setProp('E',E);
al = al.setProp('rho',rho);
al = al.setProp('nu',nu);

% Creat basic mesh
nodeId = 1;elementId = 1;
for ii = 1:yNodes
    for jj = 1:xNodes
        
        % Coordinates
        xPos = xLength*((jj-1)/xEles);
        yPos = yLength*((ii-1)/yEles);
        zPos = sqrt(r^2 - (xPos-xLength/2)^2) - r - zPosMax;
        
        % Build node
        tempNode = Node(nodeId, xPos, yPos, zPos);
        curvedPlate.AddNodeObject(tempNode);
        nodeId = nodeId + 1;
        
        if (jj == 1 || jj == xNodes)
            tempNode.SetFixed();
        end
        
        if (ii == 1 || ii == yNodes)
            tempNode.SetFixed();
        end
        
        if ii == xEles/2 + 1 && jj == yEles + 1
            curvedPlate.Add2Nset('CenterNode', tempNode);
        end
        
        % Create element
        if ii > 1 && jj > 1
            s4Ele = S4S(elementId); elementId = elementId + 1;
            nodeIds = [(ii - 2)*xNodes + (jj - 1);
                       (ii - 2)*xNodes + (jj);
                       (ii - 1)*xNodes + (jj);
                       (ii - 1)*xNodes + (jj - 1)];

            s4Ele.Build(curvedPlate.GetNodes(nodeIds), al, shellSection);
            curvedPlate.AddElementObject(s4Ele);
        end
        
    end
end

% Update the model with correct DOF indexing for nodes and elements
curvedPlate.Update();
curvedPlate.Plot();

%% Perform linear modal analysis
nmodes = 30;
modal = Frequency('modal', curvedPlate);
modal.Solve(nmodes);

figure
scale = 0.01;
subplot(4,2,1)
title(['Mode 1 : ',num2str(round(modal.fn(1))),' Hz'])
curvedPlate.Plot(scale*modal.phi(:,1),'norm')
view([0 0 -1]); grid('on');
subplot(4,2,2)
title(['Mode 2 : ',num2str(round(modal.fn(2))),' Hz'])
curvedPlate.Plot(scale*modal.phi(:,2),'norm')
view([0 0 -1]); grid('on');
subplot(4,2,3)
title(['Mode 3 : ',num2str(round(modal.fn(3))),' Hz'])
curvedPlate.Plot(scale*modal.phi(:,3),'norm')
view([0 0 -1]); grid('on');
subplot(4,2,4)
title(['Mode 4 : ',num2str(round(modal.fn(4))),' Hz'])
curvedPlate.Plot(scale*modal.phi(:,4),'norm')
view([0 0 -1]); grid('on');
subplot(4,2,5)
title(['Mode 5 : ',num2str(round(modal.fn(5))),' Hz'])
curvedPlate.Plot(scale*modal.phi(:,5),'norm')
view([0 0 -1]); grid('on');
subplot(4,2,6)
title(['Mode 6 : ',num2str(round(modal.fn(6))),' Hz'])
curvedPlate.Plot(scale*modal.phi(:,6),'norm')
view([0 0 -1]); grid('on');
subplot(4,2,7)
title(['Mode 7 : ',num2str(round(modal.fn(7))),' Hz'])
curvedPlate.Plot(scale*modal.phi(:,7),'norm')
view([0 0 -1]); grid('on');
subplot(4,2,8)
title(['Mode 8 : ',num2str(round(modal.fn(8))),' Hz'])
curvedPlate.Plot(scale*modal.phi(:,8),'norm')
view([0 0 -1]); grid('on');
% return
%% Create different type Roms 

% Modes to use
mind = [2];
loadMean = 2*thk*ones(length(mind),1);
loadSigma = 0.5*thk*loadMean;

% Rom Ic Controls
controls.sf = 1;
controls.df = 1;
controls.tf = 1;
controls.rf = 1;
controls.mf = 1;
controls.cf = 1;

% Create and build implicit condensation rom - lasso
laIcRom = LassoIcRom('IcRom',curvedPlate);
laIcRom.trainingControls.nObs = 5; % number of observations to make
laIcRom.Build(mind,loadMean,loadSigma);

% Specify and visualize different sparsities;
figure; 
laIcRom.MakeSparse(1);
subplot(3,2,1); laIcRom.PlotSparsity(); title(['Sparsity = ' , num2str(100), ' %']);
subplot(3,2,2); laIcRom.PlotKnl(laIcRom.Knl');title(['Sparsity = ' , num2str(100), ' %']);
laIcRom.MakeSparse(0.5)
subplot(3,2,3); laIcRom.PlotSparsity(); title(['Sparsity = ' , num2str(50), ' %']);
subplot(3,2,4); laIcRom.PlotKnl(laIcRom.Knl');title(['Sparsity = ' , num2str(50), ' %']);
laIcRom.MakeSparse(0.25)
subplot(3,2,5); laIcRom.PlotSparsity(); title(['Sparsity = ' , num2str(25), ' %']);
subplot(3,2,6); laIcRom.PlotKnl(laIcRom.Knl');title(['Sparsity = ' , num2str(25), ' %']);

%% Compute full and reduced space responses

% Generate response node
responseNode = curvedPlate.nset('CenterNode');  % Node number of force application
responseDof = 3; 	% DOF number of force application
nsteps = 10;        % Number of steps for NL calculation

% Generate pressure field
pVal = 0.25; % Psi
pressure = Pressure('UniformPressure',curvedPlate,pVal);
pressure.AddElements(curvedPlate.element);

% Peform nonlinear static solution - full FE
nLstatic = NLStatic('static', curvedPlate);
nLstatic.AddPressureField(pressure);
nLstatic.AddMonitor(responseNode{1}.id, responseDof);
nLstatic.Solve(nsteps);

% Create ReducedStep Object for Implicit Condensation RomDerivatives
laIcRom.MakeSparse(1);
icStep = ReducedStep('romStatic', laIcRom);
icStep.AddPressureField(pressure);
icStep.AddMonitor(responseNode{1}.id, responseDof);
icStep.Static(3*nsteps);
icResp(:,1) = icStep.finalDisplacement;

% Create ReducedStep Object for Implicit Condensation RomDerivatives
laIcRom.MakeSparse('opt');
icStep.Static(3*nsteps);
icResp(:,2) = icStep.finalDisplacement;

% Create ReducedStep Object for Implicit Condensation RomDerivatives
laIcRom.MakeSparse('std');
icStep.Static(3*nsteps);
icResp(:,3) = icStep.finalDisplacement;

% Plot comparison of response
figure;
subplot(2,2,1);
curvedPlate.Plot(nLstatic.finalDisplacement); 
view([0 0 -1]); colorbar; grid on; box on;
title('Full Fe Model')
subplot(2,2,2);
curvedPlate.Plot(icResp(:,1));
view([0 0 -1]); colorbar; grid on; box on;
title(['Implicit Condensation with Sparsity = ' , num2str(100), ' %'])
subplot(2,2,3);
curvedPlate.Plot(icResp(:,2));
view([0 0 -1]); colorbar; grid on; box on;
title(['Implicit Condensation with Sparsity = ' , num2str(50), ' %'])
subplot(2,2,4);
curvedPlate.Plot(icResp(:,3));
view([0 0 -1]); colorbar; grid on; box on;
title(['Implicit Condensation with Sparsity = ' , num2str(25), ' %'])

%% Compare the dynamic response due to an initial condition

% Set Damping
damping.alpha = 0;
damping.beta = 0;

% Generate initial displacement
sF = 1.0*thk;
x0 = sF*modal.phi(:,2)/max(abs((modal.phi(:,2))));

% Generate timeframe
nCycles = 1; T = nCycles*(1/modal.fn(1)); dt = 1e-4;

% Peform nonlinear static solution - full FE
fullDynamic = Dynamic('dynamic', curvedPlate,x0);
fullDynamic.AddMonitor(responseNode{1}.id, responseDof);
fullDynamic.SetDamping(damping.alpha,damping.beta);
fullDynamic.SetPlotOpts(true,1);
fullDynamic.Solve(T,dt);

%%
% Create ReducedStep Object for Implicit Condensation Rom
laIcRom.MakeSparse(1);
icDynamic = ReducedStep('romDynamic', laIcRom);
icDynamic.AddMonitor(responseNode{1}.id, responseDof);
icDynamic.initialDisplacement = x0;
icDynamic.SetDamping(damping.alpha,damping.beta);
icDynamic.Dynamic(T,dt);
icDynResp(:,1) = icDynamic.response;

% Create ReducedStep Object for Implicit Condensation RomDerivatives
laIcRom.MakeSparse(0.5);
icDynamic.Dynamic(T,dt);
icDynResp(:,2) = icDynamic.response;

% Create ReducedStep Object for Implicit Condensation RomDerivatives
laIcRom.MakeSparse(0.25);
icDynamic.Dynamic(T,dt);
icDynResp(:,3) = icDynamic.response;

% Plot the response of the nodal dof
figure; hold on
plot(fullDynamic.t,fullDynamic.response,'displayName','Full Fe');
plot(icDynamic.t,icDynResp(:,1),'displayName',...
      ['Implicit Condensation with Sparsity = ' , num2str(100), ' %']);
plot(icDynamic.t,icDynResp(:,2),'--','displayName',...
      ['Implicit Condensation with Sparsity = ' , num2str(50), ' %']);
plot(icDynamic.t,icDynResp(:,3),':','displayName',...
      ['Implicit Condensation with Sparsity = ' , num2str(25), ' %']);
xlabel('time (s)');ylabel('Displacement (mm)')