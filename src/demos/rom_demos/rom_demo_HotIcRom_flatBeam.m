%% This scripts generates a basic finite element model of a flat beam,
%  with an adjustable size and number of elements per direction. A thermal
%  state is specified and an implicit condensation rom is built around that
%  state.
%
%  Analyses Performed
%  1) Linear Modal
%  2) Nonlinear Static w/ Thermal Effects - Full FE
%  3) ROM Genearation via Implicit Condensation at thermal state
%  4) Nonlinear Static - FE 
%  6) Nonlinear Static - ROM
clear; clc; close all;
warning(['These scripts are used to demonstrate the ROM building capabilities ',...
        'in OsFern. ROMs generated are not gauranteed to be accurate'])
warning('This demo is still in developement')
%% Build Model

flatBeam = OsFern('flatBeam');

% Plate Dimensions and mesh properties
xLength = 9; xEles = 40; xNodes = xEles + 1;

% Cross section properties
thk = 0.031; width = 0.5;
beamSection.w = width; beamSection.t = thk;

% Material Properties (steel)
E = 2.97e7; rho = 0.000736; nu = 0.280712;
alpha = 6.3e-06;refTemp = 0;
steel = Material('steel');
steel = steel.setProp('E',E);
steel = steel.setProp('rho',rho);
steel = steel.setProp('nu',nu);
steel = steel.setProp('alpha',alpha);
steel = steel.setProp('tRef',refTemp);

% Setup constraints
endConstraints = true;  
sideConstraints = true; % enforces inplane motion only

% Creat basic mesh
nodeId = 1; eleId = 1;
for ii = 1:xNodes
        
    % Coordinates
    xPos = xLength*((ii-1)/xEles);
    yPos = 0; zPos = 0;
    
    % Create Node Object
    tempNode = Node(nodeId,xPos,yPos,zPos);

    % Build node
    flatBeam = flatBeam.AddNodeObject(tempNode);

    % Clamp end nodes
    if ii == 1 || ii == xNodes
        tempNode = tempNode.SetFixed();
    else
        % Constrain in plane motion
        if sideConstraints
            tempNode = tempNode.SetFixed([2,4,6]);
        end
    end

    % Identify center node
    if ii == xEles/2 + 1
        flatBeam.Add2Nset('CenterNode', tempNode);
    end

    % Create element
    if ii > 1
        b31Ele = B2(eleId); eleId = eleId + 1;
        eleNodeIds = [(ii - 1) (ii)]; csVec = [0 1 0];
        b31Ele = b31Ele.Build(flatBeam.GetNodes(eleNodeIds),...
                 steel, beamSection,csVec);
        flatBeam.AddElementObject(b31Ele); 
    end

    nodeId = nodeId + 1;
        
end

flatBeam = flatBeam.Update();
figure; flatBeam.Plot(); 

%% Linear modal analysis
nmodes = 40;
modal = Frequency('modal', flatBeam);
modal.Solve(nmodes);


%% Create HotModes ROM

% Generate pressure field
pVal = 0.25; % Psi
pressure = Pressure('UniformPressure',flatBeam,pVal);
pressure.AddElements(flatBeam.element);

% Choose modes
mind = [1,3,5];
wc = thk*ones(length(mind),1);
tempVec = [1e-6 2 4];
for ii = 1:length(tempVec)
    
    % Create thermal object
    thermal = Thermal(['Temp_',num2str(round(tempVec(ii)))],...
                flatBeam);

    % Create thermal field object
    thermalField = ThermalField('Uniform',flatBeam,tempVec(ii));
    thermalField.AddFieldVector(ones(length(flatBeam.node)));

    % Apply thermal field
    thermal.AddThermalField(thermalField);
    icRom(ii) = HotIcRom(['IcHotRom_Temp_',num2str(round(tempVec(ii)))],...
                    flatBeam,thermal);
    icRom(ii).Build(mind,wc);
    Knl2(ii) = icRom(ii).Knl(2);
    
    thermal.AddPressureField(pressure);
    thermal.Solve(10); Ufem(:,ii) = thermal.finalDisplacement;
    fVec = thermal.AssembleThermalField(0);
    thermalForce = Force('thermafORCE',flatBeam);
    thermalForce.AddForceVector(fVec);
    
    % Reduced step
    icStep = ReducedStep('romStatic', icRom(ii));
    icStep.AddPressureField(pressure);
    icStep.AddForce(thermalForce);
    icStep.Static(10); Urom(:,ii) = icStep.finalDisplacement;
end

% Plot results
figure; hold on; grid on; box on;
plot(Ufem(3:6:end,:))
plot(Urom(3:6:end,:),'--')
xlabel('Node #');ylabel('Displacement (in)');
legend('FE : \DeltaT=0^o','FE : \DeltaT=2^o','FE : \DeltaT=4^o',...
       'ROM : \DeltaT=0^o','ROM : \DeltaT=2^o','ROM : \DeltaT=4^o')