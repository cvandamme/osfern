%% This scripts generates a basic finite element model of a flat beam,
%  with an adjustable size and number of elements per direction. The script
%  then generates and
%  generates a ROM based upon the method from Shi and Mei

%  Analyses Performed
%  1) Linear Modal
%  2) Nonlinear Static - Full FE
%  3) ROM Genearation via Shi and Mei
%  4) Nonlinear Static - ROM

clear; clc; close all
warning(['These scripts are used to demonstrate the ROM building capabilities ',...
        'in OsFern. ROMs generated are not gauranteed to be accurate'])
warning('This demo is still in developement')
%% Build Model

% Initialize MatFem Object
flatBeam = OsFern('FlatBeam');

% Plate Dimensions and mesh properties
xLength = 9; xEles = 40; xNodes = xEles + 1;

% Element/shell thickeness - Metric
thk = 0.031; w = 0.5;
beamSection.w = w; beamSection.t = thk;

% Material Properties (steel) 
E = 10.6*10^6; rho = 2.588*10^-4; nu = 0.29; alpha = 6.3e-06;
steel = Material('steel');
steel = steel.setProp('E',E);
steel = steel.setProp('rho',rho);
steel = steel.setProp('nu',nu);

% Setup constraints
endConstraints = true; sideConstraints = true;

% Creat basic mesh
nodeId = 1; elementId = 1;
endNodes = []; sideNodes = [];
for ii = 1:xNodes
        
        % Coordinates
        xPos = xLength*((ii-1)/xEles);
        yPos = 0;
        zPos = 0;
        
        % Create Node Object
        tempNode = Node(nodeId,xPos,yPos,zPos);
        
        % Build node object
        flatBeam = flatBeam.AddNodeObject(tempNode);
        
        % Clamp end nodes
        if endConstraints
            if ii == 1 || ii == xNodes
                tempNode = tempNode.SetFixed();
            else
                if sideConstraints
                    tempNode = tempNode.SetFixed([2,4,6]);
                end
            end
        end
        
        if ii == xEles/2 + 1
            flatBeam.Add2Nset('CenterNode', tempNode);
        end
        
        if ii == xEles/4 + 1
            flatBeam.Add2Nset('QuarterNode', tempNode);
        end
        
        % Create element
        if ii > 1
            b31Ele = B2(elementId); elementId = elementId + 1;
            eleNodeIds = [(ii - 1) (ii)]; csVec = [0 1 0];
            b31Ele = b31Ele.Build(flatBeam.GetNodes(eleNodeIds),...
                steel, beamSection,csVec);
            flatBeam.AddElementObject(b31Ele);
        end
        
        nodeId = nodeId + 1;
end

% Build Constraints and DOF
flatBeam = flatBeam.Update();

% PLot Mesh
flatBeam.Plot();

% Initialize and solve modal equation
nmodes = 30;
modal = Frequency('modal', flatBeam);
modal.Solve(nmodes);
%% Perform linear modal analysis
nmodes = 30;
modal = Frequency('modal', flatBeam);
modal.Solve(nmodes);

figure
scale = 0.01;
subplot(4,2,1)
title(['Mode 1 : ',num2str(round(modal.fn(1))),' Hz'])
flatBeam.Plot(scale*modal.phi(:,1),'norm')
view([0 -1 0]); grid('on');
subplot(4,2,2)
title(['Mode 2 : ',num2str(round(modal.fn(2))),' Hz'])
flatBeam.Plot(scale*modal.phi(:,2),'norm')
view([0 -1 0]); grid('on');
subplot(4,2,3)
title(['Mode 3 : ',num2str(round(modal.fn(3))),' Hz'])
flatBeam.Plot(scale*modal.phi(:,3),'norm')
view([0 -1 0]); grid('on');
subplot(4,2,4)
title(['Mode 4 : ',num2str(round(modal.fn(4))),' Hz'])
flatBeam.Plot(scale*modal.phi(:,4),'norm')
view([0 -1 0]); grid('on');
subplot(4,2,5)
title(['Mode 5 : ',num2str(round(modal.fn(5))),' Hz'])
flatBeam.Plot(scale*modal.phi(:,5),'norm')
view([0 0 -1]); grid('on');
subplot(4,2,6)
title(['Mode 6 : ',num2str(round(modal.fn(6))),' Hz'])
flatBeam.Plot(scale*modal.phi(:,6),'norm')
view([0 0 -1]); grid('on');
subplot(4,2,7)
title(['Mode 7 : ',num2str(round(modal.fn(7))),' Hz'])
flatBeam.Plot(scale*modal.phi(:,7),'norm')
view([0 0 -1]); grid('on');
subplot(4,2,8)
title(['Mode 8 : ',num2str(round(modal.fn(8))),' Hz'])
flatBeam.Plot(scale*modal.phi(:,8),'norm')
view([0 0 -1]); grid('on');
%% Create different type Roms 

% Modes to use
mind = [1,3];
loadFactors = [0.5];
loadMean = thk*ones(length(mind),1).*loadFactors;
loadSigma = 0.01*loadMean;

% Rom Ic Controls
controls.sf = 1;
controls.df = 1;
controls.tf = 1;
controls.rf = 1;
controls.mf = 1;
controls.cf = 1;

% Create and build implicit condensation rom - lasso
laIcRom = LassoIcRom('IcRom',flatBeam);
laIcRom.trainingControls.nObs = 10; % number of observations to make
laIcRom.Build(mind,loadMean,loadSigma);

% Specify and visualize different sparsities;
figure; 
laIcRom.MakeSparse(1);
subplot(3,2,1); laIcRom.PlotSparsity(); title(['Sparsity = ' , num2str(100), ' %']);
subplot(3,2,2); laIcRom.PlotKnl(laIcRom.Knl');title(['Sparsity = ' , num2str(100), ' %']);
laIcRom.MakeSparse(0.5)
subplot(3,2,3); laIcRom.PlotSparsity(); title(['Sparsity = ' , num2str(50), ' %']);
subplot(3,2,4); laIcRom.PlotKnl(laIcRom.Knl');title(['Sparsity = ' , num2str(50), ' %']);
laIcRom.MakeSparse(0.25)
subplot(3,2,5); laIcRom.PlotSparsity(); title(['Sparsity = ' , num2str(25), ' %']);
subplot(3,2,6); laIcRom.PlotKnl(laIcRom.Knl');title(['Sparsity = ' , num2str(25), ' %']);
% return
%% Compute full and reduced space responses

% Generate response node
responseNode = flatBeam.nset('CenterNode');  % Node number of force application
responseDof = 3; 	% DOF number of force application
nsteps = 10;        % Number of steps for NL calculation

% Generate pressure field
pVal = 0.25; % Psi
pressure = Pressure('UniformPressure',flatBeam,pVal);
pressure.AddElements(flatBeam.element);
force = Force('CenterForce',flatBeam); 
force.AddForceVector(0.0001*modal.phi(:,1));

% Peform nonlinear static solution - full FE
nLstatic = NLStatic('static', flatBeam);
% nLstatic.AddPressureField(pressure);
nLstatic.AddForce(force);
nLstatic.AddMonitor(responseNode{1}.id, responseDof);
nLstatic.Solve(nsteps);

% Create ReducedStep Object for Implicit Condensation RomDerivatives
laIcRom.MakeSparse(1);
icStep = ReducedStep('romStatic', laIcRom);
% icStep.AddPressureField(pressure);
icStep.AddForce(force);
icStep.AddMonitor(responseNode{1}.id, responseDof);
icStep.Static(3*nsteps);
icResp(:,1) = icStep.finalDisplacement;

% Create ReducedStep Object for Implicit Condensation RomDerivatives
laIcRom.MakeSparse('opt');
icStep.Static(3*nsteps);
icResp(:,2) = icStep.finalDisplacement;

% Create ReducedStep Object for Implicit Condensation RomDerivatives
laIcRom.MakeSparse('std');
icStep.Static(3*nsteps);
icResp(:,3) = icStep.finalDisplacement;

% Plot comparison of response
figure;
subplot(2,2,1);
flatBeam.Plot(nLstatic.finalDisplacement); 
view([0 -1 0]); colorbar; grid on; box on;
title('Full Fe Model')
subplot(2,2,2);
flatBeam.Plot(icResp(:,1));
view([0 -1 0]); colorbar; grid on; box on;
title(['Implicit Condensation with Sparsity = ' , num2str(100), ' %'])
subplot(2,2,3);
flatBeam.Plot(icResp(:,2));
view([0 -1 0]); colorbar; grid on; box on;
title(['Implicit Condensation with Sparsity = ' , num2str(50), ' %'])
subplot(2,2,4);
flatBeam.Plot(icResp(:,3));
view([0 -1 0]); colorbar; grid on; box on;
title(['Implicit Condensation with Sparsity = ' , num2str(25), ' %'])

%% Compare the dynamic response due to an initial condition

% Set Damping
damping.alpha = 0;
damping.beta = 0;

% Generate initial displacement
sF = 1.0*thk;
x0 = sF*modal.phi(:,1)/max(abs((modal.phi(:,1))));

% Generate timeframe
nCycles = 1; T = nCycles*(1/modal.fn(1)); dt = 1e-4;

% Peform nonlinear static solution - full FE
fullDynamic = Dynamic('dynamic', flatBeam,x0);
fullDynamic.AddMonitor(responseNode{1}.id, responseDof);
fullDynamic.SetDamping(damping.alpha,damping.beta);
fullDynamic.SetPlotOpts(true,1);
fullDynamic.Solve(T,dt);

% Estimate damping from full fe
Cp = 34*flatBeam.GetMassMatrix(); 
Cm = laIcRom.basis(laIcRom.freeDof,:)'*Cp*laIcRom.basis(laIcRom.freeDof,:);
%%
% Create ReducedStep Object for Implicit Condensation Rom
laIcRom.MakeSparse(1);
icDynamic = ReducedStep('romDynamic', laIcRom);
icDynamic.AddMonitor(responseNode{1}.id, responseDof);
icDynamic.initialDisplacement = x0;
icDynamic.SetDamping(damping.alpha,damping.beta);
icDynamic.Dynamic(T,dt);
icDynResp(:,1) = icDynamic.response;

% Create ReducedStep Object for Implicit Condensation RomDerivatives
laIcRom.MakeSparse(0.5);
icDynamic.Dynamic(T,dt);
icDynResp(:,2) = icDynamic.response;

% Create ReducedStep Object for Implicit Condensation RomDerivatives
laIcRom.MakeSparse(0.25);
icDynamic.Dynamic(T,dt);
icDynResp(:,3) = icDynamic.response;

% Plot the response of the nodal dof
figure; hold on
plot(fullDynamic.t,fullDynamic.response,'displayName','Full Fe');
plot(icDynamic.t,icDynResp(:,1),'displayName',...
      ['Implicit Condensation with Sparsity = ' , num2str(100), ' %']);
plot(icDynamic.t,icDynResp(:,2),'--','displayName',...
      ['Implicit Condensation with Sparsity = ' , num2str(50), ' %']);
plot(icDynamic.t,icDynResp(:,3),':','displayName',...
      ['Implicit Condensation with Sparsity = ' , num2str(25), ' %']);
xlabel('time (s)');ylabel('Displacement (mm)')