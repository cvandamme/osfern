%% This scripts generates a basic finite element model of a curved plate,
%  with an adjustable size and number of elements per direction, and
%  generates various ROMs.
%
%  Analyses Performed
%  1) Linear Modal
%  2) Nonlinear Static - Full FE
%  3) ROM Genearation via Shi and Mei, Modal Derivatives and Implicit
%     Condensations
%  4) Nonlinear Static - ROM

clear; clc; close all;
warning(['These scripts are used to demonstrate the ROM building capabilities ',...
        'in OsFern. ROMs generated may not be accurate at this stage of development.'])
%% Build Model
CP = OsFern('curvedPlate');

% Plate Dimensions and mesh properties
xLength = 15.75; xEles = 64; xNodes = xEles + 1;
yLength = 9.75; yEles = 40; yNodes = yEles + 1;

% Radius of Curvature
r = 100; zPosMax = sqrt(r^2 - (xLength/2)^2) - r;

% Element/shell thickeness
thk = 0.048; shellSection.t = thk;

% Material Properties
E = 2.85e7; rho = 7.48e-4; nu = 0.3; alpha = 10.5e-6;
al = Material('aluminum');
al = al.setProp('E',E);
al = al.setProp('rho',rho);
al = al.setProp('nu',nu);

% Creat basic mesh
nodeId = 1;elementId = 1;
for ii = 1:yNodes
    for jj = 1:xNodes
        
        % Coordinates
        xPos = xLength*((jj-1)/xEles);
        yPos = yLength*((ii-1)/yEles);
        zPos = sqrt(r^2 - (xPos-xLength/2)^2) - r - zPosMax;
        
        % Build node
        tempNode = Node(nodeId, xPos, yPos, zPos);
        CP.AddNodeObject(tempNode);
        nodeId = nodeId + 1;
        
        if (jj == 1 || jj == xNodes)
            tempNode.SetFixed();
        end
        
        if (ii == 1 || ii == yNodes)
            tempNode.SetFixed();
        end
        
        if ii == xEles/2 + 1 && jj == yEles + 1
            CP.Add2Nset('CenterNode', tempNode);
        end
        
        % Create element
        if ii > 1 && jj > 1
            s4Ele = S4S(elementId); elementId = elementId + 1;
            nodeIds = [(ii - 2)*xNodes + (jj - 1);
                       (ii - 2)*xNodes + (jj);
                       (ii - 1)*xNodes + (jj);
                       (ii - 1)*xNodes + (jj - 1)];

            s4Ele.Build(CP.GetNodes(nodeIds), al, shellSection);
            CP.AddElementObject(s4Ele);
        end
        
    end
end

% Update the model with correct DOF indexing for nodes and elements
CP.Update();
% Create a plot of the FEM geometry.
figure(1)
CP.Plot();

%% Perform linear modal analysis
nmodes = 30;
modal = Frequency('modal', CP);
modal.Solve(nmodes);

figure(2)
scale = 0.01;
for k=1:8
    subplot(4,2,k)
    title(['Mode ',num2str(k),' : ',num2str(round(modal.fn(k))),' Hz'])
    CP.Plot(scale*modal.phi(:,k),'norm')
    view([0 0 -1]); grid('on');
end

% Also CP.uimplot
return
%% Different Roms 

% Modes to use
mind = [1,2];
loadFactors = [0.5];
loadScalings = thk*ones(length(mind),1).*loadFactors;

% Rom Ic Controls
controls.sf = 1;
controls.df = 1;
controls.tf = 1;
controls.rf = 1;
controls.mf = 1;
controls.cf = 1;

% Create and build She and Mei rom
smRom = SmRom('SmRom',CP);
smRom.Build(mind);

% Create and build modal derivatives rom
mdRom = MdRom('MdRom',CP);
mdRom.Build(mind);

% Create and build implicit condensation rom
icRom = IcRom('IcRom',CP);
icRom.Build(mind,loadScalings);

%% Compute full and reduced space responses

% Generate response node
responseNode = CP.nset('CenterNode');  % Node number of force application
responseDof = 3; 	% DOF number of force application
respind=responseNode{1}.globalDof(responseDof); % index of response node
nsteps = 10;        % Number of steps for NL calculation

% Generate pressure field
pVal = -1e-1; % psi
pressure = Pressure('UniformPressure',CP,pVal);
pressure.AddElements(CP.element);

% Peform nonlinear static solution - full FE
nLstatic = NLStatic('static', CP);
nLstatic.AddPressureField(pressure);
nLstatic.AddMonitor(responseNode{1}.id, responseDof);
nLstatic.Solve(nsteps);
maxDisp(1) = nLstatic.response;

% Create ReducedStep Object for She and Mei Rom
smStep = ReducedStep('SmRomStep', smRom);
smStep.AddPressureField(pressure);
smStep.AddMonitor(responseNode{1}.id, responseDof);
smStep.Static(nsteps);
maxDisp(2) = smStep.response;

% Create ReducedStep Object for Modal Derivatives
mdStep = ReducedStep('MdRomStep', mdRom);
mdStep.AddPressureField(pressure);
mdStep.AddMonitor(responseNode{1}.id, responseDof);
mdRom.nlOpt = true;
mdStep.Static(nsteps);
static1 = mdStep.finalDisplacement;
maxDisp(3) = mdStep.response;
mdRom.nlOpt = false;
mdStep.Static(nsteps);
static2 = mdStep.finalDisplacement;
maxDisp(4) = mdStep.response;

% Create ReducedStep Object for Implicit Condensation RomDerivatives
icStep = ReducedStep('MdRomStep', icRom);
icStep.AddPressureField(pressure);
icStep.AddMonitor(responseNode{1}.id, responseDof);
icStep.Static(nsteps);
maxDisp(5) = icStep.response;

% expand membrane displacements
icModalFinal = pinv(icRom.basis)*icStep.finalDisplacement;
icMembraneFinal = icRom.Expand(icModalFinal);
maxDisp(6) = icStep.finalDisplacement(icStep.monitor.dof)+...
    icMembraneFinal(icStep.monitor.dof);

% Plot comparison of response
figure(3);
subplot(2,3,1);
CP.Plot(nLstatic.finalDisplacement); view([0 0 -1]); colorbar;
title('Full Fe Model')
subplot(2,3,2);
CP.Plot(smStep.finalDisplacement); view([0 0 -1]); colorbar;
title('Shi and Mei')
subplot(2,3,3);
CP.Plot(static1); view([0 0 -1]); colorbar;
title('Modal Derivatives with K_{nl} assembled from full FE')
subplot(2,3,4);
CP.Plot(static2); view([0 0 -1]); colorbar;
title('Linear with MD Basis')
subplot(2,3,5);
CP.Plot(icStep.finalDisplacement); view([0 0 -1]); colorbar;
title('Implicit Condensation without expansion')
subplot(2,3,6);
CP.Plot(icStep.finalDisplacement+icMembraneFinal); view([0 0 -1]); colorbar;
title('Implicit Condensation with expansion')

maxDispErr = 100*(maxDisp(2:end) - maxDisp(1))/maxDisp(1)
figure(4);
subplot(2,1,1);
bar(maxDisp);
set(gca,'XTickLabel',{'Full Fe','Shi and Mei','MD w/ K_{nl}',...
            'Linear MD','IC','ICE'})
ylabel('Center Node Disp. (m)');
subplot(2,1,2);
bar(maxDispErr);
set(gca,'XTickLabel',{'Shi and Mei','MD w/ K_{nl}',...
            'Linear MD','IC','ICE'})
ylabel('Center Node Disp. Err (%)');

