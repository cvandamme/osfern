%% This scripts generates a basic finite element model of a flat plate,
%  with an adjustable size and number of elements per direction, and
%  generates a few ROMs.
%
%  Analyses Performed
%  1) Linear Modal
%  2) Nonlinear Static - Full FE
%  3) ROM Genearation via Shi and Mei, Modal Derivatives and Implicit
%     Condensation
%  4) Nonlinear Static - ROM
%  5) Nonlinear Dynamic - Full FE
%  6) Nonlinear Dynamic - ROM
clear; clc; close all
warning(['These scripts are used to demonstrate the ROM building capabilities ',...
        'in OsFern. ROMs generated are not gauranteed to be accurate'])
%% Build Model
flatPlate = OsFern('flatPlate');

% Plate Dimensions and mesh properties
xLength = 250; xEles = 24; xNodes = xEles + 1;
yLength = 252; yEles = 24; yNodes = yEles + 1;
zLength = 0; 

% Element/shell thickeness
thk = 1.5;
shellSection.t = thk;

% Material Properties
E = 71000; rho = 2.7000e-09; nu = 0.33; alpha = 6.3e-06;
al = Material('aluminum');
al = al.setProp('E',E);
al = al.setProp('rho',rho);
al = al.setProp('nu',nu);

% Creat basic mesh
nodeId = 1;
elementId = 1;
springId = 1000;
endNodes = []; sideNodes = [];
for ii = 1:yNodes
    for jj = 1:xNodes
        
        % Coordinates
        xPos = xLength*((jj-1)/xEles);
        yPos = yLength*((ii-1)/yEles);
        zPos = 0;
        
        % Build node
        tempNode = Node(nodeId, xPos, yPos, zPos);
        flatPlate.AddNodeObject(tempNode);
        nodeId = nodeId + 1;
        
        if (jj == 1 || jj == xNodes)
            tempNode.SetFixed();
        end
        
        if (ii == 1 || ii == xNodes)
            tempNode.SetFixed();
        end
        
        if ii == xEles/2 + 1 && jj == yEles/2 + 1
            flatPlate.Add2Nset('CenterNode', tempNode);
        end
        
        % Create element
        if ii > 1 && jj > 1
            s4Ele = S4S(elementId); elementId = elementId + 1;
            nodeIds = [(ii - 2)*xNodes + (jj - 1);
                       (ii - 2)*xNodes + (jj);
                       (ii - 1)*xNodes + (jj);
                       (ii - 1)*xNodes + (jj - 1)];

            s4Ele.Build(flatPlate.GetNodes(nodeIds), al, shellSection);
            flatPlate.AddElementObject(s4Ele);
        end
        
    end
end


% Update the model with correct DOF indexing for nodes and elements
flatPlate.Update();
flatPlate.Plot();

%% Perform linear modal analysis
nmodes = 30;
modal = Frequency('modal', flatPlate);
modal.Solve(nmodes);

figure
scale = 0.01;
subplot(4,2,1)
title(['Mode 1 : ',num2str(round(modal.fn(1))),' Hz'])
flatPlate.Plot(scale*modal.phi(:,1),'norm')
view([0 0 -1]); grid('on');
subplot(4,2,2)
title(['Mode 2 : ',num2str(round(modal.fn(2))),' Hz'])
flatPlate.Plot(scale*modal.phi(:,2),'norm')
view([0 0 -1]); grid('on');
subplot(4,2,3)
title(['Mode 3 : ',num2str(round(modal.fn(3))),' Hz'])
flatPlate.Plot(scale*modal.phi(:,3),'norm')
view([0 0 -1]); grid('on');
subplot(4,2,4)
title(['Mode 4 : ',num2str(round(modal.fn(4))),' Hz'])
flatPlate.Plot(scale*modal.phi(:,4),'norm')
view([0 0 -1]); grid('on');
subplot(4,2,5)
title(['Mode 5 : ',num2str(round(modal.fn(5))),' Hz'])
flatPlate.Plot(scale*modal.phi(:,5),'norm')
view([0 0 -1]); grid('on');
subplot(4,2,6)
title(['Mode 6 : ',num2str(round(modal.fn(6))),' Hz'])
flatPlate.Plot(scale*modal.phi(:,6),'norm')
view([0 0 -1]); grid('on');
subplot(4,2,7)
title(['Mode 7 : ',num2str(round(modal.fn(7))),' Hz'])
flatPlate.Plot(scale*modal.phi(:,7),'norm')
view([0 0 -1]); grid('on');
subplot(4,2,8)
title(['Mode 8 : ',num2str(round(modal.fn(8))),' Hz'])
flatPlate.Plot(scale*modal.phi(:,8),'norm')
view([0 0 -1]); grid('on');
%% Different Roms 

% Modes to use
mind = [1:6]; loadScalings = 0.5*thk*ones(size(mind))';

% Rom Ic Controls
controls.sf = 1;
controls.df = 1;
controls.tf = 1;
controls.rf = 1;
controls.mf = 1;
controls.cf = 1;

% Create and build She and Mei rom
smRom = SmRom('SmRom',flatPlate);
smRom.Build(mind);

% Create and build modal derivatives rom
mdRom = MdRom('MdRom',flatPlate);
mdRom.Build(mind);

% Create and build implicit condensation rom
icRom = IcRom('IcRom',flatPlate);
icRom.Build(mind,loadScalings);

% Create and build implicit condensation rom
edRom = EdRom('EdRom',flatPlate);
edRom.Build(mind,loadScalings);

%% Compute full and reduced space responses

% Generate response node
responseNode = flatPlate.nset('CenterNode');  % Node number of force application
responseDof = 3; 	% DOF number of force application
nsteps = 10;        % Number of steps for NL calculation

% Generate pressure field
pVal = 1e-2; % MPa
pressure = Pressure('UniformPressure',flatPlate,pVal);
pressure.AddElements(flatPlate.element);

% Peform nonlinear static solution - full FE
fullStatic = NLStatic('static', flatPlate);
fullStatic.AddPressureField(pressure);
fullStatic.AddMonitor(responseNode{1}.id, responseDof);
fullStatic.Solve(nsteps);

% Create ReducedStep Object for She and Mei Rom
smStatic = ReducedStep('SmRomStep', smRom);
smStatic.AddPressureField(pressure);
smStatic.AddMonitor(responseNode{1}.id, responseDof);
smStatic.Static(nsteps);

% Create ReducedStep Object for Modal Derivatives
mdStatic = ReducedStep('MdRomStep', mdRom);
mdStatic.AddPressureField(pressure);
mdStatic.AddMonitor(responseNode{1}.id, responseDof);
mdRom.nlOpt = true; mdStatic.Static(nsteps);
static1 = mdStatic.finalDisplacement;
mdRom.nlOpt = false; mdStatic.Static(nsteps);
static2 = mdStatic.finalDisplacement;

% Create ReducedStep Object for Implicit Condensation Rom
icStatic = ReducedStep('IcRomStep', icRom);
icStatic.AddPressureField(pressure);
icStatic.AddMonitor(responseNode{1}.id, responseDof);
icStatic.Static(nsteps);

% expand membrane displacements
icModalFinal = pinv(icRom.basis)*icStatic.finalDisplacement;
icMembraneFinal = icRom.Expand(icModalFinal);

% Create ReducedStep Object for Enforced Displacement Rom
edStatic = ReducedStep('EdRomStep', edRom);
edStatic.AddPressureField(pressure);
edStatic.AddMonitor(responseNode{1}.id, responseDof);
edStatic.Static(nsteps);

% Plot comparison of response
figure;
subplot(2,3,1);
flatPlate.Plot(fullStatic.finalDisplacement); view([0 0 -1]); colorbar;
title('Full Fe Model')
subplot(2,3,2);
flatPlate.Plot(smStatic.finalDisplacement); view([0 0 -1]); colorbar;
title('Shi and Mei')
subplot(2,3,3);
flatPlate.Plot(static1); view([0 0 -1]); colorbar;
title('Modal Derivatives with K_{nl} assembled from full FE')
subplot(2,3,4);
flatPlate.Plot(static2); view([0 0 -1]); colorbar;
title('Modal Derivatives without / K_{nl} assembled from full FE')
subplot(2,3,5);
flatPlate.Plot(icStatic.finalDisplacement); view([0 0 -1]); colorbar;
title('Implicit Condensation without expansion')
subplot(2,3,6);
flatPlate.Plot(edStatic.finalDisplacement); view([0 0 -1]); colorbar;
title('Enforced Displacement w/ (1,1) Dual Mode')

%% Compare the dynamic response due to an initial condition


% Generate initial displacement
sF = 1.5*thk;
x0 = sF*modal.phi(:,1)/max(abs((modal.phi(:,1))));

% Generate timeframe
nCycles = 10; T = nCycles*(1/modal.fn(1));
dt = 1e-5;

% Peform nonlinear static solution - full FE
fullDynamic = Dynamic('static', flatPlate,x0);
fullDynamic.AddMonitor(responseNode{1}.id, responseDof);
fullDynamic.SetDamping(60,0);
fullDynamic.SetPlotOpts(true,1);
fullDynamic.Solve(T,dt);

% Create ReducedStep Object for She and Mei Rom
smDynamic = ReducedStep('SmRomStep', smRom);
smDynamic.AddMonitor(responseNode{1}.id, responseDof);
smDynamic.initialDisplacement = x0;
smDynamic.SetDamping(60,0);
smDynamic.Dynamic(T,dt);

% Create ReducedStep Object for Modal Derivatives
mdDynamic = ReducedStep('MdRomStep', mdRom);
mdDynamic.AddMonitor(responseNode{1}.id, responseDof);
mdDynamic.initialDisplacement = x0;
mdDynamic.SetDamping(60,0);

mdRom.nlOpt = true; mdDynamic.Dynamic(T,dt); mdResp_1 = mdDynamic.response;
mdRom.nlOpt = false; mdDynamic.Dynamic(T,dt); mdResp_2 = mdDynamic.response;

% Create ReducedStep Object for Implicit Condensation Rom
icDynamic = ReducedStep('IcRomStep', icRom);
icDynamic.AddMonitor(responseNode{1}.id, responseDof);
icDynamic.initialDisplacement = x0;
icDynamic.SetDamping(60,0);
icDynamic.Dynamic(T,dt);

% expand membrane displacements
icModalFinal = pinv(icRom.basis)*icStatic.finalDisplacement;
icMembraneFinal = icRom.Expand(icModalFinal);

% Create ReducedStep Object for Enforced Displacement Rom
edDynamic = ReducedStep('EdRomStep', edRom);
edDynamic.AddMonitor(responseNode{1}.id, responseDof);
edDynamic.initialDisplacement = x0;
edDynamic.SetDamping(60,0);
edDynamic.Dynamic(T,dt);

% Plot the response of the nodal dof
figure; hold on
plot(fullDynamic.t,fullDynamic.response,'displayName','Full Fe');
plot(smDynamic.t,smDynamic.response,'displayName','Shi and Mei');
plot(mdDynamic.t,mdResp_1,'displayName','Modal Derivatives with Full FE K_{nl}');
plot(mdDynamic.t,mdResp_2,'displayName','Modal Derivatives without Full FE K_{nl}');
plot(icDynamic.t,icDynamic.response,'displayName','Implicit Condensation');
plot(edDynamic.t,edDynamic.response,'displayName','Enforced Displacement with (1,1) dual');
xlabel('time (s)');ylabel('Displacement (mm)')


