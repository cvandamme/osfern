%% This scripts generates a basic finite element model of a flat beam,
%  with an adjustable size and number of elements per direction, and
%  generates a few ROMs.
%
%  Analyses Performed
%  1) Linear Modal
%  2) Nonlinear Static - Full FE
%  3) ROM Genearation via Shi and Mei, Modal Derivatives and Implicit
%     Condensation
%  4) Nonlinear Static - ROM
%  (by manually running remaining cells)
%  5) Nonlinear Dynamic - Full FE
%  6) Nonlinear Dynamic - ROM
%
% Chris VanDamme, 2019
clear; clc; close all
warning(['These scripts are used to demonstrate the ROM building capabilities ',...
        'in OsFern. ROMs generated have not been validated to or verified to be accurate'])
%% Build Model

% Initialize MatFem Object
curvedBeam = OsFern('CurvedBeam');

% Plate Dimensions and mesh properties
xLength = 9; xEles = 40; xNodes = xEles + 1;

% Element/shell thickeness - Metric
thk = 0.031; w = 0.5;
beamSection.w = w; beamSection.t = thk;

% Material Properties (steel) 
E = 10.6*10^6; rho = 2.588*10^-4; nu = 0.29; alpha = 6.3e-06;
steel = Material('steel');
steel = steel.setProp('E',E);
steel = steel.setProp('rho',rho);
steel = steel.setProp('nu',nu);

% Setup constraints
endConstraints = true; sideConstraints = true;

% Create basic mesh
nodeId = 1; elementId = 1;
endNodes = []; sideNodes = [];
for ii = 1:xNodes
        
        % Coordinates
        xPos = xLength*((ii-1)/xEles);
        yPos = 0;
        zPos = 0;
        
        % Create Node Object
        tempNode = Node(nodeId,xPos,yPos,zPos);
        
        % Build node object
        curvedBeam = curvedBeam.AddNodeObject(tempNode);
        
        % Clamp end nodes
        if endConstraints
            if ii == 1 || ii == xNodes
                tempNode = tempNode.SetFixed();
            else
                if sideConstraints
                    tempNode = tempNode.SetFixed([2,4,6]);
                end
            end
        end
        
        if ii == xEles/2 + 1
            curvedBeam.Add2Nset('CenterNode', tempNode);
        end
        
        if ii == xEles/4 + 1
            curvedBeam.Add2Nset('QuarterNode', tempNode);
        end
        
        % Create element
        if ii > 1
            b31Ele = B2(elementId); elementId = elementId + 1;
            eleNodeIds = [(ii - 1) (ii)]; csVec = [0 1 0];
            b31Ele = b31Ele.Build(curvedBeam.GetNodes(eleNodeIds),...
                steel, beamSection,csVec);
            curvedBeam.AddElementObject(b31Ele);
        end
        
        nodeId = nodeId + 1;
end

% Build Constraints and DOF
curvedBeam = curvedBeam.Update();

% Plot Mesh
figure(1)
curvedBeam.Plot();

% Initialize and solve modal equation
nmodes = 30;
modal = Frequency('modal', curvedBeam);
modal.Solve(nmodes);

%% Perform linear modal analysis
nmodes = 30;
modal = Frequency('modal', curvedBeam);
modal.Solve(nmodes);

figure(2)
scale = 0.01;
for k=1:8
    subplot(4,2,k)
    title(['Mode ',num2str(k),' : ',num2str(round(modal.fn(k))),' Hz'])
    curvedBeam.Plot(scale*modal.phi(:,k),'norm')
    view([0 -1 0]); grid('on');
end
%% Create different type Roms 

% Modes to use
mind = [1,3,5];
loadFactors = [1];
loadScalings = thk*ones(length(mind),1).*loadFactors;

% Rom Ic Controls
controls.sf = 1;
controls.df = 1;
controls.tf = 1;
controls.rf = 1;
controls.mf = 1;
controls.cf = 1;

% Create and build She and Mei ROM
smRom = SmRom('SmRom',curvedBeam);
smRom.Build(mind);

% Create and build modal derivatives ROM
mdRom = MdRom('MdRom',curvedBeam);
mdRom.Build(mind);

% Create and build implicit condensation ROM
icRom = IcRom('IcRom',curvedBeam);
icRom.Build(mind,loadScalings);

% Create and build enforced displacements ROM
edRom = EdRom('EdRom',curvedBeam);
edRom.Build(mind,loadScalings);

%% Compute static response of full FEM and of ROMs

% Generate response node
responseNode = curvedBeam.nset('CenterNode');  % Node number of force application
responseDof = 3; 	% DOF number of force application
nsteps = 10;        % Number of steps for NL calculation

% Generate pressure field
pVal = 0.1; % Psi
pressure = Pressure('UniformPressure',curvedBeam,pVal);
pressure.AddElements(curvedBeam.element);

% Peform nonlinear static solution - full FE
nLstatic = NLStatic('static', curvedBeam);
nLstatic.AddPressureField(pressure);
nLstatic.AddMonitor(responseNode{1}.id, responseDof);
nLstatic.Solve(nsteps);

% Create ReducedStep Object for She and Mei Rom
smStep = ReducedStep('SmRomStep', smRom);
smStep.AddPressureField(pressure);
smStep.AddMonitor(responseNode{1}.id, responseDof);
smStep.Static(3*nsteps);

% Create ReducedStep Object for Modal Derivatives
mdStep = ReducedStep('MdRomStep', mdRom);
mdStep.AddPressureField(pressure);
mdStep.AddMonitor(responseNode{1}.id, responseDof);
mdRom.nlOpt = true; % Assemble full K and reduce using ROM basis at each timestep
mdStep.Static(3*nsteps);
mdStatic1 = mdStep.finalDisplacement;
mdRom.nlOpt = false; % ? Nonlinearity off?
mdStep.Static(3*nsteps);
mdStatic2 = mdStep.finalDisplacement;

% Create ReducedStep Object for Implicit Condensation RomDerivatives
icStep = ReducedStep('MdRomStep', icRom);
icStep.AddPressureField(pressure);
icStep.AddMonitor(responseNode{1}.id, responseDof);
icStep.Static(3*nsteps);

% Create ReducedStep Object for Implicit Condensation RomDerivatives
edStep = ReducedStep('MdRomStep', edRom);
edStep.AddPressureField(pressure);
edStep.AddMonitor(responseNode{1}.id, responseDof);
edStep.Static(3*nsteps);

% Plot comparison of response
figure(3);
subplot(2,3,1);
curvedBeam.Plot(nLstatic.finalDisplacement); 
view([0 -1 0]); colorbar; grid on; box on; zlim([0,0.04])
title('Full Fe Model')
subplot(2,3,2);
curvedBeam.Plot(smStep.finalDisplacement);
view([0 -1 0]); colorbar; grid on; box on; zlim([0,0.04])
title('Shi and Mei')
subplot(2,3,3);
curvedBeam.Plot(mdStatic1);
view([0 -1 0]); colorbar; grid on; box on; zlim([0,0.04])
title('Modal Derivatives with K_{nl} assembled from full FE')
subplot(2,3,4);
curvedBeam.Plot(mdStatic2);
view([0 -1 0]); colorbar; grid on; box on; zlim([0,0.04])
title('Linear Modal with MD Basis')
subplot(2,3,5);
curvedBeam.Plot(icStep.finalDisplacement); 
view([0 -1 0]); colorbar; grid on; box on; zlim([0,0.04])
title('Implicit Condensation without expansion')
subplot(2,3,6);
curvedBeam.Plot(edStep.finalDisplacement);
view([0 -1 0]); colorbar; grid on; box on; zlim([0,0.04])
title('Enforced Disp. with 3 dual modes')

% Compare accuracy
figure(4);
bar([nLstatic.response,smStep.response,mdStatic1(mdStep.monitor.dof),...
    mdStatic2(mdStep.monitor.dof),icStep.response,edStep.response]);
set(gca,'XTickLabel',{'Full Fe','Shi and Mei','MD w/K_{nl}',...
    'Linear MD','IC','ED w/3DM'});
ylabel('Center Displacement (m)');
title('\bfComparison of Static Accuracy');
% Line for truth value.
line([0.5,6.5],nLstatic.response*[1,1],'Color','r')

return
%% Compare the dynamic response due to an initial condition

% Generate initial displacement
sF = 1.5*thk;
x0 = sF*modal.phi(:,1)/max(abs((modal.phi(:,1))));

% Generate timeframe
nCycles = 10; T = nCycles*(1/modal.fn(1));
dt = 1e-4;

% Peform nonlinear static solution - full FE
fullDynamic = Dynamic('FullFEMDyn', curvedBeam,x0);
fullDynamic.AddMonitor(responseNode{1}.id, responseDof);
fullDynamic.SetDamping(60,0); % (alpha, beta) for proportional damping
fullDynamic.SetPlotOpts(true,1);
fullDynamic.Solve(T,dt);

%%
% Create ReducedStep Object for She and Mei Rom
smDynamic = ReducedStep('SmRomStep', smRom);
smDynamic.AddMonitor(responseNode{1}.id, responseDof);
smDynamic.initialDisplacement = x0;
smDynamic.SetDamping(60,0); % (alpha, beta) for proportional damping
smDynamic.Dynamic(T,dt);

% Create ReducedStep Object for Modal Derivatives
mdDynamic = ReducedStep('MdRomStep', mdRom);
mdDynamic.AddMonitor(responseNode{1}.id, responseDof);
mdDynamic.initialDisplacement = x0;
mdDynamic.SetDamping(60,0); % (alpha, beta) for proportional damping

mdRom.nlOpt = true; mdDynamic.Dynamic(T,dt); mdResp_1 = mdDynamic.response;
mdRom.nlOpt = false; mdDynamic.Dynamic(T,dt); mdResp_2 = mdDynamic.response;

% Create ReducedStep Object for Implicit Condensation Rom
icDynamic = ReducedStep('IcRomStep', icRom);
icDynamic.AddMonitor(responseNode{1}.id, responseDof);
icDynamic.initialDisplacement = x0;
icDynamic.SetDamping(60,0); % (alpha, beta) for proportional damping
icDynamic.Dynamic(T,dt);

% Create ReducedStep Object for Enforced Displacement Rom
edDynamic = ReducedStep('EdRomStep', edRom);
edDynamic.AddMonitor(responseNode{1}.id, responseDof);
edDynamic.initialDisplacement = x0;
edDynamic.SetDamping(60,0); % (alpha, beta) for proportional damping
edDynamic.Dynamic(T,dt);

% Plot the response of the nodal dof
figure(5); hold on
plot(fullDynamic.t,fullDynamic.response,'displayName','Full Fe');
plot(smDynamic.t,smDynamic.response,'displayName','Shi and Mei');
plot(mdDynamic.t,mdResp_1,'displayName','MD with Full FE K_{nl}');
plot(mdDynamic.t,mdResp_2,'displayName','Linear with MD Basis');
plot(icDynamic.t,icDynamic.response,'displayName','Implicit Condensation');
% plot(edDynamic.t,edDynamic.response,'displayName','Enforced Displacement with (1,1) dual');
    % ED ROM is unstable, so don't plot for now.
xlabel('time (s)');ylabel('Displacement (mm)'); legend
