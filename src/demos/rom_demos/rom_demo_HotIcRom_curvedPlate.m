%% This scripts generates a basic finite element model of a curved Plate,
%  with an adjustable size and number of elements per direction. A thermal
%  state is specified and an implicit condensation rom is built around that
%  state.
%
%  Analyses Performed
%  1) Linear Modal
%  2) Nonlinear Static w/ Thermal Effects - Full FE
%  3) ROM Genearation via Implicit Condensation at thermal state
%  4) Nonlinear Static - FE 
%  6) Nonlinear Static - ROM

clear; clc; close all;
warning(['These scripts are used to demonstrate the ROM building capabilities ',...
        'in OsFern. ROMs generated are not guaranteed to be accurate'])
%% Build Model
CP = OsFern('curvedPlate');

% Plate Dimensions (inches) and mesh properties
xLength = 15.75; xEles = 32; xNodes = xEles + 1; 
yLength = 9.75; yEles = 20; yNodes = yEles + 1;

% Radius of Curvature (in)
r = 100; zPosMax = sqrt(r^2 - (xLength/2)^2) - r;

% Element/shell thickeness
thk = 0.048; shellSection.t = thk;

% Material Properties
E = 2.85e7; % lb/in^2
rho = 0.000748; % lb-s^2/in^4
nu = 0.3; alpha = 10.5e-06; % in/(in*degF)
refTemp = 0; 
steel = Material('steel'); steel = steel.setProp('E',E);
steel = steel.setProp('rho',rho); steel = steel.setProp('nu',nu);
steel = steel.setProp('alpha',alpha);
steel = steel.setProp('tRef',refTemp);

% Create basic mesh
nodeId = 1;elementId = 1;
for ii = 1:yNodes
    for jj = 1:xNodes
        
        % Coordinates
        xPos = xLength*((jj-1)/xEles);
        yPos = yLength*((ii-1)/yEles);
        zPos = sqrt(r^2 - (xPos-xLength/2)^2) - r - zPosMax;
        
        % Build node
        tempNode = Node(nodeId, xPos, yPos, zPos);
        CP.AddNodeObject(tempNode);
        nodeId = nodeId + 1;
        
        if (jj == 1 || jj == xNodes)
            tempNode.SetFixed();
        end
        
        if (ii == 1 || ii == yNodes)
            tempNode.SetFixed();
        end
        
        if ii == xEles/2 + 1 && jj == yEles + 1
            CP.Add2Nset('CenterNode', tempNode);
        end
        
        % Create element
        if ii > 1 && jj > 1
            s4Ele = S4S(elementId); elementId = elementId + 1;
            nodeIds = [(ii - 2)*xNodes + (jj - 1);
                       (ii - 2)*xNodes + (jj);
                       (ii - 1)*xNodes + (jj);
                       (ii - 1)*xNodes + (jj - 1)];

            s4Ele.Build(CP.GetNodes(nodeIds), steel, shellSection);
            CP.AddElementObject(s4Ele);
        end
        
    end
end

% Update the model with correct DOF indexing for nodes and elements
CP.Update();
% Plot the FEM
CP.Plot();

%% Perform linear modal analysis
nmodes = 30;
modal = Frequency('modal', CP);
modal.Solve(nmodes);

figure(2)
scale = 0.01;
for k=1:8
    subplot(4,2,k)
    title(['Mode ',num2str(k),' : ',num2str(round(modal.fn(k))),' Hz'])
    CP.Plot(scale*modal.phi(:,k),'norm')
    view([0 0 -1]); grid('on');
end

% Also try CP.uimplot(modal.phi,modal.fn) for an interactive plot.

%% Create HotModes ROM

% Generate pressure field
pVal = 0.10; % Psi
pressure = Pressure('UniformPressure',CP,pVal);
pressure.AddElements(CP.element);

% Define Temperature Field
    % % CASE 1 - Uniform Temperature Field
    tDist=zeros(size(nodes,1),1); 

%     % CASE 2 - Heat the right side of the panel only with a uniform temp.
%     tDist=zeros(size(nodes,1),1); 
%         tDist(nodes(:,2)>15.75/2)=1; % apply temperature field to one side only.


% Choose modes
mind = [2,3,9,10]; thk = 0.048;
wc= 1*thk*ones(length(mind),1);
tempVec = [1e-4,10]%,20,30];
for ii = 1:length(tempVec)
    
    % Create thermal object
    thermal = Thermal(['Temp_',num2str(round(tempVec(ii)))],...
                CP);
            
    % Create thermal field object
    thermalField = ThermalField('Uniform',CP,tempVec(ii));
    thermalField.AddFieldVector(tDist);

    % Apply thermal field
    thermal.AddThermalField(thermalField);
    icRom(ii) = HotIcRom(['IcHotRom_Temp_',num2str(round(tempVec(ii)))],...
                    CP,thermal);
    icRom(ii).controls.mf = true;
    icRom(ii).Build(mind,wc);
    Knl{ii} = icRom(ii).Knl;
    
    % Solve full FE solution
    thermal.AddPressureField(pressure);
    thermal.Solve(10); U(:,ii) = thermal.finalDisplacement;
    
    % Solve ROM solution
    fVec = thermal.AssembleThermalField(0);
    thermalForce = Force('thermafORCE',CP);
    thermalForce.AddForceVector(fVec);
    
    % Reduced step
    icStep = ReducedStep('romStatic', icRom(ii));
    icStep.AddPressureField(pressure);
    icStep.AddForce(thermalForce);
    icStep.Static(10); Uic(:,ii) = icStep.finalDisplacement;
end

figure; 
subplot(2,2,1); CP.Plot(U(:,1)); title('\bfFull FEM Disp, Therm Step 1')
subplot(2,2,2); CP.Plot(Uic(:,1)); title('\bfROM Disp, Therm Step 1')
subplot(2,2,3); CP.Plot(U(:,2)); subplot(2,2,4); CP.Plot(Uic(:,2)); 
% subplot(4,2,5); CP.Plot(U(:,3)); subplot(4,2,6); CP.Plot(Uic(:,3)); 
% subplot(4,2,7); CP.Plot(U(:,4)); subplot(4,2,8); CP.Plot(Uic(:,4)); 
