%% This scripts generates a basic finite element model of a curved Plate,
%  with an adjustable size and number of elements per direction. A thermal
%  state is specified and an implicit condensation rom is built around that
%  state.
%
%  Analyses Performed
%  1) Linear Modal
%  2) Nonlinear Static w/ Thermal Effects - Full FE
%  3) ROM Genearation via Implicit Condensation at thermal state
%  4) Nonlinear Static - FE 
%  6) Nonlinear Static - ROM

clear; clc; close all;
warning(['These scripts are used to demonstrate the ROM building capabilities ',...
        'in OsFern. ROMs generated are not gauranteed to be accurate'])
%% Build Model
curvedPlate = OsFern('curvedPlate');

% Plate Dimensions and mesh properties
xLength = 15.75; xEles = 32; xNodes = xEles + 1;
yLength = 9.75; yEles = 20; yNodes = yEles + 1;

% Radius of Curvature
r = 100; zPosMax = sqrt(r^2 - (xLength/2)^2) - r;

% Element/shell thickeness
thk = 0.048; shellSection.t = thk;

% Material Properties
E = 2.85e7; rho = 0.000748; nu = 0.3; 
alpha = 10.5e-06; refTemp = 0; 
steel = Material('steel');steel = steel.setProp('E',E);
steel = steel.setProp('rho',rho);steel = steel.setProp('nu',nu);
steel = steel.setProp('alpha',alpha);
steel = steel.setProp('tRef',refTemp);

% Creat basic mesh
nodeId = 1;elementId = 1;
for ii = 1:yNodes
    for jj = 1:xNodes
        
        % Coordinates
        xPos = xLength*((jj-1)/xEles);
        yPos = yLength*((ii-1)/yEles);
        zPos = sqrt(r^2 - (xPos-xLength/2)^2) - r - zPosMax;
        
        % Build node
        tempNode = Node(nodeId, xPos, yPos, zPos);
        curvedPlate.AddNodeObject(tempNode);
        nodeId = nodeId + 1;
        
        if (jj == 1 || jj == xNodes)
            tempNode.SetFixed();
        end
        
        if (ii == 1 || ii == yNodes)
            tempNode.SetFixed();
        end
        
        if ii == xEles/2 + 1 && jj == yEles + 1
            curvedPlate.Add2Nset('CenterNode', tempNode);
        end
        
        % Create element
        if ii > 1 && jj > 1
            s4Ele = S4S(elementId); elementId = elementId + 1;
            nodeIds = [(ii - 2)*xNodes + (jj - 1);
                       (ii - 2)*xNodes + (jj);
                       (ii - 1)*xNodes + (jj);
                       (ii - 1)*xNodes + (jj - 1)];

            s4Ele.Build(curvedPlate.GetNodes(nodeIds), steel, shellSection);
            curvedPlate.AddElementObject(s4Ele);
        end
        
    end
end

% Update the model with correct DOF indexing for nodes and elements
curvedPlate.Update();
curvedPlate.Plot();

%% Perform linear modal analysis
nmodes = 30;
modal = Frequency('modal', curvedPlate);
modal.Solve(nmodes);

figure
scale = 0.01;
subplot(4,2,1)
title(['Mode 1 : ',num2str(round(modal.fn(1))),' Hz'])
curvedPlate.Plot(scale*modal.phi(:,1),'norm')
view([0 0 -1]); grid('on');
subplot(4,2,2)
title(['Mode 2 : ',num2str(round(modal.fn(2))),' Hz'])
curvedPlate.Plot(scale*modal.phi(:,2),'norm')
view([0 0 -1]); grid('on');
subplot(4,2,3)
title(['Mode 3 : ',num2str(round(modal.fn(3))),' Hz'])
curvedPlate.Plot(scale*modal.phi(:,3),'norm')
view([0 0 -1]); grid('on');
subplot(4,2,4)
title(['Mode 4 : ',num2str(round(modal.fn(4))),' Hz'])
curvedPlate.Plot(scale*modal.phi(:,4),'norm')
view([0 0 -1]); grid('on');
subplot(4,2,5)
title(['Mode 5 : ',num2str(round(modal.fn(5))),' Hz'])
curvedPlate.Plot(scale*modal.phi(:,5),'norm')
view([0 0 -1]); grid('on');
subplot(4,2,6)
title(['Mode 6 : ',num2str(round(modal.fn(6))),' Hz'])
curvedPlate.Plot(scale*modal.phi(:,6),'norm')
view([0 0 -1]); grid('on');
subplot(4,2,7)
title(['Mode 7 : ',num2str(round(modal.fn(7))),' Hz'])
curvedPlate.Plot(scale*modal.phi(:,7),'norm')
view([0 0 -1]); grid('on');
subplot(4,2,8)
title(['Mode 8 : ',num2str(round(modal.fn(8))),' Hz'])
curvedPlate.Plot(scale*modal.phi(:,8),'norm')
view([0 0 -1]); grid('on');

%% Create HotModes ROM

% Generate pressure field
pVal = 0.10; % Psi
pressure = Pressure('UniformPressure',curvedPlate,pVal);
pressure.AddElements(curvedPlate.element);

% Choose modes
mind = [1,2,3,4];
tempVec = [1e-6,50];
for ii = 1:length(tempVec)
    
    % Create thermal object
    thermal = Thermal(['Temp_',num2str(round(tempVec(ii)))],...
                curvedPlate);

    % Create thermal field object
    thermalField = ThermalField('Uniform',curvedPlate,tempVec(ii));
    thermalField.AddFieldVector(ones(length(curvedPlate.node)));

    % Apply thermal field
    thermal.AddThermalField(thermalField);
    mdRom(ii) = HotMdRom(['HotMdRom_Temp_',num2str(round(tempVec(ii)))],...
                curvedPlate,thermal);
    mdRom(ii).Build(mind);
    
    % Solve full FE solution
    thermal.AddPressureField(pressure);
    thermal.Solve(10); U(:,ii) = thermal.finalDisplacement;
    
    % Solve ROM solution
    fVec = thermal.AssembleThermalField(0);
    thermalForce = Force('thermafORCE',curvedPlate);
    thermalForce.AddForceVector(fVec);
    
    % Reduced step
    mdStep = ReducedStep('romStatic', mdRom(ii));
    mdStep.AddPressureField(pressure);
    mdStep.AddForce(thermalForce);
    mdStep.Static(100); Uic(:,ii) = mdStep.finalDisplacement;
end

figure;
subplot(4,2,1); curvedPlate.Plot(U(:,1)); subplot(4,2,2); curvedPlate.Plot(Uic(:,1)); 
subplot(4,2,3); curvedPlate.Plot(U(:,2)); subplot(4,2,4); curvedPlate.Plot(Uic(:,2)); 
subplot(4,2,5); curvedPlate.Plot(U(:,3)); subplot(4,2,6); curvedPlate.Plot(Uic(:,3)); 
subplot(4,2,7); curvedPlate.Plot(U(:,4)); subplot(4,2,8); curvedPlate.Plot(Uic(:,4)); 
