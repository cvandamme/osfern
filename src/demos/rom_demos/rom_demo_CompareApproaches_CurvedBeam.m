%% This scripts generates a basic finite element model of a curved beam,
%  with an adjustable size and number of elements per direction, and
%  generates a few ROMs.
%
%  Analyses Performed
%  1) Linear Modal
%  2) Nonlinear Static - Full FE
%  3) ROM Genearation via Shi and Mei, Modal Derivatives and Implicit
%     Condensation
%  4) Nonlinear Static - ROM
%  5) Nonlinear Dynamic - Full FE
%  6) Nonlinear Dynamic - ROM
clear; clc; close all
warning(['These scripts are used to demonstrate the ROM building capabilities ',...
        'in OsFern. ROMs generated are not gauranteed to be accurate'])
%% Build Model

% Initialize MatFem Object
curvedBeam = OsFern('CurvedBeam');

% Plate Dimensions and mesh properties
xLength = 18; xEles = 80; xNodes = xEles + 1;

% Element/shell thickeness - Metric
thk = 0.09; w = 1;
beamSection.w = w; beamSection.t = thk;

% Material Properties (PLA) - Metric E = 1.723e9
E = 10.6*10^6; rho = 2.588*10^-4; nu = 0.29; alpha = 6.3e-06;
steel = Material('pla');
steel = steel.setProp('E',E);
steel = steel.setProp('rho',rho);
steel = steel.setProp('nu',nu);

% Radius of Curvature
r = 81.25; zPosMax = sqrt(r^2 - (xLength/2)^2) - r;

% Setup constraints
endConstraints = true; sideConstraints = true;

% Decide if doing force/damped analysis or just NNM
forced = false;

% Creat basic mesh
nodeId = 1; elementId = 1;
endNodes = []; sideNodes = [];
for ii = 1:xNodes
        
        % Coordinates
        xPos = xLength*((ii-1)/xEles);
        yPos = 0;
        zPos = sqrt(r^2 - (xPos-xLength/2)^2) - r - zPosMax;
        
        % Create Node Object
        tempNode = Node(nodeId,xPos,yPos,zPos);
        
        % Build node object
        curvedBeam = curvedBeam.AddNodeObject(tempNode);
        
        % Clamp end nodes
        if endConstraints
            if ii == 1 || ii == xNodes
                tempNode = tempNode.SetFixed();
            else
                if sideConstraints
                    tempNode = tempNode.SetFixed([2,4,6]);
                end
            end
        end
        
        if ii == xEles/2 + 1
            curvedBeam.Add2Nset('CenterNode', tempNode);
        end
        
        if ii == xEles/4 + 1
            curvedBeam.Add2Nset('QuarterNode', tempNode);
        end
        
        % Create element
        if ii > 1
            b31Ele = B2(elementId); elementId = elementId + 1;
            eleNodeIds = [(ii - 1) (ii)]; csVec = [0 1 0];
            b31Ele = b31Ele.Build(curvedBeam.GetNodes(eleNodeIds),...
                steel, beamSection,csVec);
            curvedBeam.AddElementObject(b31Ele);
        end
        
        nodeId = nodeId + 1;
end

% Build Constraints and DOF
curvedBeam = curvedBeam.Update();

% PLot Mesh
curvedBeam.Plot();

% Initialize and solve modal equation
nmodes = 30;
modal = Frequency('modal', curvedBeam);
modal.Solve(nmodes);
%% Perform linear modal analysis
nmodes = 30;
modal = Frequency('modal', curvedBeam);
modal.Solve(nmodes);

figure
scale = 0.01;
subplot(4,2,1)
title(['Mode 1 : ',num2str(round(modal.fn(1))),' Hz'])
curvedBeam.Plot(scale*modal.phi(:,1),'norm')
view([0 -1 0]); grid('on');
subplot(4,2,2)
title(['Mode 2 : ',num2str(round(modal.fn(2))),' Hz'])
curvedBeam.Plot(scale*modal.phi(:,2),'norm')
view([0 -1 0]); grid('on');
subplot(4,2,3)
title(['Mode 3 : ',num2str(round(modal.fn(3))),' Hz'])
curvedBeam.Plot(scale*modal.phi(:,3),'norm')
view([0 -1 0]); grid('on');
subplot(4,2,4)
title(['Mode 4 : ',num2str(round(modal.fn(4))),' Hz'])
curvedBeam.Plot(scale*modal.phi(:,4),'norm')
view([0 -1 0]); grid('on');
subplot(4,2,5)
title(['Mode 5 : ',num2str(round(modal.fn(5))),' Hz'])
curvedBeam.Plot(scale*modal.phi(:,5),'norm')
view([0 0 -1]); grid('on');
subplot(4,2,6)
title(['Mode 6 : ',num2str(round(modal.fn(6))),' Hz'])
curvedBeam.Plot(scale*modal.phi(:,6),'norm')
view([0 0 -1]); grid('on');
subplot(4,2,7)
title(['Mode 7 : ',num2str(round(modal.fn(7))),' Hz'])
curvedBeam.Plot(scale*modal.phi(:,7),'norm')
view([0 0 -1]); grid('on');
subplot(4,2,8)
title(['Mode 8 : ',num2str(round(modal.fn(8))),' Hz'])
curvedBeam.Plot(scale*modal.phi(:,8),'norm')
view([0 0 -1]); grid('on');
%% Create different type Roms 

% Modes to use
mind = [1:4];
loadFactors = [0.25 2];
loadScalings = thk*ones(length(mind),1).*loadFactors;

% Rom Ic Controls
controls.sf = 1;
controls.df = 1;
controls.tf = 1;
controls.rf = 1;
controls.mf = 1;
controls.cf = 1;

% Create and build She and Mei rom
smRom = SmRom('SmRom',curvedBeam);
smRom.Build(mind);

% Create and build modal derivatives rom
mdRom = MdRom('MdRom',curvedBeam);
mdRom.Build(mind);

% Create and build implicit condensation rom
icRom = IcRom('IcRom',curvedBeam);
icRom.Build(mind,loadScalings);

%% Compute full and reduced space responses

% Generate response node
responseNode = curvedBeam.nset('CenterNode');  % Node number of force application
responseDof = 3; 	% DOF number of force application
nsteps = 10;        % Number of steps for NL calculation

% Generate pressure field
pVal = -1e0; % Psi
pressure = Pressure('UniformPressure',curvedBeam,pVal);
pressure.AddElements(curvedBeam.element);

% Peform nonlinear static solution - full FE
nLstatic = NLStatic('static', curvedBeam);
nLstatic.AddPressureField(pressure);
nLstatic.AddMonitor(responseNode{1}.id, responseDof);
nLstatic.Solve(nsteps);

% Create ReducedStep Object for She and Mei Rom
smStep = ReducedStep('SmRomStep', smRom);
smStep.AddPressureField(pressure);
nLstatic.AddMonitor(responseNode{1}.id, responseDof);
smStep.Static(3*nsteps);

% Create ReducedStep Object for Modal Derivatives
mdStep = ReducedStep('MdRomStep', mdRom);
mdStep.AddPressureField(pressure);
nLstatic.AddMonitor(responseNode{1}.id, responseDof);
mdRom.nlOpt = true;
mdStep.Static(3*nsteps);
static1 = mdStep.finalDisplacement;
mdRom.nlOpt = false;
mdStep.Static(3*nsteps);
static2 = mdStep.finalDisplacement;

% Create ReducedStep Object for Implicit Condensation RomDerivatives
icStep = ReducedStep('MdRomStep', icRom);
icStep.AddPressureField(pressure);
icStep.AddMonitor(responseNode{1}.id, responseDof);
icStep.Static(3*nsteps);

% expand membrane displacements
icModalFinal = pinv(icRom.basis)*icStep.finalDisplacement;
icMembraneFinal = icRom.Expand(icModalFinal);

% Plot comparison of response
figure;
subplot(2,3,1);
curvedBeam.Plot(nLstatic.finalDisplacement); 
view([0 -1 0]); colorbar; grid on; box on;
title('Full Fe Model')
subplot(2,3,2);
curvedBeam.Plot(smStep.finalDisplacement);
view([0 -1 0]); colorbar; grid on; box on;
title('Shi and Mei')
subplot(2,3,3);
curvedBeam.Plot(static1);
view([0 -1 0]); colorbar; grid on; box on;
title('Modal Derivatives with K_{nl} assembled from full FE')
subplot(2,3,4);
curvedBeam.Plot(static2);
view([0 -1 0]); colorbar; grid on; box on;
title('Modal Derivatives without / K_{nl} assembled from full FE')
subplot(2,3,5);
curvedBeam.Plot(icStep.finalDisplacement); 
view([0 -1 0]); colorbar; grid on; box on;
title('Implicit Condensation without expansion')
subplot(2,3,6);
curvedBeam.Plot(icStep.finalDisplacement+icMembraneFinal);
view([0 -1 0]); colorbar; grid on; box on;
title('Implicit Condensation with expansion')

%% Compare the dynamic response due to an initial condition


% Generate initial displacement
sF = 1.5*thk;
x0 = sF*modal.phi(:,1)/max(abs((modal.phi(:,1))));

% Generate timeframe
nCycles = 10; T = nCycles*(1/modal.fn(1));
dt = 1e-5;

% Peform nonlinear static solution - full FE
fullDynamic = Dynamic('static', curvedBeam,x0);
fullDynamic.AddMonitor(responseNode{1}.id, responseDof);
fullDynamic.SetDamping(60,0);
fullDynamic.SetPlotOpts(true,1);
fullDynamic.Solve(T,dt);

% Create ReducedStep Object for She and Mei Rom
smDynamic = ReducedStep('SmRomStep', smRom);
smDynamic.AddMonitor(responseNode{1}.id, responseDof);
smDynamic.initialDisplacement = x0;
smDynamic.SetDamping(60,0);
smDynamic.Dynamic(T,dt);

% Create ReducedStep Object for Modal Derivatives
mdDynamic = ReducedStep('MdRomStep', mdRom);
mdDynamic.AddMonitor(responseNode{1}.id, responseDof);
mdDynamic.initialDisplacement = x0;
mdDynamic.SetDamping(60,0);

mdRom.nlOpt = true; mdDynamic.Dynamic(T,dt); mdResp_1 = mdDynamic.response;
mdRom.nlOpt = false; mdDynamic.Dynamic(T,dt); mdResp_2 = mdDynamic.response;

% Create ReducedStep Object for Implicit Condensation Rom
icDynamic = ReducedStep('IcRomStep', icRom);
icDynamic.AddMonitor(responseNode{1}.id, responseDof);
icDynamic.initialDisplacement = x0;
icDynamic.SetDamping(60,0);
icDynamic.Dynamic(T,dt);

% Create ReducedStep Object for Enforced Displacement Rom
edDynamic = ReducedStep('EdRomStep', edRom);
edDynamic.AddMonitor(responseNode{1}.id, responseDof);
edDynamic.initialDisplacement = x0;
edDynamic.SetDamping(60,0);
edDynamic.Dynamic(T,dt);

% Plot the response of the nodal dof
figure; hold on
plot(fullDynamic.t,fullDynamic.response,'displayName','Full Fe');
plot(smDynamic.t,smDynamic.response,'displayName','Shi and Mei');
plot(mdDynamic.t,mdResp_1,'displayName','Modal Derivatives with Full FE K_{nl}');
plot(mdDynamic.t,mdResp_2,'displayName','Modal Derivatives without Full FE K_{nl}');
plot(icDynamic.t,icDynamic.response,'displayName','Implicit Condensation');
plot(edDynamic.t,edDynamic.response,'displayName','Enforced Displacement with (1,1) dual');
xlabel('time (s)');ylabel('Displacement (mm)')
