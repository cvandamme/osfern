%% This scripts generates a basic finite element model of a
%  flat Beam modeled with 2-Node beam Elements.
%
%   Analyses performed
%   1) Linear Buckling
%   2) Nonlinear Buckling
% 
% 
% Christopher Van Damme - 07/12/2018
% cvandamme@wisc.edu , cvandamme13@gmail.com
% 
% Add liscense
% 
clear; clc; close all; clear classes;
warning('Buckling functionality has not been validated')
%% Build Model

flatBeam = OsFern('flatBeam');

% Plate Dimensions and mesh properties (Metric)
xLength = 1; xEles = 20; xNodes = xEles + 1;

% Cross section properties
thk = 0.01; width = 0.01;
flatBeamSection.w = width; flatBeamSection.t = thk;

% Material Properties (steel)
E = 210e9; rho = 7850; nu = 0.29;
alpha = 6.3e-06; refTemp = 77; 
steel = Material('steel');
steel = steel.setProp('E',E);
steel = steel.setProp('rho',rho);
steel = steel.setProp('nu',nu);
steel = steel.setProp('alpha',alpha);
steel = steel.setProp('tRef',refTemp);

% Setup constraints
endConstraints = true;  
sideConstraints = true; % enforces inplane motion only

% Creat basic mesh
nodeId = 1; eleId = 1;
endNodes = []; sideNodes = [];
for ii = 1:xNodes
        
    % Coordinates
    xPos = xLength*((ii-1)/xEles);
    yPos = 0; 
    zPos = 0;

    % Create Node Object
    tempNode = Node(nodeId,xPos,yPos,zPos);

    % Build node
    flatBeam = flatBeam.AddNodeObject(tempNode);

    % Clamp end nodes
    if endConstraints
        if ii == 1
            tempNode = tempNode.SetFixed([2,3,4,6]);
        elseif ii == xNodes
            tempNode = tempNode.SetFixed([2,3,4,6]);
            flatBeam.Add2Nset('ForceNode', tempNode);
        else
            if sideConstraints
                tempNode = tempNode.SetFixed([2,4,6]);
            end
        end
    end

    if ii == xEles/2 + 1
        flatBeam.Add2Nset('CenterNode', tempNode);
    end

    % Create element
    if ii > 1
        b31Ele = B2(eleId); eleId = eleId + 1;
        eleNodeIds = [(ii - 1) (ii)]; csVec = [0 1 0];
        b31Ele = b31Ele.Build(flatBeam.GetNodes(eleNodeIds),...
                 steel, flatBeamSection,csVec);
        flatBeam.AddElementObject(b31Ele); 
    end

    nodeId = nodeId + 1;
        
end

flatBeam = flatBeam.Update(); 
figure; flatBeam.Plot(); view([0 -1 0]);

%% Static Solutions
name = 'CenterLoad';

% Get the center node and apply force
fNode = flatBeam.nset('ForceNode');
fNodeDof = fNode{1}.GetGlobalDof();
fNodeVertDof = fNodeDof(1);

% Create force object
force = Force('ForceNode',flatBeam);
force.AddForce(fNode,1,-1);

% Creat linear static object
buckling = Buckling('static', flatBeam);
buckling.AddForce(force);
buckling.Solve(10);

% Compute theoretical values
K = 0.5; % both ends fixed
fTheory = [1:4].^2.*(pi^2)*E*((1/12)*width*thk^3)/(xLength)^2;

% Plot the buckling modes
figure
subplot(2,2,1)
title(['Buckling : Mode 1 ',newline,...
       '\lambda_F_E = ',num2str(round(buckling.lambda(1))),newline,...
       '  \lambda_Theory = ',num2str(round(fTheory(1)))])
flatBeam.Plot(buckling.phi(:,1)/norm(buckling.phi(:,1)),'norm')
view([0 -1 0]); grid('on');
subplot(2,2,2)
title(['Buckling : Mode 2 ',newline,...
       '\lambda_F_E = ',num2str(round(buckling.lambda(2))),newline,...
       '  \lambda_Theory = ',num2str(round(fTheory(2)))])
flatBeam.Plot(buckling.phi(:,2)/norm(buckling.phi(:,2)),'norm')
view([0 -1 0]); grid('on');
subplot(2,2,3)
title(['Buckling : Mode 3 ',newline,...
       '\lambda_F_E = ',num2str(round(buckling.lambda(3))),newline,...
       '  \lambda_Theory = ',num2str(round(fTheory(3)))])
flatBeam.Plot(buckling.phi(:,3)/norm(buckling.phi(:,3)),'norm')
view([0 -1 0]); grid('on');
subplot(2,2,4)
title(['Buckling : Mode 4 ',newline,...
       '\lambda_F_E = ',num2str(round(buckling.lambda(4))),newline,...
       '  \lambda_Theory = ',num2str(round(fTheory(4)))])
flatBeam.Plot(buckling.phi(:,4)/norm(buckling.phi(:,4)),'norm')
view([0 -1 0]); grid('on');

