%% This scripts generates a basic finite element model of a curved
%  plate modeled with 4-node shell elements
%
%  Analysis then performs
%   1) Linear modal analysis for each radius of curvature controlled by a
%   design variable object
%
% Christopher Van Damme - 07/25/2019
% cvandamme@wisc.edu , cvandamme13@gmail.com
% 
% Add license info...
% 
clear; clc; close all; clear classes;
%% Build Model

curvedBeam = OsFern('curvedBeam');

% Plate Dimensions and mesh properties (Metric)
xLength = 1; xEles = 40; xNodes = xEles + 1;

% Cross section properties
thk = 0.01; width = 0.01;
curvedBeamSection.w = width; curvedBeamSection.t = thk;

% Curvature
r0 = 10; 

% Create Design Variable to control radius
dvR = DesignVariable(r0,'radius');

% Material Properties (steel)
E = 210e9; rho = 7850; nu = 0.29;
alpha = 6.3e-06; refTemp = 77; 
steel = Material('steel');
steel = steel.setProp('E',E);
steel = steel.setProp('rho',rho);
steel = steel.setProp('nu',nu);
steel = steel.setProp('alpha',alpha);
steel = steel.setProp('tRef',refTemp);

% Setup constraints
endConstraints = true;  
sideConstraints = true; % enforces inplane motion only

% Creat basic mesh
nodeId = 1; eleId = 1;
endNodes = []; sideNodes = [];
for ii = 1:xNodes
        
    % Coordinates
    xPos = xLength*((ii-1)/xEles);
    yPos = 0; 
    
    % Create function handle
    zPosFun = @(r) (sqrt(r^2 - (xPos-xLength/2)^2) - sqrt(4*r^2 - xLength^2)/2);

    % Create Design Variable
    dv(ii) = DesignVariableFun(dvR,['Node-',num2str(ii),'-Z-Position'],zPosFun);

    % Create Node Object
    tempNode = Node(nodeId,xPos,yPos,dv(ii));

    % Build node
    curvedBeam = curvedBeam.AddNodeObject(tempNode);

    % Clamp end nodes
    if endConstraints
        if ii == 1 || ii == xNodes
            tempNode = tempNode.SetFixed();
        else
            if sideConstraints
                tempNode = tempNode.SetFixed([2,4,6]);
            end
        end
    end

    if ii == xEles/2 + 1
        curvedBeam.Add2Nset('CenterNode', tempNode);
    end

    % Create element
    if ii > 1
        b31Ele = B2(eleId); eleId = eleId + 1;
        eleNodeIds = [(ii - 1) (ii)]; csVec = [0 1 0];
        b31Ele = b31Ele.Build(curvedBeam.GetNodes(eleNodeIds),...
                 steel, curvedBeamSection,csVec);
        curvedBeam.AddElementObject(b31Ele); 
    end

    nodeId = nodeId + 1;
        
end

curvedBeam = curvedBeam.Update(); 
figure; curvedBeam.Plot(); view([0 -1 0]);

nmodes = 10;
modal = Frequency('modal', curvedBeam);
modal.Solve(nmodes);

%% Loop through different radius, update mode, perform modal

rVec = [1 10 100];
figure;
for ii = 1:length(rVec)
    dvR.SetVal(rVec(ii));
    curvedBeam.Update(true);
    modal.Solve(nmodes);
    fn(:,ii) = modal.fn;
    subplot(1,3,1); hold on; view([0 -1 0])
    curvedBeam.Plot();
    subplot(1,3,2); hold on;
    plot(modal.phi(3:6:end,1));
    subplot(1,3,3); hold on;
    plot(modal.phi(3:6:end,2));
end
subplot(1,3,2); title('FE Model');
subplot(1,3,2); title('Mode 1'); legend('r = 1','r = 10','r=100');
subplot(1,3,3); title('Mode 2'); legend('r = 1','r = 10','r=100');

figure;
bar(fn(1:3,:)); xlabel('Mode #'); ylabel('Frequency (Hz)')
legend('r = 1','r = 10','r=100');