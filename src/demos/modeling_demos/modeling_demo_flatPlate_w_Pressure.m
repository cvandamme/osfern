%% This scripts generates a finite element model of a flat
%  plate modeled with 4 node shell elements.
% 
%  Analyses then performs
%   1) Modal analysis
%   2) Linear static with pressure load
%   3) Nonlinear static with pressure load
%
% Christopher Van Damme - 07/12/2018
% cvandamme@wisc.edu , cvandamme13@gmail.com
% 
% Add liscense
% 
clear; clc; close all; clear classes
%% Build Model

flatPlate = OsFern('flatPlate');

% Plate Dimensions and mesh properties
xLength = 250; xEles = 24; xNodes = xEles + 1;
yLength = 250; yEles = 24; yNodes = yEles + 1;
zLength = 0; 

% Element/shell thickeness
thk = 1.5;
shellSection.t = thk;

% Material Properties
E = 71000; rho = 2.7000e-09; nu = 0.33; alpha = 6.3e-06;
al = Material('aluminum');
al = al.setProp('E',E);
al = al.setProp('rho',rho);
al = al.setProp('nu',nu);

% Creat basic mesh
nodeId = 1;
elementId = 1;
springId = 1000;
endNodes = []; sideNodes = [];
for ii = 1:yNodes
    for jj = 1:xNodes
        
        % Coordinates
        xPos = xLength*((jj-1)/xEles);
        yPos = yLength*((ii-1)/yEles);
        zPos = 0;
        
        % Build node
        tempNode = Node(nodeId, xPos, yPos, zPos);
        flatPlate.AddNodeObject(tempNode);
        nodeId = nodeId + 1;
        
        if (jj == 1 || jj == xNodes)
            tempNode.SetFixed();
        end
        
        if (ii == 1 || ii == xNodes)
            tempNode.SetFixed();
        end
        
        if ii == xEles/2 + 1 && jj == yEles + 1
            flatPlate.Add2Nset('CenterNode', tempNode);
        end
        
        % Create element
        if ii > 1 && jj > 1
            s4Ele = S4S(elementId); elementId = elementId + 1;
            nodeIds = [(ii - 2)*xNodes + (jj - 1);
                       (ii - 2)*xNodes + (jj);
                       (ii - 1)*xNodes + (jj);
                       (ii - 1)*xNodes + (jj - 1)];

            s4Ele.Build(flatPlate.GetNodes(nodeIds), al, shellSection);
            flatPlate.AddElementObject(s4Ele);
        end
        
    end
end


% Update the model with correct DOF indexing for nodes and elements
flatPlate.Update();
flatPlate.Plot();

%% Perform linear modal analysis on the nominal model
nmodes = 30;
modal = Frequency('modal', flatPlate);
modal.Solve(nmodes);

%% Perform linear and nonlinear static analysis with nominal model
%  model is excited with uniform pressure

% Generate response
rsponseNode = flatPlate.nset('CenterNode');  % Node number of force application
rsponseDof = 3; 	% DOF number of force application
nsteps = 10;        % Number of steps for NL calculation

% Generate pressure field
pVal = 1e-2; % MPa
pressure = Pressure('UniformPressure',flatPlate,pVal);
pressure.AddElements(flatPlate.element);

% Peform linear static solution
linStatic = LinearStatic('static', flatPlate);
linStatic.AddPressureField(pressure);
linStatic.AddMonitor(rsponseNode{1}.id, rsponseDof);
linStatic.Solve();

% Peform nonlinear static solution
nLstatic = NLStatic('static', flatPlate);
nLstatic.AddPressureField(pressure);
nLstatic.AddMonitor(rsponseNode{1}.id, rsponseDof);
nLstatic.Solve(nsteps);

% Plot the two comparisons of deformation
figure;
subplot(1,2,1);
flatPlate.Plot(linStatic.finalDisplacement/thk); c = colorbar;
ylabel(c,'Displacment (xThk)','FontSize',12)
title('Linear Solution','FontSize',12)
subplot(1,2,2);
flatPlate.Plot(nLstatic.finalDisplacement/thk); c = colorbar;
ylabel(c,'Displacment (xThk)','FontSize',12)
title('Nonlinear Solution','FontSize',12)

%% Compare results
figure
subplot(3,2,1)
title(['Mode 1 : ',num2str(round(modal.fn(1))),' Hz'])
flatPlate.Plot(modal.phi(:,1),'norm')
view([0 0 -1]); grid('on');
subplot(3,2,2)
title(['Mode 2 : ',num2str(round(modal.fn(2))),' Hz'])
flatPlate.Plot(modal.phi(:,2),'norm')
view([0 0 -1]); grid('on');
subplot(3,2,3)
title(['Mode 3 : ',num2str(round(modal.fn(3))),' Hz'])
flatPlate.Plot(modal.phi(:,3),'norm')
view([0 0 -1]); grid('on');
subplot(3,2,4)
title(['Mode 4 : ',num2str(round(modal.fn(4))),' Hz'])
flatPlate.Plot(modal.phi(:,4),'norm')
view([0 0 -1]); grid('on');
subplot(3,2,5)
title(['Mode 4 : ',num2str(round(modal.fn(5))),' Hz'])
flatPlate.Plot(modal.phi(:,4),'norm')
view([0 0 -1]); grid('on');
subplot(3,2,6)
title(['Mode 4 : ',num2str(round(modal.fn(6))),' Hz'])
flatPlate.Plot(modal.phi(:,4),'norm')
view([0 0 -1]); grid('on');
