%% This scripts generates a finite element model of an exhaust plate
%  modeled with 4-Node shell elements.
%
%   Analyses performed
%   1) Linear Modal Solution
%   2) Model updating using linear normal modes and gradient based
%   optimization
% 
% Christopher Van Damme - 07/25/2019
% cvandamme@wisc.edu , cvandamme13@gmail.com
% 
% Add liscense
% 
clear; clc; close all;
%% Build Model

% Read in Curved Beam Raw Data
load('..\..\models\exhaustCoverPlate.mat');

% Initialize MatFem Object
exhaustPlate = OsFern('ExhaustCover');

% Initial Modulus 
E0 = 208e9; Ef = 168e9;
rho0 = 5120; rhoF = 5120; 

% Top radius
R0 = 20; xLength = 0.1588;

% Create Design Variable Objects 
dv{1} = DesignVariable(Ef,'Modulus');
dv{2} = DesignVariable(R0,'Radius');

% Material Properties
steel = Material('steel');
steel = steel.setProp('E',dv{1});
steel = steel.setProp('rho',rhoF);
steel = steel.setProp('nu',nu);
shellSection.t = thickness;

% Setup constraints
endConstraints = [];

% Decidie if doing force/damped analysis or just NNM
forced = false;
figure; hold on;
% Creat basic mesh
nodeId = 1; elementId = 1;
for ii = 1:length(nodes)
        
    % Node Id 
    nodeId = nodes(ii,1);
    
    % Coordinates
    xPos = nodes(ii,2); yPos = nodes(ii,3); zPos = nodes(ii,4);
    
    % Create function handle
    zPosFun = @(r) (zPos + sqrt(r^2 - xPos^2 - yPos^2)-sqrt(r^2-xLength^2));
	xPoint = (xPos-xLength/2)^2; yPoint = (yPos-xLength/2)^2;

    % Create Design Variable
    dvR(ii) = DesignVariableFun(dv{2},['Node-',num2str(ii),'-Z-Position'],zPosFun);
    
    % Check if using function or not
    if zPos > 0.018
        zPos = dvR(ii); isInterp = true;
        plot3(xPos,yPos,zPos,'o')
        text(xPos,yPos,zPos,num2str(nodeId))
    else
        zPos = nodes(ii,4); isInterp = false;
    end
     
    % Create Node Object
    tempNode = Node(nodeId,xPos,yPos,zPos);

    % Build node object
    exhaustPlate = exhaustPlate.AddNodeObject(tempNode);

    % Fix boundary nodes
    if any(nodeId==spcs(:,1))
        tempNode = tempNode.SetFixed();
        exhaustPlate.Add2Nset('BoundaryNodes',tempNode)
    end
    
    % Find inner nodes used for interpolation
    if isInterp
        exhaustPlate.Add2Nset('InterpNodes',tempNode)
    end
end

% Create the elements
for ii = 1:length(elements)
    
    % create 4-node shell element
    s4Ele = S4S(elementId); elementId = elementId + 1;
    nodeId = elements(ii,2:5);
    eleObjNodes = [exhaustPlate.node(nodeId(1)),...
                exhaustPlate.node(nodeId(2)),...
                exhaustPlate.node(nodeId(3)),...
                exhaustPlate.node(nodeId(4))];
    s4Ele = s4Ele.Build(eleObjNodes, steel, shellSection);
    exhaustPlate.AddElementObject(s4Ele);
    
end

% Build Constraints and DOF
exhaustPlate = exhaustPlate.Update();

% Plot the model
figure; exhaustPlate.Plot();

% Initialize and solve modal equation
nmodes = 30;
modal = Frequency('modal', exhaustPlate);
modal.Solve(nmodes);

% Plot Mode shapes
f = figure; scale = 1/100;
tabgp = uitabgroup(f,'Position',[.05 .05 .9 .9]);
for ii = 1:length(modal.fn)
    modalTab{ii} = uitab(tabgp,'Title',['Mode ', num2str(ii),' : ',num2str(round(modal.fn(ii))),' Hz']);
    axes('Parent',modalTab{ii});
    title(['Mode ', num2str(ii),' : ',num2str(round(modal.fn(ii))),' Hz']);
    exhaustPlate.Plot(scale*modal.phi(:,ii),'norm');  view([0 0 -1]); colorbar
end
% return
%% Perform linear model correlation

% Experimental frequncies
fnTarget = [205.36;327.84;348.65;489.16;510.22;572.62;...
            601.84;697.89;699.952;814.11;827.68;911.20;...
            916.87;928.14;938.96;1096.26;1107.31];
fnTarget(7,:) = []; % remove 7th mode data since it is not in FE model
mind = [1:10];

% Create linear model update object
exhaustPlateUpdate = LinearModelUpdate('ExhaustPlate_Update2Teset',exhaustPlate,dv);

% Set target data
exhaustPlateUpdate.SetTargetData(mind,fnTarget(mind));

% Set weigths if desired
% exhaustPlateUpdate.SetWeights();

% Start linear model updating
exhaustPlateUpdate.Start();

% Error
fnError0 = 100*(exhaustPlateUpdate.initial.fn - fnTarget(mind))./fnTarget(mind);
fnError = 100*(exhaustPlateUpdate.final.fn - fnTarget(mind))./fnTarget(mind);

% Plot errors 
figure;
bar([fnError0,fnError])
xlabel('Mode #');ylabel('% Error'); legend('Initial','Updated')
