%% This scripts generates a finite element model of a flat
%  plate modeled with 4-node shell elements with springs as boundary conditions. 
%  Additionally this example demonstates the use of the DesignVariable
%  object which serves as a way to remotely change all instances of a
%  of a variable throughout the model. In this case it is used to change
%  the stiffness of the axial springs. 
% 
% 
%  Analysis then performs
%   1) Modal Analysis
%   2) Linear Static
%   3) Nonlinear Static
%
% Christopher Van Damme - 07/12/2018
% cvandamme@wisc.edu , cvandamme13@gmail.com
% 
% Add liscense
% 

clear; clc; close all
clear classes
addpath(genpath('../../classes'));
%% Build Model

flatPlate = OsFern('flatPlate');

% Plate Dimensions and mesh properties
xLength = 250; xEles = 24; xNodes = xEles + 1;
yLength = 250; yEles = 24; yNodes = yEles + 1;
zLength = 0; 

% Element/shell thickeness
thk = 1.5;
shellSection.t = thk;

% Material Properties
E = 71000; rho = 2.7000e-09; nu = 0.33; alpha = 6.3e-06;
al = Material('aluminum');
al = al.setProp('E',E);
al = al.setProp('rho',rho);
al = al.setProp('nu',nu);

% Setup constraints and springs
endSprings = true;
sideSprings = true;
kAxialEnd = 1e2; vAxialSide = [1 0 0]; vAxialEnd = [0 1 0];
kTransverse = 1e2; vTransverse = [1 0 0];
springLength = 10;

% Create Design Variable to control radius
dv = DesignVariable(kAxialEnd,'Axial Spring Stiffness');

% Creat basic mesh
nodeId = 1;
elementId = 1;
springId = 1000;
endNodes = []; sideNodes = [];
for ii = 1:yNodes
    for jj = 1:xNodes
        
        % Coordinates
        xPos = xLength*((jj-1)/xEles);
        yPos = yLength*((ii-1)/yEles);
        zPos = 0;
        
        % Build node
        tempNode = Node(nodeId, xPos, yPos, zPos);
        flatPlate.AddNodeObject(tempNode);
        nodeId = nodeId + 1;
        
        if (jj == 1 || jj == xNodes) && endSprings
            flatPlate.Add2Nset('SpringPlateNodes', tempNode);
%             tempNode.SetFixed([2,4,5,6]);
        end
        
        if (ii == 1 || ii == xNodes) && sideSprings
            flatPlate.Add2Nset('SpringPlateNodes', tempNode);
%             tempNode.SetFixed([1,4,5,6]);
        end
        
        
        if ii == xEles/2 + 1 && jj == yEles/2 + 1
            flatPlate.Add2Nset('CenterNode', tempNode);
        end
        
        % Create element
        if ii > 1 && jj > 1
            s4Ele = S4S(elementId); elementId = elementId + 1;
            % Alternate element choice:
%             s4Ele = S4BDI(elementId); elementId = elementId + 1;
            nodeIds = [(ii - 2)*xNodes + (jj - 1);
                       (ii - 2)*xNodes + (jj);
                       (ii - 1)*xNodes + (jj);
                       (ii - 1)*xNodes + (jj - 1)];

            s4Ele.Build(flatPlate.GetNodes(nodeIds), al, shellSection);
            flatPlate.AddElementObject(s4Ele);
        end
        
    end
end


% Build End Spring Elements
if endSprings
    for ii = [1,xNodes]
        for jj = 1:yNodes
            for kk = 1:2
                if kk == 1
                    kLin = kTransverse;
                    v = vTransverse;
                    % Build New Coordinates
                    xPos = xLength*((ii-1)/xEles);
                    yPos = yLength*((jj-1)/yEles);
                    zPos = -springLength; % Just translated down a bit
                    % Create spring object
                    nodeId2 = flatPlate.findNODEfromXYZ([xPos,yPos,0]);
                elseif kk == 2
                    kLin = dv;
                    v = vAxialEnd;
                    % Build New Coordinates
                    if ii == 1
                        xPos = xLength*((ii-1)/xEles) - springLength;
                    else
                        xPos = xLength*((ii-1)/xEles) + springLength;
                        % Create spring object
                        nodeId2 = flatPlate.findNODEfromXYZ([xLength*((ii-1)/xEles),yPos,0]);
                    end
                    yPos = yLength*((jj-1)/yEles);
                    zPos = 0; % Just translated down a bit
                end

                % Build node
                tempNode = Node(nodeId, xPos, yPos, zPos);
                flatPlate.AddNodeObject(tempNode);
                nodeId = nodeId + 1;

                % Fix the node 
                tempNode = tempNode.SetFixed();

                % Add to the node set
                flatPlate.Add2Nset('SpringGroundNodes', tempNode);

                % Create the spring object 
                springObj = Spring(elementId); elementId = elementId + 1;
                springObj.Build(flatPlate.GetNodes([nodeId-1,nodeId2]),...
                          kLin,0,0,v);
                flatPlate.AddElementObject(springObj);

                % Add spring object to certain 
                flatPlate.Add2Elset('SpringGroundNodes', springObj);
            end
        end
    end
end

% Build Side Spring Elements
if sideSprings
    for ii = 1:xNodes
        for jj = [1,yNodes]
            for kk = 1:2
                if kk == 1
                    kLin = kTransverse;
                    v = vTransverse;
                    % Build New Coordinates
                    xPos = xLength*((ii-1)/xEles);
                    yPos = yLength*((jj-1)/yEles);
                    zPos = -springLength; % Just translated down a bit
                    % Create spring object
                    nodeId2 = flatPlate.findNODEfromXYZ([xPos,yPos,0]);
                elseif kk == 2
                    kLin = dv;
                    v = vAxialSide;
                    % Build New Coordinates
                    if jj == 1
                        yPos = yLength*((jj-1)/xEles) - springLength;
                    else
                        yPos = yLength*((jj-1)/xEles) + springLength;
                        % Create spring object
                        nodeId2 = flatPlate.findNODEfromXYZ([xLength*((ii-1)/xEles),yPos,0]);
                    end
                    xPos = xLength*((ii-1)/yEles);
                    zPos = 0; % Inline
                end
                
                % Build node
                tempNode = Node(nodeId, xPos, yPos, zPos);
                flatPlate.AddNodeObject(tempNode);
                nodeId = nodeId + 1;
                
                % Fix the node
                tempNode = tempNode.SetFixed();
                
                % Add to the node set
                flatPlate.Add2Nset('SpringGroundNodes', tempNode);
                
                % Create the spring object
                springObj = Spring(elementId); elementId = elementId + 1;
                springObj.Build(flatPlate.GetNodes([nodeId-1,nodeId2]),...
                                kLin,0,0,v);
                flatPlate.AddElementObject(springObj);
                
                % Add spring object to certain
                flatPlate.Add2Elset('SpringGroundNodes', springObj);
            end
        end
    end
end

% Update the model with correct DOF indexing for nodes and elements
flatPlate.Update();
flatPlate.Plot();

%% Perform linear modal analysis on the nominal model
nmodes = 30;
modal{1} = Frequency('modal', flatPlate);
modal{1}.Solve(nmodes);

%% Perform linear and nonlinear static analysis with nominal model


% Generate Force
forceNode = flatPlate.nset('CenterNode');  % Node number of force application
forceDof = 3;                        % DOF number of force application
forceAmplitude = 25;                 % Force amplitude (signed)
nsteps = 10;                         % Number of steps for NL calculation

% Create force object
force = Force('CenterNode',flatPlate);
force.AddForce(forceNode{1},3,forceAmplitude);
    
% Peform linear static solution
linStatic = LinearStatic('static', flatPlate);
linStatic.AddForce(force);
linStatic.AddMonitor(forceNode{1}.id, forceDof);
% linStatic.Solve();

% Peform nonlinear static solution
nLstatic = NLStatic('static', flatPlate);
nLstatic.AddForce(force);
nLstatic.AddMonitor(forceNode{1}.id, forceDof);
% nLstatic.Solve(nsteps);

% Vector of spring stiffnesses
kVec = [1e1 1e2 1e3 1e4];


figure; index = 1;
% Perform parameter study
for ii = 1:length(kVec)
    dv.SetVal(kVec(ii)); flatPlate.Update(true);
    linStatic.Solve();
    subplot(4,2,index); flatPlate.Plot(linStatic.finalDisplacement); view([0 0 -1])
    nLstatic.Solve(10);
    subplot(4,2,index+1); flatPlate.Plot(nLstatic.finalDisplacement); view([0 0 -1])
    index = index+2;
end
