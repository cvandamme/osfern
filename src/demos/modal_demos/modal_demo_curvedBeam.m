%% This scripts generates a basic finite element model of a curved
%  curvedBeam modeled with 2-Node beam Elements.
%
%   Analyses performed
%   1) Linear Static Solution
%   2) Nonlinear Static Solution
%   3) Modal analysis about the equilibrium state
% 
% Christopher Van Damme - 07/12/2018
% cvandamme@wisc.edu , cvandamme13@gmail.com
% 
% Add liscense
% 
clear; clc; close all;
clear classes;
%% Build Model

CB = OsFern('CB');
    % This is an object which will contain data and acess to functions that
    % know how to operate on this data.

% Plate Dimensions and mesh properties (Metric)
xLength = 1; xEles = 40; xNodes = xEles + 1;

% Cross section properties
thk = 0.01; width = 0.01;
curvedBeamSection.w = width; curvedBeamSection.t = thk;

% Specify rise ratio (ratio of height to thickness)
riseRatio = 2; 

% Radius of curvaturea
if riseRatio == 0
    zPosMax = 0; r = 0;
else
    zPosMax = thk*riseRatio; 
    r = (zPosMax^2 + (xLength/2)^2)/(2*zPosMax);
end

% Material Properties (steel)
E = 210e9; rho = 7850; nu = 0.29;
alpha = 6.3e-06; refTemp = 77; 
steel = Material('steel');
steel = steel.setProp('E',E);
steel = steel.setProp('rho',rho);
steel = steel.setProp('nu',nu);
steel = steel.setProp('alpha',alpha);
steel = steel.setProp('tRef',refTemp);

% Setup boolean to flag which constraints are on
endConstraints = true;  
sideConstraints = true; % enforces inplane motion only

% Create basic mesh
nodeId = 1; eleId = 1;
endNodes = []; sideNodes = [];
for ii = 1:xNodes
        
    % Coordinates
    xPos = xLength*((ii-1)/xEles);
    yPos = 0; 
    if riseRatio == 0
        zPos = 0;
    else
        zPos = sqrt(r^2 - (xPos-xLength/2)^2) - r + zPosMax;
    end

    % Create Node Object
    tempNode = Node(nodeId,xPos,yPos,zPos);

    % Add Node to FEM
    CB = CB.AddNodeObject(tempNode);

    % Clamp end nodes
    if endConstraints
        if ii == 1 || ii == xNodes
            tempNode = tempNode.SetFixed();
        else
            if sideConstraints
                tempNode = tempNode.SetFixed([2,4,6]);
            end
        end
    end

    if ii == xEles/2 + 1
        CB.Add2Nset('CenterNode', tempNode);
    end

    % Create element
    if ii > 1
        b31Ele = B2(eleId); eleId = eleId + 1;
        eleNodeIds = [(ii - 1) (ii)]; csVec = [0 1 0];
        b31Ele = b31Ele.Build(CB.GetNodes(eleNodeIds),...
                 steel, curvedBeamSection,csVec);
        CB.AddElementObject(b31Ele); 
    end

    nodeId = nodeId + 1;
        
end

CB = CB.Update(); 
figure(1); CB.Plot(); view([0 -1 0]);
title('Visualization of OsFern FEM');

%% Linear modal analysis
nmodes = 10;
baseLineModal = Frequency('modal', CB);
baseLineModal.Solve(nmodes);

% To see the natural frequencies it obtained
baseLineModal.fn % in Hz

% To see all available fields
fieldnames(baseLineModal)
    % Most of these are settings and things that aren't of interest for
    % basic users, but everything is captured here in case it is needed
    % later.
    
% Plot some of the mode shapes (if desired)
% {
figure(2);
    % Find the maximum dimension of the model:
    maxDim=max(cellfun(@(x) sqrt(x.x^2+x.y^2+x.z^2),CB.node));
%     scale=0.2*maxDim/norm(baseLineModal.phi(3:6:end,1));
        % Scale to 20% of model dimension, or use xLength instead of maxDim
    scale=thk*maxDim/norm(baseLineModal.phi(3:6:end,1));
        % Scale deformation to the order of the thickness (so curvature is
        % visible still.)
        
for k=1:4; % this only plots the first 4 modes
    subplot(4,2,2*k-1);
    CB.Plot(scale*baseLineModal.phi(:,k),'norm'); view([0 -1 0]);
    title(sprintf('Mode %i at %5.1f Hz',k,baseLineModal.fn(k)));
end
% Alternative, show deformation only, only works for beam-type plots
% Grab x-locations of nodes
xs=cellfun(@(S) S.x,CB.node);
for k=1:4; % this only plots the first 4 modes
    subplot(4,2,2*k);
    plot(xs,scale*baseLineModal.phi(3:6:end,k));
    title(sprintf('Deformation of Mode %i at %5.1f Hz',k,baseLineModal.fn(k)));
end

%}

return

%% Static Solutions - Demonstrates additional capability - solve for 
% nonlinear response of FEM and then perform modal analysis at each
% deformed state.
name = 'CenterLoad';

% Get the center node and apply force
cNode = CB.nset('CenterNode');
cNodeDof = cNode{1}.GetGlobalDof();
cNodeVertDof = cNodeDof(3);

% Create force object
force = Force('CenterNode',CB);
force.AddForce(cNode,3,-1);

% Creat nonlinear static object
nlStatic = NLStatic('static', CB);
nlStatic.AddForce(force);
nlStatic.AddMonitor(cNode{1}.id, 3);

% Vector of force amplitude
forceVec = linspace(1,1000,50);
linDef = zeros(size(forceVec)); nlDef = zeros(size(forceVec));

% Loop through and solve each - note this is not an efficient way to do
% multiple loads because it reassembles the linear stifffness matrix each
% time.
defModal = cell(size(forceVec)); 
fn = zeros(size(forceVec)); phi = zeros(CB.nDof,length(forceVec));
clear nlDefFF nlDef
for ii = 1:length(forceVec)
    
    % Since we are using handle classes, this automatically updates in the
    % step class, i.e. in "nlStatic.force"
    force.val(1) = -1*forceVec(ii);
    
    % Nonlinear solution
    nlStatic.Solve(10); % Solve the problem using 10 load increments.

    % Store deformation at node of interest (specified above)
    nlDef(ii) = nlStatic.response;
    nlDefFF(:,ii)=nlStatic.finalDisplacement;
    
    % This doesn't work because these are "handle" classes, like global
    % variables.
%     % Store full solution at this load step
%     nlS{ii}=nlStatic;

    % Perform modal about deformed state
    defModal{ii} = Frequency('modal', CB,nlStatic.finalDisplacement);
    defModal{ii}.Solve(nmodes); % call modal solution
    
    % Grab first mode results
    fn(ii) = defModal{ii}.fn(1);
    phi(:,ii) = defModal{ii}.phi(:,1);
end

% Plot the response
figure(3); hold on; 
subplot(2,1,1); plot(forceVec,nlDef/thk,'-o');  box on; grid on;
title('Force-Displacement Curve for Center Node');
xlabel('Force (N)'); ylabel('Displacement (xThk)')
subplot(2,1,2); plot(fn,nlDef/thk,'-o');  box on; grid on;
title('Linearized Frequency versus Displacement of Center Node');
xlabel('Frequency (Hz)'); ylabel('Displacement (xThk)')

% To see the deformation at a certain solution point.
plt_pt=30;
figure(4); 
CB.Plot(nlDefFF(:,plt_pt)); view([0 -1 0]);
% Add a marker to Fig. 3 to show which solution this is
figure(3); subplot(2,1,1);
line(forceVec(plt_pt),nlDef(plt_pt)/thk,'Color','r','Marker','*');

