%% This scripts generates a basic finite element model of a
%  flat Beam modeled with 2-Node beam Elements.
%
%   Analyses performed
%   1) Linear modal analysis
%   2) Nonlinear Static Solution
%   3) Modal about equilibrium
% 
% Christopher Van Damme - 07/12/2018
% cvandamme@wisc.edu , cvandamme13@gmail.com
% 
% Add liscense
% 
clear; clc; close all;
clear classes;
%% Build Model

flatBeam = OsFern('flatBeam');

% Plate Dimensions and mesh properties (Metric)
xLength = 1; xEles = 40; xNodes = xEles + 1;

% Cross section properties
thk = 0.01; width = 0.01;
flatBeamSection.w = width; flatBeamSection.t = thk;

% Material Properties (steel)
E = 210e9; rho = 7850; nu = 0.29;
alpha = 6.3e-06; refTemp = 77; 
steel = Material('steel');
steel = steel.setProp('E',E);
steel = steel.setProp('rho',rho);
steel = steel.setProp('nu',nu);
steel = steel.setProp('alpha',alpha);
steel = steel.setProp('tRef',refTemp);

% Setup constraints
endConstraints = true;  
sideConstraints = true; % enforces inplane motion only

% Creat basic mesh
nodeId = 1; eleId = 1;
endNodes = []; sideNodes = [];
for ii = 1:xNodes
        
    % Coordinates
    xPos = xLength*((ii-1)/xEles);
    yPos = 0; 
    zPos = 0;

    % Create Node Object
    tempNode = Node(nodeId,xPos,yPos,zPos);
        % Creates a new node in the space of the script.  On second pass
        % (ii=2), this destroys the last tempNode and replaces it with a
        % new one.
        
    % Build node
    flatBeam = flatBeam.AddNodeObject(tempNode);
        % Adds "tempNode" to the FEM.  But, the object tempNode still
        % points to the same node that is in flatBeam.  So the operations
        % below change the global copy of tempNode in both this script and
        % in flatBeam.
        
    % Clamp end nodes
    if endConstraints
        if ii == 1 || ii == xNodes
            tempNode = tempNode.SetFixed();
        else
            if sideConstraints
                tempNode = tempNode.SetFixed([2,4,6]);
            end
        end
    end
        % These changes automatically change both the local version of
        % tempNode and the version within "flatBeam."
        
    if ii == xEles/2 + 1
        flatBeam.Add2Nset('CenterNode', tempNode);
    end

    % Create element
    if ii > 1
        b31Ele = B2(eleId); eleId = eleId + 1;
        eleNodeIds = [(ii - 1) (ii)]; csVec = [0 1 0];
        b31Ele = b31Ele.Build(flatBeam.GetNodes(eleNodeIds),...
                 steel, flatBeamSection,csVec);
        flatBeam.AddElementObject(b31Ele); 
    end

    nodeId = nodeId + 1;
        
end

flatBeam = flatBeam.Update(); 
% Create a plot of the FE geometry (a line of nodes in this case).
figure; flatBeam.Plot(); view([0 -1 0]);

%% Linear modal analysis
nmodes = 10;
baseLineModal = Frequency('modal', flatBeam);
baseLineModal.Solve(nmodes);

% Plot some of the shapes (if desired)
% {
figure;
    % Find the maximum dimension of the model:
    maxDim=max(cellfun(@(x) sqrt(x.x^2+x.y^2+x.z^2),flatBeam.node));
    scale=0.2*maxDim/norm(baseLineModal.phi(3:6:end,1)); % or use xLength instead of maxDim
for k=1:4; % this only plots the first 4 modes
    subplot(4,1,k);
    flatBeam.Plot(scale*baseLineModal.phi(:,k),'norm'); view([0 -1 0]);
    title(sprintf('Mode %i at %10.1f Hz',k,baseLineModal.fn(k)));
end
%}

%% Static Solutions
name = 'CenterLoad'; % User defined name for the load case

% Find the center node and apply force
cNode = flatBeam.nset('CenterNode');
cNodeDof = cNode{1}.GetGlobalDof();
cNodeVertDof = cNodeDof(3);

% Create force object
force = Force('CenterNode',flatBeam);
force.AddForce(cNode,3,-1);

% Creat nonlinear static object
nlStatic = NLStatic('static', flatBeam);
nlStatic.AddForce(force);
nlStatic.AddMonitor(cNode{1}.id, 3);

% Vector of force amplitude
forceVec = linspace(1,1000,25);
linDef = zeros(size(forceVec)); nlDef = zeros(size(forceVec));

% Loop through and solve each - note this is not an efficient way to do
% multiple loads because it reassembles the linear stifffness matrix each
% time
defModal = cell(size(forceVec));
for ii = 1:length(forceVec)
    
    % Since we are using handle classes, this automatically updates in the
    % step classe
    force.val(1) = -1*forceVec(ii);
    
    % Nonlinear solution
    nlStatic.Solve(10);

    % Store deformations
    nlDef(ii) = nlStatic.response;

    % Perform modal about deformed state
    defModal{ii} = Frequency('modal', flatBeam,...
                   nlStatic.finalDisplacement);
    defModal{ii}.Solve(nmodes);
    
    % Grab first mode results
    fn(ii) = defModal{ii}.fn(1);
    phi(:,ii) = defModal{ii}.phi(:,1);
end

% Plot the response
figure; hold on;
subplot(2,1,1); plot(forceVec,nlDef./thk,'-o');
title('Force-Displacement of Center Node');
xlabel('Force (N)'); ylabel('Displacement (xThk)')
legend('Linear Solution','Nonlinear Solution');
subplot(2,1,2); plot(fn,nlDef./thk,'-o');
title('Linearized Frequency-Displacement of Center Node');
xlabel('Frequency (Hz)'); ylabel('Displacement (xThk)')
