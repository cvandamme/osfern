%% This scripts generates a basic finite element model of a curved
%  beam modeled with 2-Node beam Elements.
%
%   Analyses performed
%   1) Continuation of Static Equilibrium Path
% 
% 
% Christopher Van Damme - 07/12/2018
% cvandamme@wisc.edu , cvandamme13@gmail.com
% 
% Add liscense
% 
clear; clc; close all;
clear classes;
%% Build Model

curvedBeam = OsFern('curvedBeam');

% Plate Dimensions and mesh properties (Metric)
xLength = 1; xEles = 100; xNodes = xEles + 1;

% Cross section properties
thk = 0.01; width = 0.01;
curvedBeamSection.w = width; curvedBeamSection.t = thk;

% Rise ratio (ratio of height to thickness)
riseRatio = 1; 

% Radius of curvaturea
if riseRatio == 0
    zPosMax = 0; r = 0;
else
    zPosMax = thk*riseRatio; 
    r = (zPosMax^2 + (xLength/2)^2)/(2*zPosMax);
end

% Material Properties (steel)
E = 210e9; rho = 7850; nu = 0.29;
alpha = 6.3e-06; refTemp = 77; 
steel = Material('steel');
steel = steel.setProp('E',E);
steel = steel.setProp('rho',rho);
steel = steel.setProp('nu',nu);
steel = steel.setProp('alpha',alpha);
steel = steel.setProp('tRef',refTemp);

% Setup constraints
endConstraints = true;  
sideConstraints = true; % enforces inplane motion only

% Creat basic mesh
nodeId = 1; eleId = 1;
endNodes = []; sideNodes = [];
for ii = 1:xNodes
        
    % Coordinates
    xPos = xLength*((ii-1)/xEles);
    yPos = 0; 
    if riseRatio == 0
        zPos = 0;
    else
        zPos = sqrt(r^2 - (xPos-xLength/2)^2) - r + zPosMax;
    end

    % Create Node Object
    tempNode = Node(nodeId,xPos,yPos,zPos);

    % Build node
    curvedBeam = curvedBeam.AddNodeObject(tempNode);

    % Clamp end nodes
    if endConstraints
        if ii == 1 || ii == xNodes
            tempNode = tempNode.SetFixed();
        else
            if sideConstraints
                tempNode = tempNode.SetFixed([2,4,6]);
            end
        end
    end

    if ii == xEles/2 + 1
        curvedBeam.Add2Nset('CenterNode', tempNode);
    end

    % Create element
    if ii > 1
        b31Ele = B2(eleId); eleId = eleId + 1;
        eleNodeIds = [(ii - 1) (ii)]; csVec = [0 1 0];
        b31Ele = b31Ele.Build(curvedBeam.GetNodes(eleNodeIds),...
                 steel, curvedBeamSection,csVec);
        curvedBeam.AddElementObject(b31Ele); 
    end

    nodeId = nodeId + 1;
        
end

curvedBeam = curvedBeam.Update(); 
figure; curvedBeam.Plot(); view([0 -1 0]);

%% Initialize Static Continuation object 
curvedPlateCont = StaticCont('CurvedPlateCont',curvedBeam);


% Generate pressure field
pVal = -1e2; % Pa
pressure = Pressure('UniformPressure',curvedBeam,pVal);
pressure.AddElements(curvedBeam.element);

% Generate force Vector
% pressure.GetF
fExt = pressure.AssembleForce(0);

% Create Force Vector 
% fExt = zeros(curvedBeam.nDof,1);
centerNode = curvedBeam.nset('CenterNode');
centerNodeDof = centerNode{1}.GetGlobalDof();
% fExt(centerNodeDof(3)) = -2000;
curvedPlateCont.contOpts.stpmax = 0.0001;
curvedPlateCont.bifurCalc.tol = 0.01;

pDof = find(find(curvedBeam.freeDof)==centerNodeDof(3));
curvedPlateCont.plotOpts.dof2plot = pDof;

% Start Static Continuation
curvedPlateCont = curvedPlateCont.Start(fExt);

% Plot center displacement
figure;
plot(curvedPlateCont.x(pDof,:),curvedPlateCont.lam,'-o');
xlabel('Displacement');ylabel('\lambda');

