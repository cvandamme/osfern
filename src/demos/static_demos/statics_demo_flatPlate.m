%% This scripts generates a basic finite element model of a
%  flat plate modeled with 4-Node shell Elements.
%
%   Analyses performed
%   1) Linear Static Solution
%   2) Nonlinear Static Solution
% 
% 
% Christopher Van Damme - 07/12/2018
% cvandamme@wisc.edu , cvandamme13@gmail.com
% 
% Add liscense
% 

clear; clc; close all
clear classes
addpath('../../classes');
%% Build Model

flatPlate = OsFern('flatPlate');

% Plate Dimensions and mesh properties
xLength = 0.5; xEles = 40; xNodes = xEles + 1;
yLength = 0.5; yEles = 40; yNodes = yEles + 1;

% Element/shell thickeness
thk = 1.5/1000; shellSection.t = thk;

% Material Properties - Steel
E = 2.07e11; rho = 7.83e3; nu = 0.29;
alpha = 6.3e-06; refTemp = 77; 
steel = Material('steel');
steel = steel.setProp('E',E);
steel = steel.setProp('rho',rho);
steel = steel.setProp('nu',nu);
steel = steel.setProp('alpha',alpha);
steel = steel.setProp('tRef',refTemp);

% Setup constraints
endConstraints = true;
sideConstraints = true;

% Creat basic mesh
nodeId = 1; elementId = 1;
endNodes = []; sideNodes = [];
for ii = 1:yNodes
    for jj = 1:xNodes
        
        % Coordinates
        xPos = xLength*((jj-1)/xEles);
        yPos = yLength*((ii-1)/yEles);
        zPos = 0;
        
        % Create Node Object
        tempNode = Node(nodeId,xPos,yPos,zPos);
        
        % Build node
        flatPlate = flatPlate.AddNodeObject(tempNode);
        
        % Clamp end nodes
        if endConstraints
            if jj == 1 || jj == xNodes
                tempNode = tempNode.SetFixed();
                flatPlate.Add2Nset('BoundaryNodes', tempNode);
            end
        end
        
        if sideConstraints
             if ii == 1 || ii == yNodes
                tempNode = tempNode.SetFixed();
                flatPlate.Add2Nset('BoundaryNodes', tempNode);
             end
        end
        
        if ii == yEles/2 + 1 && jj == xEles/2 + 1
            flatPlate.Add2Nset('CenterNode', tempNode);
        end
        
        % Create element
        if ii > 1 && jj > 1
            s4Ele = S4S(elementId); elementId = elementId + 1;
            nodeIds = [(ii - 2)*xNodes + (jj - 1);
                       (ii - 2)*xNodes + (jj);
                       (ii - 1)*xNodes + (jj);
                       (ii - 1)*xNodes + (jj - 1)];
            s4Ele = s4Ele.Build(flatPlate.GetNodes(nodeIds), steel, shellSection);
            flatPlate.AddElementObject(s4Ele);
        end
        nodeId = nodeId + 1;
        
    end
end

% Update the model with correct DOF indexing for nodes and elements
flatPlate.Update();
flatPlate.Plot();

%% Static Solutions
name = 'CenterLoad';

% Get the center node and apply force
cNode = flatPlate.nset('CenterNode');
cNodeDof = cNode{1}.GetGlobalDof();
cNodeVertDof = cNodeDof(3);

% Create force object
force = Force('CenterNode',flatPlate);
force.AddForce(cNode,3,-1);

% Creat linear static object
linStatic = LinearStatic('static', flatPlate);
linStatic.AddForce(force);
linStatic.AddMonitor(cNode{1}.id, 3);

% Creat nonlinear static object
nlStatic = NLStatic('static', flatPlate);
nlStatic.AddForce(force);
nlStatic.AddMonitor(cNode{1}.id, 3);

% Vector of force amplitude
forceVec = linspace(1,1000,10);
linDef = zeros(size(forceVec)); nlDef = zeros(size(forceVec));

% Loop through and solve each - note this is not an efficient way to do
% multiple loads because it reassembles the linear stifffness matrix each
% time
for ii = 1:length(forceVec)
    
    % Since we are using handle classes, this automatically updates in the
    % step class
    force.val(1) = -1*forceVec(ii);
    
    % Linear solution
    linStatic.Solve();

    % Nonlinear solution
    nlStatic.Solve(10);

    % Store deformations
    linDef(ii) = linStatic.response;
    nlDef(ii) = nlStatic.response;

end

% Plot the response
figure; hold on;
plot(forceVec,linDef./thk,'-o');
plot(forceVec,nlDef./thk,'-o');
title('Force-Displacement of Center Node');
xlabel('Force (N)'); ylabel('Displacement (xThk)')
legend('Linear Solution','Nonlinear Solution');

% Compute the stresses
linSigma = flatPlate.ComputeStress(linStatic.finalDisplacement);
nlSigma = flatPlate.ComputeStress(nlStatic.finalDisplacement);

% Compute the strains
linEpsilon= flatPlate.ComputeStrain(linStatic.finalDisplacement);
nlEpsilon = flatPlate.ComputeStrain(nlStatic.finalDisplacement);

% Compute von-mises stress
linVm = zeros(size(linSigma,1),1);
for ii = 1:size(linSigma,1)
    linVm(ii) = sqrt(0.5*sum([(linSigma(ii,1)-linSigma(ii,2))^2,...
                         (linSigma(ii,2)-linSigma(ii,3))^2,...
                         (linSigma(ii,3)-linSigma(ii,1))^2]));
end
nlVm = zeros(size(linSigma,1),1);
for ii = 1:size(linSigma,1)
    nlVm(ii) = sqrt(0.5*sum([(nlSigma(ii,1)-nlSigma(ii,2))^2,...
                         (nlSigma(ii,2)-nlSigma(ii,3))^2,...
                         (nlSigma(ii,3)-nlSigma(ii,1))^2]));
end
        
% Plot stresses
figure; hold on
subplot(4,1,1); hold on; ylabel('\sigma_x');
plot(linSigma(1:xEles,1),'--');plot(nlSigma(1:xEles,1),'-o');legend('Linear','Nonlinear');
subplot(4,1,2); hold on; ylabel('\sigma_y');
plot(linSigma(1:xEles,2),'--');plot(nlSigma(1:xEles,2),'-o');
subplot(4,1,3); hold on; ylabel('\sigma_z');xlabel('Element #');
plot(linSigma(1:xEles,3),'--');plot(nlSigma(1:xEles,3),'-o');
subplot(4,1,4); hold on; ylabel('\sigma_{VM}');xlabel('Element #');
plot(linVm(1:xEles),'--');plot(nlVm(1:xEles),'-o');


