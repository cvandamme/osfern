%% This scripts generates a basic finite element model of a curved
%  plate modeled with 4-Node shell Elements.
%
%   Analyses performed
%   1) Linear Static Solution
%   2) Nonlinear Static Solution
% 
% 
% Christopher Van Damme - 07/12/2018
% cvandamme@wisc.edu , cvandamme13@gmail.com
% 
% Add liscense
% 
clear; clc; close all;
clear classes;
%% Build Model


curvedPlate = OsFern('CurvedBeam');

% Plate Dimensions and mesh Properties (keep even # of elements)
% Metric units
xLength = 0.5; xEles = 20; xNodes = xEles + 1;
yLength = 0.5; yEles = 20; yNodes = yEles + 1;

% Element/shell thickeness
thk = 1.5/1000; shellSection.t = thk;

% Rtaio of height to thickness
riseRatio = 2.5;

% Radius of curvaturea
if riseRatio == 0
    zPosMax = 0; r = 0;
else
    zPosMax = thk*riseRatio; 
    r = (zPosMax^2 + (xLength/2)^2)/(2*zPosMax);
end

% Material Properties - Steel
E = 2.07e11; rho = 7.83e3; nu = 0.29;
alpha = 6.3e-06; refTemp = 77; 
steel = Material('steel');
steel = steel.setProp('E',E);
steel = steel.setProp('rho',rho);
steel = steel.setProp('nu',nu);
steel = steel.setProp('alpha',alpha);
steel = steel.setProp('tRef',refTemp);

% Specify if sides are to be constrained
sideConstraints = true;

% Creat basic mesh
nodeId = 1; elementId = 1;
endNodes = []; sideNodes = [];
for ii = 1:yNodes
    for jj = 1:xNodes
        
        % Coordinates
        xPos = xLength*((jj-1)/xEles);
        yPos = yLength*((ii-1)/yEles); 
        if riseRatio == 0
            zPos = 0;
        else
            zPos = sqrt(r^2 - (xPos-xLength/2)^2) - r + zPosMax;
        end
        
        % Create node object
        tempNode = Node(nodeId,xPos,yPos,zPos);
        
        % Build node object
        curvedPlate = curvedPlate.AddNodeObject(tempNode);
        
        % Contrain ends
        if (jj == 1 || jj == xNodes)
            curvedPlate.Add2Nset('BoundaryNodes', tempNode);
            tempNode = tempNode.SetFixed();
        end
        
        % Contrain ends or add springs
        if sideConstraints
            if (jj == 1 || jj == xNodes)
                curvedPlate.Add2Nset('BoundaryNodes', tempNode);
                tempNode = tempNode.SetFixed();
            end
        end
        
        % Create Center Node Object
        if ii == yEles/2 + 1 && jj == xEles/2 + 1
            curvedPlate.Add2Nset('CenterNode', tempNode);
        end
        
        % Create element
        if ii > 1 && jj > 1
            s4Ele = S4S(elementId); elementId = elementId+1;
            nodeIds = [(ii - 2) * xNodes + (jj - 1),...
                       (ii - 2) * xNodes + (jj),...
                	    (ii-1) * xNodes + (jj),...
                        (ii-1) * xNodes + (jj - 1)];
            s4Ele = s4Ele.Build(curvedPlate.GetNodes(nodeIds), steel, shellSection);
            curvedPlate.AddElementObject(s4Ele);
        end
                
        % Add to node count
        nodeId = nodeId + 1;
    end
end


% Build Constraints and DOF
curvedPlate = curvedPlate.Update();
curvedPlate.Plot();

%% Static Solutions
name = 'CenterLoad';

% Get the center node and apply force
cNode = curvedPlate.nset('CenterNode');
cNodeDof = cNode{1}.GetGlobalDof();
cNodeVertDof = cNodeDof(3);

% Create force object
force = Force('CenterNode',curvedPlate);
force.AddForce(cNode,3,-1);

% Creat linear static object
linStatic = LinearStatic('static', curvedPlate);
linStatic.AddForce(force);
linStatic.AddMonitor(cNode{1}.id, 3);

% Creat nonlinear static object
nlStatic = NLStatic('static', curvedPlate);
nlStatic.AddForce(force);
nlStatic.AddMonitor(cNode{1}.id, 3);

% Vector of force amplitude
forceVec = linspace(1,500,20);
linDef = zeros(size(forceVec)); nlDef = zeros(size(forceVec));

% Loop through and solve each - note this is not an efficient way to do
% multiple loads because it reassembles the linear stifffness matrix each
% time
for ii = 1:length(forceVec)
    
    % Since we are using handle classes, this automatically updates in the
    % step class
    force.val(1) = -1*forceVec(ii);
    
    % Linear solution
    linStatic.Solve();

    % Nonlinear solution
    nlStatic.Solve(10);

    % Store deformations
    linDef(ii) = linStatic.response;
    nlDef(ii) = nlStatic.response;

end

% Plot the response
figure; hold on; grid on; box on;
plot(forceVec,linDef./thk,'-o');
plot(forceVec,nlDef./thk,'-o');
title('Force-Displacement of Center Node');
xlabel('Force (N)'); ylabel('Displacement (xThk)')
legend('Linear Solution','Nonlinear Solution');

% Compute the stresses
linSigma = curvedPlate.ComputeStress(linStatic.finalDisplacement);
nlSigma = curvedPlate.ComputeStress(nlStatic.finalDisplacement);

% Compute the strains
linEpsilon= curvedPlate.ComputeStrain(linStatic.finalDisplacement);
nlEpsilon = curvedPlate.ComputeStrain(nlStatic.finalDisplacement);

% Compute von-mises stress
linVm = zeros(size(linSigma,1),1);
for ii = 1:size(linSigma,1)
    linVm(ii) = sqrt(0.5*sum([(linSigma(ii,1)-linSigma(ii,2))^2,...
                         (linSigma(ii,2)-linSigma(ii,3))^2,...
                         (linSigma(ii,3)-linSigma(ii,1))^2]));
end
nlVm = zeros(size(linSigma,1),1);
for ii = 1:size(linSigma,1)
    nlVm(ii) = sqrt(0.5*sum([(nlSigma(ii,1)-nlSigma(ii,2))^2,...
                         (nlSigma(ii,2)-nlSigma(ii,3))^2,...
                         (nlSigma(ii,3)-nlSigma(ii,1))^2]));
end
        
% Plot stresses
figure; hold on
subplot(4,1,1); hold on; ylabel('\sigma_x');
plot(linSigma(1:xEles,1),'--');plot(nlSigma(1:xEles,1),'-o');legend('Linear','Nonlinear');
subplot(4,1,2); hold on; ylabel('\sigma_y');
plot(linSigma(1:xEles,2),'--');plot(nlSigma(1:xEles,2),'-o');
subplot(4,1,3); hold on; ylabel('\sigma_z');xlabel('Element #');
plot(linSigma(1:xEles,3),'--');plot(nlSigma(1:xEles,3),'-o');
subplot(4,1,4); hold on; ylabel('\sigma_{VM}');xlabel('Element #');
plot(linVm(1:xEles),'--');plot(nlVm(1:xEles),'-o');
