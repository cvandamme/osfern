%% This scripts generates a basic finite element model of a curved
%  curvedBeam modeled with 2-Node beam Elements.
%
%   Analyses performed
%   1) Linear Static Solution
%   2) Nonlinear Static Solution
% 
% 
% Christopher Van Damme - 07/12/2018
% cvandamme@wisc.edu , cvandamme13@gmail.com
% 
% Add liscense
% 
clear; clc; close all;
clear classes;
%% Build Model

curvedBeam = OsFern('curvedBeam');

% Plate Dimensions and mesh properties (Metric)
xLength = 1; xEles = 40; xNodes = xEles + 1;

% Cross section properties
thk = 0.01; width = 0.01;
curvedBeamSection.w = width; curvedBeamSection.t = thk;

% Rise ratio (ratio of height to thickness)
riseRatio = 2; 

% Radius of curvaturea
if riseRatio == 0
    zPosMax = 0; r = 0;
else
    zPosMax = thk*riseRatio; 
    r = (zPosMax^2 + (xLength/2)^2)/(2*zPosMax);
end

% Material Properties (steel)
E = 210e9; rho = 7850; nu = 0.29;
alpha = 6.3e-06; refTemp = 77; 
steel = Material('steel');
steel = steel.setProp('E',E);
steel = steel.setProp('rho',rho);
steel = steel.setProp('nu',nu);
steel = steel.setProp('alpha',alpha);
steel = steel.setProp('tRef',refTemp);

% Setup constraints
endConstraints = true;  
sideConstraints = true; % enforces inplane motion only

% Creat basic mesh
nodeId = 1; eleId = 1;
endNodes = []; sideNodes = [];
for ii = 1:xNodes
        
    % Coordinates
    xPos = xLength*((ii-1)/xEles);
    yPos = 0; 
    if riseRatio == 0
        zPos = 0;
    else
        zPos = sqrt(r^2 - (xPos-xLength/2)^2) - r + zPosMax;
    end

    % Create Node Object
    tempNode = Node(nodeId,xPos,yPos,zPos);

    % Build node
    curvedBeam = curvedBeam.AddNodeObject(tempNode);

    % Clamp end nodes
    if endConstraints
        if ii == 1 || ii == xNodes
            tempNode = tempNode.SetFixed();
        else
            if sideConstraints
                tempNode = tempNode.SetFixed([2,4,6]);
            end
        end
    end

    if ii == xEles/2 + 1
        curvedBeam.Add2Nset('CenterNode', tempNode);
    end

    % Create element
    if ii > 1
        b31Ele = B2(eleId); eleId = eleId + 1;
        eleNodeIds = [(ii - 1) (ii)]; csVec = [0 1 0];
        b31Ele = b31Ele.Build(curvedBeam.GetNodes(eleNodeIds),...
                 steel, curvedBeamSection,csVec);
        curvedBeam.AddElementObject(b31Ele); 
    end

    nodeId = nodeId + 1;
        
end

curvedBeam = curvedBeam.Update(); 
figure; curvedBeam.Plot(); view([0 -1 0]);

%% Static Solutions
name = 'CenterLoad';

% Get the center node and apply force
cNode = curvedBeam.nset('CenterNode');
cNodeDof = cNode{1}.GetGlobalDof();
cNodeVertDof = cNodeDof(3);

% Create force object
force = Force('CenterNode',curvedBeam);
force.AddForce(cNode,3,-1);

% Creat linear static object
linStatic = LinearStatic('static', curvedBeam);
linStatic.AddForce(force);
linStatic.AddMonitor(cNode{1}.id, 3);

% Creat nonlinear static object
nlStatic = NLStatic('static', curvedBeam);
nlStatic.AddForce(force);
nlStatic.AddMonitor(cNode{1}.id, 3);

% Vector of force amplitude
forceVec = linspace(1,1000,50);
linDef = zeros(size(forceVec)); nlDef = zeros(size(forceVec));

% Loop through and solve each - note this is not an efficient way to do
% multiple loads because it reassembles the linear stifffness matrix each
% time
for ii = 1:length(forceVec)
    
    % Since we are using handle classes, this automatically updates in the
    % step classe
    force.val(1) = -1*forceVec(ii);
    
    % Linear solution
    linStatic.Solve();

    % Nonlinear solution
    nlStatic.Solve(10);

    % Store deformations
    linDef(ii) = linStatic.response;
    nlDef(ii) = nlStatic.response;

end

% Plot the response
figure; hold on;
plot(forceVec,linDef./thk,'-o');
plot(forceVec,nlDef./thk,'-o');
title('Force-Displacement of Center Node');
xlabel('Force (N)'); ylabel('Displacement (xThk)')
legend('Linear Solution','Nonlinear Solution');