%% This scripts generates a finite element model of a flat beam 
%  using 2-Node beam elements. 
% 
%  The analyses/procedures that are performed
%   1) Build Model
%   2) Modal Analysis
%   3) Nonlinear Dynamic Analysis

clear; clc; close all
clear classes
addpath('../../classes');

%% Build Model

flatBeam = OsFern('flatBeam');

% Plate Dimensions and mesh properties
xLength = 9; xEles = 40; xNodes = xEles + 1;

% Cross-section properties
thk = 0.031; width = 0.5;
beamSection.t = thk; beamSection.w = width;

% Material Properties - Steel
E = 2.97e7; rho = 0.000736; nu = 0.280712; alpha = 6.3e-06;
steel = Material('steel');
steel = steel.setProp('E',E);
steel = steel.setProp('rho',rho);
steel = steel.setProp('nu',nu);
shellSection.t = thk;

% Setup boundary conditions
endConstraints = true;
sideConstraints = true;

% Creat mesh
nodeId = 1; elementId = 1;
for ii = 1:xNodes

    % Coordinates
    xPos = xLength*((ii-1)/xEles);
    yPos = 0;
    zPos = 0;

    % Create Node Object
    tempNode = Node(nodeId,xPos,yPos,zPos);

    % Build node
    flatBeam = flatBeam.AddNodeObject(tempNode);

    % Clamp end nodes
    if ii == 1 || ii == xNodes
        tempNode = tempNode.SetFixed();
        flatBeam.Add2Nset('EndNodes',tempNode);
    else
        if sideConstraints
            tempNode = tempNode.SetFixed([2,4,6]);
        end
    end

    if ii == xEles/2+1
        flatBeam.Add2Nset('CenterNode',tempNode);
    end

    % Create element
    if ii > 1
        b31Ele = B2(elementId); elementId = elementId + 1;
        eleNodeIds = [(ii - 1) (ii)]; csVec = [0 1 0];
        b31Ele = b31Ele.Build(flatBeam.GetNodes(eleNodeIds), steel,...
            beamSection,csVec);
        flatBeam.AddElementObject(b31Ele); 
    end
    nodeId = nodeId + 1;

end

% Update the model with correct DOF indexing for nodes and elements
flatBeam.Update();

% Plot the model
flatBeam.Plot(); view([0 -1 0]); grid('on');

%% Linear modal analysis
nmodes = 10;
modal = Frequency('modal', flatBeam);
modal.Solve(nmodes);

figure
subplot(2,2,1)
title(['Mode 1 : ',num2str(round(modal.fn(1))),' Hz'])
flatBeam.Plot(modal.phi(:,1)/norm(modal.phi(:,1)),'norm')
view([0 -1 0]); grid('on');
subplot(2,2,2)
title(['Mode 2 : ',num2str(round(modal.fn(2))),' Hz'])
flatBeam.Plot(modal.phi(:,2)/norm(modal.phi(:,2)),'norm')
view([0 -1 0]); grid('on');
subplot(2,2,3)
title(['Mode 3 : ',num2str(round(modal.fn(3))),' Hz'])
flatBeam.Plot(modal.phi(:,3)/norm(modal.phi(:,3)),'norm')
view([0 -1 0]); grid('on');
subplot(2,2,4)
title(['Mode 4 : ',num2str(round(modal.fn(4))),' Hz'])
flatBeam.Plot(modal.phi(:,4)/norm(modal.phi(:,4)),'norm')
view([0 -1 0]); grid('on');

%% Integrate Model
name = 'FullDynamic';

% Set duration and number of steps 
numCycles = 20; T = numCycles*(1/modal.fn(1));
dt = 1e-4; nSteps = floor(T/dt);
t = linspace(0,T,nSteps); dt = T/length(t);

% Get the center node and apply force
cNode = flatBeam.nset('CenterNode');
cNodeDof = cNode{1}.GetGlobalDof();
cNodeVertDof = cNodeDof(3);

% Initial Displacement 
xScale = 0.1;
xDisp = xScale*modal.phi(:,1)/norm(modal.phi(:,1));

% Initialize dynamic object
dynamic = Dynamic(name, flatBeam, xDisp);

% Add rayleigh damping
dynamic.SetDamping(34,1e-4); % (mass proportional, stiffness proportional)

% Add dnodes and dof to monitor
dynamic.AddMonitor(cNode{1}.id, 3);

% Add plotting options
dynamic.SetPlotOpts(true,5);

% Integrate model
tic
dynamic.Solve(T, dt);
tSimSerial = toc;

% Plot the response
figure;
plot(dynamic.t,dynamic.response);
xlabel('Time (s)'); ylabel('Response (in)')
