%% This scripts generates a basic finite element model of a flat
%  plate with different boundary conditions. 
%  Analysis then performs
%   1) linear static analysis
%   2) Modal Analysis
%   3) Nonlinear Static
%   4) Modal about nonlinar state 
clear; clc; close all

%% Build Model

flatBeam = OsFern('flatBeam');

% Plate Dimensions and mesh properties
xLength = 9; xEles = 50; xNodes = xEles + 1;
yLength = 0.5; yEles = 2; yNodes = yEles + 1;

% Element/shell thickeness
thk = 0.031;

% Material Properties
E = 2.97e7; rho = 0.000736; nu = 0.280712; alpha = 6.3e-06;
steel = Material('steel');
steel = steel.setProp('E',E);
steel = steel.setProp('rho',rho);
steel = steel.setProp('nu',nu);
shellSection.t = thk;

% Setup constraints
endConstraints = true;
sideConstraints = false;

% Creat basic mesh
nodeId = 1; elementId = 1;
endNodes = []; sideNodes = [];
for ii = 1:yNodes
    for jj = 1:xNodes
        
        % Coordinates
        xPos = xLength*((jj-1)/xEles);
        yPos = yLength*((ii-1)/yEles);
        zPos = 0;
        
        % Create Node Object
        tempNode = Node(nodeId,xPos,yPos,zPos);
        
        % Build node
        flatBeam = flatBeam.AddNodeObject(tempNode);
        
        % Clamp end nodes
        if endConstraints
            if jj == 1 || jj == xNodes
                tempNode = tempNode.SetFixed();
                endNodes = [endNodes; tempNode];
            end
        end
        
        if sideConstraints
             if ii == 1 || ii == yNodes
                tempNode = tempNode.SetFixed();
                sideNodes = [sideNodes; tempNode];
             end
        end
        
        % Create element
        if ii > 1 && jj > 1
            s4Ele = S4S(elementId); elementId = elementId + 1;
            % Alternate element choice:
            % s4Ele = S4BDI(elementId); elementId = elementId + 1;
            nodeIds = [(ii - 2)*xNodes + (jj - 1);
                (ii - 2)*xNodes + (jj);
                (ii - 1)*xNodes + (jj);
                (ii - 1)*xNodes + (jj - 1)];
            
            s4Ele = s4Ele.Build(flatBeam.GetNodes(nodeIds), steel, shellSection);
            flatBeam.AddElementObject(s4Ele);
        end
        nodeId = nodeId + 1;
        
    end
end

% Update the model with correct DOF indexing for nodes and elements
flatBeam.Update();
flatBeam.Plot();

%% Linear modal analysis
nmodes = 10;
modal = Frequency('modal', flatBeam);
modal.Solve(nmodes);

figure
subplot(2,2,1)
title(['Mode 1 : ',num2str(round(modal.fn(1))),' Hz'])
flatBeam.Plot(modal.phi(:,1)/norm(modal.phi(:,1)),'norm')
view([0 0 -1]); grid('on');
subplot(2,2,2)
title(['Mode 2 : ',num2str(round(modal.fn(2))),' Hz'])
flatBeam.Plot(modal.phi(:,2)/norm(modal.phi(:,2)),'norm')
view([0 0 -1]); grid('on');
subplot(2,2,3)
title(['Mode 3 : ',num2str(round(modal.fn(3))),' Hz'])
flatBeam.Plot(modal.phi(:,3)/norm(modal.phi(:,3)),'norm')
view([0 0 -1]); grid('on');
subplot(2,2,4)
title(['Mode 4 : ',num2str(round(modal.fn(4))),' Hz'])
flatBeam.Plot(modal.phi(:,4)/norm(modal.phi(:,4)),'norm')
view([0 0 -1]); grid('on');

%% Integrate Model
xScale = 1;
xDisp = xScale*modal.phi(:,2)/norm(modal.phi(:,2));

T = 1/modal.fn(1)/10;

name = 'fullDynamic';
dynamic = Explicit(name, flatBeam, xDisp);

% Find nodes for plotting
xyz = [1.5,0.25,0;...
        3,0.25,0;...
       4.5,0.25,0;...
       6,0.25,0;...
       7.5,0.25,0];
[nodeIds] = flatBeam.findNODEfromXYZ(xyz);
dynamic = dynamic.AddMonitor(nodeIds,3.*ones(size(nodeIds)));

% Plotting and printing options
dynamic = dynamic.SetPlotOpts(false,100);
dynamic = dynamic.SetPrintOpts(true,100);

% Full serial
tic
dynamic.Solve(T);
tSimSerial = toc;

