%% This scripts generates a basic finite element model of a curved
%  curvedBeam modeled with 2-Node beam elements.
%
%  Analysis then performs
%   1) Linear modal analysis
%   2) Nonlinear dynamic free response analysis 
% 
% 
% Christopher Van Damme - 07/12/2018
% cvandamme@wisc.edu , cvandamme13@gmail.com
% 
% Add liscense
% 
clear; clc; close all;
clear classes;
%% Build Model


curvedBeam = OsFern('curvedBeam');

% Plate Dimensions and mesh properties (Metric)
xLength = 1; xEles = 40; xNodes = xEles + 1;

% Cross section properties
thk = 0.01; width = 0.01;
curvedBeamSection.w = width; curvedBeamSection.t = thk;

% Rise ratio (ratio of height to thickness)
riseRatio = 2; 

% Radius of curvaturea
if riseRatio == 0
    zPosMax = 0; r = 0;
else
    zPosMax = thk*riseRatio; 
    r = (zPosMax^2 + (xLength/2)^2)/(2*zPosMax);
end

% Material Properties (steel)
E = 210e9; rho = 7850; nu = 0.29;
alpha = 6.3e-06; refTemp = 77; 
steel = Material('steel');
steel = steel.setProp('E',E);
steel = steel.setProp('rho',rho);
steel = steel.setProp('nu',nu);
steel = steel.setProp('alpha',alpha);
steel = steel.setProp('tRef',refTemp);

% Setup constraints
endConstraints = true;  
sideConstraints = true; % enforces inplane motion only

% Creat basic mesh
nodeId = 1; eleId = 1;
endNodes = []; sideNodes = [];
for ii = 1:xNodes
        
    % Coordinates
    xPos = xLength*((ii-1)/xEles);
    yPos = 0; 
    if riseRatio == 0
        zPos = 0;
    else
        zPos = sqrt(r^2 - (xPos-xLength/2)^2) - r + zPosMax;
    end

    % Create Node Object
    tempNode = Node(nodeId,xPos,yPos,zPos);

    % Build node
    curvedBeam = curvedBeam.AddNodeObject(tempNode);

    % Clamp end nodes
    if endConstraints
        if ii == 1 || ii == xNodes
            tempNode = tempNode.SetFixed();
        else
            if sideConstraints
                tempNode = tempNode.SetFixed([2,4,6]);
            end
        end
    end

    if ii == xEles/2 + 1
        curvedBeam.Add2Nset('CenterNode', tempNode);
    end

    % Create element
    if ii > 1
        b31Ele = B2(eleId); eleId = eleId + 1;
        eleNodeIds = [(ii - 1) (ii)]; csVec = [0 1 0];
        b31Ele = b31Ele.Build(curvedBeam.GetNodes(eleNodeIds),...
                 steel, curvedBeamSection,csVec);
        curvedBeam.AddElementObject(b31Ele); 
    end

    nodeId = nodeId + 1;
        
end

curvedBeam = curvedBeam.Update(); 
figure; curvedBeam.Plot(); view([0 -1 0]);

%% Linear modal analysis
nmodes = 10;
modal = Frequency('modal', curvedBeam);
modal.Solve(nmodes);

figure
subplot(2,2,1)
title(['Mode 1 : ',num2str(round(modal.fn(1))),' Hz'])
curvedBeam.Plot(modal.phi(:,1),'norm')
view([0 -1 0]); grid('on');
subplot(2,2,2)
title(['Mode 2 : ',num2str(round(modal.fn(2))),' Hz'])
curvedBeam.Plot(modal.phi(:,2),'norm')
view([0 -1 0]); grid('on');
subplot(2,2,3)
title(['Mode 3 : ',num2str(round(modal.fn(3))),' Hz'])
curvedBeam.Plot(modal.phi(:,3),'norm')
view([0 -1 0]); grid('on');
subplot(2,2,4)
title(['Mode 4 : ',num2str(round(modal.fn(4))),' Hz'])
curvedBeam.Plot(modal.phi(:,4),'norm')
view([0 -1 0]); grid('on');

%% Integrate Model
name = 'FullDynamic';

% Set duration and number of steps 
numCycles = 20; T = numCycles*(1/modal.fn(1));
dt = 1e-4; nSteps = floor(T/dt);
t = linspace(0,T,nSteps); dt = T/length(t);

% Get the center node and apply force
cNode = curvedBeam.nset('CenterNode');
cNodeDof = cNode{1}.GetGlobalDof();
cNodeVertDof = cNodeDof(3);

% Initial Displacement 
xScale = 0.2;
xDisp = xScale*modal.phi(:,1)/norm(modal.phi(:,1));

% Initialize dynamic object
dynamic = Dynamic(name, curvedBeam, xDisp);

% Add rayleigh damping
dynamic.SetDamping(34,1e-4); % (mass proportional, stiffness proportional)

% Add dnodes and dof to monitor
dynamic.AddMonitor(cNode{1}.id, 3);

% Add plotting options
dynamic.SetPlotOpts(true,5);

% Integrate model
tic
dynamic.Solve(T, dt);
tSimSerial = toc;

% Plot the response
figure;
plot(dynamic.t,dynamic.response);
xlabel('Time (s)'); ylabel('Response (in)')