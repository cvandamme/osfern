%% This scripts generates a basic finite element model of uniaxial rod
%  modeled with 1D 2-node linear spring elemens
%  Analysis then performs
%   1) linear static analysis
%   2) Modal Analysis
%   3) Nonlinear Static
%   4) Modal about nonlinar state 
clear; clc; close all
clear classes
addpath('../classes');


%% Build Model
springMesh = OsFern('flatBeam');

% Plate Dimensions and mesh properties
xLength = 9; yLength = 0.5;
xEles = 500; xNodes = xEles + 1;

% Element/shell thickeness
thk = 0.031;
A = thk*yLength;

% Material Properties
E = 2.97e7; rho = 0.000736; nu = 0.280712; alpha = 6.3e-06;
steel = Material('steel');
steel = steel.setProp('E',E);
steel = steel.setProp('rho',rho);
steel = steel.setProp('nu',nu);
shellSection.t = thk;

% Estimate bar based off material properties
kLin = E*A/(thk*xLength); % stiffness
mLin = A*rho*xLength/xEles; % mass

v = [0;1;0]; % local cooridnate system normal

% Setup constraints
endConstraints = true;

% Use nonlinearity in springs
nonlinear = false;
kNl = 1e10; nlfun = @(x) 3*kNl*x.^2;

% Creat basic mesh
nodeId = 1; elementId = 1;
endNodes = []; sideNodes = [];
for ii = 1:xNodes
  
        % Coordinates
        xPos = xLength*((ii-1)/xEles);
        yPos = 0;
        zPos = 0;
        
        % Create Node Object
        tempNode = Node(nodeId,xPos,yPos,zPos);
        
        % Build node
        springMesh = springMesh.AddNodeObject(tempNode);
        
        % Constrain all dof but axial
        tempNode = tempNode.SetFixed([2,3,4,5,6]);
        
        % Clamp end nodes
        if endConstraints
            if ii == xNodes
                tempNode = tempNode.SetFixed();
                springMesh.Add2Nset('BoundaryNodes', tempNode);
            end
        end
        
        % Center Node, Force application point
        if (ii == 1)
            springMesh.Add2Nset('ForceNode', tempNode);
        end
        
        % Create the element (spring) object 
        if ii > 1
            nodeIds = [(ii); (ii-1)];
            springObj = Spring(elementId); elementId = elementId + 1;
            springObj.Build(springMesh.GetNodes(nodeIds),kLin,[],mLin,v);
            springMesh.AddElementObject(springObj);
            
            if nonlinear
                springObj.SetNonlinearStiffness(nlfun);
            end
        end
        nodeId = nodeId + 1;
end

% Update the model with correct DOF indexing for nodes and elements
springMesh.Update();
springMesh.Plot();

%% Linear modal analysis
nmodes = 10;
modal = Frequency('modal', springMesh);
modal.Solve(nmodes);
freeDof = springMesh.freeDof;

figure; hold on
lString{1} = ['Mode 1 : ',num2str(round(modal.fn(1))),' Hz'];
plot(modal.phi(freeDof,1)/norm(modal.phi(freeDof,1)))
lString{2} = ['Mode 2 : ',num2str(round(modal.fn(2))),' Hz'];
plot(modal.phi(freeDof,2)/norm(modal.phi(freeDof,2)))
lString{3} = ['Mode 3 : ',num2str(round(modal.fn(3))),' Hz'];
plot(modal.phi(freeDof,3)/norm(modal.phi(freeDof,3)))
lString{4} = ['Mode 4 : ',num2str(round(modal.fn(4))),' Hz'];
plot(modal.phi(freeDof,4)/norm(modal.phi(freeDof,4)))
grid('on');
legend(lString);

%% Integrate Model using Explicit Analysis
% return
xScale = 0;
xDisp = xScale*modal.phi(:,2)/norm(modal.phi(:,2));

T = 2e-4;

name = 'fullDynamic';
dynamic = Explicit(name, springMesh, xDisp);

% Find nodes for plotting
xyz = [1.5,0.25,0;...
        3,0.25,0;...
       4.5,0.25,0;...
       6,0.25,0;...
       7.5,0.25,0];
[nodeIds] = springMesh.findNODEfromXYZ(xyz);
dofIds = 3.*ones(size(nodeIds));
dynamic = dynamic.AddMonitor(nodeIds,dofIds);

% Plotting and printing options
dynamic = dynamic.SetPlotOpts(false,100);
dynamic = dynamic.SetPrintOpts(true,100);

% Get Center Node
fExt = zeros(size(freeDof));
forceNode = springMesh.nset('ForceNode');

% Create force object
force = Force('EndForce',springMesh);
force = force.AddForce(forceNode,1,10);

% Add force object to step
dynamic.AddForce(force);

% Integrate model
tic
dynamic.Solve(T);
tSimSerial = toc;

% Plot velocity at a few points along the rod versus time
figure;
plot(dynamic.t,dynamic.solution.velocity([7,1495,2995],:))
legend('Left Side of Rod','Center of Rod','Right Side of Rod')
xlabel('Time (s)'); ylabel('Velocity (in/s');