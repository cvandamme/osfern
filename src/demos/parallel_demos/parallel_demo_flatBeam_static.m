%% This scripts generates a basic finite element model of a
%  flat Beam modeled with 2-Node beam Elements. This script is meant to
%  show how to parllelize multiple load cases. 
%
%   Analyses performed
%   1) Linear Static Solution
%   2) Nonlinear Static Solution
%   3) Enforced Displacement Solution
% 
% Christopher Van Damme - 07/12/2018
% cvandamme@wisc.edu , cvandamme13@gmail.com
% 
% Add liscense
% 
clear; clc; close all;
clear classes;
%% Build Model

% Check to see if toolbox is present
CheckMatlabToolboxes

% intialize main object
flatBeam = OsFern('flatBeam');

% Plate Dimensions and mesh properties (Metric)
xLength = 1; xEles = 40; xNodes = xEles + 1;

% Cross section properties
thk = 0.01; width = 0.01;
flatBeamSection.w = width; flatBeamSection.t = thk;

% Material Properties (steel)
E = 210e9; rho = 7850; nu = 0.29;
alpha = 6.3e-06; refTemp = 77; 
steel = Material('steel');
steel = steel.setProp('E',E);
steel = steel.setProp('rho',rho);
steel = steel.setProp('nu',nu);
steel = steel.setProp('alpha',alpha);
steel = steel.setProp('tRef',refTemp);

% Setup constraints
endConstraints = true;  
sideConstraints = true; % enforces inplane motion only

% Creat basic mesh
nodeId = 1; eleId = 1;
endNodes = []; sideNodes = [];
for ii = 1:xNodes
        
    % Coordinates
    xPos = xLength*((ii-1)/xEles);
    yPos = 0; 
    zPos = 0;

    % Create Node Object
    tempNode = Node(nodeId,xPos,yPos,zPos);

    % Build node
    flatBeam = flatBeam.AddNodeObject(tempNode);

    % Clamp end nodes
    if endConstraints
        if ii == 1 || ii == xNodes
            tempNode = tempNode.SetFixed();
        else
            if sideConstraints
                tempNode = tempNode.SetFixed([2,4,6]);
            end
        end
    end

    if ii == xEles/2 + 1
        flatBeam.Add2Nset('CenterNode', tempNode);
    end

    % Create element
    if ii > 1
        b31Ele = B2(eleId); eleId = eleId + 1;
        eleNodeIds = [(ii - 1) (ii)]; csVec = [0 1 0];
        b31Ele = b31Ele.Build(flatBeam.GetNodes(eleNodeIds),...
                 steel, flatBeamSection,csVec);
        flatBeam.AddElementObject(b31Ele); 
    end

    nodeId = nodeId + 1;
        
end

flatBeam = flatBeam.Update(); 
figure; flatBeam.Plot(); view([0 -1 0]);

%% Linear modal analysis
nmodes = 30;
modal = Frequency('modal', flatBeam);
modal.Solve(nmodes);

%% Static Solutions
name = 'Mode_1_Force';

% Get the center node and apply force
cNode = flatBeam.nset('CenterNode');
cNodeDof = cNode{1}.GetGlobalDof();
cNodeVertDof = cNodeDof(3);

% Creat linear static object
linStatic = LinearStatic('static', flatBeam);
linStatic.AddMonitor(cNode{1}.id, 3);

% Creat nonlinear static object
nlStatic = NLStatic('static', flatBeam);
nlStatic.AddMonitor(cNode{1}.id, 3);

% Vector of force amplitude
forceVec = linspace(1e-4,10,50);
forceShape = modal.phi(:,1); % shape in first mode 

% Linear Static Solution - no parallelization
tic
linDef = linStatic.SolveMultiple(forceVec.*forceShape);
linDefSer = toc;

% Nonlinear Static Solution - no parallelization
tic
nlDef = nlStatic.SolveMultiple(10,forceVec.*forceShape);
nlDefSer = toc;

% Set parallel options
linStatic.parallelOpts.logic = true; 
linStatic.parallelOpts.numWorkers = 4; 
nlStatic.parallelOpts.logic = true; 
nlStatic.parallelOpts.numWorkers = 4; 

% Linear Static Solution - parallelization
tic
linDef = linStatic.SolveMultiple(forceVec.*forceShape);
linDefPar = toc;

% Nonlinear Static Solution - parallelization
tic
nlDef = nlStatic.SolveMultiple(10,forceVec.*forceShape);
nlDefPar = toc;

% Plot the displacement of center node versus force
figure; hold on;
plot(forceVec,linDef(cNodeVertDof,:)./thk,'-o');
plot(forceVec,nlDef(cNodeVertDof,:)./thk,'-o');
title('Force-Displacement of Center Node');
xlabel('Force (N)'); ylabel('Displacement (xThk)')
legend('Linear Solution','Nonlinear Solution');


% Plot the parallel timing info
figure;
subplot(1,2,1); bar([linDefSer,linDefPar]); title('Linear Solution')
xticklabels({'Serial','Parallel'}); ylabel('time (s)')
subplot(1,2,2); bar([nlDefSer,nlDefPar]); title('Nonlinear Solution')
xticklabels({'Serial','Parallel'}); ylabel('time (s)')