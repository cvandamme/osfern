# OSFERN: Open Source Finite Element Research code for Nonlinearity #

This is a finite element code targeted towards structural dynamics researchers interested
in nonlinear effects on structures, particularly geometric nonlinear effects
on thin plates and membranes. The code is based in Matlab using an object
oriented architecture. The code was originally motivated and adopted some structure
based on "BobTran" by Bob Gordon, Joe Hollkamp, S. Michael Spottswood, and others
at the Air Force Research Laboratory Structural Sciences Center. The code 
currently contains nonlinear spring, beam and shell elements for modeling.
This code is not inteded for large scale problems but rather intended for 
researchers to be able to implement quick and efficient algorithms in a open 
source format. 

# Capabilities #

- Elements
- 2-node spring element with geometric nonlinear capabilities
- 2-node beam element with geometric nonlinear capabilities
- 4-node shell element with geometric nonlinear capabilities
- 8-node hexagonal (in progress)

- Solvers / Methods 
- Linear modal analysis
- Linear and nonlinear static capabilites
- Nonlinear dynamics (explict and implicit solvers) 
- Thermal solver
- Modal model correlation (requires optimization toolbox)
- Model reduction
- Optimization (requires optimization toolbox)

- Modeling
- Time varying forces
- Time varying constraints 

- Meshing
- No meshing of geometry is performed, code required specified nodal and
  element information
 
Version: 1.1
Date: 2/17/2020
Authors: Joe Schoneman and Christopher Van Damme

# Where to Start #

Users unfamiliari with Git, see "Getting Started with Git" below.

Once the repository has been cloned and MATLAB is running, execute the script /src/SetPathScriptOsFern.m 
to add all required MATLAB paths.

The best way to start is to to /src/demos to become familiar with the 
modeling workflow and analysis setup. The demonstrations are
intended to show not only how to use the code, but also point to class 
methods which provide critical code functionality.

The entirety of the MATLAB code proper is contained in /src/main. 

Documentation is located in the /doc/ folder, but is still a work in progress.

Finally, the best way to see a list of all class methods and properties (aside from reading
the source code in detail) is to type "doc Class" at the MATLAB command prompt to open
the HTML documentation tool. 

# Getting Started with Git #

Git is a version control tool which streamlines code development and maintenance activities. The 
recommended approach for obtaining OSFERN is through Git, as opposed to simply downloading the 
repository. 

If you have not used Git before, you'll first need to download and install the appropriate version 
for your operating system:

https://git-scm.com/downloads

Once installed, navigate to the location where you want to put OSFERN (the home MATLAB directory, 
for example) and open a command window or Git Bash terminal. The "clone" operation in Git will
copy a remote repository to your local workstation. You can click the "clone" button on the top
right of the Bitbucket screen to copy and paste the https clone command, which will be

git clone https://bitbucket.org/cvandamme/osfern.git

Paste this into your command window and execute to clone the repository. That's it!

For more advanced Git usage, refer to any number of online tutorials or cheat sheets.


# Major TODO List #

- An automated testing framework. 
- Automatic time-stepping capability for implicit dynamic integrator.
- Finish and validate 8-node hexagonal element
- Element loads/distributed load capabilities.
- Surface definitions to facilitate the use of distributed loads.
- A native input deck reader (priority: Abaqus)
- Handle assemblies of components, along with substructuring methods.
- Ability to compute residual modes to frequency steps. 
- Better documentation, both external and in-code.
- Thermal solver validation.

